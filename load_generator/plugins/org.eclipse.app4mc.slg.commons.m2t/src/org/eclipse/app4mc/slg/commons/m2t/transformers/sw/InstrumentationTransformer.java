/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers.sw;

import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.INC_TYPE;
import static org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseSettings.SRC_TYPE;

import org.eclipse.app4mc.slg.commons.m2t.generators.InstrumentationGenerator;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;

public class InstrumentationTransformer extends SLGBaseTransformer {

	@Inject private OutputBuffer outputBuffer;

	public void transform(final String filePath) {
		String headerContent = InstrumentationGenerator.getHeaderFileContent();
		String sourceContent = InstrumentationGenerator.getSourceFileContent();

		outputBuffer.appendTo(INC_TYPE, filePath, headerContent);
		outputBuffer.appendTo(SRC_TYPE, filePath, sourceContent);
	}

}
