/**
 ********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers.sw;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class GroupSourceTransformer extends ActivityGraphItemTransformer {

	@Inject private ActivityGraphItemTransformer activityGraphItemTransformer;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Group group) {
		final List<Object> key = List.of(group);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(group);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, group);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(final Group group) {
		if ((group == null)) {
			return new SLGTranslationUnit("UNSPECIFIED GROUP");
		} else {
			String basePath = "groups";
			String moduleName = "groups";
			String call = "group_" + group.getName() + "()";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final Group group) {
		genFiles(tu, group);
	}

	protected void genFiles(SLGTranslationUnit tu, final Group group) {
		if (group == null)
			return; // do nothing
		
		final List<String> groupItemIncludes = new LinkedList<>();	// Mc: why list here ? (See GroupHeaderTransformer)
		final List<String> groupItemCalls = new LinkedList<>();

		for (ActivityGraphItem item : group.getItems()) {

			final SLGTranslationUnit tmpTU = activityGraphItemTransformer.transform(item);
			if (tmpTU.isValid()) {
				groupItemIncludes.add(getIncPath(tmpTU));
				groupItemCalls.add(tmpTU.getCall());
			}
		}

		srcAppend(tu, "\n//Group " + group.getName() + "----\n");

		for (String include : groupItemIncludes) {
			srcAppend(tu, "#include \"" + include + ".h\"\n");
		}

		srcAppend(tu, "void " + tu.getCall() + "{\n");

		for (String call : groupItemCalls) {
			srcAppend(tu, call + ";\n");
		}

		srcAppend(tu, "}");
	}

}
