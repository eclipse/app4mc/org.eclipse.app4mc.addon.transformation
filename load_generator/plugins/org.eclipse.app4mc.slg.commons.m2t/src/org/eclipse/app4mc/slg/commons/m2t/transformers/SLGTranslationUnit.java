/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers;

public class SLGTranslationUnit {

	private String basePath;
	private String moduleName;
	private String call;
	private boolean valid;

	public SLGTranslationUnit(final String _basePath, final String _moduleName, final String _call) {
		basePath = _basePath;
		moduleName = _moduleName;
		call = _call;
		valid = true;
	}
	
	public SLGTranslationUnit(final String errorMessage) {
		basePath = errorMessage;
		moduleName = errorMessage;
		call = errorMessage;
		valid = false;
	}

	public String getBasePath() {
		return basePath;
	}

	public String getModuleName() {
		return moduleName;
	}

	public String getCall() {
		return call;
	}

	public boolean isValid() {
		return valid;
	}

	// derived attributes

	public String getModulePath() { return basePath + "/" + moduleName; }

}
