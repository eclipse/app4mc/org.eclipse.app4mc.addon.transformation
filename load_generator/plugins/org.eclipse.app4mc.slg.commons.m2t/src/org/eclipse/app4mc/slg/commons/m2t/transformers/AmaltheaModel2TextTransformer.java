/**
 * *******************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.commons.m2t.transformers;

import java.util.Properties;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigurationFileLoader;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.google.inject.Inject;

public class AmaltheaModel2TextTransformer extends SLGBaseTransformer {

	@Inject private SessionLogger logger;
	@Inject private Properties properties;
	@Inject private CustomObjectsStore customObjsStore;

	public ConfigModel configModel;

	public void transform(final Amalthea model, final String outputPath) {
		// to be implemented in subclasses
	}

	public void m2tTransformation(final ResourceSet inputResourceSet) {

		for (final Resource resource : inputResourceSet.getResources()) {
			for (final EObject model : resource.getContents()) {
				// check javadoc : https://www.eclipse.org/xtend/documentation/204_activeannotations.html#active-annotations-expression
				logger.info("Processing file : {0}", resource.getURI());

				final String outputFolder = getProperty(TransformationConstants.TRANSFORMATION_OUTPUT_FOLDER);
				final String configurationFilePath = getProperty("configurationFile");

				if ((configurationFilePath != null)) {
					try {
						configModel = ConfigurationFileLoader.loadConfigurationFile(configurationFilePath);
					} catch (IllegalStateException e) {
						logger.error(e.getMessage(), e);
						return;
					}
					customObjsStore.<ConfigModel>injectMembers(ConfigModel.class, configModel);
				} else {
					logger.error("configuration file path is not supplied as a command line argument");
					return;
				}

				if ((configModel == null)) {
					logger.error("As no configuration model is available , unable to proceed with the load generator application !!");
					return;
				}

				if ((model instanceof Amalthea)) {
					this.transform(((Amalthea) model), outputFolder);
				}

				logger.info("Script file generated at : {0}", outputFolder);

			}
		}
	}

	protected String getProperty(final String propKey) {
		final Object value = properties.get(propKey);
		if (value == null) {
			throw new NullPointerException(
					(("Request input key : \"" + propKey) + "\" not supplied in the input properties file"));
		}
		return value.toString();
	}
	
	protected String getProperty(final String propKey, String defaultValue) {
		final Object value = properties.get(propKey);
		if (value == null) {
			return defaultValue;
		}
		return value.toString();
	}

}
