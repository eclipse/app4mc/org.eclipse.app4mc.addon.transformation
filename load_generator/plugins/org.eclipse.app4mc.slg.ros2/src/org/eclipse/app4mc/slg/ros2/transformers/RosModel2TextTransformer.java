/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers;

import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.OTHER_TYPE;
import static org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils.MICROROS_TYPES;
import static org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils.ROS2_TYPES;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.BooleanObject;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.AmaltheaModel2TextTransformer;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.microros.generators.MicroRosModel2TextGenerator;
import org.eclipse.app4mc.slg.microros.transformers.MicroRosLabelTransformer;
import org.eclipse.app4mc.slg.microros.transformers.MicroRosTicksUtilsTransformer;
import org.eclipse.app4mc.slg.ros2.generators.RosModel2TextGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.common.RosNodeTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.common.RosTargetTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosLabelTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.sw.RosTicksUtilsTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosModel2TextTransformer extends AmaltheaModel2TextTransformer {

	@Inject private OutputBuffer outputBuffer;
	@Inject private RosTargetTransformer targetTransformer;
	@Inject private RosNodeTransformer nodeTransformer;
	@Inject private RosTicksUtilsTransformer ticksUtilsTransformer;
	@Inject private MicroRosTicksUtilsTransformer microRosTicksUtilsTransformer;
	@Inject private RosLabelTransformer labelTransformer;
	@Inject private MicroRosLabelTransformer microRosLabelTransformer;
	@Inject private Properties properties;
	@Inject private CustomObjectsStore customObjsStore;

	@Override
	public void transform(final Amalthea model, final String outputFolder) {

		// Check model consistency + Create explicit node names
		boolean ok = RosModelUtils.checkAndExtendROSTags(model);

		if (!ok) return; // STOP GENERATION

		try {
			File output = new File(outputFolder);
			if (output.exists()) {
				FileUtils.cleanDirectory(new File(outputFolder));
			} else {
				output.mkdirs();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		RosBaseSettings.initializeOutputBuffer(this.outputBuffer, outputFolder);

		// collect ROS tags
		List<Tag> rosTags = RosModelUtils.getROSTags(model);

		// collect target info: name, rosType
		Map<String, TargetInfo> targets = new HashMap<>();
		for (Tag tag : rosTags) {
			String targetName = RosModelUtils.getROSTargetName(tag);
			TargetInfo targetInfo = targets.computeIfAbsent(targetName, k -> new TargetInfo());
			targetInfo.name = targetName;
			targetInfo.rosType = RosModelUtils.getROSTypeName(tag);
		}

		for (TargetInfo info : targets.values()) {
			targetTransformer.transform(info.name, info.rosType);				
		}

		for (Tag tag : rosTags){
			nodeTransformer.transform(tag);
		}
		
		/* extract information needed for top-level CMake file */
		/* find runnables that use service files and use the tag to set the dependencies */

		for (Task task : ModelUtil.getOrCreateSwModel(model).getTasks()) {
			// skip non-ROS tasks
			if (RosModelUtils.getROSTags(task).isEmpty()) continue;
			
			String targetName = RosModelUtils.getROSTargetName(task);
			String nodeName = RosModelUtils.getROSNodeName(task);

			TargetInfo targetInfo = targets.get(targetName);
			targetInfo.nodes.add(nodeName);
			
			List<Stimulus> listOfInterProcessStimulus = task.getStimuli().stream()
					.filter(InterProcessStimulus.class::isInstance).collect(Collectors.toList());

			// check if task has interprocess stimulus, if so task is service callback
			if(!listOfInterProcessStimulus.isEmpty()) {
				for (Stimulus interProcessStimulus : listOfInterProcessStimulus) {
					targetInfo.services.add(((InterProcessStimulus) interProcessStimulus).getName() + "_service");
				}
			} else {
				// iterate over runnables called by task
				for (Runnable run : SoftwareUtil.getRunnableList(task, null)) {
					// check custom properties
					Value perf = run.getCustomProperties().get("measure_performance");
					if (perf instanceof BooleanObject && ((BooleanObject) perf).isValue()) {
						targetInfo.hasPerformanceMeasurement = true;
					}

					// check activity graph
					for (ActivityGraphItem item : run.getRunnableItems()) {
						if (item instanceof LabelAccess) {
							targetInfo.hasLabel = true;
						}
						if (item instanceof Ticks) {
							targetInfo.hasTicks = true;
						}
						if (item instanceof InterProcessTrigger) {
							targetInfo.hasInterprocess = true;
						}
						if (item instanceof ChannelSend) {
							targetInfo.hasChannelSend = true;
						}
					}
				}
			}
		}

		final boolean externalCode = Boolean.parseBoolean(this.properties.getProperty("enableExternalCode", "false"));

		final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);
		final String workingDirectory = customObjsStore.getData(TransformationConstants.WORKING_DIRECTORY);

		for (TargetInfo targetInfo : targets.values()) {
			System.out.println("################## TARGET TRANSFORMATION ##################\n"
					+ "name: " + targetInfo.name +"\n"
					+ "ROS type: " + targetInfo.rosType +"\n"
					+ "services: " + targetInfo.services.size() +"\n"
					+ "interprocess: " + targetInfo.hasInterprocess + "\n"
					+ "label: " + targetInfo.hasLabel + "\n");
			
			if (MICROROS_TYPES.contains(targetInfo.rosType)) {

				microRosTicksUtilsTransformer.createCMake(targetInfo.name);
				microRosLabelTransformer.createCMake(targetInfo.name);

				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/CMakeLists.txt",
						MicroRosModel2TextGenerator.toCmake(targetInfo, externalCode, configModel, workingDirectory));
				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/package.xml",
						MicroRosModel2TextGenerator.toPackageXml(targetInfo.services));
				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/build.sh",
						MicroRosModel2TextGenerator.toBuildScript(targetInfo.services));
				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/launch.py",
						MicroRosModel2TextGenerator.toLaunchFile(targetInfo.nodes));	

			} else if  (ROS2_TYPES.contains(targetInfo.rosType)) {

				ticksUtilsTransformer.createCMake(targetInfo.name);
				labelTransformer.createCMake(targetInfo.name);

				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/main.cpp",
						RosModel2TextGenerator.toMain(targetInfo.nodes));

				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/CMakeLists.txt",
						RosModel2TextGenerator.toCmake(targetInfo, externalCode, configModel, workingDirectory));
				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/package.xml",
						RosModel2TextGenerator.toPackageXml(targetInfo.services));
				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/build.sh",
						RosModel2TextGenerator.toBuildScript(targetInfo.services));
				outputBuffer.appendTo(OTHER_TYPE, targetInfo.name + "/launch.py",
						RosModel2TextGenerator.toLaunchFile(targetInfo.nodes));
			}
		}

		// additional output
		//outputBuffer.appendTo(OTHER_TYPE, "json/annotations.json", "bla bla")

		outputBuffer.finish(true, true, false);
	}

	public static class TargetInfo {
		String name;
		String rosType;

		final Set<String> nodes = new HashSet<>();
		final Set<String> services = new HashSet<>();

		boolean hasTicks = false;
		boolean hasLabel = false;
		boolean hasInterprocess = false;
		boolean hasChannelSend = false;
		boolean hasPerformanceMeasurement = false;

		// access methods

		public String getName() {return name;}
		public String getRosType() {return rosType;}
		public Set<String> getNodes() {return Collections.unmodifiableSet(nodes);}
		public Set<String> getServices() {return Collections.unmodifiableSet(services);}

		public boolean hasNodes() {return !nodes.isEmpty();}
		public boolean hasServices() {return !services.isEmpty();}

		public boolean hasTicks() {return hasTicks;}
		public boolean hasLabel() {return hasLabel;}
		public boolean hasInterprocess() {return hasInterprocess;}
		public boolean hasChannelSend() {return hasChannelSend;}

		public boolean hasPerformanceMeasurement() {return hasPerformanceMeasurement;}
	}

}
