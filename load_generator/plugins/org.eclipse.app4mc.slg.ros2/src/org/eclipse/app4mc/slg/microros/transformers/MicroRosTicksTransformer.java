/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.slg.commons.m2t.generators.TicksGenerator;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.transformers.MicroRosBaseTransformer;
import org.eclipse.emf.common.util.EMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MicroRosTicksTransformer extends MicroRosBaseTransformer {

	@Inject private MicroRosTicksUtilsTransformer ticksUtilsTransformer;

	public SLGTranslationUnit transform(final Ticks ticks, final Tag tag) {
		if (ticks.getDefault() != null) {
			final SLGTranslationUnit ticksUtilsTU = ticksUtilsTransformer.transform(ticks.getDefault(), tag);

			return createTranslationUnit(ticks, ticksUtilsTU, ticks.getDefault());
		}

		return null;
	}

	protected SLGTranslationUnit createTranslationUnit(Ticks ticks, SLGTranslationUnit ticksUtilsTU, IDiscreteValueDeviation value) {
		if ((ticksUtilsTU == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TICKS");			
		}

		String basePath = ticksUtilsTU.getBasePath();
		String moduleName = ticksUtilsTU.getModuleName();
		String call = computeCall(value);
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	protected String computeCall(IDiscreteValueDeviation value) {
		String parameters = TicksGenerator.getParameters(value);
		String className = value.eClass().getName();
		return "executeTicks_" + className +"(" + parameters + ")";
	}

	public Map<String, SLGTranslationUnit> transformAllItems(final Ticks ticks, final Tag tag) {
		final Map<String, SLGTranslationUnit> result = new HashMap<>();
		
		final IDiscreteValueDeviation defaultTicks = ticks.getDefault();
		if ((defaultTicks != null)) {
			final SLGTranslationUnit ticksUtilsTU = ticksUtilsTransformer.transform(defaultTicks, tag);
			result.put("default", createTranslationUnit(ticks, ticksUtilsTU, defaultTicks));
		}

		final EMap<ProcessingUnitDefinition, IDiscreteValueDeviation> extendedTicksMap = ticks.getExtended();
		for (final Entry<ProcessingUnitDefinition, IDiscreteValueDeviation> entry : extendedTicksMap.entrySet()) {
			final ProcessingUnitDefinition puDefinition = entry.getKey();
			final IDiscreteValueDeviation ticksDeviation = entry.getValue();

			if (puDefinition != null && ticksDeviation != null) {
				final SLGTranslationUnit ticksUtilsTU = ticksUtilsTransformer.transform(ticksDeviation, tag);
				result.put(puDefinition.getName(), createTranslationUnit(ticks, ticksUtilsTU, ticksDeviation));
			}
		}
		return result;
	}

}
