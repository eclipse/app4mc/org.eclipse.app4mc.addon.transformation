/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.ILocalModeValueSource;
import org.eclipse.app4mc.amalthea.model.LocalModeLabelAssignment;
import org.eclipse.app4mc.amalthea.model.ModeLiteralConst;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.emf.common.util.EList;

import com.google.inject.Singleton;

@Singleton
public class RosRunnableCache {

	private Map<SLGTranslationUnit, RunnableStore> cache = new HashMap<>();

	public RunnableStore getStore(SLGTranslationUnit tu) {
		return cache.get(tu);
	}

	public void storeValues(SLGTranslationUnit tu, Runnable runnable, String param, String nodeParam,
			Collection<String> publishers, Collection<String> clientDeclarations, Collection<String> clientInits) {
		RunnableStore store = new RunnableStore(runnable, param, nodeParam, publishers, clientDeclarations, clientInits);
		cache.put(tu, store);
	}

	public class RunnableStore {
		private final Runnable runnable;
		private final String param;
		private final String nodeParam;
		private final Collection<String> publishers;
		private final Collection<String> clientDeclarations;
		private final Collection<String> clientInits;

		public RunnableStore(Runnable runnable, String param, String nodeParam, Collection<String> publishers,
				Collection<String> clientDeclarations, Collection<String> clientInits) {
			this.runnable = runnable;
			this.param = param;
			this.nodeParam = nodeParam;
			this.publishers = publishers;
			this.clientDeclarations = clientDeclarations;
			this.clientInits = clientInits;
		}

		public Runnable getRunnable() {
			return runnable;
		}

		public String getParam() {
			return param;
		}

		public String getNodeParam() {
			return nodeParam;
		}

		public Collection<String> getPublishers() {
			return publishers;
		}

		public Collection<String> getClientDeclarations() {
			return clientDeclarations;
		}

		public Collection<String> getClientInits() {
			return clientInits;
		}

		public String getCall1() {
			return "run_" + runnable.getName() + "(" + param + ")";
		}

		public String getNodeCall1() {
			return "run_" + runnable.getName() + "(" + nodeParam + ")";
		}

		public String getCall(RunnableCall runnableCall) {
			List<String> contextParameters = getContextParameters(runnableCall);

			if (contextParameters.isEmpty()) {
				return "run_" + runnable.getName() + "(" + param + ")";

			} else {
				return "run_" + runnable.getName() + "_Context" + "("
						+ param + (param.isEmpty() ? "" : ", ")
						+ String.join(", ", contextParameters) + ")";
			}
		}

		public String getNodeCall(RunnableCall runnableCall) {
			List<String> contextParameters = getContextParameters(runnableCall);

			if (contextParameters.isEmpty()) {
				return "run_" + runnable.getName() + "(" + nodeParam + ")";
			} else {
				return "run_" + runnable.getName() + "_Context" + "("
						+ nodeParam + (nodeParam.isEmpty() ? "" : ", ")
						+ String.join(", ", contextParameters) + ")";
			}
		}

		private List<String> getContextParameters(RunnableCall item) {

			final Runnable run = item.getRunnable();
			List<String> contextParameters = new ArrayList<>();
			if (run != null) {

				EList<LocalModeLabelAssignment> context = item.getContext();

				for (LocalModeLabelAssignment localModeLabelAssignment : context) {

					ILocalModeValueSource valueSource = localModeLabelAssignment.getValueSource();

					if (valueSource instanceof ModeLiteralConst) {
						contextParameters.add("\"" + ((ModeLiteralConst) valueSource).getValue().getName() + "\"");
					}
				}
			}
			return contextParameters;
		}
	}

}
