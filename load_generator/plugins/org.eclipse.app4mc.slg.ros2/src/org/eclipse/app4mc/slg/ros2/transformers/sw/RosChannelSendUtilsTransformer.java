/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.generators.RosChannelSendUtilsGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosChannelSendUtilsTransformer extends RosBaseTransformer {

	@Inject private Properties properties;

	public static final String LIB_NAME = "CHANNELSEND_UTIL";
	public static final String BASE_PATH = "synthetic_gen";
	public static final String MODULE_NAME = "channelSendUtils";
	public static final String MODULE_PATH = BASE_PATH + "/" + MODULE_NAME;
	public static final String MAKEFILE_PATH = MODULE_PATH + "/CMakeLists.txt";

	private List<String> topicList = new ArrayList<>();

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final ChannelSend cs, final Tag tag) {
		final List<Object> key = List.of(cs);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(cs, tag);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, cs);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final ChannelSend cs, final Tag tag) {
		if (cs == null) {
			return new SLGTranslationUnit("UNSPECIFIED CHANNEL SEND");
		} else {
			String target = RosModelUtils.getROSTargetName(tag);
			String basePath = target + "/" + BASE_PATH;
			String moduleName = MODULE_NAME;
			String call = ""; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final ChannelSend cs) {
		genFiles(tu, cs);
	}

	private void genFiles(final SLGTranslationUnit tu, final ChannelSend cs) {
		if (isSrcFileEmpty(tu)) {
			incAppend(tu,"#pragma once \n");
			incAppend(tu, RosChannelSendUtilsGenerator.toHeader());
		}
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, RosChannelSendUtilsGenerator.toCPPHead());
		}

		defineData(tu, cs);
	}

	private void defineData(final SLGTranslationUnit tu, final ChannelSend cs) {
		if (cs.getData() == null || cs.getData().getName() == null || cs.getData().getName().isEmpty())
			return;

		String topic = cs.getData().getName();

		if (!topicList.contains(topic)) {
			incAppend(tu, RosChannelSendUtilsGenerator.toH(cs));
			srcAppend(tu, RosChannelSendUtilsGenerator.toCPP(cs, properties));

			topicList.add(topic);
		}
	}

}
