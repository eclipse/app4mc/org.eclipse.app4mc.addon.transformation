/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.stimuli;

import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.OTHER_TYPE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.generators.RosInterProcessStimulusGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosInterProcessStimulusTransformer extends RosBaseTransformer {

	@Inject private OutputBuffer outputBuffer;


	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final InterProcessStimulus stimulus) {
		final List<Object> key = List.of(stimulus);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(stimulus);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final InterProcessStimulus stimulus) {
		if ((stimulus == null)) {
			return new SLGTranslationUnit("UNSPECIFIED INTER PROCESS STIMULUS");
		} else {
			String basePath = "services";
			String moduleName = stimulus.getName() + "_service";
			String call = "";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	@Override
	public String getSrcFile(SLGTranslationUnit tu) {
		return (Utils.toIdlCompliantName(tu.getModuleName()) + ".srv");
	}
	
	@Override
	public String getSrcPath(SLGTranslationUnit tu) {
		return tu.getModulePath() + "/srv/" + getSrcFile(tu);
	}

	@Override
	public boolean srcAppend(SLGTranslationUnit tu, String str) {
		return outputBuffer.appendTo(OTHER_TYPE, getSrcPath(tu), str);
	}

	private void doTransform(final SLGTranslationUnit tu) {
		genFiles(tu);
	}

	private void genFiles(final SLGTranslationUnit tu) {

		srcAppend(tu, RosInterProcessStimulusGenerator.toSrvFile());

		outputBuffer.appendTo(OTHER_TYPE, tu.getModulePath() + "/CMakeLists.txt", RosInterProcessStimulusGenerator.toCMake(tu.getModuleName()));
		outputBuffer.appendTo(OTHER_TYPE, tu.getModulePath() + "/package.xml", RosInterProcessStimulusGenerator.toPackageXML(tu.getModuleName()));
	}

}
