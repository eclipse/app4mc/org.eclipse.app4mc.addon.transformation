/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.generators

import java.util.List
import java.util.Set
import org.eclipse.app4mc.amalthea.model.Runnable

class RosRunnableGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toCMake(String libName, List<String> srcFiles)
	'''
		# «libName» ################################################################
		####
		add_library(«libName» STATIC
		«FOR srcFile : srcFiles»
			«""»	${CMAKE_CURRENT_LIST_DIR}/_src/«srcFile»
		«ENDFOR»
		)
		
		target_include_directories(«libName»
			PUBLIC  ${CMAKE_CURRENT_LIST_DIR}/_inc
		)
		
		target_include_directories(«libName»
			PUBLIC ${CMAKE_CURRENT_LIST_DIR}/../ticksUtils/_inc
			${CMAKE_CURRENT_LIST_DIR}/../labels/_inc
			${CMAKE_CURRENT_LIST_DIR}/../channelSendUtils/_inc
		)
		
		target_link_libraries(RUNNABLES_LIB 
			PRIVATE LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS
		)
	'''

	static def String toH(String call, Set<String> includes) {
		val builder = new StringBuilder()

		includes.sort.forEach [ include |
			builder.append("#include \"" + include + "\"\n")
		]
		builder.append('''void «call»;
		''')

		builder.toString
	}

	static def String toCpp(Runnable runnable, Set<String> includes, String call, List<String> calls,
		boolean measure_performance) {
		val builder = new StringBuilder()

		// write runnable body
		builder.append("void " + call + "{\n")
		if (measure_performance) {
			builder.append('''
				uint64_t event_list[] = {0x11, 0x13, 0x17}; //CPU CYCLES, MEM ACCESS, L2 Cache Refill
				int total_events =  sizeof(event_list)/sizeof(event_list[0]);
				int fd = instrument_start(0,event_list, total_events);
			''')
		}
		calls.forEach [ c |
			builder.append("\t" + c + ";\n")
		]
		if (measure_performance) {
			builder.append('''
				instrument_stop(fd, "«runnable.name».log");
			''')
		}
		builder.append("}\n\n")

		builder.toString
	}

}
