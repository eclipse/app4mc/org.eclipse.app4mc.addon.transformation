/**
 * Copyright (c) 2020-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.microros.generators

import java.util.Properties
import org.eclipse.app4mc.amalthea.model.ChannelSend
import org.eclipse.app4mc.amalthea.model.Tag
import org.eclipse.app4mc.slg.ros2.transformers.utils.Code
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils

import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT

class MicroRosChannelSendUtilsGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toCPPHead() '''
		#include "channelSendUtils.h"
		#include "output_console.h"
	'''

//	static def String initForTagsGen(ChannelSend cs, Tag tag) {
//		val String param = cs.data?.name
//		val String rosNodeName = RosModelUtils.getROSNodeName(tag)
//
//		// void «rosNodeName»_publish_to_«param»(rcl_publisher_t *  «rosNodeName»Node_«param»_publisher,  std_msgs__msg__String * «rosNodeName»Node_«param»_msg){
//
//		'''
//		«Code.IFDEF_EXTERN_C_BEGIN»
//		extern rcl_publisher_t «rosNodeName»Node_«param»_publisher;
//
//		void «rosNodeName»_publish_to_«param»(std_msgs__msg__String «rosNodeName»Node_«param»_msg){
//			rcl_ret_t rc;
//			snprintf(«tag.getName()»Node_«param»_msg.data.data, «rosNodeName»Node_«param»_msg.data.capacity, "AAAAAAAAAAAAAAAAAAA");
//			«rosNodeName»Node_«param»_msg.data.size = strlen(«rosNodeName»Node_«param»_msg.data.data);
//		}
//		«Code.IFDEF_EXTERN_C_END»
//		'''
//	}

	static def String toCPP(ChannelSend cs, Tag tag, Properties properties) {
		val String param = cs.data?.name
		val String rosNodeName = RosModelUtils.getROSNodeName(tag)

		val genDebugOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_DEBUG_OUTPUT))

		'''
		«Code.IFDEF_EXTERN_C_BEGIN»
		extern rcl_publisher_t «rosNodeName.toLowerCase»Node_«param»_publisher;
		
		void «rosNodeName»_publish_to_«param»(std_msgs__msg__String «rosNodeName»Node_«param»_msg){
			rcl_ret_t rc;
			snprintf(«rosNodeName»Node_«param»_msg.data.data, «rosNodeName»Node_«param»_msg.data.capacity, "AAAAAAAAAAAAAAAAAAA");
			«rosNodeName»Node_«param»_msg.data.size = strlen(«rosNodeName»Node_«param»_msg.data.data);

			rc = rcl_publish(&«rosNodeName.toLowerCase»Node_«param»_publisher, &«rosNodeName»Node_«param»_msg, NULL);
			«IF genDebugOutput»

				debug_printf("  Published message on '«param»' topic\n");
				debug_printf("  Published message '%s'\n", «rosNodeName»Node_«param»_msg.data.data);
			«ENDIF»
		}
		«Code.IFDEF_EXTERN_C_END»
		'''
	}

	static def String toHeader()
	'''
		#include <stdio.h>
		#include <std_msgs/msg/string.h>
		#include <rclc/executor.h>
		#include <rclc/rclc.h>

	'''

	static def String toH(ChannelSend cs, Tag tag) {
		val String param = cs.data?.name
		val String rosNodeName = RosModelUtils.getROSNodeName(tag)

		// void «rosNodeName»_publish_to_«param»(rcl_publisher_t *  «rosNodeName»Node_«param»_publisher,  std_msgs__msg__String * «rosNodeName»Node_«param»_msg);

		'''
		«Code.IFDEF_EXTERN_C_BEGIN»
		void «rosNodeName»_publish_to_«param»(std_msgs__msg__String «rosNodeName»Node_«param»_msg);
		«Code.IFDEF_EXTERN_C_END»
		'''
	}

}
