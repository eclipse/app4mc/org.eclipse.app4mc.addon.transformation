/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.transformers.MicroRosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;

import com.google.inject.Singleton;

@Singleton
public class MicroRosChannelReceiveUtilsTransformer extends MicroRosBaseTransformer {

	public static final String LIB_NAME = "MICROROSCHANNELRECEIVE_UTIL";
	public static final String BASE_PATH = "synthetic_gen";
	public static final String MODULE_NAME = "microroschannelReceiveUtils";
	public static final String MODULE_PATH = BASE_PATH + "/" + MODULE_NAME;
	public static final String MAKEFILE_PATH = MODULE_PATH + "/CMakeLists.txt";

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final ChannelReceive cr, final Tag tag) {
		final String target = RosModelUtils.getROSTargetName(tag);
		final List<Object> key = List.of(cr, target);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(cr, tag);
			transformCache.put(key, tu);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final ChannelReceive cr, final Tag tag) {
		if (cr == null) {
			return new SLGTranslationUnit("UNSPECIFIED CHANNEL RECEIVE");
		} else {
			String target = RosModelUtils.getROSTargetName(tag);
			String basePath = target + "/" + BASE_PATH;
			String moduleName = MODULE_NAME;
			String call = ""; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

}
