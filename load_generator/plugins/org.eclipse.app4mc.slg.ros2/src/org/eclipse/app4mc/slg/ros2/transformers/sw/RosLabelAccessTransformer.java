/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import static org.eclipse.app4mc.amalthea.model.LabelAccessEnum._UNDEFINED_;

import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.MinAvgMaxStatistic;
import org.eclipse.app4mc.amalthea.model.NumericStatistic;
import org.eclipse.app4mc.amalthea.model.SingleValueStatistic;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosLabelAccessTransformer {

	@Inject private RosLabelTransformer rosLabelTransformer;

	public SLGTranslationUnit transform(final LabelAccess labelAccess, final Tag tag) {
		final SLGTranslationUnit labelTU = rosLabelTransformer.transform(labelAccess.getData(), tag);

		return createTranslationUnit(labelAccess, labelTU);
	}

	private SLGTranslationUnit createTranslationUnit(LabelAccess labelAccess, SLGTranslationUnit labelTU) {
		if ((labelTU == null)) {
			return new SLGTranslationUnit("UNSPECIFIED LABEL ACCESS");
		}

		String basePath = labelTU.getBasePath();
		String moduleName = labelTU.getModuleName();
		String call = computeCall(labelAccess);
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	private String computeCall(final LabelAccess labelAccess) {

		if (labelAccess == null
				|| labelAccess.getData() == null
				|| labelAccess.getData().getName() == null
				|| labelAccess.getData().getName().isEmpty())
			return "/* unspecified label access */";	
		
		if (labelAccess.getAccess() == _UNDEFINED_) {
			return ("/*underspecified access to label" + labelAccess.getData().getName()) + "*/";				
		}

		final NumericStatistic stat = statisticValueOrNull(labelAccess);
		final String statString = (stat == null) ? "1" : getNumericStatistic(stat);

		switch (labelAccess.getAccess()) {
		case READ:
			return "read_" + labelAccess.getData().getName() + "(" + statString + ")"; 
		case WRITE:
			return "write_" + labelAccess.getData().getName() + "(" + statString + ")"; 
		default:
			return null;
		}
	}

	private NumericStatistic statisticValueOrNull(final LabelAccess access) {
		if (access == null || access.getStatistic() == null) return null;
		
		return access.getStatistic().getValue();
	}

	private String getNumericStatistic(final NumericStatistic statistic) {
		if (statistic instanceof MinAvgMaxStatistic) {
			MinAvgMaxStatistic minAvgMaxStatistic = (MinAvgMaxStatistic) statistic;
			Float avg = minAvgMaxStatistic.getAvg();
			Double ceil = Math.ceil(avg);
			String valueString = String.format("%d", ceil.intValue());
			return (valueString + " /*MinAvgMaxStatistic not supported yet*/");
		} else if (statistic instanceof SingleValueStatistic) {
			Float value = ((SingleValueStatistic) statistic).getValue();
			Double ceil = Math.ceil(value.doubleValue());
			return String.format("%d", ceil.intValue());
		}
		return null;
	}

}
