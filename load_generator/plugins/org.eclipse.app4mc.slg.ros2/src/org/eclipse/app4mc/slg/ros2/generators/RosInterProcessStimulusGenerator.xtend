/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.generators

import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils

class RosInterProcessStimulusGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toSrvFile() '''
		int64  request
		---
		int64  response
	'''

	static def String toPackageXML(String moduleName)
	'''
		<?xml version="1.0"?>
		<?xml-model href="http://download.ros.org/schema/package_format3.xsd" schematypens="http://www.w3.org/2001/XMLSchema"?>
		<package format="3">
			<name>«moduleName»</name>
			<version>0.0.0</version>
			<description>TODO: Package description</description>
			<maintainer email="app4mc-dev@eclipse.org">app4mc-dev</maintainer>
			<license>TODO: License declaration</license>
		
			<buildtool_depend>ament_cmake</buildtool_depend>
		
			<build_depend>rosidl_default_generators</build_depend>
		
			<exec_depend>rosidl_default_runtime</exec_depend>
		
			<member_of_group>rosidl_interface_packages</member_of_group>
		
			<test_depend>ament_lint_auto</test_depend>
			<test_depend>ament_lint_common</test_depend>
		
			<export>
				<build_type>ament_cmake</build_type>
			</export>
		</package>
		
	'''

	static def String toCMake(String moduleName) '''
		
		cmake_minimum_required(VERSION 3.5)
		project(«moduleName»)
		
		
		if(NOT CMAKE_C_STANDARD)
			set(CMAKE_C_STANDARD 99)
		endif()
		
		# Default to C++14
		if(NOT CMAKE_CXX_STANDARD)
			set(CMAKE_CXX_STANDARD 14)
		endif()
		if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
			add_compile_options(-Wall -Wextra -Wpedantic)
		endif()
		find_package(ament_cmake REQUIRED)
		
		
		find_package(rosidl_default_generators REQUIRED)
		rosidl_generate_interfaces(${PROJECT_NAME}
			"srv/«Utils.toIdlCompliantName(moduleName)».srv"
		)
		ament_package()
	'''

}

