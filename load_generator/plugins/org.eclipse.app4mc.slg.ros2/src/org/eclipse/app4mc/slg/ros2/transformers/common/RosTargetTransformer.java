/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.common;

import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.C_INC_FOLDER;
import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.C_INC_TYPE;
import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.C_SRC_FOLDER;
import static org.eclipse.app4mc.slg.ros2.transformers.RosBaseSettings.C_SRC_TYPE;

import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.generators.RosTargetGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;

public class RosTargetTransformer extends RosBaseTransformer {
	public SLGTranslationUnit transform(final String targetName, final String rosType) {
		final SLGTranslationUnit tu = createTranslationUnit(targetName);

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, rosType);
		}

		return tu;
	}

	private SLGTranslationUnit createTranslationUnit(final String target) {
		if (target == null || target.trim().isEmpty()) {
			return new SLGTranslationUnit("UNSPECIFIED TARGET");
		} else {
			String basePath = target + "/utils";
			String moduleName = "aml";
			String call = ""; // unused

			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final String rosType) {
		genFiles(tu, rosType);
	}

	private void genFiles(final SLGTranslationUnit tu, final String rosType) {
		boolean isMicroRos = RosModelUtils.MICROROS_TYPES.contains(rosType);

		if (isMicroRos) {
			// generate C files (requires special handling because default is C++) 
			String incFile = tu.getModulePath() + C_INC_FOLDER + tu.getModuleName();
			String header = RosTargetGenerator.toH();
			customAppend(C_INC_TYPE, incFile, header);

			String srcFile = tu.getModulePath() + C_SRC_FOLDER + tu.getModuleName();
			String code = RosTargetGenerator.toC();
			customAppend(C_SRC_TYPE, srcFile, code);
		} else {
			incAppend(tu, RosTargetGenerator.toHpp());
			srcAppend(tu, RosTargetGenerator.toCpp());			
		}
	}

}
