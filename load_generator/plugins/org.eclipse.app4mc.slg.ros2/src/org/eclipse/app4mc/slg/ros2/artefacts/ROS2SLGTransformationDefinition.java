/**
 ********************************************************************************
 * Copyright (c) 2022-2023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.artefacts;

import java.io.File;
import java.util.List;

import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.osgi.service.component.annotations.Component;

@Component
public class ROS2SLGTransformationDefinition implements TransformationDefinition {
	public static final String PARAM_CONFIGURATION_FILE = "configurationFile";
	public static final String PARAM_GENERATE_STATUS_OUTPUT = "generateStatusOutput";
	public static final String PARAM_GENERATE_DEBUG_OUTPUT = "generateDebugOutput";
	public static final String PARAM_ENABLE_EXTERNAL_CODE = "enableExternalCode";

	@Override
    public String getName() {
        return "Amalthea to ROS2 SLG project transformation";
    }

    @Override
    public String getDescription() {
        return "Transformation definition for converting an Amalthea model to a ROS2 SLG project";
    }

    @Override
    public String getM2MKey() {
        return null;
    }

    @Override
    public String getM2TKey() {
        return "ROS_SLG";
    }

    @Override
	public List<TransformationParameter> getTransformationParameter() {
		return List.of(
			new TransformationParameter("SLG Settings",
					PARAM_CONFIGURATION_FILE, "Configuration Model", File.class),
			new TransformationParameter("SLG Settings",
					PARAM_GENERATE_STATUS_OUTPUT, "Generate Status Output", Boolean.class, "true"),
			new TransformationParameter("SLG Settings",
					PARAM_GENERATE_DEBUG_OUTPUT, "Generate Debug Output", Boolean.class, "false"),

			new TransformationParameter("Linux SLG Settings",
					PARAM_ENABLE_EXTERNAL_CODE, "Enable External Code", Boolean.class)
		);
	}

}
