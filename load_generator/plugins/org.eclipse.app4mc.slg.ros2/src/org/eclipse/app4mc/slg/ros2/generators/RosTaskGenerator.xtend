/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.generators

import java.util.Collection
import java.util.Properties
import org.eclipse.app4mc.amalthea.model.EventStimulus
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus
import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amalthea.model.Time
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils

import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT

class RosTaskGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String getDeclaration(Collection<Stimulus> stimuli, Collection<String> publishers, Collection<String> clientDeclarations) '''
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus»
				rclcpp::TimerBase::SharedPtr timer_«(stimulus as PeriodicStimulus).name»_;
			«ENDIF»
			«IF stimulus instanceof EventStimulus»
				rclcpp::Subscription<std_msgs::msg::String>::SharedPtr «(stimulus as EventStimulus).name»_subscription_;
			«ENDIF»
			«IF stimulus instanceof InterProcessStimulus»
				rclcpp::Service<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»>::SharedPtr «(stimulus as InterProcessStimulus).name»_service;
			«ENDIF»
		«ENDFOR»
		«FOR publisher : publishers»
			rclcpp::Publisher<std_msgs::msg::String>::SharedPtr «publisher»;
  		«ENDFOR»
  		«FOR decl: clientDeclarations»
			«decl»;
		«ENDFOR»
	'''

	static def String getInitialisation(String nodeName, Collection<Stimulus> stimuli, Collection<String> publishers, Collection<String> clientInits) '''
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus»
				timer_«(stimulus as PeriodicStimulus).name»_ = this->create_wall_timer(
						«(stimulus as PeriodicStimulus).recurrence.value»«(stimulus as PeriodicStimulus).recurrence.unit», std::bind(&«nodeName»::timer_«(stimulus as PeriodicStimulus).name»_callback, this));
			«ENDIF»
			«IF stimulus instanceof EventStimulus»
				«(stimulus as EventStimulus).name»_subscription_ = this->create_subscription<std_msgs::msg::String>(
					"«(stimulus as EventStimulus).name.toLowerCase»", rclcpp::QoS(10), std::bind(&«nodeName»::«(stimulus as EventStimulus).name»_subscription_callback, this, _1));
			«ENDIF»
			«IF stimulus instanceof InterProcessStimulus»
				«(stimulus as InterProcessStimulus).name»_service = this->create_service<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»>(
					"«(stimulus as InterProcessStimulus).name»_service", 
					std::bind(&«nodeName»::«(stimulus as InterProcessStimulus).name»_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			«ENDIF»
		«ENDFOR»
		«FOR publisher : publishers»
			«publisher» = this->create_publisher<std_msgs::msg::String>("«publisher.replace("_publisher", "").toLowerCase»", 10);
		«ENDFOR»
		«FOR init : clientInits»
			«init»;
		«ENDFOR»
	'''

	static def String getServiceCallback(Collection<Stimulus> stimuli, Collection<String> stepCalls, Properties properties) {
		val genStatusOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_STATUS_OUTPUT))

		'''
		«FOR stimulus : stimuli»
			«IF stimulus instanceof InterProcessStimulus»
				void «(stimulus as InterProcessStimulus).name»_service_callback(const std::shared_ptr<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»::Request> request,
					std::shared_ptr<«(stimulus as InterProcessStimulus).name»_service::srv::«Utils.toIdlCompliantName((stimulus as InterProcessStimulus).name + '_service')»::Response> response) {
						(void)request;
						(void)response;
				«IF genStatusOutput»
					#ifdef CONSOLE_ENABLED
						std::cout << "Starting «(stimulus as InterProcessStimulus).name»_service_callback" << std::endl;
					#endif
				«ENDIF»
					«FOR call : stepCalls»
						«call»;
					«ENDFOR»
				}
			«ENDIF»

		«ENDFOR»
		'''
	}
	
	static def String getCallback(Collection<Stimulus> stimuli, Collection<String> stepCalls, Properties properties) {
		val genStatusOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_STATUS_OUTPUT))

		'''
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus || stimulus instanceof RelativePeriodicStimulus»
				void timer_«stimulus.name»_callback() {
				«IF genStatusOutput»
					#ifdef CONSOLE_ENABLED
						std::cout << "Timer_«stimulus.name»_callback («getPeriod(stimulus).value»«getPeriod(stimulus).unit»)" << std::endl;
					#endif
				«ENDIF»
					«FOR call : stepCalls»
						«call»;
					«ENDFOR»
				}
			«ENDIF»
			«IF stimulus instanceof EventStimulus»
				void «(stimulus as EventStimulus).name»_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
				«IF genStatusOutput»
					#ifdef CONSOLE_ENABLED
						std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
					#endif
				«ENDIF»
					«FOR call : stepCalls»
						«call»;
					«ENDFOR»
				}
			«ENDIF»

		«ENDFOR»
		'''
	}

	private static def Time getPeriod(Stimulus stimulus) {
		if (stimulus instanceof PeriodicStimulus) {
			return stimulus.recurrence
		}
		if (stimulus instanceof RelativePeriodicStimulus) {
			return stimulus.nextOccurrence.lowerBound
		}
		return FactoryUtil.createTime()
	}

	static def Collection<String> getHeaders(Collection<String> includes) {
		includes.map[include |"#include \"" + include + "\"\n"].toList
	}

}

