/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.microros.generators.MicroRosTaskGenerator;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MicroRosTaskCache {

	@Inject private Properties properties;

	private Map<SLGTranslationUnit, MicroRosTaskStore> cache = new HashMap<>();

	public MicroRosTaskStore getStore(SLGTranslationUnit tu) {
		return cache.get(tu);
	}

	public void storeValues(SLGTranslationUnit tu, Task task, Collection<Stimulus> stimuli,
			Collection<String> includes, Collection<String> initCalls, Collection<String> stepCalls,
			Collection<String> publishers, Collection<String> clientDeclarations,
			Collection<String> clientInits, Map<Runnable, Entry<String, String>> runnable_DataMap) {
		MicroRosTaskStore store = new MicroRosTaskStore(task, stimuli, includes, initCalls, stepCalls, publishers, clientDeclarations, clientInits, runnable_DataMap);
		cache.put(tu, store);
	}

	public class MicroRosTaskStore {
		private final Task task;
		private final Collection<Stimulus> stimuli;
		private final Collection<String> includes;
		private final Collection<String> initCalls;
		private final Collection<String> stepCalls;
		private final Collection<String> publishers;
		private final Collection<String> clientDeclarations;
		private final Collection<String> clientInits;

		private final Map<Runnable, Entry<String, String>> runnable_DataMap;

		public MicroRosTaskStore(Task task, Collection<Stimulus> stimuli,
				Collection<String> includes, Collection<String> initCalls, Collection<String> stepCalls,
				Collection<String> publishers, Collection<String> clientDeclarations, Collection<String> clientInits,
				Map<Runnable, Entry<String, String>> runnable_DataMap) {
			this.task = task;
			this.stimuli = stimuli;
			this.includes = includes;
			this.initCalls = initCalls;
			this.stepCalls = stepCalls;
			this.publishers = publishers;
			this.clientDeclarations = clientDeclarations;
			this.clientInits = clientInits;
			this.runnable_DataMap=runnable_DataMap;
		}

		public Task getTask() {
			return task;
		}

		public Collection<Stimulus> getStimuli() {
			return stimuli;
		}


		public Collection<String> getIncludes() {
			return includes;
		}

		public Collection<String> getInitCalls() {
			return initCalls;
		}

		public Collection<String> getStepCalls() {
			return stepCalls;
		}

		public Collection<String> getPublishers() {
			return publishers;
		}

		public Collection<String> getClientDeclarations() {
			return clientDeclarations;
		}

		public Collection<String> getClientInits() {
			return clientInits;
		}


		public String getDeclaration() {
			return MicroRosTaskGenerator.getDeclaration(stimuli, publishers, clientDeclarations);
		}
		
		public String getGlobalDeclaration(Tag tag) {
			return MicroRosTaskGenerator.getGlobalDeclaration(tag);
		}
		
		public String getInitialisation(String nodeName) {
			return MicroRosTaskGenerator.getInitialisation(nodeName, stimuli, publishers, clientInits);
		}

		public String getServiceCallback() {
			return MicroRosTaskGenerator.getServiceCallback(stimuli, stepCalls);
		}

		public String getPeriodicTaskCalls(Tag tag) {
			return MicroRosTaskGenerator.getPeriodicTaskCalls(tag, stimuli, stepCalls);
		}

		public String getCallback(Tag tag) {
			return MicroRosTaskGenerator.getCallback(tag, properties, stimuli, stepCalls);
		}

		public Collection<String> getHeaders() {
			return MicroRosTaskGenerator.getHeaders(includes);
		}
		
		public Map<Runnable, Entry<String, String>> getRunnableDataMap(){
			return runnable_DataMap;
		}
	}

}
