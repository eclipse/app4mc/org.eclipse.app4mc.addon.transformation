/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.generators.RosInterProcessTriggerUtilsGenerator;
import org.eclipse.app4mc.slg.ros2.transformers.RosBaseTransformer;
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils;

import com.google.inject.Singleton;

@Singleton
public class RosInterProcessTriggerUtilsTransformer extends RosBaseTransformer {

	public static final String LIB_NAME = "INTERPROCESSTRIGGER_UTIL";
	public static final String BASE_PATH = "synthetic_gen";
	public static final String MODULE_NAME = "interProcessTriggerUtils";
	public static final String MODULE_PATH = BASE_PATH + "/" + MODULE_NAME;
	public static final String MAKEFILE_PATH = MODULE_PATH + "/CMakeLists.txt";

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final InterProcessTrigger ipt, final Tag tag) {
		InterProcessStimulus stimulus = ipt.getStimulus();
		String target = RosModelUtils.getROSTargetName(tag);
		final List<Object> key = List.of(stimulus, target);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(ipt, tag);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, ipt);
		}

		return tu;
	}

	// ---------------------------------------------------

	protected SLGTranslationUnit createTranslationUnit(final InterProcessTrigger ipt, final Tag tag) {
		if (ipt == null) {
			return new SLGTranslationUnit("UNSPECIFIED INTER PROCESS TRIGGER");
		} else {
			String target = RosModelUtils.getROSTargetName(tag);
			String basePath = target + "/" + BASE_PATH;
			String moduleName = MODULE_NAME;
			String call = ""; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	protected void doTransform(final SLGTranslationUnit tu, final InterProcessTrigger ipt) {
		genFiles(tu, ipt);
	}

	protected void genFiles(final SLGTranslationUnit tu, final InterProcessTrigger ipt) {
		if (isSrcFileEmpty(tu)) {
			incAppend(tu, RosInterProcessTriggerUtilsGenerator.toHeader());
		}
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, RosInterProcessTriggerUtilsGenerator.toCPPHead());
		}

		incAppend(tu, RosInterProcessTriggerUtilsGenerator.toH(ipt));
		srcAppend(tu, RosInterProcessTriggerUtilsGenerator.toCPP(ipt));
	}

}
