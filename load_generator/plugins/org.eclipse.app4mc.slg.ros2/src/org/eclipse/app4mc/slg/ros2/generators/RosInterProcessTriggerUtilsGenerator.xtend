/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.generators

import org.eclipse.app4mc.slg.ros2.transformers.utils.Utils
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger

class RosInterProcessTriggerUtilsGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toCPPHead() '''
		#include "interProcessTriggerUtils.hpp"
		
		using namespace std::chrono_literals;
	'''

	static def String toHeader() '''
		#include <string>
		
		#include "rclcpp/rclcpp.hpp"
		#include "std_msgs/msg/string.hpp"
		
	'''

	static def String toCPP(InterProcessTrigger ipt) '''
		
		void call_service_«ipt.stimulus.name»(rclcpp::Client<«ipt.stimulus.name»_service::srv::«Utils.toIdlCompliantName(ipt.stimulus.name + '_service')»>::SharedPtr& client) {
			
			auto request = std::make_shared<«ipt.stimulus.name»_service::srv::«Utils.toIdlCompliantName(ipt.stimulus.name + '_service')»::Request>();
			
			while (!client->wait_for_service(50ms)) {
				if (!rclcpp::ok()) {
					RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
					exit;
				}
				RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
			}
			auto result = client->async_send_request(request);	
		}
	'''

	static def String toH(InterProcessTrigger ipt) '''
		#include "«ipt.stimulus.name»_service/srv/«ipt.stimulus.name»_service.hpp"
		void call_service_«ipt.stimulus.name»(rclcpp::Client<«ipt.stimulus.name»_service::srv::«Utils.toIdlCompliantName(ipt.stimulus.name + '_service')»>::SharedPtr& client);
		
	'''

}
