/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.microros.generators

import com.google.inject.Singleton
import java.util.Set
import org.eclipse.app4mc.slg.config.ConfigModel
import org.eclipse.app4mc.slg.ros2.transformers.RosModel2TextTransformer.TargetInfo

@Singleton
class MicroRosModel2TextGenerator {

	static def String toLaunchFile(Set<String> nodes)
	'''
		from launch import LaunchDescription
		from launch_ros.actions import Node
		
		def generate_launch_description():
			return LaunchDescription([
				«FOR node : nodes.sort»
					Node(
						package='amalthea_micro_ros_model',
						node_namespace='',
						node_executable='«node»',
					),
				«ENDFOR»
			])
	'''

	static def String toBuildScript(Set<String> services)
	'''
		«FOR service : services.sort»
			#building service «service»
			
			cd services/«service»
			colcon build
			. install/setup.bash
			cd ../..
			
		«ENDFOR»
		colcon build
		. install/setup.bash
	'''

	// TODO: define attributes via ConfigModel
	static def String toPackageXml(Set<String> services)
	'''
		<?xml version="1.0"?>
		<?xml-model href="http://download.ros.org/schema/package_format2.xsd" schematypens="http://www.w3.org/2001/XMLSchema"?>
		<package format="2">
			<name>amalthea_micro_ros_model</name>
			<version>1.0.1</version>
			<description>Example of using rclc_executor</description>
			<maintainer email="app4mc-dev@eclipse.org">app4mc-dev</maintainer>
		
			<license>Apache License 2.0</license>
		
			<buildtool_depend>ament_cmake_ros</buildtool_depend>
		
			<build_depend>rcl</build_depend>
			<build_depend>rclc</build_depend>
			<build_depend>rclc_lifecycle</build_depend>
			<build_depend>std_msgs</build_depend>
			<build_depend>lifecycle_msgs</build_depend>
			<build_depend>example_interfaces</build_depend>
		
			<exec_depend>rcl</exec_depend>
			<exec_depend>rclc</exec_depend>
			<exec_depend>rclc_lifecycle</exec_depend>
			<exec_depend>std_msgs</exec_depend>
			<exec_depend>lifecycle_msgs</exec_depend>
			<exec_depend>example_interfaces</exec_depend>
			«FOR service : services.sort»
				<depend>«service»</depend>
			«ENDFOR»
		
			<test_depend>ament_lint_auto</test_depend>
			<test_depend>ament_lint_common</test_depend>
		
			<export>
				<build_type>ament_cmake</build_type>
			</export>
		</package>
	'''


	static def String toCmake(TargetInfo targetInfo, boolean externalCode, ConfigModel configModel, String workingDirectory)
	'''
		# CMakeLists for Nodes
		cmake_minimum_required(VERSION 3.5)
		project(amalthea_micro_ros_model)
		
		# Default to C11
		if(NOT CMAKE_C_STANDARD)
			set(CMAKE_C_STANDARD 11)
		endif()
		
		if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
			add_compile_options(-Wall -Wextra -Wpedantic)
		endif()
		
		set(CMAKE_VERBOSE_MAKEFILE ON)
		
		find_package(ament_cmake_ros REQUIRED)
		find_package(rcl REQUIRED)
		find_package(rcl_lifecycle REQUIRED)
		find_package(rclc REQUIRED)
		find_package(rclc_lifecycle REQUIRED)
		find_package(std_msgs REQUIRED)
		find_package(lifecycle_msgs REQUIRED)
		find_package(example_interfaces REQUIRED)
		
		include_directories(include) ««« Temporary

		«FOR service : targetInfo.services.sort»
			find_package(«service» REQUIRED)
		«ENDFOR»
		«IF targetInfo.hasChannelSend»
			
			add_library(CHANNELSEND_UTILS  STATIC
				synthetic_gen/channelSendUtils/_src/channelSendUtils.cpp
			)
			ament_target_dependencies(CHANNELSEND_UTILS rcl rclc std_msgs)
			target_include_directories(CHANNELSEND_UTILS
				PUBLIC synthetic_gen/channelSendUtils/_inc/
			)
		«ENDIF»
		«IF targetInfo.hasInterprocess»
			
			add_library(INTERPROCESSTRIGGER_UTIL STATIC
				synthetic_gen/interProcessTriggerUtils/_src/interProcessTriggerUtils.cpp
			)
			ament_target_dependencies(INTERPROCESSTRIGGER_UTIL rcl rclc «FOR service : targetInfo.services.sort»«service» «ENDFOR»)
			target_include_directories(INTERPROCESSTRIGGER_UTIL
				PUBLIC synthetic_gen/interProcessTriggerUtils/_inc
			)
		«ENDIF»
		«IF targetInfo.hasPerformanceMeasurement»
			
			add_library(AML_LIB  STATIC
				utils/aml/_src/aml.cpp
			)
			target_include_directories(AML_LIB
			    PUBLIC utils/aml/_inc/
			)
		«ENDIF»

		«IF targetInfo.hasLabel»
			add_subdirectory (synthetic_gen/labels)
		«ENDIF»
		«IF targetInfo.hasTicks»
			add_subdirectory (synthetic_gen/ticksUtils)
		«ENDIF»
		
		# RUNNABLES_LIB ################################################################
		
		####
		add_library(RUNNABLES_LIB STATIC
			synthetic_gen/runnables/_src/runnables.cpp
		)
		
		target_include_directories(RUNNABLES_LIB
			PUBLIC  synthetic_gen/runnables/_inc
		)	
		
		target_include_directories(RUNNABLES_LIB
			PUBLIC 
			«IF targetInfo.hasTicks»
				synthetic_gen/ticksUtils/_inc
			«ENDIF»
			«IF targetInfo.hasLabel»
				synthetic_gen/labels/_inc
			«ENDIF»
			«IF targetInfo.hasChannelSend»
				synthetic_gen/channelSendUtils/_inc
			«ENDIF»
			«IF targetInfo.hasInterprocess»
				synthetic_gen/interProcessTriggerUtils/_inc
			«ENDIF»
			«IF targetInfo.hasPerformanceMeasurement»
			utils/aml/_inc
			«ENDIF»
		)
		target_link_libraries(RUNNABLES_LIB
		    PRIVATE «IF targetInfo.hasPerformanceMeasurement»AML_LIB«ENDIF» «IF targetInfo.hasLabel»LABELS_LIB«ENDIF» «IF targetInfo.hasTicks»TICKS_UTILS«ENDIF» «IF targetInfo.hasChannelSend»CHANNELSEND_UTILS«ENDIF» «IF targetInfo.hasInterprocess»INTERPROCESSTRIGGER_UTIL«ENDIF»
		)
		
		«FOR node : targetInfo.nodes.sort»
			
			add_executable(«node» «node»/_src/«node».cpp)
			target_link_libraries(«node»  «IF targetInfo.hasLabel»LABELS_LIB«ENDIF» RUNNABLES_LIB)
			ament_target_dependencies(«node» rcl rclc std_msgs «FOR service : targetInfo.services.sort»«service» «ENDFOR»)
		«ENDFOR»
		
		
		install(TARGETS
			«FOR node : targetInfo.nodes.sort»
				«node»
			«ENDFOR»
			DESTINATION lib/${PROJECT_NAME}
		)
		
		ament_package()

	'''

}
