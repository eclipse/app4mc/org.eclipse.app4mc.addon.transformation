/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.microros.generators

import java.util.Collection
import java.util.List
import java.util.Properties
import java.util.Set
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amalthea.model.Tag
import org.eclipse.app4mc.slg.ros2.transformers.utils.Code
import org.eclipse.app4mc.slg.ros2.transformers.utils.RosModelUtils

import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_DEBUG_OUTPUT
import static org.eclipse.app4mc.slg.ros2.artefacts.ROS2SLGTransformationDefinition.PARAM_GENERATE_STATUS_OUTPUT

class MicroRosNodeGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toC(Tag tag, String moduleName, Properties properties, Set<String> headers, List<String> declarations,
			List<String> inits, List<String> periodicCalls, List<String> calls, List<String> serviceCallbacks, Set<String> globalDeclarations,
			Amalthea model, Collection<Stimulus> stimuli) {

		val nodeName = RosModelUtils.getROSNodeName(tag).toLowerCase
		val subscriberChannelNames = RosModelUtils.getSubscribers(tag).map[name.toLowerCase]
		val publisherChannelNames = RosModelUtils.getPublishers(tag).map[name.toLowerCase]

		val isErika = RosModelUtils.getROSTypeName(tag).startsWith("Erika")
		val genStatusOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_STATUS_OUTPUT))
		val genDebugOutput = Boolean.parseBoolean(properties.getProperty(PARAM_GENERATE_DEBUG_OUTPUT))
		
		'''
		«Code.IFDEF_EXTERN_C_BEGIN»
		#include <stdio.h>
		#include <std_msgs/msg/string.h>
		#include <rclc/executor.h>
		#include <rclc/rclc.h>
		«FOR header : headers»
			«header»
		«ENDFOR»
		
		«IF isErika»
			/* ERIKA include */
			#include "ee.h"
			#include "erika_board_cfg.h"

			/* Console output */
			#include "output_console.h"
		«ENDIF»			
		
		/******************************************************************************
		 *  PERIODIC Tasks
		 *****************************************************************************/
		«FOR call : periodicCalls»
			«call»
		«ENDFOR»
		
		/******************************************************************************
		 *  MICROROS Task
		 *****************************************************************************/
		/*** MICROROS DEFINITION ****/
		#define PUB_MSG_CAPACITY 20
		
		rcl_allocator_t allocator;
		rclc_support_t support;
		rclc_executor_t executor;
		rcl_node_t «nodeName»_node;
		
		//timers --> Periodic tasks
«««		«FOR stimulus : stimuli»
«««			«IF stimulus instanceof PeriodicStimulus»
«««				rcl_timer_t «(stimulus as PeriodicStimulus).name»_timer;
«««			«ENDIF»
«««		«ENDFOR»
		
		«FOR topic : publisherChannelNames»
			//«topic» publisher
			rcl_publisher_t «nodeName»Node_«topic»_publisher;
			std_msgs__msg__String «nodeName»Node_«topic»_msg;
			char «nodeName»Node_«topic»_msg_buffer[PUB_MSG_CAPACITY];
		«ENDFOR»
		
		«FOR topic : subscriberChannelNames»
			//«topic» subscription
			rcl_subscription_t  «nodeName»Node_«topic»_subscription;
			std_msgs__msg__String «nodeName»Node_«topic»_msg;
			char «nodeName»Node_«topic»_msg_buffer[PUB_MSG_CAPACITY];
		«ENDFOR»
		
		/*** MICROROS CALLBACKS *******/
		«FOR call : calls»
			«call»
		«ENDFOR»
		
		
		/*** MICROROS INITIALIZATION *****/
		int uros_init(void)
		{
			allocator = rcl_get_default_allocator();
			rcl_ret_t rc;
		
			//Create init options
			«IF genStatusOutput»
				printf("Erika: create uROS init_options\n");
			«ENDIF»
			rc = rclc_support_init(&support, 0, NULL, &allocator);
			if (rc != RCL_RET_OK) {
				printf("Error rclc_support_init.\n");
				return -1;
			}
			//Create rcl node
			«IF genStatusOutput»
				printf("Erika: create uROS node\n");
			«ENDIF»
			rc = rclc_node_init_default(&«nodeName»_node, "«nodeName»", "", &support);
			if (rc != RCL_RET_OK) {
			    printf("Error in rclc_node_init_default\n");
			    return -1;
			}
			
«««		«FOR stimulus : stimuli»
«««			«IF stimulus instanceof PeriodicStimulus»
«««				//Create timer «(stimulus as PeriodicStimulus).name»
«««				printf("Erika: create «(stimulus as PeriodicStimulus).name» timer\n");
«««				rc = rclc_timer_init_default(
«««					&«(stimulus as PeriodicStimulus).name»_timer,
«««					&support,
«««					RCL_MS_TO_NS(«(stimulus as PeriodicStimulus).recurrence.value»),
«««					«(stimulus as PeriodicStimulus).name»_timer_callback);
«««				if (rc != RCL_RET_OK) {
«««					printf("Error in rcl_timer_init_default.\n");
«««					return -1;
«««				}
«««			«ENDIF»
«««		«ENDFOR»
		
			«FOR topic : publisherChannelNames»
				//Create a publisher of topic «topic»
				const char * «topic»_topic_name = "«topic»";
				const rosidl_message_type_support_t * «topic»_type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, String);
				«IF genStatusOutput»
					printf("Erika: create publisher %s\n", «topic»_topic_name);
				«ENDIF»
				rc = rclc_publisher_init_default(
					&«nodeName»Node_«topic»_publisher,
					&«nodeName»_node,
					«topic»_type_support,
					«topic»_topic_name);
				if (RCL_RET_OK != rc) {
					printf("Error in rclc_publisher_init_default %s.\n", «topic»_topic_name);
					return -1;
				}
				«nodeName»Node_«topic»_msg.data.data = «nodeName»Node_«topic»_msg_buffer;
				«nodeName»Node_«topic»_msg.data.capacity = PUB_MSG_CAPACITY;
				snprintf(«nodeName»Node_«topic»_msg.data.data, «nodeName»Node_«topic»_msg.data.capacity, "AAAAAAAAAAAAAAAAAAA");
				«nodeName»Node_«topic»_msg.data.size = strlen(«nodeName»Node_«topic»_msg.data.data);
			«ENDFOR»
		
			«FOR topic : subscriberChannelNames»
				//Create subscription on topic «topic»
				const char * «topic»_topic_name = "«topic»";
				const rosidl_message_type_support_t * «topic»_type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, String);
				«IF genStatusOutput»
					printf("Erika: create subscriber %s\n", «topic»_topic_name);
				«ENDIF»
				rc = rclc_subscription_init_default(
					&«nodeName»Node_«topic»_subscription,
					&«nodeName»_node,
					«topic»_type_support,
					«topic»_topic_name);
				if (rc != RCL_RET_OK) {
					printf("Failed to create subscriber %s.\n", «topic»_topic_name);
					return -1;
				}
				«nodeName»Node_«topic»_msg.data.data = «nodeName»Node_«topic»_msg_buffer;
				«nodeName»Node_«topic»_msg.data.capacity = PUB_MSG_CAPACITY;
			«ENDFOR»
		
			//Configure the RCL Executor
			«IF genStatusOutput»
				printf("Configure executor\n");
			«ENDIF»
			executor = rclc_executor_get_zero_initialized_executor();
			//Setup the number of handles = #timers + #subscriptions
			unsigned int num_handles =  «stimuli.size()»;
			«IF genDebugOutput»
				//printf("Erika: number of DDS handles: %u\n", num_handles);
			«ENDIF»
			rclc_executor_init(&executor, &support.context, num_handles, &allocator);

«««		«FOR stimulus : stimuli»
«««			«IF stimulus instanceof PeriodicStimulus»
«««				//Add «(stimulus as PeriodicStimulus).name» timer to executor
«««				rc= rclc_executor_add_timer(&executor, &«(stimulus as PeriodicStimulus).name»_timer);
«««				if (rc != RCL_RET_OK) {
«««					printf("Error in rclc_executor_add_timer.\n");
«««					return -1;
«««				}
«««			«ENDIF»
«««		«ENDFOR»

			«FOR item : subscriberChannelNames»
				//Add subscription «item» to executor
				rc = rclc_executor_add_subscription(
					&executor,
					&«nodeName»Node_«item»_subscription,
					&«nodeName»Node_«item»_msg, &«item»_subscription_callback,
					ON_NEW_DATA);
				if (rc != RCL_RET_OK) {
					printf("Error in rclc_executor_add_subscription. \n");
					return -1;
				}
			«ENDFOR»

			«IF genStatusOutput»
				printf("Successful MicroROS Initalization\n");
			«ENDIF»
			return 0;
		}
		
		/*** MICROROS TASK *****/
		extern void uros_communication_setup(void);
		int uros_init_done = 0;
		
		/* uROS Initialization task (one-shot) */
		«IF isErika»		
			DeclareTask(uROSInitTask);
			TASK(uROSInitTask)
			{
		«ENDIF»
			«IF genStatusOutput»
				printf("Activate MicroROS Init Task\n");
			«ENDIF»
			/* Setup microros transport functions */
			uros_communication_setup();
			/* Initialized microros data for PingPong app */
			int ret = uros_init();
			/* Set the initialization as DONE */
			if (ret == 0) {
				uros_init_done = 1;
			}

		«IF isErika»		
				TerminateTask();
			}
		«ENDIF»
		
		/* uROS Ping Pong task (periodic = 10000usec) */
		«IF isErika»
			DeclareTask(uROSPeriodicTask);
			TASK(uROSPeriodicTask)
			{
		«ENDIF»
			if(uros_init_done == 1) {
				rclc_executor_spin_some(&executor, RCL_MS_TO_NS(10));
			}
		«IF isErika»
			}
		«ENDIF»
		
		
		/******************************************************************************
		 *  MAIN
		 *****************************************************************************/
		void idle_hook(void);
		void idle_hook(void)
		{
			«IF genStatusOutput»
				printf("Starting communication over UART\n");
			«ENDIF»
			«IF isErika»
				/* Activate task performing MicroROS app */
				ActivateTask(uROSInitTask);
			«ENDIF»
			/* endless loop*/
			while (1) {
				asm volatile("wfi": : : "memory");
			}
		}
		 
		int main(void)
		{
			«IF isErika»
			     erika_console_init();
			     «IF genStatusOutput»
			     	printf("Starting «nodeName.toUpperCase» (MicroROS node)\n");
			     «ENDIF»
			     StartOS(OSDEFAULTAPPMODE);
			     return 0;
			«ENDIF»
		}
		«Code.IFDEF_EXTERN_C_END»
		'''
	}

}
