/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.ActivityGraphItemTransformer;

import com.google.inject.Inject;
import com.google.inject.Singleton;


@Singleton
public class RosActivityGraphItemTransformer extends ActivityGraphItemTransformer {

	@Inject private RosInterProcessTriggerTransformer ipTransformer;
	@Inject private RosRunnableTransformer runnableTransformer;
	@Inject private RosLabelAccessTransformer labelAccessTransformer;
	@Inject private RosChannelSendTransformer channelSendTransformer;
	@Inject private RosChannelReceiveTransformer channelReceiveTransformer;
	@Inject private RosTicksTransformer ticksTransformer;

    public SLGTranslationUnit transform(final ActivityGraphItem graphItem, final Tag tag) {
        if (graphItem == null) {
            throw new IllegalArgumentException("Unhandled parameter type: null");
        }

		if (graphItem instanceof InterProcessTrigger) {
			return ipTransformer.transform((InterProcessTrigger) graphItem, tag);

		} else if (graphItem instanceof RunnableCall) {
			return runnableTransformer.transform(((RunnableCall) graphItem).getRunnable(), tag);

		} else if (graphItem instanceof LabelAccess) {
			return labelAccessTransformer.transform((LabelAccess) graphItem, tag);

		} else if (graphItem instanceof ChannelSend) {
			return channelSendTransformer.transform((ChannelSend) graphItem, tag);

		} else if (graphItem instanceof ChannelReceive) {
			return channelReceiveTransformer.transform((ChannelReceive) graphItem, tag);

		} else if (graphItem instanceof Ticks) {
			return ticksTransformer.transform((Ticks) graphItem, tag);

		} else {
			return super.transform(graphItem); // should not happen
		}
	}

}
