/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.ros2.generators.RosTaskGenerator;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosTaskCache {

	@Inject private Properties properties;

	private Map<SLGTranslationUnit, TaskStore> cache = new HashMap<>();

	public TaskStore getStore(SLGTranslationUnit tu) {
		return cache.get(tu);
	}

	public void storeValues(SLGTranslationUnit tu, Task task, Collection<Stimulus> stimuli,
			Collection<String> includes, Collection<String> initCalls, Collection<String> stepCalls,
			Collection<String> publishers, Collection<String> clientDeclarations, Collection<String> clientInits) {
		TaskStore store = new TaskStore(task, stimuli, includes, initCalls, stepCalls, publishers, clientDeclarations, clientInits);
		cache.put(tu, store);
	}

	public class TaskStore {
		private final Task task;
		private final Collection<Stimulus> stimuli;
		private final Collection<String> includes;
		private final Collection<String> initCalls;
		private final Collection<String> stepCalls;
		private final Collection<String> publishers;
		private final Collection<String> clientDeclarations;
		private final Collection<String> clientInits;

		public TaskStore(Task task, Collection<Stimulus> stimuli,
				Collection<String> includes, Collection<String> initCalls, Collection<String> stepCalls,
				Collection<String> publishers, Collection<String> clientDeclarations, Collection<String> clientInits) {
			this.task = task;
			this.stimuli = stimuli;
			this.includes = includes;
			this.initCalls = initCalls;
			this.stepCalls = stepCalls;
			this.publishers = publishers;
			this.clientDeclarations = clientDeclarations;
			this.clientInits = clientInits;
		}

		public Task getTask() {
			return task;
		}

		public Collection<Stimulus> getStimuli() {
			return stimuli;
		}

		public Collection<String> getIncludes() {
			return includes;
		}

		public Collection<String> getInitCalls() {
			return initCalls;
		}

		public Collection<String> getStepCalls() {
			return stepCalls;
		}

		public Collection<String> getPublishers() {
			return publishers;
		}

		public Collection<String> getClientDeclarations() {
			return clientDeclarations;
		}

		public Collection<String> getClientInits() {
			return clientInits;
		}

		public String getDeclaration() {
			return RosTaskGenerator.getDeclaration(stimuli, publishers, clientDeclarations);
		}

		public String getInitialisation(String nodeName) {
			return RosTaskGenerator.getInitialisation(nodeName, stimuli, publishers, clientInits);
		}

		public String getServiceCallback() {
			return RosTaskGenerator.getServiceCallback(stimuli, stepCalls, properties);
		}

		public String getCallback() {
			return RosTaskGenerator.getCallback(stimuli, stepCalls, properties);
		}

		public Collection<String> getHeaders() {
			return RosTaskGenerator.getHeaders(includes);
		}
	}

}
