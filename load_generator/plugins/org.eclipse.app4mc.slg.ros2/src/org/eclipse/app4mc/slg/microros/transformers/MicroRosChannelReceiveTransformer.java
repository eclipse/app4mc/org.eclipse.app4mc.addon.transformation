/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.microros.transformers;

import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MicroRosChannelReceiveTransformer {

	@Inject private MicroRosChannelReceiveUtilsTransformer channelReceiveUtilTransformer;

	public SLGTranslationUnit transform(final ChannelReceive cr, final Tag tag) {
		final SLGTranslationUnit utilTU = channelReceiveUtilTransformer.transform(cr, tag);

		return createTranslationUnit(cr, utilTU);
	}

	private SLGTranslationUnit createTranslationUnit(final ChannelReceive cr, final SLGTranslationUnit utilTU) {

		return new SLGTranslationUnit("");
	}

}
