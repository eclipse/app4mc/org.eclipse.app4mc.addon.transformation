/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosInterProcessTriggerTransformer {

	@Inject private RosInterProcessTriggerUtilsTransformer iptUtilTransformer;

	public SLGTranslationUnit transform(final InterProcessTrigger ipt, final Tag tag) {
		final SLGTranslationUnit utilTU = this.iptUtilTransformer.transform(ipt, tag);

		return createTranslationUnit(ipt, utilTU);
	}

	private SLGTranslationUnit createTranslationUnit(final InterProcessTrigger ipt, final SLGTranslationUnit utilTU) {
		if (ipt == null) {
			return new SLGTranslationUnit("UNSPECIFIED INTER PROCESS TRIGGER");
		}

		String basePath = utilTU.getBasePath();
		String moduleName = utilTU.getModuleName();
		String call = computeCall(ipt);
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	private String computeCall(final InterProcessTrigger ipt) {
		String param = ipt.getStimulus().getName();
		return "call_service_" + param + "(" + param + "_client)";
	}

}
