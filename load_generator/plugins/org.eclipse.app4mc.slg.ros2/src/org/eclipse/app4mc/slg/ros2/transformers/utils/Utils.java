/**
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.transformers.utils;

import org.eclipse.xtext.xbase.lib.StringExtensions;

public class Utils {

	// Suppress default constructor
	private Utils() {
		throw new IllegalStateException("Utility class");
	}

	public static String toIdlCompliantName(final String str) {
		StringBuilder builder = new StringBuilder();
		for (final String s : str.split("_")) {
			builder.append(StringExtensions.toFirstUpper(s));
		}
		return builder.toString();
	}

}
