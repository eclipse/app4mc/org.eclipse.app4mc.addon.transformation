/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.ros2.transformers.sw;

import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RosChannelSendTransformer {

	@Inject private RosChannelSendUtilsTransformer channelSendUtilTransformer;

	public SLGTranslationUnit transform(final ChannelSend cs, final Tag tag) {
		final SLGTranslationUnit utilTU = channelSendUtilTransformer.transform(cs, tag);

		return createTranslationUnit(cs, utilTU);
	}

	private SLGTranslationUnit createTranslationUnit(final ChannelSend cs, final SLGTranslationUnit utilTU) {
		if (cs == null) {
			return new SLGTranslationUnit("UNSPECIFIED CHANNEL SEND");
		}

		String basePath = utilTU.getBasePath();
		String moduleName = utilTU.getModuleName();
		String call = computeCall(cs);
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	private String computeCall(final ChannelSend cs) {
		String param = cs.getData().getName();
		return "publish_to_" + param + "(" + param + "_publisher)";
	}

}
