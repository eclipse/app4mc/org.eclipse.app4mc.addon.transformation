/**
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.ros2.transformers.utils;

public class Code {

	// Suppress default constructor
	private Code() {
		throw new IllegalStateException("Utility class");
	}

	public static final String IFDEF_EXTERN_C_BEGIN = "\n#ifdef __cplusplus \n extern \"C\" \n {\n#endif\n\n";
	public static final String IFDEF_EXTERN_C_END = "\n#ifdef __cplusplus \n }\n#endif\n\n";

	public static final boolean ERIKA = true; // temporary: just an attempt to mark the ERIKA parts of the code

}
