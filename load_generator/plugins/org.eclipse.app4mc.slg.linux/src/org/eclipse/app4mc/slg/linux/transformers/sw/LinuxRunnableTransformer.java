/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils;
import org.eclipse.app4mc.slg.linux.generators.LinuxRunnableGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LinuxRunnableTransformer extends LinuxBaseTransformer {

	@Inject private Properties properties;
	@Inject private CustomObjectsStore customObjsStore;
	@Inject private SessionLogger logger;
	
	// ---------- generic part "def create new transform(...)" ----------

    // ---------------------------------------------testing fetching names

	/*	final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);
	
	public static void listFilesInFolder(final File folder) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesInFolder(fileEntry);
	        } else {
	            System.out.println(fileEntry.getName());
	        }
	    }
	}	
*/
	
	// ------------------------------------------------testing fetching names

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();


	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Runnable runnable) {
		final List<Object> key = List.of(runnable);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(runnable);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, runnable);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final Runnable runnable) {
		if ((runnable == null)) {
			return new SLGTranslationUnit("UNSPECIFIED RUNNABLE");
		} else {
			String basePath = "synthetic_gen";
			String moduleName = "runnables";
			String call = runnable.getName() + "()";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final Runnable runnable) {
		genFiles(tu, runnable);
	}

	private void genFiles(SLGTranslationUnit tu, final Runnable runnable) {
		final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);

		if (isIncFileEmpty(tu)) {
			
			//----------------------------------/* fetch headers names*/--------------------------------------
			// to fetch the headers names we use the getHeaderFilesDirectories function and we go through every directory with the for loop
			
			boolean enableExtCode = Boolean.parseBoolean(properties.getProperty("enableExternalCode"));
			if (enableExtCode) {
				// input property to test if we need to include the external code headerfiles
				String workingDirectory = customObjsStore.getData(TransformationConstants.WORKING_DIRECTORY);
				for (String hDir : ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.RUNNABLE,
						workingDirectory, logger)) {
					final File folder = new File(hDir.trim()); // fetching all the names in the headerfile directory.
					String names = ConfigModelUtils.getHeaderFilesIncludeMultiString(folder, logger);
					incAppend(tu, names);
				}
			}
			// ------------------------------------------------------------------------

			incAppend(tu, LinuxRunnableGenerator.snippetIncStart());
		}

		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, LinuxRunnableGenerator.snippetSrcStart(configModel));
		}

		// write header
		incAppend(tu, "\n//Runnable " + runnable.getName() + "----\n");
		incAppend(tu, "void " + "run_"+runnable.getName() + "(char* coreName);\n");

		boolean useExperimental = Boolean.parseBoolean(properties.getProperty("experimentalCodeSnippetMatching"));
		boolean enableExtCode = Boolean.parseBoolean(properties.getProperty("enableExternalCode"));
		
		String codeString = LinuxRunnableGenerator.syntheticLoad(runnable,useExperimental,properties,enableExtCode);
		srcAppend(tu, LinuxRunnableGenerator.snippetSrcBody(runnable, codeString, enableExtCode ));
	}

}
