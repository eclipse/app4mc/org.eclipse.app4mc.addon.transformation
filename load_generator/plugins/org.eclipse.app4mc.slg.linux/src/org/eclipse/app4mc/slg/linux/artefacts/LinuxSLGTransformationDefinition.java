/**
 ********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.artefacts;

import java.io.File;
import java.util.List;

import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.osgi.service.component.annotations.Component;

@Component
public class LinuxSLGTransformationDefinition implements TransformationDefinition {

	@Override
	public String getName() {
		return "Amalthea to Linux SLG project transformation";
	}

	@Override
	public String getDescription() {
		return "Transformation definition for converting an Amalthea model to a Linux SLG project";
	}

	@Override
	public String getM2MKey() {
		return null;
	}

	@Override
	public String getM2TKey() {
		return "LINUX_SLG";
	}

	@Override
	public List<TransformationParameter> getTransformationParameter() {
		return List.of(
				new TransformationParameter("SLG Settings", "configurationFile", "Configuration Model", File.class),
				new TransformationParameter("Linux SLG Settings", "experimentalCodeSnippetMatching", "Experimental Code Snippet Matching", Boolean.class),
				new TransformationParameter("Linux SLG Settings", "enableInstrumentation_Tasks", "Enable Instrumentation", Boolean.class),				
				new TransformationParameter("Linux SLG Settings", "enableInstrumentation_Runnables", "Enable Instrumentation Runnables", Boolean.class),
				new TransformationParameter("Linux SLG Settings", "enableExternalCode", "Enable External Code", Boolean.class));
		
	}
}
