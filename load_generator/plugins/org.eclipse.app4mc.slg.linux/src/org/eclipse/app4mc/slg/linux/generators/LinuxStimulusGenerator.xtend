/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators

import java.util.List
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.Stimulus

class LinuxStimulusGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def String toSrc(List<Stimulus> stimuli, Stimulus lastStimulus, boolean enableInstrumentation) '''
		#include "tasks.h"
		#include <pthread.h>
		#include <unistd.h>
		«IF enableInstrumentation»
			#include "instrument.h"
			
			int counter=0;
		«ENDIF»
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus»
				void *«stimulus.name»Entry(){
				«FOR task : stimulus.affectedProcesses»
					«IF enableInstrumentation»instr_start();«ENDIF»
						«task.name»();
					«IF enableInstrumentation»instr_stop();«ENDIF»
				«ENDFOR»
				}
			«ENDIF»
		«ENDFOR»
		«FOR stimulus : stimuli»
			«IF stimulus instanceof PeriodicStimulus»
				void *«stimulus.name»Loop(){
					pthread_t «stimulus.name»;
					for(;;){
						pthread_create(&«stimulus.name», NULL, «stimulus.name»Entry, NULL);
						
						«IF stimulus.recurrence!==null»
							«IF stimulus.recurrence.toString.split(" ").get(1) == "us"»
								usleep(«stimulus.recurrence.toString.split(" ").get(0)»);
							«ENDIF»
							«IF stimulus.recurrence.toString.split(" ").get(1) == "ms"»
								usleep(«stimulus.recurrence.toString.split(" ").get(0)»000);
								«ENDIF»
							«ENDIF»
							}
						}
					«ENDIF»
			«ENDFOR»
			int main(int argc, char **argv){
				«FOR stimulus : stimuli»
					«IF stimulus instanceof PeriodicStimulus»
						pthread_t «stimulus.name»_;
						pthread_create(&«stimulus.name»_, NULL, «stimulus.name»Loop, NULL);
							
					«ENDIF»
				«ENDFOR»
				«IF lastStimulus !== null»pthread_join(«lastStimulus.name»_, NULL);«ENDIF»
			}
	'''

}
