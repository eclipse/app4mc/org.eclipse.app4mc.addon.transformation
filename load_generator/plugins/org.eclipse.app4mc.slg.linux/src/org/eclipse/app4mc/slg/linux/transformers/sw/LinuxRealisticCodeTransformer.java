/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.linux.generators.LinuxRealisiticSyntheticGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;

import com.google.inject.Singleton;

@Singleton
public class LinuxRealisticCodeTransformer extends LinuxBaseTransformer {

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform() {
		final List<Object> key = List.of();
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit();
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit() {
		String basePath = "synthetic_gen";
		String moduleName = "codesnippets";
		String call = "<realistic code>"; // unused
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	private void doTransform(SLGTranslationUnit tu) {
		LinuxRealisiticSyntheticGenerator.getCodeSnippets();
		
		incAppend(tu, LinuxRealisiticSyntheticGenerator.toH( ));
		srcAppend(tu, LinuxRealisiticSyntheticGenerator.toCPP( ));
	}

}
