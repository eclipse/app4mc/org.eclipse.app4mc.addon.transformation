/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.ILocalModeValueSource;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.LocalModeLabelAssignment;
import org.eclipse.app4mc.amalthea.model.ModeLiteralConst;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.StringObject;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.CustomPropertyImpl;
import org.eclipse.app4mc.amalthea.model.util.DeploymentUtil;
import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.util.ConfigModelUtils;
import org.eclipse.app4mc.slg.linux.generators.LinuxTaskGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.emf.ecore.EObject;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LinuxTaskTransformer extends LinuxBaseTransformer {

	private static final String KEY_CODEHOOK = "codehook";
	private static final String KEY_CODEHOOK_OVERWRITE = "codehook_overwrite";

	@Inject private Properties properties;
	@Inject private CustomObjectsStore customObjsStore;
	@Inject private SessionLogger logger;
	
	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Task task) {
		final List<Object> key = List.of(task);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(task);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, task);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final Task task) {
		if ((task == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TASK");
		} else {
			String basePath = "synthetic_gen";
			String moduleName = "tasks";
			String call = task.getName() + "()";
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final Task task) {
		genFiles(tu, task);
	}

	private void genFiles(SLGTranslationUnit tu, final Task task) {

		boolean enableInstrumentation = Boolean.parseBoolean(properties.getProperty("enableInstrumentation_Runnables")); // runnable instrumentation
		boolean enableExtCode = Boolean.parseBoolean(properties.getProperty("enableExternalCode"));

		boolean extOverwrite = false; // enabling codehook overwriting

		final Set<ProcessingUnit> assignedCores = DeploymentUtil.getAssignedCoreForProcess(task);

		final StringBuilder puDefinition = new StringBuilder("default");

		if (assignedCores != null && !assignedCores.isEmpty()) {
			puDefinition.delete(0, puDefinition.length());

			ProcessingUnit pu = assignedCores.iterator().next();
			ProcessingUnitDefinition puDef = pu.getDefinition();
			String puDefName = (puDef == null) ? null : puDef.getName();

			puDefinition.append(puDefName);
		}

		final List<String> statements = new ArrayList<>();
		final List<Process> processedTasks = new ArrayList<>();
		final List<String> statementsOverwrite = new ArrayList<>();

		if (task != null && task.getActivityGraph() != null) {
			for (final EObject item : task.getActivityGraph().eContents()) {

				if (item instanceof CustomPropertyImpl) {
					CustomPropertyImpl customProp = (CustomPropertyImpl) item;

					if (enableExtCode) {

						if (customProp.getKey().equals(KEY_CODEHOOK)) {
							Value value = customProp.getValue();
							if (value instanceof StringObject) {
								statements.add(((StringObject) value).getValue() + ";");
							}

						} else if (customProp.getKey().equals(KEY_CODEHOOK_OVERWRITE)) {
							extOverwrite = true;
							Value value1 = customProp.getValue();
							if (value1 instanceof StringObject) {
								statementsOverwrite.add(((StringObject) value1).getValue());
							}
						}
					}
				}

				if (item instanceof Group) {
					for (ActivityGraphItem item2 : ((Group) item).getItems()) {

						if ((item2 instanceof RunnableCall)) {
							final Runnable runnable = ((RunnableCall) item2).getRunnable();
							if (enableInstrumentation) {
								statements.add("instr_start();");
								statements.add("run_" + runnable.getName() + "(\"" + puDefinition + "\");");
								statements.add("instr_stop();");
							} else {
								statements.add("run_" + runnable.getName() + "(\"" + puDefinition + "\");");
							}
						} else if ((item2 instanceof InterProcessTrigger)) {
							InterProcessStimulus stimulus = ((InterProcessTrigger) item2).getStimulus();
							LinuxTaskGenerator.handleInterProcessTrigger(statements, processedTasks, stimulus);
						}
					}

				} else if (item instanceof RunnableCall) {
					final Runnable runnable = ((RunnableCall) item).getRunnable();
					if ((runnable != null)) {

						List<String> contextParameters = new ArrayList<>();
						List<LocalModeLabelAssignment> context = ((RunnableCall) item).getContext();

						for (LocalModeLabelAssignment localModeLabelAssignment : context) {

							ILocalModeValueSource valueSource = localModeLabelAssignment.getValueSource();

							if (valueSource instanceof ModeLiteralConst) {
								contextParameters
										.add("\"" + ((ModeLiteralConst) valueSource).getValue().getName() + "\"");
							}
						}

						if (enableInstrumentation) {
							statements.add("instr_start();");
							if (!contextParameters.isEmpty()) {
								statements.add("run_" + runnable.getName() + "_Context" + "(\"" + puDefinition + "\" , "
										+ String.join(",", contextParameters) + ");");
							} else {
								statements.add("run_" + runnable.getName() + "(\"" + puDefinition + "\");");
							}
							statements.add("instr_stop();");

						} else {

							if (!contextParameters.isEmpty()) {
								statements.add("run_" + runnable.getName() + "_Context" + "(\"" + puDefinition + "\" , "
										+ String.join(",", contextParameters) + ");");
							} else {
								statements.add("run_" + runnable.getName() + "(\"" + puDefinition + "\");");
							}

						}
					}

				} else if (item instanceof InterProcessTrigger) {
					InterProcessStimulus stimulus = ((InterProcessTrigger) item).getStimulus();
					LinuxTaskGenerator.handleInterProcessTrigger(statements, processedTasks, stimulus);
				}
			}
		}

		if (isIncFileEmpty(tu)) {
			// ----------------------------------/* fetch headers
			// names*/--------------------------------------
			// to fetch the headers names we use the getHeaderFilesDirectories function and
			// we go through every directory with the for loop
			final ConfigModel configModel = customObjsStore.<ConfigModel>getInstance(ConfigModel.class);

			if (enableExtCode) // input property to test if we need to include the external code headerfiles
			{
				String workingDirectory = customObjsStore.getData(TransformationConstants.WORKING_DIRECTORY);

				for (String hDir : ConfigModelUtils.getHeaderFilesDirectories(configModel, CodehookType.TASK, workingDirectory, logger)) {
					final File folder = new File(hDir.trim()); // fetching all the names in the headerfile directory.
					String names = ConfigModelUtils.getHeaderFilesIncludeMultiString(folder, logger);
					incAppend(tu, names);
				}
			}
			// ------------------------------------------------------------------------

			incAppend(tu, LinuxTaskGenerator.snippetIncStart());

		}
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, LinuxTaskGenerator.snippetSrcStart(enableInstrumentation));
		}

		// ------------------------ write body with overwrite codehook function

		if (extOverwrite) {
			// This code is
			String statement_overwrite = "void " + task.getName() + "(){ \n\n";
			srcAppend(tu, statement_overwrite);

			for (String statement : statementsOverwrite) {
				srcAppend(tu, statement + ";" + "\n");
			}
			srcAppend(tu, "\n" + "}" + "\n");

			srcAppend(tu, "\n void *" + task.getName() + "_entry(){ \n \n");
			srcAppend(tu, task.getName() + "(); \n");
			srcAppend(tu, "\n" + "}" + "\n");

		} else {
			srcAppend(tu, LinuxTaskGenerator.toCpp(task, statements));
		}
		incAppend(tu, LinuxTaskGenerator.toH(task));
	}

}
