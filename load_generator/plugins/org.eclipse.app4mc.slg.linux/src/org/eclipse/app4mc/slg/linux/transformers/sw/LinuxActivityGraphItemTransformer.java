/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;

import com.google.inject.Inject;

public class LinuxActivityGraphItemTransformer extends LinuxBaseTransformer {

	@Inject private LinuxLabelAccessTransformer linuxLabelAccessTransformer;
	@Inject private LinuxTicksTransformer linuxTicksTransformer;

	public SLGTranslationUnit transform(final ActivityGraphItem graphItem) {
		if (graphItem == null) {
			throw new IllegalArgumentException("Unhandled parameter type: null");
		}
		
		if (graphItem instanceof LabelAccess) {
			return linuxLabelAccessTransformer.transform((LabelAccess) graphItem);
		} else if (graphItem instanceof Ticks) {
			return linuxTicksTransformer.transform((Ticks) graphItem);
		}
		
		String warning = "/*[feature " + graphItem.eClass().getName() + " in not supported]";
		return new SLGTranslationUnit(warning);
	}

}
