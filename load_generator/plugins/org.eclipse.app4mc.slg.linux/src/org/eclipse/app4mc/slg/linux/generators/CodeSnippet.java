/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators;

public class CodeSnippet {
	public final int ticks;
	public final int reads;
	public final int writes;
	public final int stallsload;
	public final int stallsstore;
	public final String code;
	public final String name;
	public final int part;

	public CodeSnippet(final int ticks, final int reads, final int writes, final int stallsload, final int stallsstore,
			final String code, final String name, final int part) {
		this.ticks = ticks;
		this.reads = reads;
		this.writes = writes;
		this.stallsload = stallsload;
		this.stallsstore = stallsstore;
		this.code = code;
		this.name = name;
		this.part = part;
	}

}
