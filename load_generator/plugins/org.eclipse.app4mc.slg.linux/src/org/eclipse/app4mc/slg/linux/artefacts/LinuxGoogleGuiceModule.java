/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.slg.linux.artefacts;

import java.util.Properties;

import org.eclipse.app4mc.slg.commons.m2t.CustomObjectsStore;
import org.eclipse.app4mc.slg.commons.m2t.transformers.sw.RunnableTransformer;
import org.eclipse.app4mc.slg.customization.CustomRunnableTransformer;
import org.eclipse.app4mc.transformation.util.TransformationGuiceModule;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

public class LinuxGoogleGuiceModule extends TransformationGuiceModule {

	protected Properties properties;

	public LinuxGoogleGuiceModule(SessionLogger logger, Properties properties) {
		super(logger);
		this.properties = properties;
	}

	@Override
	protected void configure() {
		super.configure();
		
		bind(Properties.class).toInstance(this.properties);
		bind(CustomObjectsStore.class).toInstance(new CustomObjectsStore());

		bind(RunnableTransformer.class).to(CustomRunnableTransformer.class);
	}

}
