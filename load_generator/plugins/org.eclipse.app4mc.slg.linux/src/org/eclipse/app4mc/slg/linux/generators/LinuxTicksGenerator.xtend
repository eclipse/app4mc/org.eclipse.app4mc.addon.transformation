/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators

import org.eclipse.app4mc.amalthea.model.DiscreteValueBetaDistribution
import org.eclipse.app4mc.amalthea.model.DiscreteValueBoundaries
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution
import org.eclipse.app4mc.amalthea.model.DiscreteValueHistogram
import org.eclipse.app4mc.amalthea.model.DiscreteValueStatistics
import org.eclipse.app4mc.amalthea.model.DiscreteValueUniformDistribution
import org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation

class LinuxTicksGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def dispatch String getParameters(DiscreteValueConstant value)
		'''«value.value»'''

	static def dispatch String getParameters(DiscreteValueStatistics value)
		'''«value.average», «value.lowerBound», «value.upperBound»'''

	static def dispatch String getParameters(DiscreteValueWeibullEstimatorsDistribution value)
		'''«value.average»'''

	static def dispatch String getParameters(IDiscreteValueDeviation value) '''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED	
	'''

	static def dispatch String getParameters(DiscreteValueBetaDistribution value) '''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED
	'''

	static def dispatch String getParameters(DiscreteValueBoundaries value) '''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED
	'''

	static def dispatch String getParameters(DiscreteValueGaussDistribution value) '''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED
	'''

	static def dispatch String getParameters(DiscreteValueHistogram value) '''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED
	'''

	static def dispatch String getParameters(DiscreteValueUniformDistribution value) '''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED
	'''

}
