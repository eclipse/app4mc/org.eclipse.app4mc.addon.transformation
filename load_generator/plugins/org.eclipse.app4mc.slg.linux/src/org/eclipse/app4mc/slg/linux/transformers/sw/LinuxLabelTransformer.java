/**
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.linux.generators.LinuxLabelGenerator;
import org.eclipse.app4mc.slg.linux.transformers.LinuxBaseTransformer;

import com.google.inject.Singleton;

@Singleton
public class LinuxLabelTransformer extends LinuxBaseTransformer {

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, SLGTranslationUnit> transformCache = new HashMap<>();

	@Override
	public Map<List<Object>, SLGTranslationUnit> getCache() {
		return this.transformCache;
	}

	public SLGTranslationUnit transform(final Label label) {
		final List<Object> key = List.of(label);
		final SLGTranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(label);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, label);
		}

		return tu;
	}

	// ---------------------------------------------------

	private SLGTranslationUnit createTranslationUnit(final Label label) {
		if ((label == null)) {
			return new SLGTranslationUnit("UNSPECIFIED LABEL");
		} else {
			String basePath = "synthetic_gen";
			String moduleName = "labels";
			String call = "burnTicks(<params>)"; // unused
			return new SLGTranslationUnit(basePath, moduleName, call);
		}
	}

	private void doTransform(final SLGTranslationUnit tu, final Label label) {
		genFiles(tu, label);
	}

	private void genFiles(final SLGTranslationUnit tu, final Label label) {
		if (isSrcFileEmpty(tu)) {
			srcAppend(tu, "#include \"" + getIncFile(tu) + "\"\n");
			srcAppend(tu, "#include <stdbool.h>\n\n");
		}

		incAppend(tu, LinuxLabelGenerator.toH(label));
		srcAppend(tu, LinuxLabelGenerator.toCpp(label));
	}

}
