/**
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.slg.linux.transformers.sw;

import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGBaseTransformer;
import org.eclipse.app4mc.slg.commons.m2t.transformers.SLGTranslationUnit;
import org.eclipse.app4mc.slg.linux.generators.LinuxTicksGenerator;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LinuxTicksTransformer extends SLGBaseTransformer {

	@Inject private LinuxTicksUtilsTransformer ticksUtilsTransformer;

	public SLGTranslationUnit transform(final Ticks ticks) {
		if ((ticks.getDefault() != null)) {
			final SLGTranslationUnit ticksUtilsTU = ticksUtilsTransformer.transform(ticks.getDefault());

			return createTranslationUnit(ticks, ticksUtilsTU);
		}

		return null;
	}

	protected SLGTranslationUnit createTranslationUnit(Ticks ticks, SLGTranslationUnit ticksUtilsTU) {
		if ((ticksUtilsTU == null)) {
			return new SLGTranslationUnit("UNSPECIFIED TICKS");			
		}

		String basePath = ticksUtilsTU.getBasePath();
		String moduleName = ticksUtilsTU.getModuleName();
		String call = computeCall(ticks);
		return new SLGTranslationUnit(basePath, moduleName, call);
	}

	protected String computeCall(Ticks ticks) {
		String parameters = LinuxTicksGenerator.getParameters(ticks.getDefault());
		String className = ticks.getDefault().eClass().getName();
		return "executeTicks_" + className +"(" + parameters + ")";
	}

}
