/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.linux.generators

import java.util.List
import org.eclipse.app4mc.amalthea.model.Process
import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amalthea.model.Task

class LinuxTaskGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	static def void handleInterProcessTrigger(List<String> statements, List<Process> processedTasks,
		Stimulus stimulus) {
		statements.add("//interprocess trigger");

		if (stimulus !== null) {
			val processes = stimulus.affectedProcesses

			for (process : processes) {

				if (process instanceof Task) {
					val String codeSnippet = '''
						«IF processedTasks.contains(process) == false»
							pthread_t «process.name»_;
							«{processedTasks.add(process);""}»
						«ENDIF»	
							
							pthread_create(&«process.name»_, NULL, *«process.name»_entry, NULL);
							//pthread_join(«process.name»_, NULL);
							
					''';

					statements.add(codeSnippet)
				}
			}
		}
	}

	static def String snippetIncStart() '''
		#include "runnables.h"
	'''

	static def String snippetSrcStart(boolean enableInstrumentation_R) '''
		#include "tasks.h"
		#include <pthread.h>
		«IF enableInstrumentation_R»
			#include "instrument.h"
			
		«ENDIF»
		
	'''

	static def String toH(Task task) '''
		
			void «task.name»();
			void *«task.name»_entry();
	'''

	static def String toCpp(Task task, List<String> statements) '''
		void «task.name»(){
			«FOR statement : statements»
				«statement»
			«ENDFOR»
		}
		
		void *«task.name»_entry(){
			«task.name»();
		}
	'''

}
