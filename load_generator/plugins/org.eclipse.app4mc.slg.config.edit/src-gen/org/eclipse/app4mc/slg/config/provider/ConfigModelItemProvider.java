/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.ConfigurationFactory;
import org.eclipse.app4mc.slg.config.ConfigurationPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link org.eclipse.app4mc.slg.config.ConfigModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ConfigModelItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVrteVersionPropertyDescriptor(object);
			addDefaultTickTypePropertyDescriptor(object);
			addPlatformArchitectureTypePropertyDescriptor(object);
			addVrteAdaptiveStudioVersionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Vrte Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVrteVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ConfigModel_vrteVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ConfigModel_vrteVersion_feature", "_UI_ConfigModel_type"),
				 ConfigurationPackage.Literals.CONFIG_MODEL__VRTE_VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Default Tick Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultTickTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ConfigModel_defaultTickType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ConfigModel_defaultTickType_feature", "_UI_ConfigModel_type"),
				 ConfigurationPackage.Literals.CONFIG_MODEL__DEFAULT_TICK_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Platform Architecture Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPlatformArchitectureTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ConfigModel_platformArchitectureType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ConfigModel_platformArchitectureType_feature", "_UI_ConfigModel_type"),
				 ConfigurationPackage.Literals.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Vrte Adaptive Studio Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVrteAdaptiveStudioVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ConfigModel_vrteAdaptiveStudioVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ConfigModel_vrteAdaptiveStudioVersion_feature", "_UI_ConfigModel_type"),
				 ConfigurationPackage.Literals.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ConfigurationPackage.Literals.CONFIG_MODEL__CUSTOM_TICK_IMPL);
			childrenFeatures.add(ConfigurationPackage.Literals.CONFIG_MODEL__CUSTOM_READ_IMPL);
			childrenFeatures.add(ConfigurationPackage.Literals.CONFIG_MODEL__CUSTOM_WRITE_IMPL);
			childrenFeatures.add(ConfigurationPackage.Literals.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS);
			childrenFeatures.add(ConfigurationPackage.Literals.CONFIG_MODEL__CODE_HOOKS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ConfigModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ConfigModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ConfigModel)object).getVrteVersion();
		return label == null || label.length() == 0 ?
			getString("_UI_ConfigModel_type") :
			getString("_UI_ConfigModel_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ConfigModel.class)) {
			case ConfigurationPackage.CONFIG_MODEL__VRTE_VERSION:
			case ConfigurationPackage.CONFIG_MODEL__DEFAULT_TICK_TYPE:
			case ConfigurationPackage.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE:
			case ConfigurationPackage.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL:
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL:
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL:
			case ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS:
			case ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ConfigurationPackage.Literals.CONFIG_MODEL__CUSTOM_TICK_IMPL,
				 ConfigurationFactory.eINSTANCE.createCustomTickImpl()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigurationPackage.Literals.CONFIG_MODEL__CUSTOM_READ_IMPL,
				 ConfigurationFactory.eINSTANCE.createCustomReadImpl()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigurationPackage.Literals.CONFIG_MODEL__CUSTOM_WRITE_IMPL,
				 ConfigurationFactory.eINSTANCE.createCustomWriteImpl()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigurationPackage.Literals.CONFIG_MODEL__CODE_HOOKS,
				 ConfigurationFactory.eINSTANCE.createCodeHooks()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ConfigurationEditPlugin.INSTANCE;
	}

}
