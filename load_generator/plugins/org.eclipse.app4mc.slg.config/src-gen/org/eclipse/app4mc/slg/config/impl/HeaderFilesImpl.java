/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.impl;

import java.util.Collection;

import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigurationPackage;
import org.eclipse.app4mc.slg.config.HeaderFiles;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Header Files</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.HeaderFilesImpl#getCodehookType <em>Codehook Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.HeaderFilesImpl#getHeaderFilesDirectories <em>Header Files Directories</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HeaderFilesImpl extends MinimalEObjectImpl.Container implements HeaderFiles {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The default value of the '{@link #getCodehookType() <em>Codehook Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodehookType()
	 * @generated
	 * @ordered
	 */
	protected static final CodehookType CODEHOOK_TYPE_EDEFAULT = CodehookType.RUNNABLE;

	/**
	 * The cached value of the '{@link #getCodehookType() <em>Codehook Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodehookType()
	 * @generated
	 * @ordered
	 */
	protected CodehookType codehookType = CODEHOOK_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHeaderFilesDirectories() <em>Header Files Directories</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFilesDirectories()
	 * @generated
	 * @ordered
	 */
	protected EList<String> headerFilesDirectories;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HeaderFilesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.HEADER_FILES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CodehookType getCodehookType() {
		return codehookType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCodehookType(CodehookType newCodehookType) {
		CodehookType oldCodehookType = codehookType;
		codehookType = newCodehookType == null ? CODEHOOK_TYPE_EDEFAULT : newCodehookType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.HEADER_FILES__CODEHOOK_TYPE, oldCodehookType, codehookType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getHeaderFilesDirectories() {
		if (headerFilesDirectories == null) {
			headerFilesDirectories = new EDataTypeUniqueEList<String>(String.class, this, ConfigurationPackage.HEADER_FILES__HEADER_FILES_DIRECTORIES);
		}
		return headerFilesDirectories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.HEADER_FILES__CODEHOOK_TYPE:
				return getCodehookType();
			case ConfigurationPackage.HEADER_FILES__HEADER_FILES_DIRECTORIES:
				return getHeaderFilesDirectories();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.HEADER_FILES__CODEHOOK_TYPE:
				setCodehookType((CodehookType)newValue);
				return;
			case ConfigurationPackage.HEADER_FILES__HEADER_FILES_DIRECTORIES:
				getHeaderFilesDirectories().clear();
				getHeaderFilesDirectories().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.HEADER_FILES__CODEHOOK_TYPE:
				setCodehookType(CODEHOOK_TYPE_EDEFAULT);
				return;
			case ConfigurationPackage.HEADER_FILES__HEADER_FILES_DIRECTORIES:
				getHeaderFilesDirectories().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.HEADER_FILES__CODEHOOK_TYPE:
				return codehookType != CODEHOOK_TYPE_EDEFAULT;
			case ConfigurationPackage.HEADER_FILES__HEADER_FILES_DIRECTORIES:
				return headerFilesDirectories != null && !headerFilesDirectories.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (codehookType: ");
		result.append(codehookType);
		result.append(", headerFilesDirectories: ");
		result.append(headerFilesDirectories);
		result.append(')');
		return result.toString();
	}

} //HeaderFilesImpl
