/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.impl;

import org.eclipse.app4mc.slg.config.CodeHooks;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.ConfigurationFactory;
import org.eclipse.app4mc.slg.config.ConfigurationPackage;
import org.eclipse.app4mc.slg.config.CustomImpl;
import org.eclipse.app4mc.slg.config.CustomReadImpl;
import org.eclipse.app4mc.slg.config.CustomTickImpl;
import org.eclipse.app4mc.slg.config.CustomWriteImpl;
import org.eclipse.app4mc.slg.config.FileType;
import org.eclipse.app4mc.slg.config.HeaderFiles;
import org.eclipse.app4mc.slg.config.LibLocations;
import org.eclipse.app4mc.slg.config.MiddleWareSettings;
import org.eclipse.app4mc.slg.config.PlatformArchitecture;
import org.eclipse.app4mc.slg.config.TickType;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConfigurationPackageImpl extends EPackageImpl implements ConfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customImplEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customTickImplEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customReadImplEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customWriteImplEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass middleWareSettingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeHooksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass libLocationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass headerFilesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fileTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum tickTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum platformArchitectureEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum codehookTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ConfigurationPackageImpl() {
		super(eNS_URI, ConfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ConfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ConfigurationPackage init() {
		if (isInited) return (ConfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ConfigurationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredConfigurationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ConfigurationPackageImpl theConfigurationPackage = registeredConfigurationPackage instanceof ConfigurationPackageImpl ? (ConfigurationPackageImpl)registeredConfigurationPackage : new ConfigurationPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theConfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theConfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theConfigurationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ConfigurationPackage.eNS_URI, theConfigurationPackage);
		return theConfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConfigModel() {
		return configModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConfigModel_VrteVersion() {
		return (EAttribute)configModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConfigModel_DefaultTickType() {
		return (EAttribute)configModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConfigModel_CustomTickImpl() {
		return (EReference)configModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConfigModel_CustomReadImpl() {
		return (EReference)configModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConfigModel_CustomWriteImpl() {
		return (EReference)configModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConfigModel_PlatformArchitectureType() {
		return (EAttribute)configModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConfigModel_VrteAdaptiveStudioVersion() {
		return (EAttribute)configModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConfigModel_MiddleWareSpecificSettings() {
		return (EReference)configModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConfigModel_CodeHooks() {
		return (EReference)configModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCustomImpl() {
		return customImplEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCustomImpl_Enable() {
		return (EAttribute)customImplEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCustomImpl_Value() {
		return (EAttribute)customImplEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCustomTickImpl() {
		return customTickImplEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCustomReadImpl() {
		return customReadImplEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCustomWriteImpl() {
		return customWriteImplEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMiddleWareSettings() {
		return middleWareSettingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCodeHooks() {
		return codeHooksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodeHooks_LibLocations() {
		return (EReference)codeHooksEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCodeHooks_HeaderFiles() {
		return (EReference)codeHooksEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLibLocations() {
		return libLocationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLibLocations_LinkedLibraries() {
		return (EAttribute)libLocationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLibLocations_PackageNames() {
		return (EAttribute)libLocationsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHeaderFiles() {
		return headerFilesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHeaderFiles_CodehookType() {
		return (EAttribute)headerFilesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHeaderFiles_HeaderFilesDirectories() {
		return (EAttribute)headerFilesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getFileType() {
		return fileTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getTickType() {
		return tickTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPlatformArchitecture() {
		return platformArchitectureEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCodehookType() {
		return codehookTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConfigurationFactory getConfigurationFactory() {
		return (ConfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		configModelEClass = createEClass(CONFIG_MODEL);
		createEAttribute(configModelEClass, CONFIG_MODEL__VRTE_VERSION);
		createEAttribute(configModelEClass, CONFIG_MODEL__DEFAULT_TICK_TYPE);
		createEReference(configModelEClass, CONFIG_MODEL__CUSTOM_TICK_IMPL);
		createEReference(configModelEClass, CONFIG_MODEL__CUSTOM_READ_IMPL);
		createEReference(configModelEClass, CONFIG_MODEL__CUSTOM_WRITE_IMPL);
		createEAttribute(configModelEClass, CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE);
		createEAttribute(configModelEClass, CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION);
		createEReference(configModelEClass, CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS);
		createEReference(configModelEClass, CONFIG_MODEL__CODE_HOOKS);

		customImplEClass = createEClass(CUSTOM_IMPL);
		createEAttribute(customImplEClass, CUSTOM_IMPL__ENABLE);
		createEAttribute(customImplEClass, CUSTOM_IMPL__VALUE);

		customTickImplEClass = createEClass(CUSTOM_TICK_IMPL);

		customReadImplEClass = createEClass(CUSTOM_READ_IMPL);

		customWriteImplEClass = createEClass(CUSTOM_WRITE_IMPL);

		middleWareSettingsEClass = createEClass(MIDDLE_WARE_SETTINGS);

		codeHooksEClass = createEClass(CODE_HOOKS);
		createEReference(codeHooksEClass, CODE_HOOKS__LIB_LOCATIONS);
		createEReference(codeHooksEClass, CODE_HOOKS__HEADER_FILES);

		libLocationsEClass = createEClass(LIB_LOCATIONS);
		createEAttribute(libLocationsEClass, LIB_LOCATIONS__LINKED_LIBRARIES);
		createEAttribute(libLocationsEClass, LIB_LOCATIONS__PACKAGE_NAMES);

		headerFilesEClass = createEClass(HEADER_FILES);
		createEAttribute(headerFilesEClass, HEADER_FILES__CODEHOOK_TYPE);
		createEAttribute(headerFilesEClass, HEADER_FILES__HEADER_FILES_DIRECTORIES);

		// Create enums
		fileTypeEEnum = createEEnum(FILE_TYPE);
		tickTypeEEnum = createEEnum(TICK_TYPE);
		platformArchitectureEEnum = createEEnum(PLATFORM_ARCHITECTURE);
		codehookTypeEEnum = createEEnum(CODEHOOK_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		customTickImplEClass.getESuperTypes().add(this.getCustomImpl());
		customReadImplEClass.getESuperTypes().add(this.getCustomImpl());
		customWriteImplEClass.getESuperTypes().add(this.getCustomImpl());

		// Initialize classes, features, and operations; add parameters
		initEClass(configModelEClass, ConfigModel.class, "ConfigModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConfigModel_VrteVersion(), ecorePackage.getEString(), "vrteVersion", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigModel_DefaultTickType(), this.getTickType(), "defaultTickType", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfigModel_CustomTickImpl(), this.getCustomTickImpl(), null, "customTickImpl", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfigModel_CustomReadImpl(), this.getCustomReadImpl(), null, "customReadImpl", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfigModel_CustomWriteImpl(), this.getCustomWriteImpl(), null, "customWriteImpl", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigModel_PlatformArchitectureType(), this.getPlatformArchitecture(), "platformArchitectureType", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConfigModel_VrteAdaptiveStudioVersion(), ecorePackage.getEString(), "vrteAdaptiveStudioVersion", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfigModel_MiddleWareSpecificSettings(), this.getMiddleWareSettings(), null, "middleWareSpecificSettings", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConfigModel_CodeHooks(), this.getCodeHooks(), null, "codeHooks", null, 0, 1, ConfigModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(customImplEClass, CustomImpl.class, "CustomImpl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCustomImpl_Enable(), ecorePackage.getEBoolean(), "enable", null, 0, 1, CustomImpl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCustomImpl_Value(), ecorePackage.getEString(), "value", null, 0, 1, CustomImpl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(customTickImplEClass, CustomTickImpl.class, "CustomTickImpl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(customReadImplEClass, CustomReadImpl.class, "CustomReadImpl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(customWriteImplEClass, CustomWriteImpl.class, "CustomWriteImpl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(middleWareSettingsEClass, MiddleWareSettings.class, "MiddleWareSettings", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(codeHooksEClass, CodeHooks.class, "CodeHooks", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeHooks_LibLocations(), this.getLibLocations(), null, "libLocations", null, 0, 1, CodeHooks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeHooks_HeaderFiles(), this.getHeaderFiles(), null, "headerFiles", null, 0, -1, CodeHooks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(libLocationsEClass, LibLocations.class, "LibLocations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLibLocations_LinkedLibraries(), ecorePackage.getEString(), "linkedLibraries", null, 0, -1, LibLocations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLibLocations_PackageNames(), ecorePackage.getEString(), "packageNames", null, 0, -1, LibLocations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(headerFilesEClass, HeaderFiles.class, "HeaderFiles", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHeaderFiles_CodehookType(), this.getCodehookType(), "codehookType", null, 0, 1, HeaderFiles.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHeaderFiles_HeaderFilesDirectories(), ecorePackage.getEString(), "headerFilesDirectories", null, 0, -1, HeaderFiles.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(fileTypeEEnum, FileType.class, "FileType");
		addEEnumLiteral(fileTypeEEnum, FileType.UNDEFINED);
		addEEnumLiteral(fileTypeEEnum, FileType.C);
		addEEnumLiteral(fileTypeEEnum, FileType.H);
		addEEnumLiteral(fileTypeEEnum, FileType.CPP);
		addEEnumLiteral(fileTypeEEnum, FileType.HPP);

		initEEnum(tickTypeEEnum, TickType.class, "TickType");
		addEEnumLiteral(tickTypeEEnum, TickType.AVERAGE);
		addEEnumLiteral(tickTypeEEnum, TickType.MINIMUM);
		addEEnumLiteral(tickTypeEEnum, TickType.MAXIMUM);

		initEEnum(platformArchitectureEEnum, PlatformArchitecture.class, "PlatformArchitecture");
		addEEnumLiteral(platformArchitectureEEnum, PlatformArchitecture.X86_64);
		addEEnumLiteral(platformArchitectureEEnum, PlatformArchitecture.X86_32);
		addEEnumLiteral(platformArchitectureEEnum, PlatformArchitecture.AARCH64);

		initEEnum(codehookTypeEEnum, CodehookType.class, "CodehookType");
		addEEnumLiteral(codehookTypeEEnum, CodehookType.RUNNABLE);
		addEEnumLiteral(codehookTypeEEnum, CodehookType.TASK);

		// Create resource
		createResource(eNS_URI);
	}

} //ConfigurationPackageImpl
