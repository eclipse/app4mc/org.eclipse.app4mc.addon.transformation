/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Hooks</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.CodeHooks#getLibLocations <em>Lib Locations</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.CodeHooks#getHeaderFiles <em>Header Files</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getCodeHooks()
 * @model
 * @generated
 */
public interface CodeHooks extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Lib Locations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lib Locations</em>' containment reference.
	 * @see #setLibLocations(LibLocations)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getCodeHooks_LibLocations()
	 * @model containment="true"
	 * @generated
	 */
	LibLocations getLibLocations();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.CodeHooks#getLibLocations <em>Lib Locations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lib Locations</em>' containment reference.
	 * @see #getLibLocations()
	 * @generated
	 */
	void setLibLocations(LibLocations value);

	/**
	 * Returns the value of the '<em><b>Header Files</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.slg.config.HeaderFiles}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Files</em>' containment reference list.
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getCodeHooks_HeaderFiles()
	 * @model containment="true"
	 * @generated
	 */
	EList<HeaderFiles> getHeaderFiles();

} // CodeHooks
