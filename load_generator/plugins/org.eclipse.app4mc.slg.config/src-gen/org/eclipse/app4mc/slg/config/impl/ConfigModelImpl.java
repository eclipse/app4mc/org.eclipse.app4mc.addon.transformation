/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.impl;

import org.eclipse.app4mc.slg.config.CodeHooks;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.ConfigurationPackage;
import org.eclipse.app4mc.slg.config.CustomReadImpl;
import org.eclipse.app4mc.slg.config.CustomTickImpl;
import org.eclipse.app4mc.slg.config.CustomWriteImpl;
import org.eclipse.app4mc.slg.config.MiddleWareSettings;
import org.eclipse.app4mc.slg.config.PlatformArchitecture;
import org.eclipse.app4mc.slg.config.TickType;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Config Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getVrteVersion <em>Vrte Version</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getDefaultTickType <em>Default Tick Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getCustomTickImpl <em>Custom Tick Impl</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getCustomReadImpl <em>Custom Read Impl</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getCustomWriteImpl <em>Custom Write Impl</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getPlatformArchitectureType <em>Platform Architecture Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getVrteAdaptiveStudioVersion <em>Vrte Adaptive Studio Version</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getMiddleWareSpecificSettings <em>Middle Ware Specific Settings</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl#getCodeHooks <em>Code Hooks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigModelImpl extends MinimalEObjectImpl.Container implements ConfigModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The default value of the '{@link #getVrteVersion() <em>Vrte Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVrteVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VRTE_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVrteVersion() <em>Vrte Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVrteVersion()
	 * @generated
	 * @ordered
	 */
	protected String vrteVersion = VRTE_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultTickType() <em>Default Tick Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultTickType()
	 * @generated
	 * @ordered
	 */
	protected static final TickType DEFAULT_TICK_TYPE_EDEFAULT = TickType.AVERAGE;

	/**
	 * The cached value of the '{@link #getDefaultTickType() <em>Default Tick Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultTickType()
	 * @generated
	 * @ordered
	 */
	protected TickType defaultTickType = DEFAULT_TICK_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCustomTickImpl() <em>Custom Tick Impl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomTickImpl()
	 * @generated
	 * @ordered
	 */
	protected CustomTickImpl customTickImpl;

	/**
	 * The cached value of the '{@link #getCustomReadImpl() <em>Custom Read Impl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomReadImpl()
	 * @generated
	 * @ordered
	 */
	protected CustomReadImpl customReadImpl;

	/**
	 * The cached value of the '{@link #getCustomWriteImpl() <em>Custom Write Impl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomWriteImpl()
	 * @generated
	 * @ordered
	 */
	protected CustomWriteImpl customWriteImpl;

	/**
	 * The default value of the '{@link #getPlatformArchitectureType() <em>Platform Architecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlatformArchitectureType()
	 * @generated
	 * @ordered
	 */
	protected static final PlatformArchitecture PLATFORM_ARCHITECTURE_TYPE_EDEFAULT = PlatformArchitecture.X86_64;

	/**
	 * The cached value of the '{@link #getPlatformArchitectureType() <em>Platform Architecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlatformArchitectureType()
	 * @generated
	 * @ordered
	 */
	protected PlatformArchitecture platformArchitectureType = PLATFORM_ARCHITECTURE_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getVrteAdaptiveStudioVersion() <em>Vrte Adaptive Studio Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVrteAdaptiveStudioVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VRTE_ADAPTIVE_STUDIO_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVrteAdaptiveStudioVersion() <em>Vrte Adaptive Studio Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVrteAdaptiveStudioVersion()
	 * @generated
	 * @ordered
	 */
	protected String vrteAdaptiveStudioVersion = VRTE_ADAPTIVE_STUDIO_VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMiddleWareSpecificSettings() <em>Middle Ware Specific Settings</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMiddleWareSpecificSettings()
	 * @generated
	 * @ordered
	 */
	protected MiddleWareSettings middleWareSpecificSettings;

	/**
	 * The cached value of the '{@link #getCodeHooks() <em>Code Hooks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCodeHooks()
	 * @generated
	 * @ordered
	 */
	protected CodeHooks codeHooks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.CONFIG_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVrteVersion() {
		return vrteVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVrteVersion(String newVrteVersion) {
		String oldVrteVersion = vrteVersion;
		vrteVersion = newVrteVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__VRTE_VERSION, oldVrteVersion, vrteVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TickType getDefaultTickType() {
		return defaultTickType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDefaultTickType(TickType newDefaultTickType) {
		TickType oldDefaultTickType = defaultTickType;
		defaultTickType = newDefaultTickType == null ? DEFAULT_TICK_TYPE_EDEFAULT : newDefaultTickType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__DEFAULT_TICK_TYPE, oldDefaultTickType, defaultTickType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CustomTickImpl getCustomTickImpl() {
		return customTickImpl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCustomTickImpl(CustomTickImpl newCustomTickImpl, NotificationChain msgs) {
		CustomTickImpl oldCustomTickImpl = customTickImpl;
		customTickImpl = newCustomTickImpl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL, oldCustomTickImpl, newCustomTickImpl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCustomTickImpl(CustomTickImpl newCustomTickImpl) {
		if (newCustomTickImpl != customTickImpl) {
			NotificationChain msgs = null;
			if (customTickImpl != null)
				msgs = ((InternalEObject)customTickImpl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL, null, msgs);
			if (newCustomTickImpl != null)
				msgs = ((InternalEObject)newCustomTickImpl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL, null, msgs);
			msgs = basicSetCustomTickImpl(newCustomTickImpl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL, newCustomTickImpl, newCustomTickImpl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CustomReadImpl getCustomReadImpl() {
		return customReadImpl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCustomReadImpl(CustomReadImpl newCustomReadImpl, NotificationChain msgs) {
		CustomReadImpl oldCustomReadImpl = customReadImpl;
		customReadImpl = newCustomReadImpl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL, oldCustomReadImpl, newCustomReadImpl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCustomReadImpl(CustomReadImpl newCustomReadImpl) {
		if (newCustomReadImpl != customReadImpl) {
			NotificationChain msgs = null;
			if (customReadImpl != null)
				msgs = ((InternalEObject)customReadImpl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL, null, msgs);
			if (newCustomReadImpl != null)
				msgs = ((InternalEObject)newCustomReadImpl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL, null, msgs);
			msgs = basicSetCustomReadImpl(newCustomReadImpl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL, newCustomReadImpl, newCustomReadImpl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CustomWriteImpl getCustomWriteImpl() {
		return customWriteImpl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCustomWriteImpl(CustomWriteImpl newCustomWriteImpl, NotificationChain msgs) {
		CustomWriteImpl oldCustomWriteImpl = customWriteImpl;
		customWriteImpl = newCustomWriteImpl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL, oldCustomWriteImpl, newCustomWriteImpl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCustomWriteImpl(CustomWriteImpl newCustomWriteImpl) {
		if (newCustomWriteImpl != customWriteImpl) {
			NotificationChain msgs = null;
			if (customWriteImpl != null)
				msgs = ((InternalEObject)customWriteImpl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL, null, msgs);
			if (newCustomWriteImpl != null)
				msgs = ((InternalEObject)newCustomWriteImpl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL, null, msgs);
			msgs = basicSetCustomWriteImpl(newCustomWriteImpl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL, newCustomWriteImpl, newCustomWriteImpl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PlatformArchitecture getPlatformArchitectureType() {
		return platformArchitectureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPlatformArchitectureType(PlatformArchitecture newPlatformArchitectureType) {
		PlatformArchitecture oldPlatformArchitectureType = platformArchitectureType;
		platformArchitectureType = newPlatformArchitectureType == null ? PLATFORM_ARCHITECTURE_TYPE_EDEFAULT : newPlatformArchitectureType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE, oldPlatformArchitectureType, platformArchitectureType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVrteAdaptiveStudioVersion() {
		return vrteAdaptiveStudioVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVrteAdaptiveStudioVersion(String newVrteAdaptiveStudioVersion) {
		String oldVrteAdaptiveStudioVersion = vrteAdaptiveStudioVersion;
		vrteAdaptiveStudioVersion = newVrteAdaptiveStudioVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION, oldVrteAdaptiveStudioVersion, vrteAdaptiveStudioVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MiddleWareSettings getMiddleWareSpecificSettings() {
		return middleWareSpecificSettings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMiddleWareSpecificSettings(MiddleWareSettings newMiddleWareSpecificSettings, NotificationChain msgs) {
		MiddleWareSettings oldMiddleWareSpecificSettings = middleWareSpecificSettings;
		middleWareSpecificSettings = newMiddleWareSpecificSettings;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS, oldMiddleWareSpecificSettings, newMiddleWareSpecificSettings);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMiddleWareSpecificSettings(MiddleWareSettings newMiddleWareSpecificSettings) {
		if (newMiddleWareSpecificSettings != middleWareSpecificSettings) {
			NotificationChain msgs = null;
			if (middleWareSpecificSettings != null)
				msgs = ((InternalEObject)middleWareSpecificSettings).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS, null, msgs);
			if (newMiddleWareSpecificSettings != null)
				msgs = ((InternalEObject)newMiddleWareSpecificSettings).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS, null, msgs);
			msgs = basicSetMiddleWareSpecificSettings(newMiddleWareSpecificSettings, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS, newMiddleWareSpecificSettings, newMiddleWareSpecificSettings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CodeHooks getCodeHooks() {
		return codeHooks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCodeHooks(CodeHooks newCodeHooks, NotificationChain msgs) {
		CodeHooks oldCodeHooks = codeHooks;
		codeHooks = newCodeHooks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS, oldCodeHooks, newCodeHooks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCodeHooks(CodeHooks newCodeHooks) {
		if (newCodeHooks != codeHooks) {
			NotificationChain msgs = null;
			if (codeHooks != null)
				msgs = ((InternalEObject)codeHooks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS, null, msgs);
			if (newCodeHooks != null)
				msgs = ((InternalEObject)newCodeHooks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS, null, msgs);
			msgs = basicSetCodeHooks(newCodeHooks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS, newCodeHooks, newCodeHooks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL:
				return basicSetCustomTickImpl(null, msgs);
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL:
				return basicSetCustomReadImpl(null, msgs);
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL:
				return basicSetCustomWriteImpl(null, msgs);
			case ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS:
				return basicSetMiddleWareSpecificSettings(null, msgs);
			case ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS:
				return basicSetCodeHooks(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.CONFIG_MODEL__VRTE_VERSION:
				return getVrteVersion();
			case ConfigurationPackage.CONFIG_MODEL__DEFAULT_TICK_TYPE:
				return getDefaultTickType();
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL:
				return getCustomTickImpl();
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL:
				return getCustomReadImpl();
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL:
				return getCustomWriteImpl();
			case ConfigurationPackage.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE:
				return getPlatformArchitectureType();
			case ConfigurationPackage.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION:
				return getVrteAdaptiveStudioVersion();
			case ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS:
				return getMiddleWareSpecificSettings();
			case ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS:
				return getCodeHooks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.CONFIG_MODEL__VRTE_VERSION:
				setVrteVersion((String)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__DEFAULT_TICK_TYPE:
				setDefaultTickType((TickType)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL:
				setCustomTickImpl((CustomTickImpl)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL:
				setCustomReadImpl((CustomReadImpl)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL:
				setCustomWriteImpl((CustomWriteImpl)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE:
				setPlatformArchitectureType((PlatformArchitecture)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION:
				setVrteAdaptiveStudioVersion((String)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS:
				setMiddleWareSpecificSettings((MiddleWareSettings)newValue);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS:
				setCodeHooks((CodeHooks)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.CONFIG_MODEL__VRTE_VERSION:
				setVrteVersion(VRTE_VERSION_EDEFAULT);
				return;
			case ConfigurationPackage.CONFIG_MODEL__DEFAULT_TICK_TYPE:
				setDefaultTickType(DEFAULT_TICK_TYPE_EDEFAULT);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL:
				setCustomTickImpl((CustomTickImpl)null);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL:
				setCustomReadImpl((CustomReadImpl)null);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL:
				setCustomWriteImpl((CustomWriteImpl)null);
				return;
			case ConfigurationPackage.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE:
				setPlatformArchitectureType(PLATFORM_ARCHITECTURE_TYPE_EDEFAULT);
				return;
			case ConfigurationPackage.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION:
				setVrteAdaptiveStudioVersion(VRTE_ADAPTIVE_STUDIO_VERSION_EDEFAULT);
				return;
			case ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS:
				setMiddleWareSpecificSettings((MiddleWareSettings)null);
				return;
			case ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS:
				setCodeHooks((CodeHooks)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.CONFIG_MODEL__VRTE_VERSION:
				return VRTE_VERSION_EDEFAULT == null ? vrteVersion != null : !VRTE_VERSION_EDEFAULT.equals(vrteVersion);
			case ConfigurationPackage.CONFIG_MODEL__DEFAULT_TICK_TYPE:
				return defaultTickType != DEFAULT_TICK_TYPE_EDEFAULT;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_TICK_IMPL:
				return customTickImpl != null;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_READ_IMPL:
				return customReadImpl != null;
			case ConfigurationPackage.CONFIG_MODEL__CUSTOM_WRITE_IMPL:
				return customWriteImpl != null;
			case ConfigurationPackage.CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE:
				return platformArchitectureType != PLATFORM_ARCHITECTURE_TYPE_EDEFAULT;
			case ConfigurationPackage.CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION:
				return VRTE_ADAPTIVE_STUDIO_VERSION_EDEFAULT == null ? vrteAdaptiveStudioVersion != null : !VRTE_ADAPTIVE_STUDIO_VERSION_EDEFAULT.equals(vrteAdaptiveStudioVersion);
			case ConfigurationPackage.CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS:
				return middleWareSpecificSettings != null;
			case ConfigurationPackage.CONFIG_MODEL__CODE_HOOKS:
				return codeHooks != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (vrteVersion: ");
		result.append(vrteVersion);
		result.append(", defaultTickType: ");
		result.append(defaultTickType);
		result.append(", platformArchitectureType: ");
		result.append(platformArchitectureType);
		result.append(", vrteAdaptiveStudioVersion: ");
		result.append(vrteAdaptiveStudioVersion);
		result.append(')');
		return result.toString();
	}

} //ConfigModelImpl
