/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.util;

import org.eclipse.app4mc.slg.config.*;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage
 * @generated
 */
public class ConfigurationSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ConfigurationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationSwitch() {
		if (modelPackage == null) {
			modelPackage = ConfigurationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ConfigurationPackage.CONFIG_MODEL: {
				ConfigModel configModel = (ConfigModel)theEObject;
				T result = caseConfigModel(configModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.CUSTOM_IMPL: {
				CustomImpl customImpl = (CustomImpl)theEObject;
				T result = caseCustomImpl(customImpl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.CUSTOM_TICK_IMPL: {
				CustomTickImpl customTickImpl = (CustomTickImpl)theEObject;
				T result = caseCustomTickImpl(customTickImpl);
				if (result == null) result = caseCustomImpl(customTickImpl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.CUSTOM_READ_IMPL: {
				CustomReadImpl customReadImpl = (CustomReadImpl)theEObject;
				T result = caseCustomReadImpl(customReadImpl);
				if (result == null) result = caseCustomImpl(customReadImpl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.CUSTOM_WRITE_IMPL: {
				CustomWriteImpl customWriteImpl = (CustomWriteImpl)theEObject;
				T result = caseCustomWriteImpl(customWriteImpl);
				if (result == null) result = caseCustomImpl(customWriteImpl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.MIDDLE_WARE_SETTINGS: {
				MiddleWareSettings middleWareSettings = (MiddleWareSettings)theEObject;
				T result = caseMiddleWareSettings(middleWareSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.CODE_HOOKS: {
				CodeHooks codeHooks = (CodeHooks)theEObject;
				T result = caseCodeHooks(codeHooks);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.LIB_LOCATIONS: {
				LibLocations libLocations = (LibLocations)theEObject;
				T result = caseLibLocations(libLocations);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigurationPackage.HEADER_FILES: {
				HeaderFiles headerFiles = (HeaderFiles)theEObject;
				T result = caseHeaderFiles(headerFiles);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Config Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Config Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigModel(ConfigModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Impl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Impl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomImpl(CustomImpl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Tick Impl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Tick Impl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomTickImpl(CustomTickImpl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Read Impl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Read Impl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomReadImpl(CustomReadImpl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Write Impl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Write Impl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomWriteImpl(CustomWriteImpl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Middle Ware Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Middle Ware Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMiddleWareSettings(MiddleWareSettings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Hooks</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Hooks</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeHooks(CodeHooks object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lib Locations</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lib Locations</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLibLocations(LibLocations object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Header Files</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Header Files</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHeaderFiles(HeaderFiles object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ConfigurationSwitch
