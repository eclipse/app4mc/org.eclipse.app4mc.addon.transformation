/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.impl;

import java.util.Collection;

import org.eclipse.app4mc.slg.config.CodeHooks;
import org.eclipse.app4mc.slg.config.ConfigurationPackage;
import org.eclipse.app4mc.slg.config.HeaderFiles;
import org.eclipse.app4mc.slg.config.LibLocations;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code Hooks</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.CodeHooksImpl#getLibLocations <em>Lib Locations</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.CodeHooksImpl#getHeaderFiles <em>Header Files</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CodeHooksImpl extends MinimalEObjectImpl.Container implements CodeHooks {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The cached value of the '{@link #getLibLocations() <em>Lib Locations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLibLocations()
	 * @generated
	 * @ordered
	 */
	protected LibLocations libLocations;

	/**
	 * The cached value of the '{@link #getHeaderFiles() <em>Header Files</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderFiles()
	 * @generated
	 * @ordered
	 */
	protected EList<HeaderFiles> headerFiles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeHooksImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.CODE_HOOKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LibLocations getLibLocations() {
		return libLocations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLibLocations(LibLocations newLibLocations, NotificationChain msgs) {
		LibLocations oldLibLocations = libLocations;
		libLocations = newLibLocations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS, oldLibLocations, newLibLocations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLibLocations(LibLocations newLibLocations) {
		if (newLibLocations != libLocations) {
			NotificationChain msgs = null;
			if (libLocations != null)
				msgs = ((InternalEObject)libLocations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS, null, msgs);
			if (newLibLocations != null)
				msgs = ((InternalEObject)newLibLocations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS, null, msgs);
			msgs = basicSetLibLocations(newLibLocations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS, newLibLocations, newLibLocations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<HeaderFiles> getHeaderFiles() {
		if (headerFiles == null) {
			headerFiles = new EObjectContainmentEList<HeaderFiles>(HeaderFiles.class, this, ConfigurationPackage.CODE_HOOKS__HEADER_FILES);
		}
		return headerFiles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS:
				return basicSetLibLocations(null, msgs);
			case ConfigurationPackage.CODE_HOOKS__HEADER_FILES:
				return ((InternalEList<?>)getHeaderFiles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS:
				return getLibLocations();
			case ConfigurationPackage.CODE_HOOKS__HEADER_FILES:
				return getHeaderFiles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS:
				setLibLocations((LibLocations)newValue);
				return;
			case ConfigurationPackage.CODE_HOOKS__HEADER_FILES:
				getHeaderFiles().clear();
				getHeaderFiles().addAll((Collection<? extends HeaderFiles>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS:
				setLibLocations((LibLocations)null);
				return;
			case ConfigurationPackage.CODE_HOOKS__HEADER_FILES:
				getHeaderFiles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.CODE_HOOKS__LIB_LOCATIONS:
				return libLocations != null;
			case ConfigurationPackage.CODE_HOOKS__HEADER_FILES:
				return headerFiles != null && !headerFiles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CodeHooksImpl
