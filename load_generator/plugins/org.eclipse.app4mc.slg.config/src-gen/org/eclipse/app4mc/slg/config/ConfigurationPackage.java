/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.app4mc.slg.config.ConfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface ConfigurationPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "config";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://config";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "config";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigurationPackage eINSTANCE = org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl <em>Config Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigModelImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getConfigModel()
	 * @generated
	 */
	int CONFIG_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Vrte Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__VRTE_VERSION = 0;

	/**
	 * The feature id for the '<em><b>Default Tick Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__DEFAULT_TICK_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Custom Tick Impl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__CUSTOM_TICK_IMPL = 2;

	/**
	 * The feature id for the '<em><b>Custom Read Impl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__CUSTOM_READ_IMPL = 3;

	/**
	 * The feature id for the '<em><b>Custom Write Impl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__CUSTOM_WRITE_IMPL = 4;

	/**
	 * The feature id for the '<em><b>Platform Architecture Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Vrte Adaptive Studio Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION = 6;

	/**
	 * The feature id for the '<em><b>Middle Ware Specific Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS = 7;

	/**
	 * The feature id for the '<em><b>Code Hooks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL__CODE_HOOKS = 8;

	/**
	 * The number of structural features of the '<em>Config Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Config Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIG_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.CustomImplImpl <em>Custom Impl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.CustomImplImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomImpl()
	 * @generated
	 */
	int CUSTOM_IMPL = 1;

	/**
	 * The feature id for the '<em><b>Enable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_IMPL__ENABLE = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_IMPL__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Custom Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_IMPL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Custom Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_IMPL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.CustomTickImplImpl <em>Custom Tick Impl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.CustomTickImplImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomTickImpl()
	 * @generated
	 */
	int CUSTOM_TICK_IMPL = 2;

	/**
	 * The feature id for the '<em><b>Enable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TICK_IMPL__ENABLE = CUSTOM_IMPL__ENABLE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TICK_IMPL__VALUE = CUSTOM_IMPL__VALUE;

	/**
	 * The number of structural features of the '<em>Custom Tick Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TICK_IMPL_FEATURE_COUNT = CUSTOM_IMPL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Custom Tick Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_TICK_IMPL_OPERATION_COUNT = CUSTOM_IMPL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.CustomReadImplImpl <em>Custom Read Impl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.CustomReadImplImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomReadImpl()
	 * @generated
	 */
	int CUSTOM_READ_IMPL = 3;

	/**
	 * The feature id for the '<em><b>Enable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_READ_IMPL__ENABLE = CUSTOM_IMPL__ENABLE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_READ_IMPL__VALUE = CUSTOM_IMPL__VALUE;

	/**
	 * The number of structural features of the '<em>Custom Read Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_READ_IMPL_FEATURE_COUNT = CUSTOM_IMPL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Custom Read Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_READ_IMPL_OPERATION_COUNT = CUSTOM_IMPL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.CustomWriteImplImpl <em>Custom Write Impl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.CustomWriteImplImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomWriteImpl()
	 * @generated
	 */
	int CUSTOM_WRITE_IMPL = 4;

	/**
	 * The feature id for the '<em><b>Enable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_WRITE_IMPL__ENABLE = CUSTOM_IMPL__ENABLE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_WRITE_IMPL__VALUE = CUSTOM_IMPL__VALUE;

	/**
	 * The number of structural features of the '<em>Custom Write Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_WRITE_IMPL_FEATURE_COUNT = CUSTOM_IMPL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Custom Write Impl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_WRITE_IMPL_OPERATION_COUNT = CUSTOM_IMPL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.MiddleWareSettingsImpl <em>Middle Ware Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.MiddleWareSettingsImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getMiddleWareSettings()
	 * @generated
	 */
	int MIDDLE_WARE_SETTINGS = 5;

	/**
	 * The number of structural features of the '<em>Middle Ware Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIDDLE_WARE_SETTINGS_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Middle Ware Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIDDLE_WARE_SETTINGS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.CodeHooksImpl <em>Code Hooks</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.CodeHooksImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCodeHooks()
	 * @generated
	 */
	int CODE_HOOKS = 6;

	/**
	 * The feature id for the '<em><b>Lib Locations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_HOOKS__LIB_LOCATIONS = 0;

	/**
	 * The feature id for the '<em><b>Header Files</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_HOOKS__HEADER_FILES = 1;

	/**
	 * The number of structural features of the '<em>Code Hooks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_HOOKS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Code Hooks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_HOOKS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.LibLocationsImpl <em>Lib Locations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.LibLocationsImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getLibLocations()
	 * @generated
	 */
	int LIB_LOCATIONS = 7;

	/**
	 * The feature id for the '<em><b>Linked Libraries</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIB_LOCATIONS__LINKED_LIBRARIES = 0;

	/**
	 * The feature id for the '<em><b>Package Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIB_LOCATIONS__PACKAGE_NAMES = 1;

	/**
	 * The number of structural features of the '<em>Lib Locations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIB_LOCATIONS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Lib Locations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIB_LOCATIONS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.impl.HeaderFilesImpl <em>Header Files</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.impl.HeaderFilesImpl
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getHeaderFiles()
	 * @generated
	 */
	int HEADER_FILES = 8;

	/**
	 * The feature id for the '<em><b>Codehook Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_FILES__CODEHOOK_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Header Files Directories</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_FILES__HEADER_FILES_DIRECTORIES = 1;

	/**
	 * The number of structural features of the '<em>Header Files</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_FILES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Header Files</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_FILES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.FileType <em>File Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.FileType
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getFileType()
	 * @generated
	 */
	int FILE_TYPE = 9;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.TickType <em>Tick Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.TickType
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getTickType()
	 * @generated
	 */
	int TICK_TYPE = 10;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.PlatformArchitecture <em>Platform Architecture</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.PlatformArchitecture
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getPlatformArchitecture()
	 * @generated
	 */
	int PLATFORM_ARCHITECTURE = 11;


	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.slg.config.CodehookType <em>Codehook Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.slg.config.CodehookType
	 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCodehookType()
	 * @generated
	 */
	int CODEHOOK_TYPE = 12;


	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.ConfigModel <em>Config Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Config Model</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel
	 * @generated
	 */
	EClass getConfigModel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.ConfigModel#getVrteVersion <em>Vrte Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vrte Version</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getVrteVersion()
	 * @see #getConfigModel()
	 * @generated
	 */
	EAttribute getConfigModel_VrteVersion();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.ConfigModel#getDefaultTickType <em>Default Tick Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Tick Type</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getDefaultTickType()
	 * @see #getConfigModel()
	 * @generated
	 */
	EAttribute getConfigModel_DefaultTickType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomTickImpl <em>Custom Tick Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Custom Tick Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getCustomTickImpl()
	 * @see #getConfigModel()
	 * @generated
	 */
	EReference getConfigModel_CustomTickImpl();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomReadImpl <em>Custom Read Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Custom Read Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getCustomReadImpl()
	 * @see #getConfigModel()
	 * @generated
	 */
	EReference getConfigModel_CustomReadImpl();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomWriteImpl <em>Custom Write Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Custom Write Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getCustomWriteImpl()
	 * @see #getConfigModel()
	 * @generated
	 */
	EReference getConfigModel_CustomWriteImpl();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.ConfigModel#getPlatformArchitectureType <em>Platform Architecture Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Platform Architecture Type</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getPlatformArchitectureType()
	 * @see #getConfigModel()
	 * @generated
	 */
	EAttribute getConfigModel_PlatformArchitectureType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.ConfigModel#getVrteAdaptiveStudioVersion <em>Vrte Adaptive Studio Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vrte Adaptive Studio Version</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getVrteAdaptiveStudioVersion()
	 * @see #getConfigModel()
	 * @generated
	 */
	EAttribute getConfigModel_VrteAdaptiveStudioVersion();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.app4mc.slg.config.ConfigModel#getMiddleWareSpecificSettings <em>Middle Ware Specific Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Middle Ware Specific Settings</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getMiddleWareSpecificSettings()
	 * @see #getConfigModel()
	 * @generated
	 */
	EReference getConfigModel_MiddleWareSpecificSettings();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCodeHooks <em>Code Hooks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Code Hooks</em>'.
	 * @see org.eclipse.app4mc.slg.config.ConfigModel#getCodeHooks()
	 * @see #getConfigModel()
	 * @generated
	 */
	EReference getConfigModel_CodeHooks();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.CustomImpl <em>Custom Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.CustomImpl
	 * @generated
	 */
	EClass getCustomImpl();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.CustomImpl#isEnable <em>Enable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enable</em>'.
	 * @see org.eclipse.app4mc.slg.config.CustomImpl#isEnable()
	 * @see #getCustomImpl()
	 * @generated
	 */
	EAttribute getCustomImpl_Enable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.CustomImpl#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.app4mc.slg.config.CustomImpl#getValue()
	 * @see #getCustomImpl()
	 * @generated
	 */
	EAttribute getCustomImpl_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.CustomTickImpl <em>Custom Tick Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Tick Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.CustomTickImpl
	 * @generated
	 */
	EClass getCustomTickImpl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.CustomReadImpl <em>Custom Read Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Read Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.CustomReadImpl
	 * @generated
	 */
	EClass getCustomReadImpl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.CustomWriteImpl <em>Custom Write Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Write Impl</em>'.
	 * @see org.eclipse.app4mc.slg.config.CustomWriteImpl
	 * @generated
	 */
	EClass getCustomWriteImpl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.MiddleWareSettings <em>Middle Ware Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Middle Ware Settings</em>'.
	 * @see org.eclipse.app4mc.slg.config.MiddleWareSettings
	 * @generated
	 */
	EClass getMiddleWareSettings();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.CodeHooks <em>Code Hooks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Hooks</em>'.
	 * @see org.eclipse.app4mc.slg.config.CodeHooks
	 * @generated
	 */
	EClass getCodeHooks();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.app4mc.slg.config.CodeHooks#getLibLocations <em>Lib Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lib Locations</em>'.
	 * @see org.eclipse.app4mc.slg.config.CodeHooks#getLibLocations()
	 * @see #getCodeHooks()
	 * @generated
	 */
	EReference getCodeHooks_LibLocations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.app4mc.slg.config.CodeHooks#getHeaderFiles <em>Header Files</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Header Files</em>'.
	 * @see org.eclipse.app4mc.slg.config.CodeHooks#getHeaderFiles()
	 * @see #getCodeHooks()
	 * @generated
	 */
	EReference getCodeHooks_HeaderFiles();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.LibLocations <em>Lib Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lib Locations</em>'.
	 * @see org.eclipse.app4mc.slg.config.LibLocations
	 * @generated
	 */
	EClass getLibLocations();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.app4mc.slg.config.LibLocations#getLinkedLibraries <em>Linked Libraries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Linked Libraries</em>'.
	 * @see org.eclipse.app4mc.slg.config.LibLocations#getLinkedLibraries()
	 * @see #getLibLocations()
	 * @generated
	 */
	EAttribute getLibLocations_LinkedLibraries();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.app4mc.slg.config.LibLocations#getPackageNames <em>Package Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Package Names</em>'.
	 * @see org.eclipse.app4mc.slg.config.LibLocations#getPackageNames()
	 * @see #getLibLocations()
	 * @generated
	 */
	EAttribute getLibLocations_PackageNames();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.slg.config.HeaderFiles <em>Header Files</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Header Files</em>'.
	 * @see org.eclipse.app4mc.slg.config.HeaderFiles
	 * @generated
	 */
	EClass getHeaderFiles();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.slg.config.HeaderFiles#getCodehookType <em>Codehook Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Codehook Type</em>'.
	 * @see org.eclipse.app4mc.slg.config.HeaderFiles#getCodehookType()
	 * @see #getHeaderFiles()
	 * @generated
	 */
	EAttribute getHeaderFiles_CodehookType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.app4mc.slg.config.HeaderFiles#getHeaderFilesDirectories <em>Header Files Directories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Header Files Directories</em>'.
	 * @see org.eclipse.app4mc.slg.config.HeaderFiles#getHeaderFilesDirectories()
	 * @see #getHeaderFiles()
	 * @generated
	 */
	EAttribute getHeaderFiles_HeaderFilesDirectories();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.app4mc.slg.config.FileType <em>File Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>File Type</em>'.
	 * @see org.eclipse.app4mc.slg.config.FileType
	 * @generated
	 */
	EEnum getFileType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.app4mc.slg.config.TickType <em>Tick Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Tick Type</em>'.
	 * @see org.eclipse.app4mc.slg.config.TickType
	 * @generated
	 */
	EEnum getTickType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.app4mc.slg.config.PlatformArchitecture <em>Platform Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Platform Architecture</em>'.
	 * @see org.eclipse.app4mc.slg.config.PlatformArchitecture
	 * @generated
	 */
	EEnum getPlatformArchitecture();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.app4mc.slg.config.CodehookType <em>Codehook Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Codehook Type</em>'.
	 * @see org.eclipse.app4mc.slg.config.CodehookType
	 * @generated
	 */
	EEnum getCodehookType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigurationFactory getConfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.ConfigModelImpl <em>Config Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigModelImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getConfigModel()
		 * @generated
		 */
		EClass CONFIG_MODEL = eINSTANCE.getConfigModel();

		/**
		 * The meta object literal for the '<em><b>Vrte Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIG_MODEL__VRTE_VERSION = eINSTANCE.getConfigModel_VrteVersion();

		/**
		 * The meta object literal for the '<em><b>Default Tick Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIG_MODEL__DEFAULT_TICK_TYPE = eINSTANCE.getConfigModel_DefaultTickType();

		/**
		 * The meta object literal for the '<em><b>Custom Tick Impl</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIG_MODEL__CUSTOM_TICK_IMPL = eINSTANCE.getConfigModel_CustomTickImpl();

		/**
		 * The meta object literal for the '<em><b>Custom Read Impl</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIG_MODEL__CUSTOM_READ_IMPL = eINSTANCE.getConfigModel_CustomReadImpl();

		/**
		 * The meta object literal for the '<em><b>Custom Write Impl</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIG_MODEL__CUSTOM_WRITE_IMPL = eINSTANCE.getConfigModel_CustomWriteImpl();

		/**
		 * The meta object literal for the '<em><b>Platform Architecture Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIG_MODEL__PLATFORM_ARCHITECTURE_TYPE = eINSTANCE.getConfigModel_PlatformArchitectureType();

		/**
		 * The meta object literal for the '<em><b>Vrte Adaptive Studio Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIG_MODEL__VRTE_ADAPTIVE_STUDIO_VERSION = eINSTANCE.getConfigModel_VrteAdaptiveStudioVersion();

		/**
		 * The meta object literal for the '<em><b>Middle Ware Specific Settings</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIG_MODEL__MIDDLE_WARE_SPECIFIC_SETTINGS = eINSTANCE.getConfigModel_MiddleWareSpecificSettings();

		/**
		 * The meta object literal for the '<em><b>Code Hooks</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIG_MODEL__CODE_HOOKS = eINSTANCE.getConfigModel_CodeHooks();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.CustomImplImpl <em>Custom Impl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.CustomImplImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomImpl()
		 * @generated
		 */
		EClass CUSTOM_IMPL = eINSTANCE.getCustomImpl();

		/**
		 * The meta object literal for the '<em><b>Enable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_IMPL__ENABLE = eINSTANCE.getCustomImpl_Enable();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_IMPL__VALUE = eINSTANCE.getCustomImpl_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.CustomTickImplImpl <em>Custom Tick Impl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.CustomTickImplImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomTickImpl()
		 * @generated
		 */
		EClass CUSTOM_TICK_IMPL = eINSTANCE.getCustomTickImpl();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.CustomReadImplImpl <em>Custom Read Impl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.CustomReadImplImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomReadImpl()
		 * @generated
		 */
		EClass CUSTOM_READ_IMPL = eINSTANCE.getCustomReadImpl();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.CustomWriteImplImpl <em>Custom Write Impl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.CustomWriteImplImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCustomWriteImpl()
		 * @generated
		 */
		EClass CUSTOM_WRITE_IMPL = eINSTANCE.getCustomWriteImpl();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.MiddleWareSettingsImpl <em>Middle Ware Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.MiddleWareSettingsImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getMiddleWareSettings()
		 * @generated
		 */
		EClass MIDDLE_WARE_SETTINGS = eINSTANCE.getMiddleWareSettings();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.CodeHooksImpl <em>Code Hooks</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.CodeHooksImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCodeHooks()
		 * @generated
		 */
		EClass CODE_HOOKS = eINSTANCE.getCodeHooks();

		/**
		 * The meta object literal for the '<em><b>Lib Locations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_HOOKS__LIB_LOCATIONS = eINSTANCE.getCodeHooks_LibLocations();

		/**
		 * The meta object literal for the '<em><b>Header Files</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_HOOKS__HEADER_FILES = eINSTANCE.getCodeHooks_HeaderFiles();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.LibLocationsImpl <em>Lib Locations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.LibLocationsImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getLibLocations()
		 * @generated
		 */
		EClass LIB_LOCATIONS = eINSTANCE.getLibLocations();

		/**
		 * The meta object literal for the '<em><b>Linked Libraries</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIB_LOCATIONS__LINKED_LIBRARIES = eINSTANCE.getLibLocations_LinkedLibraries();

		/**
		 * The meta object literal for the '<em><b>Package Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIB_LOCATIONS__PACKAGE_NAMES = eINSTANCE.getLibLocations_PackageNames();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.impl.HeaderFilesImpl <em>Header Files</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.impl.HeaderFilesImpl
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getHeaderFiles()
		 * @generated
		 */
		EClass HEADER_FILES = eINSTANCE.getHeaderFiles();

		/**
		 * The meta object literal for the '<em><b>Codehook Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HEADER_FILES__CODEHOOK_TYPE = eINSTANCE.getHeaderFiles_CodehookType();

		/**
		 * The meta object literal for the '<em><b>Header Files Directories</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HEADER_FILES__HEADER_FILES_DIRECTORIES = eINSTANCE.getHeaderFiles_HeaderFilesDirectories();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.FileType <em>File Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.FileType
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getFileType()
		 * @generated
		 */
		EEnum FILE_TYPE = eINSTANCE.getFileType();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.TickType <em>Tick Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.TickType
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getTickType()
		 * @generated
		 */
		EEnum TICK_TYPE = eINSTANCE.getTickType();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.PlatformArchitecture <em>Platform Architecture</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.PlatformArchitecture
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getPlatformArchitecture()
		 * @generated
		 */
		EEnum PLATFORM_ARCHITECTURE = eINSTANCE.getPlatformArchitecture();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.slg.config.CodehookType <em>Codehook Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.slg.config.CodehookType
		 * @see org.eclipse.app4mc.slg.config.impl.ConfigurationPackageImpl#getCodehookType()
		 * @generated
		 */
		EEnum CODEHOOK_TYPE = eINSTANCE.getCodehookType();

	}

} //ConfigurationPackage
