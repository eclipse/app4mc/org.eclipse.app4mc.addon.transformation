/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header Files</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.HeaderFiles#getCodehookType <em>Codehook Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.HeaderFiles#getHeaderFilesDirectories <em>Header Files Directories</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getHeaderFiles()
 * @model
 * @generated
 */
public interface HeaderFiles extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Codehook Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.app4mc.slg.config.CodehookType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Codehook Type</em>' attribute.
	 * @see org.eclipse.app4mc.slg.config.CodehookType
	 * @see #setCodehookType(CodehookType)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getHeaderFiles_CodehookType()
	 * @model
	 * @generated
	 */
	CodehookType getCodehookType();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.HeaderFiles#getCodehookType <em>Codehook Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Codehook Type</em>' attribute.
	 * @see org.eclipse.app4mc.slg.config.CodehookType
	 * @see #getCodehookType()
	 * @generated
	 */
	void setCodehookType(CodehookType value);

	/**
	 * Returns the value of the '<em><b>Header Files Directories</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Files Directories</em>' attribute list.
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getHeaderFiles_HeaderFilesDirectories()
	 * @model
	 * @generated
	 */
	EList<String> getHeaderFilesDirectories();

} // HeaderFiles
