/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Config Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getVrteVersion <em>Vrte Version</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getDefaultTickType <em>Default Tick Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomTickImpl <em>Custom Tick Impl</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomReadImpl <em>Custom Read Impl</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomWriteImpl <em>Custom Write Impl</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getPlatformArchitectureType <em>Platform Architecture Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getVrteAdaptiveStudioVersion <em>Vrte Adaptive Studio Version</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getMiddleWareSpecificSettings <em>Middle Ware Specific Settings</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.ConfigModel#getCodeHooks <em>Code Hooks</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel()
 * @model
 * @generated
 */
public interface ConfigModel extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Vrte Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vrte Version</em>' attribute.
	 * @see #setVrteVersion(String)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_VrteVersion()
	 * @model
	 * @generated
	 */
	String getVrteVersion();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getVrteVersion <em>Vrte Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vrte Version</em>' attribute.
	 * @see #getVrteVersion()
	 * @generated
	 */
	void setVrteVersion(String value);

	/**
	 * Returns the value of the '<em><b>Default Tick Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.app4mc.slg.config.TickType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Tick Type</em>' attribute.
	 * @see org.eclipse.app4mc.slg.config.TickType
	 * @see #setDefaultTickType(TickType)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_DefaultTickType()
	 * @model
	 * @generated
	 */
	TickType getDefaultTickType();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getDefaultTickType <em>Default Tick Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Tick Type</em>' attribute.
	 * @see org.eclipse.app4mc.slg.config.TickType
	 * @see #getDefaultTickType()
	 * @generated
	 */
	void setDefaultTickType(TickType value);

	/**
	 * Returns the value of the '<em><b>Custom Tick Impl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Tick Impl</em>' containment reference.
	 * @see #setCustomTickImpl(CustomTickImpl)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_CustomTickImpl()
	 * @model containment="true"
	 * @generated
	 */
	CustomTickImpl getCustomTickImpl();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomTickImpl <em>Custom Tick Impl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Custom Tick Impl</em>' containment reference.
	 * @see #getCustomTickImpl()
	 * @generated
	 */
	void setCustomTickImpl(CustomTickImpl value);

	/**
	 * Returns the value of the '<em><b>Custom Read Impl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Read Impl</em>' containment reference.
	 * @see #setCustomReadImpl(CustomReadImpl)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_CustomReadImpl()
	 * @model containment="true"
	 * @generated
	 */
	CustomReadImpl getCustomReadImpl();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomReadImpl <em>Custom Read Impl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Custom Read Impl</em>' containment reference.
	 * @see #getCustomReadImpl()
	 * @generated
	 */
	void setCustomReadImpl(CustomReadImpl value);

	/**
	 * Returns the value of the '<em><b>Custom Write Impl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Write Impl</em>' containment reference.
	 * @see #setCustomWriteImpl(CustomWriteImpl)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_CustomWriteImpl()
	 * @model containment="true"
	 * @generated
	 */
	CustomWriteImpl getCustomWriteImpl();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCustomWriteImpl <em>Custom Write Impl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Custom Write Impl</em>' containment reference.
	 * @see #getCustomWriteImpl()
	 * @generated
	 */
	void setCustomWriteImpl(CustomWriteImpl value);

	/**
	 * Returns the value of the '<em><b>Platform Architecture Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.app4mc.slg.config.PlatformArchitecture}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Platform Architecture Type</em>' attribute.
	 * @see org.eclipse.app4mc.slg.config.PlatformArchitecture
	 * @see #setPlatformArchitectureType(PlatformArchitecture)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_PlatformArchitectureType()
	 * @model
	 * @generated
	 */
	PlatformArchitecture getPlatformArchitectureType();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getPlatformArchitectureType <em>Platform Architecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Platform Architecture Type</em>' attribute.
	 * @see org.eclipse.app4mc.slg.config.PlatformArchitecture
	 * @see #getPlatformArchitectureType()
	 * @generated
	 */
	void setPlatformArchitectureType(PlatformArchitecture value);

	/**
	 * Returns the value of the '<em><b>Vrte Adaptive Studio Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vrte Adaptive Studio Version</em>' attribute.
	 * @see #setVrteAdaptiveStudioVersion(String)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_VrteAdaptiveStudioVersion()
	 * @model
	 * @generated
	 */
	String getVrteAdaptiveStudioVersion();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getVrteAdaptiveStudioVersion <em>Vrte Adaptive Studio Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vrte Adaptive Studio Version</em>' attribute.
	 * @see #getVrteAdaptiveStudioVersion()
	 * @generated
	 */
	void setVrteAdaptiveStudioVersion(String value);

	/**
	 * Returns the value of the '<em><b>Middle Ware Specific Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Middle Ware Specific Settings</em>' containment reference.
	 * @see #setMiddleWareSpecificSettings(MiddleWareSettings)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_MiddleWareSpecificSettings()
	 * @model containment="true"
	 * @generated
	 */
	MiddleWareSettings getMiddleWareSpecificSettings();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getMiddleWareSpecificSettings <em>Middle Ware Specific Settings</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Middle Ware Specific Settings</em>' containment reference.
	 * @see #getMiddleWareSpecificSettings()
	 * @generated
	 */
	void setMiddleWareSpecificSettings(MiddleWareSettings value);

	/**
	 * Returns the value of the '<em><b>Code Hooks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Hooks</em>' containment reference.
	 * @see #setCodeHooks(CodeHooks)
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getConfigModel_CodeHooks()
	 * @model containment="true"
	 * @generated
	 */
	CodeHooks getCodeHooks();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.slg.config.ConfigModel#getCodeHooks <em>Code Hooks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code Hooks</em>' containment reference.
	 * @see #getCodeHooks()
	 * @generated
	 */
	void setCodeHooks(CodeHooks value);

} // ConfigModel
