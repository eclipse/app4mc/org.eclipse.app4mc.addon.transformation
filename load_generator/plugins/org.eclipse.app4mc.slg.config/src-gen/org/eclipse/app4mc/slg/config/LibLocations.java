/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lib Locations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.LibLocations#getLinkedLibraries <em>Linked Libraries</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.LibLocations#getPackageNames <em>Package Names</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getLibLocations()
 * @model
 * @generated
 */
public interface LibLocations extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Linked Libraries</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linked Libraries</em>' attribute list.
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getLibLocations_LinkedLibraries()
	 * @model
	 * @generated
	 */
	EList<String> getLinkedLibraries();

	/**
	 * Returns the value of the '<em><b>Package Names</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Names</em>' attribute list.
	 * @see org.eclipse.app4mc.slg.config.ConfigurationPackage#getLibLocations_PackageNames()
	 * @model
	 * @generated
	 */
	EList<String> getPackageNames();

} // LibLocations
