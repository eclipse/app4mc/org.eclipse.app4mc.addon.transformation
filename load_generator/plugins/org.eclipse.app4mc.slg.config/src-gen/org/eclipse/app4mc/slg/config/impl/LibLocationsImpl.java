/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.slg.config.impl;

import java.util.Collection;

import org.eclipse.app4mc.slg.config.ConfigurationPackage;
import org.eclipse.app4mc.slg.config.LibLocations;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lib Locations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.LibLocationsImpl#getLinkedLibraries <em>Linked Libraries</em>}</li>
 *   <li>{@link org.eclipse.app4mc.slg.config.impl.LibLocationsImpl#getPackageNames <em>Package Names</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LibLocationsImpl extends MinimalEObjectImpl.Container implements LibLocations {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The cached value of the '{@link #getLinkedLibraries() <em>Linked Libraries</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkedLibraries()
	 * @generated
	 * @ordered
	 */
	protected EList<String> linkedLibraries;

	/**
	 * The cached value of the '{@link #getPackageNames() <em>Package Names</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageNames()
	 * @generated
	 * @ordered
	 */
	protected EList<String> packageNames;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LibLocationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.LIB_LOCATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getLinkedLibraries() {
		if (linkedLibraries == null) {
			linkedLibraries = new EDataTypeUniqueEList<String>(String.class, this, ConfigurationPackage.LIB_LOCATIONS__LINKED_LIBRARIES);
		}
		return linkedLibraries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getPackageNames() {
		if (packageNames == null) {
			packageNames = new EDataTypeUniqueEList<String>(String.class, this, ConfigurationPackage.LIB_LOCATIONS__PACKAGE_NAMES);
		}
		return packageNames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.LIB_LOCATIONS__LINKED_LIBRARIES:
				return getLinkedLibraries();
			case ConfigurationPackage.LIB_LOCATIONS__PACKAGE_NAMES:
				return getPackageNames();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.LIB_LOCATIONS__LINKED_LIBRARIES:
				getLinkedLibraries().clear();
				getLinkedLibraries().addAll((Collection<? extends String>)newValue);
				return;
			case ConfigurationPackage.LIB_LOCATIONS__PACKAGE_NAMES:
				getPackageNames().clear();
				getPackageNames().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.LIB_LOCATIONS__LINKED_LIBRARIES:
				getLinkedLibraries().clear();
				return;
			case ConfigurationPackage.LIB_LOCATIONS__PACKAGE_NAMES:
				getPackageNames().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.LIB_LOCATIONS__LINKED_LIBRARIES:
				return linkedLibraries != null && !linkedLibraries.isEmpty();
			case ConfigurationPackage.LIB_LOCATIONS__PACKAGE_NAMES:
				return packageNames != null && !packageNames.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (linkedLibraries: ");
		result.append(linkedLibraries);
		result.append(", packageNames: ");
		result.append(packageNames);
		result.append(')');
		return result.toString();
	}

} //LibLocationsImpl
