/**
 * *******************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.slg.config.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.slg.config.CodeHooks;
import org.eclipse.app4mc.slg.config.CodehookType;
import org.eclipse.app4mc.slg.config.ConfigModel;
import org.eclipse.app4mc.slg.config.HeaderFiles;
import org.eclipse.app4mc.slg.config.LibLocations;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.emf.common.util.EList;

public class ConfigModelUtils {

	// Suppress default constructor
	private ConfigModelUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static List<String> getHeaderFilesDirectories(ConfigModel configModel, CodehookType codehookType,String workingDir,SessionLogger logger) {

		List<String> ls = new ArrayList<>();

		CodeHooks codeHooks = configModel.getCodeHooks();
		if (codeHooks != null) {
			for (HeaderFiles headerFile : codeHooks.getHeaderFiles()) {
				if (headerFile.getCodehookType().getName().equals(codehookType.getName())) {
					for (String hfileDir : headerFile.getHeaderFilesDirectories()) {
						
						File file=new File(hfileDir);
						
						if(file.exists()) {
							ls.add(hfileDir);
						}else {
							String absolutePath=workingDir+File.separator+hfileDir;
							if(new File(absolutePath).exists()) {
								ls.add(absolutePath);
							}
						}
					}

				}
			}

		}

		return ls;
	}

	public static String getHeaderFilesIncludeMultiString(File folder, SessionLogger logger) {
		if (folder == null) return null;

		StringBuilder sb = new StringBuilder();

		if (folder.exists() && folder.isDirectory()) {

			for (File fileEntry : folder.listFiles()) {
				if (!fileEntry.isDirectory() && fileEntry.getName().endsWith(".h")) {
					logger.info("Header file included from the external dirctory : "+ fileEntry.getName());
					sb.append("#include \"");
					sb.append(fileEntry.getName());
					sb.append('\"');
					sb.append(System.getProperty("line.separator"));
				}
			}

		} else {
			logger.error("Input header folder doesnot exist/it is not a directory : " + folder.getPath());
		}

		return sb.toString();
	}
	

	public static List<String> getLinkedLibraryInstructions(ConfigModel configModel ,String workingDir ) {
		List<String> ls = new ArrayList<>();

		CodeHooks codeHooks = configModel.getCodeHooks();

		if(codeHooks!=null) {
			LibLocations libLocations = codeHooks.getLibLocations();

			if (libLocations != null) {
				EList<String> linkedLibraries = libLocations.getLinkedLibraries();

				for (String linkedLibrary : linkedLibraries) {

					File file = new File(linkedLibrary);

					boolean added=false;
					if (!file.exists()) {
						String absolutePath = workingDir + File.separator + linkedLibrary;
						if (new File(absolutePath).exists()) {
							ls.add(absolutePath);
							added=true;
						}
					}

					if(!added) {
						ls.add(linkedLibrary);
					}
				}
			}
		}
		return ls;	
	}
	
	
	public static List<String> getPackageNames(ConfigModel configModel ,String workingDir ) {
		List<String> ls = new ArrayList<>();

		CodeHooks codeHooks = configModel.getCodeHooks();

		if(codeHooks!=null) {
			LibLocations libLocations = codeHooks.getLibLocations();

			if (libLocations != null) {
				EList<String> packageNames = libLocations.getPackageNames();

				for (String packageName : packageNames) {

					File file = new File(packageName);

					boolean added=false;
					if (!file.exists()) {
						String absolutePath = workingDir + File.separator + packageName;
						if (new File(absolutePath).exists()) {
							ls.add(absolutePath);
							added=true;
						}
					}

					if(!added) {
						ls.add(packageName);
					}
				}
			}
		}
		return ls;	
	}




		
}
