# commons

This repository includes the model transformation framework, the configuration model and the generic Transformer classes which are required for Synthetic Load Generation.

To build SLG tools, following versions of Maven and Java are used:

Maven : 3.6.3

Java JDK : amazon-corretto-11.0.8.10.1-windows-x64-jdk\jdk11.0.8_10

As an example of JAVA_HOME and path variable set before running the maven build: 

```
set JAVA_HOME=D:\01_Main\01_Main\18_DevUtilities\60_java_jdk\amazon\amazon-corretto-11.0.8.10.1-windows-x64-jdk\jdk11.0.8_10\

set path="D:\mvn_3.6.3\bin";D:\01_Main\01_Main\18_DevUtilities\60_java_jdk\amazon\amazon-corretto-11.0.8.10.1-windows-x64-jdk\jdk11.0.8_10\bin
```

For running maven build, following command is used:

```
mvn clean verify
```