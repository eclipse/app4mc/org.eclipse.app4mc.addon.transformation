
APP4MC AMALTHEA Headless Synthetic Load Generator (SLG) for ROS2 and MicroROS

Launch via executable jar

  java -jar ros2_slg.jar ...

Transformation configured via Properties file:
[-p, --properties <file>] [-w, --workingDirectory <dir>]

Transformation configured via parameter:
[-o, --output <dir>] [-m, --m2m <keys>] [-t, --m2t <keys>] [-A<key-value>] <directory>

Options:
        -p, --properties        The Properties file that should be used to configure the transformation.
                        If the properties option is not set, the input needs to be provided as parameter.

        -w, --workingDirectory  The working directory. Used to resolve file parameters in the Properties file in a relative manner. [optional].

        -o, --output    The directory in which the output should be generated. Only interpreted if --properties is not set. [optional].
                        If not provided the output directory defaults to <input_directory>/result.

        -m, --m2m       Comma separated list of m2m transformation keys [optional].
                        If not provided and no m2t transformation keys are set, all available m2m transformations will be executed.

        -t, --m2t       Comma separated list of m2t transformation keys [optional].
                        If not provided and no m2m transformation keys are set, all available m2t transformations will be executed.

        -A              Option to provide additional transformation specific properties. [optional].
                        Need to be provided in the key=value format.

        -h, --help, /?  Show this help.

Parameter:
        directory       The directory that contains model files to transform

Available M2M transformations:

Available M2T transformations:
 - ROS_SLG


Example:

Execute the ROS SLG with the following command
----------------------------------------------
java -jar ros2_slg.jar -p input.properties


Folder structure
----------------
├── input
│   ├── model.amxmi
│   └── ros.config
├── output
│   └── ...
├── input.properties
└── ros2_slg.jar


File "ros.config"
-----------------
<?xml version="1.0" encoding="UTF-8"?>
<config:ConfigModel xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:config="http://config"

  platformArchitectureType="aarch64">

  <customTickImpl value=""/>
  <customReadImpl value=""/>
  <customWriteImpl value=""/>

</config:ConfigModel>


File "input.properties"
-----------------------
input_models_folder=./input
configurationFile=./input/ros.config

output_folder=./output
log_file=./output/transformation.txt
m2m_output_folder=./output/m2m_output_models
m2t_output_folder=./output/m2t_output_text_files

enableExternalCode=true
experimentalCodeSnippetMatching=false
enableInstrumentation_Tasks=false
enableInstrumentation_Runnables=false

generateStatusOutput=true
generateDebugOutput=true

m2tTransformers=ROS_SLG

