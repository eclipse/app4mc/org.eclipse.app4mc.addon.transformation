#                                                                                    _                       
#    _    ._ _|_ |_   _ _|_ o  _  |  _   _.  _|   _   _  ._   _  ._ _. _|_  _  ._  _|_ _  ._  | o ._         
#   _> \/ | | |_ | | (/_ |_ | (_  | (_) (_| (_|  (_| (/_ | | (/_ | (_|  |_ (_) |    | (_) |   | | | | |_| >< 
#      /                                          _|                                                         

Based on the operating system (win64/linux/mac) of your machine, download the appropriate zip file and also download sample test data (testdata folder contains configuration file and a sample AMALTHEA model '2.1.0 version' from WATERS challenge 2016 )

→ For executing Linux Synthetic Load Generator tool on windows. Follow the below sample steps:

1) Unzip linux.slg.product-win32.win32.x86_64.zip file into a folder (e.g: c:\linuxslg can be considered as working directory)
2) Download contents of 'testdata' folder into working directory (e.g: 'c:\linuxslg' containing the extracted contents of linux slg)
3) Open command prompt in folder working directory (e.g: 'c:\linuxslg') and execute following command
			eclipsec.exe --properties ./input.properties
4) After execution of the SLG tool, files are generated inside 'output' folder in the execution directory.

#    __                                                  ___     _                             
#   (_      ._  ._   _  ._ _|_  _   _|   /\  |\/|  /\  |  | |_| |_  /\       _  ._ _ o  _  ._  
#   __) |_| |_) |_) (_) |   |_ (/_ (_|  /--\ |  | /--\ |_ | | | |_ /--\  \/ (/_ | _> | (_) | | 
#           |   |                                                                              

This version supports only AMALTHEA version 2.1.0. In case you are having older version of AMALTHEA model, do convert it to 2.1.0 version using "model migration" feature part of APP4MC tool.

#               _                                                        
#    _  _  ._ _|_ o  _      ._ _. _|_ o  _  ._    _  ._ _|_ o  _  ._   _ 
#   (_ (_) | | |  | (_| |_| | (_|  |_ | (_) | |  (_) |_) |_ | (_) | | _> 
#                    _|                              |                   

Below are the configurable switches which are specified in the input.properties file

- input_models_folder : This is the location containing the AMALTHEA model file

- configurationFile : This is the configuration file required for code generation. In future there were be additional elements which will be introduced as a part of this file.

- output_folder : This is the location where 'SLG ouput' will be generated.

- log_file : This is the path of tool log file

- experimentalCodeSnippetMatching : This switch supports options "true/false".
 		- by specifying "true" for this switch, experimental code snippet matching will be enabled and this will consider internal algorithms for burning time, and in addition it will generate "atomic blocks"
 		- by specifying "false" for this switch, label accesses and ticks will be considered as specified in the AMALTHEA model for Runnable elements and accordingly code is generated.
  
- enableInstrumentation : This switch supports options "true/false".
		- by specifying "true" for this switch, instrumentation code is injected into the generated code
		

