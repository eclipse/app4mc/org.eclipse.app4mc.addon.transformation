
# Eclipse APP4MC - Transformation

This is the repository of the Eclipse APP4MC transformation components:

    framework                       | Model transformation framework
    load_generator                  | Synthetic load generators for Linus and ROS2
    simulation_generator/app4mc.sim | Generation of SystemC simulation code

## Build

The project uses Maven/Tycho to build.

The build creates a P2 update site and an executable jar.  

From the root folder of the components:

```
$ mvn clean verify
```
You need Maven 3.6.3 or higher to perform the POM-less Tycho build.

## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/

## Further Info

[APP4MC Transformation - Wiki](https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.addon.transformation/-/wikis/home)

[APP4MC Online Help - Model Transformation](https://eclipse.dev/app4mc/help/latest/index.html#section5.4)
