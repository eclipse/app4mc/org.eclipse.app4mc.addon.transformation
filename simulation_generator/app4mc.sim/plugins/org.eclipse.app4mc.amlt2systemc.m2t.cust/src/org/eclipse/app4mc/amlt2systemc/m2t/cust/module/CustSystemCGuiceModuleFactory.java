/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.cust.module;

import java.util.Properties;

import org.eclipse.app4mc.amlt2systemc.m2t.SystemCGuiceModule;
import org.eclipse.app4mc.amlt2systemc.m2t.SystemCGuiceModuleFactory;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.osgi.service.component.annotations.Component;

@Component(
	property = { "service.ranking:Integer=10" }, 
	service= SystemCGuiceModuleFactory.class
)
public class CustSystemCGuiceModuleFactory extends SystemCGuiceModuleFactory {

	@Override
	public SystemCGuiceModule getModule(SessionLogger logger, Properties properties) {
		return new CustSystemCGuiceModule(logger,properties);
	}

}
