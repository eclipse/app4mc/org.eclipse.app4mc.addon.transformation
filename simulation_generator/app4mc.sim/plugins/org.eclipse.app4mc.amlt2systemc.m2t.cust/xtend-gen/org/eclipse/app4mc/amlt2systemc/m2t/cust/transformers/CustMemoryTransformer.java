/**
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.cust.transformers;

import com.google.inject.Singleton;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.MemoryTransformer;

@Singleton
@SuppressWarnings("all")
public class CustMemoryTransformer extends MemoryTransformer {
}
