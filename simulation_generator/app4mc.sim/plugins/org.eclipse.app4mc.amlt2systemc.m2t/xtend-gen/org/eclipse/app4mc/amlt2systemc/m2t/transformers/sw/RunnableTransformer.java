/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class RunnableTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private ConditionTransformer conditionTransformer;

  @Inject
  private ActivityGraphTransformer activityGraphTransformer;

  public static String getName(final org.eclipse.app4mc.amalthea.model.Runnable runnable) {
    return runnable.getName();
  }

  public static String getModuleName(final org.eclipse.app4mc.amalthea.model.Runnable runnable) {
    String _modulePath = SWModelTransformer.getModulePath();
    String _plus = (_modulePath + "/runnables/");
    String _name = RunnableTransformer.getName(runnable);
    return (_plus + _name);
  }

  public static String getFunctionDef(final org.eclipse.app4mc.amalthea.model.Runnable runnable) {
    String _name = RunnableTransformer.getName(runnable);
    String _plus = ("get_" + _name);
    return (_plus + "()");
  }

  public TranslationUnit transform(final org.eclipse.app4mc.amalthea.model.Runnable runnable) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(runnable);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = RunnableTransformer.getModuleName(runnable);
      String _functionDef = RunnableTransformer.getFunctionDef(runnable);
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, runnable);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final org.eclipse.app4mc.amalthea.model.Runnable runnable) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(runnable));
    String _name = RunnableTransformer.getName(runnable);
    String _moduleName = RunnableTransformer.getModuleName(runnable);
    final AgiContainerBuffer runnableContents = new AgiContainerBuffer(_name, _moduleName, true);
    this.activityGraphTransformer.transform(runnable.getActivityGraph(), runnableContents);
    final ConditionTransformer.ConditionBuffer conditionContent = this.conditionTransformer.transformCondition(runnable.getExecutionCondition(), RunnableTransformer.getName(runnable), true);
    runnableContents.addAll(conditionContent);
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(runnable, runnableContents));
  }

  private String toH(final org.eclipse.app4mc.amalthea.model.Runnable runnable) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"SoftwareModel.h\"");
    _builder.newLine();
    _builder.append("std::shared_ptr<Runnable> ");
    String _functionDef = RunnableTransformer.getFunctionDef(runnable);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final org.eclipse.app4mc.amalthea.model.Runnable runnable, final AgiContainerBuffer graphContent) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.append("#include \"");
    String _moduleName = RunnableTransformer.getModuleName(runnable);
    _builder.append(_moduleName);
    _builder.append(".h\"\t");
    _builder.newLineIfNotEmpty();
    {
      LinkedHashSet<String> _includes = graphContent.getIncludes();
      for(final String include : _includes) {
        _builder.append(include);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("std::shared_ptr<Runnable> ");
    String _name = RunnableTransformer.getName(runnable);
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<Runnable>  ");
    String _functionDef = RunnableTransformer.getFunctionDef(runnable);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = RunnableTransformer.getName(runnable);
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    _builder.append("\t\t");
    String _name_2 = RunnableTransformer.getName(runnable);
    _builder.append(_name_2, "\t\t");
    _builder.append(" = std::make_shared<Runnable>(\"");
    String _name_3 = RunnableTransformer.getName(runnable);
    _builder.append(_name_3, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    {
      LinkedList<String> _source = graphContent.getSource();
      for(final String source : _source) {
        _builder.append("\t\t");
        _builder.append(source, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_4 = RunnableTransformer.getName(runnable);
    _builder.append(_name_4, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
