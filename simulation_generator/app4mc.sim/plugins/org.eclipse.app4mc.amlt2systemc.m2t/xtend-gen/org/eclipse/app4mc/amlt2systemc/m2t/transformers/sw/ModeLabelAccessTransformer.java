/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.Mode;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.NumericMode;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.xtend2.lib.StringConcatenation;

@Singleton
@SuppressWarnings("all")
public class ModeLabelAccessTransformer extends BaseTransformer {
  @Inject
  private ModeLabelTransformer labelTransformer;

  @Inject
  private ModeTransformer modeTransformer;

  public void transform(final ModeLabelAccess _access, final AgiContainerBuffer content) {
    final TranslationUnit tuLabel = this.labelTransformer.transform(_access.getData());
    content.addInclude(tuLabel.getModule());
    ModeLabelAccessEnum _access_1 = _access.getAccess();
    boolean _tripleEquals = (_access_1 == ModeLabelAccessEnum.READ);
    if (_tripleEquals) {
      content.addAsActivityGraphItem(this.transformRead(tuLabel.getCall()));
    } else {
      Mode _mode = _access.getData().getMode();
      if ((_mode instanceof EnumMode)) {
        ModeLabelAccessEnum _access_2 = _access.getAccess();
        boolean _tripleEquals_1 = (_access_2 == ModeLabelAccessEnum.SET);
        if (_tripleEquals_1) {
          final TranslationUnit tuMode = this.modeTransformer.transform(_access.getData().getMode());
          content.addInclude(tuMode.getModule());
          content.addAsActivityGraphItem(this.transformSetEnum(tuLabel.getCall(), tuMode.getCall(), _access.getValue()));
        }
      } else {
        Mode _mode_1 = _access.getData().getMode();
        if ((_mode_1 instanceof NumericMode)) {
          ModeLabelAccessEnum _access_3 = _access.getAccess();
          boolean _tripleEquals_2 = (_access_3 == ModeLabelAccessEnum.SET);
          if (_tripleEquals_2) {
            content.addAsActivityGraphItem(this.transformSetNumeric(tuLabel.getCall(), _access.getValue()));
          } else {
            ModeLabelAccessEnum _access_4 = _access.getAccess();
            boolean _tripleEquals_3 = (_access_4 == ModeLabelAccessEnum.INCREMENT);
            if (_tripleEquals_3) {
              content.addAsActivityGraphItem(this.transformIncrement(tuLabel.getCall(), _access.getStep()));
            } else {
              ModeLabelAccessEnum _access_5 = _access.getAccess();
              boolean _tripleEquals_4 = (_access_5 == ModeLabelAccessEnum.DECREMENT);
              if (_tripleEquals_4) {
                content.addAsActivityGraphItem(this.transformDecrement(tuLabel.getCall(), _access.getStep()));
              }
            }
          }
        } else {
          String _name = _access.getData().getName();
          String _plus = ("ModeLabelAccess of " + _name);
          String _plus_1 = (_plus + " has specified neither read nor write");
          throw new RuntimeException(_plus_1);
        }
      }
    }
  }

  private String transformSetEnum(final String call, final String mode, final String value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ModeLabelAccess>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(mode);
    _builder.append("->getLiteral(\"");
    _builder.append(value);
    _builder.append("\"), ModeAccessType::SET})");
    return _builder.toString();
  }

  private String transformSetNumeric(final String call, final String value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ModeLabelAccess>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(value);
    _builder.append(", ModeAccessType::SET})");
    return _builder.toString();
  }

  private String transformIncrement(final String call, final int value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ModeLabelAccess>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(value);
    _builder.append(", ModeAccessType::INCREMENT})");
    return _builder.toString();
  }

  private String transformDecrement(final String call, final int value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ModeLabelAccess>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(value);
    _builder.append(", ModeAccessType::DECREMENT})");
    return _builder.toString();
  }

  private String transformRead(final String call) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ModeLabelAccess>({");
    _builder.append(call);
    _builder.append(", ModeAccessType::READ})");
    return _builder.toString();
  }
}
