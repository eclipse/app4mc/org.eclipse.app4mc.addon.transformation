/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.LabelAccessStatistic;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.xtend2.lib.StringConcatenation;

@Singleton
@SuppressWarnings("all")
public class LabelAccessTransformer extends BaseTransformer {
  @Inject
  private LabelTransformer labelTransformer;

  @Inject
  private NumericStatisticTransformer numericStatisticTransformer;

  public void transform(final LabelAccess _access, final AgiContainerBuffer content) {
    final TranslationUnit tuLabel = this.labelTransformer.transform(_access.getData());
    content.addInclude(tuLabel.getModule());
    String statistics = "1";
    LabelAccessStatistic _statistic = _access.getStatistic();
    boolean _tripleNotEquals = (_statistic != null);
    if (_tripleNotEquals) {
      statistics = this.numericStatisticTransformer.transform(_access.getStatistic());
    }
    LabelAccessEnum _access_1 = _access.getAccess();
    boolean _tripleEquals = (_access_1 == LabelAccessEnum.WRITE);
    if (_tripleEquals) {
      content.addAsActivityGraphItem(this.transformWrite(tuLabel.getCall(), statistics));
    } else {
      LabelAccessEnum _access_2 = _access.getAccess();
      boolean _tripleEquals_1 = (_access_2 == LabelAccessEnum.READ);
      if (_tripleEquals_1) {
        content.addAsActivityGraphItem(this.transformRead(tuLabel.getCall(), statistics));
      } else {
        String _name = _access.getData().getName();
        String _plus = ("//Access of " + _name);
        String _plus_1 = (_plus + " has specified neither read nor write");
        content.addBody(_plus_1);
      }
    }
  }

  private String transformWrite(final String call, final String statistics) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<LabelAccess>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(statistics);
    _builder.append(", tlm::TLM_WRITE_COMMAND})");
    return _builder.toString();
  }

  private String transformRead(final String call, final String statistics) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<LabelAccess>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(statistics);
    _builder.append(", tlm::TLM_READ_COMMAND})");
    return _builder.toString();
  }
}
