/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amalthea.model.WhileLoop;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;

@SuppressWarnings("all")
public class WhileLoopTransformer extends IActivityGraphItemContainerTransformer {
  @Inject
  private ConditionTransformer conditionTransformer;

  private static int cnt = 0;

  public static String getName() {
    return ("whileLoop_" + Integer.valueOf(WhileLoopTransformer.cnt));
  }

  @Override
  protected String getContainerDescription(final IActivityGraphItemContainer container) {
    return "WhileLoop";
  }

  @Override
  protected void transformContainer(final IActivityGraphItemContainer container, final AgiContainerBuffer parentContent) {
    final String name = WhileLoopTransformer.getName();
    WhileLoopTransformer.cnt++;
    parentContent.addBody((("WhileLoop " + name) + ";"));
    final AgiContainerBuffer content = new AgiContainerBuffer(name, parentContent.module, false);
    final ConditionTransformer.ConditionBuffer conditionContent = this.conditionTransformer.transformCondition(((WhileLoop) container).getCondition(), name, false);
    final AgiContainerBuffer whileLoopContents = this.transformItems(((WhileLoop) container), name, content.module, content.parentIsPointer);
    whileLoopContents.addAll(conditionContent);
    parentContent.getIncludes().addAll(whileLoopContents.getIncludes());
    parentContent.getSource().addAll(whileLoopContents.getSource());
    parentContent.addAsActivityGraphItem((("<WhileLoop>(" + name) + ")"));
  }
}
