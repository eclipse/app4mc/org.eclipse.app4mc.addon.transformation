/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.SingleStimulus;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;

@Singleton
@SuppressWarnings("all")
public class StimulusTransformer extends BaseTransformer {
  @Inject
  private PeriodicStimulusTransformer periodicStimulusTransformer;

  @Inject
  private SingleStimulusTransformer singleStimulusTransformer;

  @Inject
  private RelativePeriodicStimulusTransformer relativePeriodicStimulusTransformer;

  @Inject
  private InterProcessStimulusTransformer interProcessStimulusTransformer;

  @Inject
  private EventStimulusTransformer eventStimulusTransformer;

  protected TranslationUnit _transform(final PeriodicStimulus stimulus) {
    return this.periodicStimulusTransformer.transform(stimulus);
  }

  protected TranslationUnit _transform(final SingleStimulus stimulus) {
    return this.singleStimulusTransformer.transform(stimulus);
  }

  protected TranslationUnit _transform(final RelativePeriodicStimulus stimulus) {
    return this.relativePeriodicStimulusTransformer.transform(stimulus);
  }

  protected TranslationUnit _transform(final InterProcessStimulus stimulus) {
    return this.interProcessStimulusTransformer.transform(stimulus);
  }

  protected TranslationUnit _transform(final Stimulus stimulus) {
    String _name = stimulus.getName();
    String _plus = ("//unsupported stimulus " + _name);
    String _name_1 = stimulus.getName();
    String _plus_1 = ("//unsupported stimulus " + _name_1);
    return new TranslationUnit(_plus, _plus_1);
  }

  protected TranslationUnit _transform(final EventStimulus stimulus) {
    return this.eventStimulusTransformer.transform(stimulus);
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache(final Class<? extends Stimulus> type) {
    boolean _matched = false;
    if (Objects.equal(type, SingleStimulus.class)) {
      _matched=true;
      return this.singleStimulusTransformer.getCache();
    }
    if (!_matched) {
      if (Objects.equal(type, PeriodicStimulus.class)) {
        _matched=true;
        return this.periodicStimulusTransformer.getCache();
      }
    }
    if (!_matched) {
      if (Objects.equal(type, InterProcessStimulus.class)) {
        _matched=true;
        return this.interProcessStimulusTransformer.getCache();
      }
    }
    if (!_matched) {
      if (Objects.equal(type, EventStimulus.class)) {
        _matched=true;
        return this.eventStimulusTransformer.getCache();
      }
    }
    if (!_matched) {
      if (Objects.equal(type, RelativePeriodicStimulus.class)) {
        _matched=true;
        return this.relativePeriodicStimulusTransformer.getCache();
      }
    }
    return null;
  }

  public TranslationUnit transform(final Stimulus stimulus) {
    if (stimulus instanceof EventStimulus) {
      return _transform((EventStimulus)stimulus);
    } else if (stimulus instanceof InterProcessStimulus) {
      return _transform((InterProcessStimulus)stimulus);
    } else if (stimulus instanceof PeriodicStimulus) {
      return _transform((PeriodicStimulus)stimulus);
    } else if (stimulus instanceof RelativePeriodicStimulus) {
      return _transform((RelativePeriodicStimulus)stimulus);
    } else if (stimulus instanceof SingleStimulus) {
      return _transform((SingleStimulus)stimulus);
    } else if (stimulus != null) {
      return _transform(stimulus);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(stimulus).toString());
    }
  }
}
