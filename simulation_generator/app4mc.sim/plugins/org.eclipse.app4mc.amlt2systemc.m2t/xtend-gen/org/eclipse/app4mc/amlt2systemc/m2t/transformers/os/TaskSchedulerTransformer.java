/**
 * Copyright (c) 2019-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.SchedulingParameterTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class TaskSchedulerTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private SchedulingParameterTransformer schedulingParameterTransformer;

  public String getName(final TaskScheduler taskScheduler) {
    String _name = taskScheduler.getName();
    String _plus = (_name + "_");
    String _call = TaskSchedulerTransformer.chooseSchedulerBasedOnSchedulerDefinition(taskScheduler.getDefinition()).getCall();
    return (_plus + _call);
  }

  private String getModulePath(final TaskScheduler taskScheduler) {
    String _modulePath = OsModelTransformer.getModulePath();
    String _plus = (_modulePath + "/tasks/");
    String _name = this.getName(taskScheduler);
    return (_plus + _name);
  }

  private String getCall(final TaskScheduler taskScheduler) {
    String _name = this.getName(taskScheduler);
    return ("get_" + _name);
  }

  public TranslationUnit transform(final TaskScheduler taskScheduler) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(taskScheduler);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(taskScheduler);
      String _call = this.getCall(taskScheduler);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, taskScheduler);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final TaskScheduler taskScheduler) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(taskScheduler));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(taskScheduler));
  }

  private String toH(final TaskScheduler taskScheduler) {
    StringConcatenation _builder = new StringConcatenation();
    final TranslationUnit scheduler = TaskSchedulerTransformer.chooseSchedulerBasedOnSchedulerDefinition(taskScheduler.getDefinition());
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"SoftwareModel.h\"");
    _builder.newLine();
    _builder.append("#include \"");
    String _module = scheduler.getModule();
    _builder.append(_module);
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("//Task runnableA----");
    _builder.newLine();
    _builder.append("std::shared_ptr<");
    String _call = scheduler.getCall();
    _builder.append(_call);
    _builder.append("> ");
    String _call_1 = this.getCall(taskScheduler);
    _builder.append(_call_1);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String transformSchedulerAssociation(final TaskScheduler taskScheduler) {
    StringConcatenation _builder = new StringConcatenation();
    {
      SchedulerAssociation _parentAssociation = taskScheduler.getParentAssociation();
      EMap<SchedulingParameterDefinition, Value> _schedulingParameters = null;
      if (_parentAssociation!=null) {
        _schedulingParameters=_parentAssociation.getSchedulingParameters();
      }
      boolean _tripleNotEquals = (_schedulingParameters != null);
      if (_tripleNotEquals) {
        _builder.append("TaskAllocation ta;");
        _builder.newLine();
        {
          Set<Map.Entry<SchedulingParameterDefinition, Value>> _entrySet = taskScheduler.getParentAssociation().getSchedulingParameters().entrySet();
          for(final Map.Entry<SchedulingParameterDefinition, Value> paramEntry : _entrySet) {
            String _createSchedParam = this.schedulingParameterTransformer.createSchedParam("ta", "setSchedulingParameter", false, paramEntry.getKey(), paramEntry.getValue());
            _builder.append(_createSchedParam);
            _builder.newLineIfNotEmpty();
          }
        }
        String _name = taskScheduler.getName();
        _builder.append(_name);
        _builder.append("->setTaskAllocation(ta);");
        _builder.newLineIfNotEmpty();
      }
    }
    String _call = this.getCall(taskScheduler.getParentScheduler());
    _builder.append(_call);
    _builder.append("()->addTaskMapping(");
    String _name_1 = taskScheduler.getName();
    _builder.append(_name_1);
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final TaskScheduler taskScheduler) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(taskScheduler);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    final TranslationUnit scheduler = TaskSchedulerTransformer.chooseSchedulerBasedOnSchedulerDefinition(taskScheduler.getDefinition());
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<");
    String _call = scheduler.getCall();
    _builder.append(_call);
    _builder.append("> ");
    String _name = taskScheduler.getName();
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<");
    String _call_1 = scheduler.getCall();
    _builder.append(_call_1);
    _builder.append("> ");
    String _call_2 = this.getCall(taskScheduler);
    _builder.append(_call_2);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = taskScheduler.getName();
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    _builder.append("\t\t");
    String _name_2 = taskScheduler.getName();
    _builder.append(_name_2, "\t\t");
    _builder.append(" = std::make_shared<");
    String _call_3 = scheduler.getCall();
    _builder.append(_call_3, "\t\t");
    _builder.append(">(\"");
    String _name_3 = taskScheduler.getName();
    _builder.append(_name_3, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    {
      SchedulerAssociation _parentAssociation = taskScheduler.getParentAssociation();
      boolean _tripleNotEquals = (_parentAssociation != null);
      if (_tripleNotEquals) {
        _builder.append("\t\t");
        String _transformSchedulerAssociation = this.transformSchedulerAssociation(taskScheduler);
        _builder.append(_transformSchedulerAssociation, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EMap<SchedulingParameterDefinition, Value> _schedulingParameters = null;
      if (taskScheduler!=null) {
        _schedulingParameters=taskScheduler.getSchedulingParameters();
      }
      boolean _tripleNotEquals_1 = (_schedulingParameters != null);
      if (_tripleNotEquals_1) {
        {
          Set<Map.Entry<SchedulingParameterDefinition, Value>> _entrySet = taskScheduler.getSchedulingParameters().entrySet();
          for(final Map.Entry<SchedulingParameterDefinition, Value> paramEntry : _entrySet) {
            _builder.append("\t\t");
            String _createSchedParam = this.schedulingParameterTransformer.createSchedParam(taskScheduler.getName(), "setAlgorithmSchedulingParameter", true, paramEntry.getKey(), paramEntry.getValue());
            _builder.append(_createSchedParam, "\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_4 = taskScheduler.getName();
    _builder.append(_name_4, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  private static final TranslationUnit chooseSchedulerBasedOnSchedulerDefinition(final SchedulerDefinition schedulerDefinition) {
    String _name = schedulerDefinition.getName();
    boolean _matched = false;
    String _algorithmName = StandardSchedulers.Algorithm.FIXED_PRIORITY_PREEMPTIVE.getAlgorithmName();
    if (Objects.equal(_name, _algorithmName)) {
      _matched=true;
      return new TranslationUnit("PriorityScheduler.h", "PriorityScheduler");
    }
    if (!_matched) {
      String _algorithmName_1 = StandardSchedulers.Algorithm.PRIORITY_BASED_ROUND_ROBIN.getAlgorithmName();
      if (Objects.equal(_name, _algorithmName_1)) {
        _matched=true;
        return new TranslationUnit("PriorityRoundRobinScheduler.h", "PriorityRoundRobinScheduler");
      }
    }
    String _name_1 = schedulerDefinition.getName();
    String _plus = ("Scheduler definition " + _name_1);
    String _plus_1 = (_plus + " is not supported.");
    throw new IllegalArgumentException(_plus_1);
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
