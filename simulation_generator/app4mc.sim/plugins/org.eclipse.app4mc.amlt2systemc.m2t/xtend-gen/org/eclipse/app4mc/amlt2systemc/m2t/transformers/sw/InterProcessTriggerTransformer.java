/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli.InterProcessStimulusTransformer;

@Singleton
@SuppressWarnings("all")
public class InterProcessTriggerTransformer extends BaseTransformer {
  @Inject
  private InterProcessStimulusTransformer interProcessStimulusTransformer;

  public void transform(final InterProcessTrigger _trigger, final AgiContainerBuffer content) {
    final TranslationUnit ips = this.interProcessStimulusTransformer.transform(_trigger.getStimulus());
    content.addInclude(ips.getModule());
    String _call = ips.getCall();
    String _plus = ("<InterProcessTrigger>({" + _call);
    String _plus_1 = (_plus + "})");
    content.addAsActivityGraphItem(_plus_1);
  }
}
