/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchDefault;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;

@SuppressWarnings("all")
public class SwitchTransformer {
  @Inject
  private SwitchEntryTransformer switchEntryTransformer;

  private static int cnt = 0;

  public static String getName() {
    return ("switch_" + Integer.valueOf(SwitchTransformer.cnt));
  }

  public void transform(final Switch _switch, final AgiContainerBuffer parentContent) {
    final String name = SwitchTransformer.getName();
    SwitchTransformer.cnt++;
    parentContent.addBody((("Switch " + name) + ";"));
    final AgiContainerBuffer switchContent = new AgiContainerBuffer(name, parentContent.module, false);
    final Consumer<SwitchEntry> _function = (SwitchEntry entry) -> {
      this.switchEntryTransformer.transform(entry, switchContent);
    };
    _switch.getEntries().forEach(_function);
    SwitchDefault _defaultEntry = _switch.getDefaultEntry();
    boolean _tripleNotEquals = (_defaultEntry != null);
    if (_tripleNotEquals) {
      this.switchEntryTransformer.transform(_switch.getDefaultEntry(), switchContent);
    }
    parentContent.getIncludes().addAll(switchContent.getIncludes());
    parentContent.getSource().addAll(switchContent.getSource());
    parentContent.addAsActivityGraphItem((("<Switch>(" + name) + ")"));
  }
}
