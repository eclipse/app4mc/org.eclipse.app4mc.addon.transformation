package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.SingleStimulus;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;

@Singleton
@SuppressWarnings("all")
public class StimuliModelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private StimulusTransformer stimulusDispatcher;

  protected static String getModulePath() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/stimuliModel");
  }

  private String getModuleName() {
    String _modulePath = StimuliModelTransformer.getModulePath();
    return (_modulePath + "/stimuliModel");
  }

  private String getFunctionDef() {
    return "init_stimuliModel()";
  }

  public TranslationUnit transform(final StimuliModel[] stimuliModels) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(stimuliModels);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName();
      String _functionDef = this.getFunctionDef();
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, stimuliModels);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final StimuliModel[] stimuliModels) {
    final Consumer<StimuliModel> _function = (StimuliModel stimuliModel) -> {
      final Consumer<Stimulus> _function_1 = (Stimulus it_1) -> {
        this.stimulusDispatcher.transform(it_1);
      };
      stimuliModel.getStimuli().forEach(_function_1);
    };
    ((List<StimuliModel>)Conversions.doWrapArray(stimuliModels)).forEach(_function);
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH());
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp());
    String _modulePath = StimuliModelTransformer.getModulePath();
    String _plus = (_modulePath + "/CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus, this.getCMake());
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("//include model elements»");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.stimulusDispatcher.getCache(SingleStimulus.class).values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("#include \"");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.stimulusDispatcher.getCache(PeriodicStimulus.class).values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("#include \"");
        String _module_1 = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module_1);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_2 = TuSort.byModule(this.stimulusDispatcher.getCache(RelativePeriodicStimulus.class).values());
      for(final TranslationUnit obj_2 : _byModule_2) {
        _builder.append("#include \"");
        String _module_2 = ((TranslationUnit) obj_2).getModule();
        _builder.append(_module_2);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_3 = TuSort.byModule(this.stimulusDispatcher.getCache(InterProcessStimulus.class).values());
      for(final TranslationUnit obj_3 : _byModule_3) {
        _builder.append("#include \"");
        String _module_3 = ((TranslationUnit) obj_3).getModule();
        _builder.append(_module_3);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_4 = TuSort.byModule(this.stimulusDispatcher.getCache(EventStimulus.class).values());
      for(final TranslationUnit obj_4 : _byModule_4) {
        _builder.append("#include \"");
        String _module_4 = ((TranslationUnit) obj_4).getModule();
        _builder.append(_module_4);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("/* Software */");
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append("{\t\t");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byCall = TuSort.byCall(this.stimulusDispatcher.getCache(SingleStimulus.class).values());
      for(final TranslationUnit obj_5 : _byCall) {
        _builder.append("\t");
        String _call = ((TranslationUnit) obj_5).getCall();
        _builder.append(_call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byCall_1 = TuSort.byCall(this.stimulusDispatcher.getCache(PeriodicStimulus.class).values());
      for(final TranslationUnit obj_6 : _byCall_1) {
        _builder.append("\t");
        String _call_1 = ((TranslationUnit) obj_6).getCall();
        _builder.append(_call_1, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byCall_2 = TuSort.byCall(this.stimulusDispatcher.getCache(RelativePeriodicStimulus.class).values());
      for(final TranslationUnit obj_7 : _byCall_2) {
        _builder.append("\t");
        String _call_2 = ((TranslationUnit) obj_7).getCall();
        _builder.append(_call_2, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byCall_3 = TuSort.byCall(this.stimulusDispatcher.getCache(InterProcessStimulus.class).values());
      for(final TranslationUnit obj_8 : _byCall_3) {
        _builder.append("\t");
        String _call_3 = ((TranslationUnit) obj_8).getCall();
        _builder.append(_call_3, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byCall_4 = TuSort.byCall(this.stimulusDispatcher.getCache(EventStimulus.class).values());
      for(final TranslationUnit obj_9 : _byCall_4) {
        _builder.append("\t");
        String _call_4 = ((TranslationUnit) obj_9).getCall();
        _builder.append(_call_4, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: CMake project for StimuliModel of \"");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append("\".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.append("# Add sources of Stimuli");
    _builder.newLine();
    _builder.append("target_sources (");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append("  PRIVATE");
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.stimulusDispatcher.getCache(SingleStimulus.class).values());
      for(final TranslationUnit obj : _byModule) {
        _builder.newLineIfNotEmpty();
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.stimulusDispatcher.getCache(PeriodicStimulus.class).values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_1 = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module_1, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_2 = TuSort.byModule(this.stimulusDispatcher.getCache(RelativePeriodicStimulus.class).values());
      for(final TranslationUnit obj_2 : _byModule_2) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_2 = ((TranslationUnit) obj_2).getModule();
        _builder.append(_module_2, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_3 = TuSort.byModule(this.stimulusDispatcher.getCache(InterProcessStimulus.class).values());
      for(final TranslationUnit obj_3 : _byModule_3) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_3 = ((TranslationUnit) obj_3).getModule();
        _builder.append(_module_3, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_4 = TuSort.byModule(this.stimulusDispatcher.getCache(EventStimulus.class).values());
      for(final TranslationUnit obj_4 : _byModule_4) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_4 = ((TranslationUnit) obj_4).getModule();
        _builder.append(_module_4, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName, "\t");
    _builder.append(".cpp\")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
