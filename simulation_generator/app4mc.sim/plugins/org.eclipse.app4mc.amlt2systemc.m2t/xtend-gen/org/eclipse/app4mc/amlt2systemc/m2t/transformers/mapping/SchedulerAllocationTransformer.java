/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.ProcessingUnitTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.TaskSchedulerTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class SchedulerAllocationTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private ProcessingUnitTransformer processingUnitTransformer;

  @Inject
  private TaskSchedulerTransformer taskSchedulerTransformer;

  private String getModulePath() {
    String _modulePath = MappingModelTransformer.getModulePath();
    return (_modulePath + "/schedulerAllocation");
  }

  private String getModuleName(final SchedulerAllocation schedulerAllocation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      ProcessingUnit _executingPU = schedulerAllocation.getExecutingPU();
      boolean _tripleEquals = (_executingPU == null);
      if (_tripleEquals) {
        String _modulePath = this.getModulePath();
        _builder.append(_modulePath);
        _builder.append("/schedulerAllocation_");
        String _name = schedulerAllocation.getScheduler().getName();
        _builder.append(_name);
        _builder.append("_");
        String _name_1 = schedulerAllocation.getResponsibility().get(0).getName();
        _builder.append(_name_1);
      } else {
        _builder.newLineIfNotEmpty();
        String _modulePath_1 = this.getModulePath();
        _builder.append(_modulePath_1);
        _builder.append("/schedulerAllocation_");
        String _name_2 = schedulerAllocation.getScheduler().getName();
        _builder.append(_name_2);
        _builder.append("_");
        String _name_3 = schedulerAllocation.getExecutingPU().getName();
        _builder.append(_name_3);
      }
    }
    return _builder.toString();
  }

  private String getCall(final SchedulerAllocation schedulerAllocation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Scheduler _scheduler = schedulerAllocation.getScheduler();
      if ((_scheduler instanceof TaskScheduler)) {
        _builder.append("init_SchedulerAllocation_");
        Scheduler _scheduler_1 = schedulerAllocation.getScheduler();
        String _name = this.taskSchedulerTransformer.getName(((TaskScheduler) _scheduler_1));
        _builder.append(_name);
        _builder.append("_");
        ProcessingUnit _executingPU = schedulerAllocation.getExecutingPU();
        String _name_1 = this.processingUnitTransformer.getName(((HwModule) _executingPU));
        _builder.append(_name_1);
      } else {
        _builder.newLineIfNotEmpty();
        _builder.append("/*Scheduler allocation for Interrupt Controller not yet implemented*/");
        _builder.newLine();
      }
    }
    return _builder.toString();
  }

  public TranslationUnit transform(final SchedulerAllocation schedulerAllocation) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(schedulerAllocation);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName(schedulerAllocation);
      String _call = this.getCall(schedulerAllocation);
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, schedulerAllocation);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final SchedulerAllocation schedulerAllocation) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(it));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(schedulerAllocation));
  }

  private String toH(final TranslationUnit tu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void ");
    String _call = tu.getCall();
    _builder.append(_call);
    _builder.append("();\t\t");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final SchedulerAllocation schedulerAllocation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Scheduler _scheduler = schedulerAllocation.getScheduler();
      if ((_scheduler instanceof TaskScheduler)) {
        Scheduler _scheduler_1 = schedulerAllocation.getScheduler();
        final TranslationUnit tuScheduler = this.taskSchedulerTransformer.transform(((TaskScheduler) _scheduler_1));
        _builder.newLineIfNotEmpty();
        ProcessingUnit _executingPU = schedulerAllocation.getExecutingPU();
        final TranslationUnit tuPu = this.processingUnitTransformer.transform(((ProcessingUnit) _executingPU));
        _builder.newLineIfNotEmpty();
        _builder.append("#include \"");
        String _module = tuScheduler.getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
        _builder.append("#include \"");
        String _module_1 = tuPu.getModule();
        _builder.append(_module_1);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
        {
          EList<ProcessingUnit> _responsibility = schedulerAllocation.getResponsibility();
          for(final ProcessingUnit responsiblePU : _responsibility) {
            final TranslationUnit tuResponsiblePU = this.processingUnitTransformer.transform(((ProcessingUnit) responsiblePU));
            _builder.newLineIfNotEmpty();
            _builder.append("#include \"");
            String _module_2 = tuResponsiblePU.getModule();
            _builder.append(_module_2);
            _builder.append(".h\"");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.newLine();
        _builder.append("void ");
        String _call = this.getCall(schedulerAllocation);
        _builder.append(_call);
        _builder.append("(){");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        String _call_1 = tuScheduler.getCall();
        _builder.append(_call_1, "\t");
        _builder.append("()->setExecutionCore(");
        String _call_2 = tuPu.getCall();
        _builder.append(_call_2, "\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
        {
          EList<ProcessingUnit> _responsibility_1 = schedulerAllocation.getResponsibility();
          for(final ProcessingUnit responsiblePU_1 : _responsibility_1) {
            _builder.append("\t");
            final TranslationUnit tuResponsiblePU_1 = this.processingUnitTransformer.transform(((ProcessingUnit) responsiblePU_1));
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            String _call_3 = tuScheduler.getCall();
            _builder.append(_call_3, "\t");
            _builder.append("()->addResponsibleCore(");
            String _call_4 = tuResponsiblePU_1.getCall();
            _builder.append(_call_4, "\t");
            _builder.append("());");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("}");
        _builder.newLine();
      } else {
        _builder.append("/*Scheduler allocation for Interrupt Controller not yet implemented*/");
        _builder.newLine();
      }
    }
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
