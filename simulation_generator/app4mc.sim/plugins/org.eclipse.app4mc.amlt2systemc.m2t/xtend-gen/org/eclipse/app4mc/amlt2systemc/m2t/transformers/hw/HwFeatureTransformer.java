/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.HwFeature;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class HwFeatureTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  public static String getModulePath(final HwFeature hwFeature) {
    String _modulePath = HwModelTransformer.getModulePath();
    String _plus = (_modulePath + "/features/");
    String _name = HwFeatureTransformer.getName(hwFeature);
    return (_plus + _name);
  }

  public static String getCall(final HwFeature hwFeature) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_HwFeature_");
    String _name = HwFeatureTransformer.getName(hwFeature);
    _builder.append(_name);
    _builder.append("()");
    return _builder.toString();
  }

  public static String getName(final HwFeature hwFeature) {
    String _name = hwFeature.getContainingCategory().getName();
    String _plus = (_name + "_");
    String _name_1 = hwFeature.getName();
    return (_plus + _name_1);
  }

  public TranslationUnit transform(final HwFeature hwFeature) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(hwFeature);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = HwFeatureTransformer.getModulePath(hwFeature);
      String _call = HwFeatureTransformer.getCall(hwFeature);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, hwFeature);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final HwFeature hwFeature) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(hwFeature));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(hwFeature));
  }

  public String toH(final HwFeature hwFeature) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"ProcessingUnitDefinition.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("HwFeature* ");
    String _call = HwFeatureTransformer.getCall(hwFeature);
    _builder.append(_call);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String toCpp(final HwFeature hwFeature) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = HwFeatureTransformer.getModulePath(hwFeature);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    final String name = HwFeatureTransformer.getName(hwFeature);
    _builder.newLineIfNotEmpty();
    _builder.append("HwFeature*  ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.append("HwFeature* ");
    String _call = HwFeatureTransformer.getCall(hwFeature);
    _builder.append(_call);
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = new HwFeature(\"");
    String _name = hwFeature.getName();
    _builder.append(_name, "\t\t");
    _builder.append("\", ");
    String _string = Double.valueOf(hwFeature.getValue()).toString();
    _builder.append(_string, "\t\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
