/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.Mode;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.NumericMode;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class ModeTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  public TranslationUnit transform(final Mode mode) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(mode);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = ModeTransformer.getModuleName(mode);
      String _functionDef = ModeTransformer.getFunctionDef(mode);
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, mode);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final Mode mode) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(mode));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(mode));
  }

  private static String getName(final Mode mode) {
    return mode.getName();
  }

  public static String getFunctionDef(final Mode mode) {
    String _name = ModeTransformer.getName(mode);
    String _plus = ("get_" + _name);
    return (_plus + "()");
  }

  public static String getModuleName(final Mode mode) {
    String _modulePath = SWModelTransformer.getModulePath();
    String _plus = (_modulePath + "/modes/");
    String _name = ModeTransformer.getName(mode);
    return (_plus + _name);
  }

  private String _toH(final EnumMode mode) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Mode.h\"\t\t\t");
    _builder.newLine();
    _builder.append("std::shared_ptr<EnumMode> ");
    String _functionDef = ModeTransformer.getFunctionDef(mode);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String _toH(final NumericMode mode) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Mode.h\"\t\t\t");
    _builder.newLine();
    _builder.append("std::shared_ptr<NumericMode> ");
    String _functionDef = ModeTransformer.getFunctionDef(mode);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String _toCpp(final EnumMode mode) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _moduleName = ModeTransformer.getModuleName(mode);
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<EnumMode> ");
    String _name = ModeTransformer.getName(mode);
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<EnumMode>  ");
    String _functionDef = ModeTransformer.getFunctionDef(mode);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = ModeTransformer.getName(mode);
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _name_2 = ModeTransformer.getName(mode);
    _builder.append(_name_2, "\t\t");
    _builder.append(" = EnumMode::CreateMode(\"");
    String _name_3 = ModeTransformer.getName(mode);
    _builder.append(_name_3, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    {
      EList<ModeLiteral> _literals = mode.getLiterals();
      for(final ModeLiteral literal : _literals) {
        _builder.append("\t\t");
        String _name_4 = ModeTransformer.getName(mode);
        _builder.append(_name_4, "\t\t");
        _builder.append("->addLiteral(\"");
        String _name_5 = literal.getName();
        _builder.append(_name_5, "\t\t");
        _builder.append("\");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_6 = ModeTransformer.getName(mode);
    _builder.append(_name_6, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  private String _toCpp(final NumericMode mode) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _moduleName = ModeTransformer.getModuleName(mode);
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<NumericMode> ");
    String _name = ModeTransformer.getName(mode);
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<NumericMode>  ");
    String _functionDef = ModeTransformer.getFunctionDef(mode);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = ModeTransformer.getName(mode);
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _name_2 = ModeTransformer.getName(mode);
    _builder.append(_name_2, "\t\t");
    _builder.append(" = NumericMode::CreateMode(\"");
    String _name_3 = ModeTransformer.getName(mode);
    _builder.append(_name_3, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_4 = ModeTransformer.getName(mode);
    _builder.append(_name_4, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }

  private String toH(final Mode mode) {
    if (mode instanceof EnumMode) {
      return _toH((EnumMode)mode);
    } else if (mode instanceof NumericMode) {
      return _toH((NumericMode)mode);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(mode).toString());
    }
  }

  private String toCpp(final Mode mode) {
    if (mode instanceof EnumMode) {
      return _toCpp((EnumMode)mode);
    } else if (mode instanceof NumericMode) {
      return _toCpp((NumericMode)mode);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(mode).toString());
    }
  }
}
