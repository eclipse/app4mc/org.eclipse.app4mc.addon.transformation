/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.TriggerEvent;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.event.EventTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class EventStimulusTransformer extends StimulusBaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private EventTransformer eventsDispatcher;

  @Inject
  private ConditionTransformer conditionTransformer;

  public TranslationUnit transform(final EventStimulus stim) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(stim);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(stim);
      String _functionDef = this.getFunctionDef(stim);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, stim);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final EventStimulus stim) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(stim));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(stim));
  }

  private String toH(final EventStimulus stim) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// This code was generated for simulation with app4mc.sim");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("#pragma once");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"Stimuli/EventStimulus.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("//stim runnableA----");
    _builder.newLine();
    _builder.append("std::shared_ptr<EventStimulus> ");
    String _functionDef = this.getFunctionDef(stim);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final EventStimulus stim) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// This code was generated for simulation with app4mc.sim");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(stim);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    final ConditionTransformer.ConditionBuffer conditionContent = this.conditionTransformer.transformCondition(
      stim.getExecutionCondition(), this.getName(stim), true);
    _builder.newLineIfNotEmpty();
    CharSequence _conditionIncludes = ConditionTransformer.ConditionBuffer.getConditionIncludes(conditionContent);
    _builder.append(_conditionIncludes);
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    {
      EList<TriggerEvent> _triggeringEvents = stim.getTriggeringEvents();
      for(final TriggerEvent event : _triggeringEvents) {
        _builder.append("#include \"");
        String _module = this.eventsDispatcher.transform(event).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("std::shared_ptr<EventStimulus> ");
    String _name = stim.getName();
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<EventStimulus>  ");
    String _functionDef = this.getFunctionDef(stim);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = stim.getName();
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    {
      EList<TriggerEvent> _triggeringEvents_1 = stim.getTriggeringEvents();
      for(final TriggerEvent event_1 : _triggeringEvents_1) {
        _builder.append("\t\t");
        String _name_2 = stim.getName();
        _builder.append(_name_2, "\t\t");
        _builder.append(" = std::make_shared<EventStimulus>(\"");
        String _name_3 = stim.getName();
        _builder.append(_name_3, "\t\t");
        _builder.append("\", ");
        String _call = this.eventsDispatcher.transform(event_1).getCall();
        _builder.append(_call, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    CharSequence _conditionSource = ConditionTransformer.ConditionBuffer.getConditionSource(conditionContent);
    _builder.append(_conditionSource, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_4 = stim.getName();
    _builder.append(_name_4, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
