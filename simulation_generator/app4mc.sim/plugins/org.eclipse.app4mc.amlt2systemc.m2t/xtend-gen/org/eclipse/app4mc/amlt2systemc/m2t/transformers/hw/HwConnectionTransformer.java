/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class HwConnectionTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private HwStructureTransformer hwStructureTransformer;

  protected static String getName(final HwConnection hwConnection) {
    return hwConnection.getName();
  }

  protected static String getCall(final HwConnection hwConnection) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_");
    String _name = HwConnectionTransformer.getName(hwConnection);
    _builder.append(_name);
    return _builder.toString();
  }

  protected static String getModulePath(final HwConnection hwConnection) {
    String _modulePath = HwModelTransformer.getModulePath();
    String _plus = (_modulePath + "/connections/");
    String _name = HwConnectionTransformer.getName(hwConnection);
    return (_plus + _name);
  }

  public TranslationUnit transform(final HwConnection hwConnection) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(hwConnection);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = HwConnectionTransformer.getModulePath(hwConnection);
      String _call = HwConnectionTransformer.getCall(hwConnection);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, hwConnection);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit tu, final HwConnection hwConnection) {
    this.outputBuffer.appendTo("INC", tu.getModule(), this.toH(hwConnection));
    final HwStructure parentHwStructure = HwStructureTransformer.getParentHwStructureOrNull(hwConnection);
    if ((parentHwStructure != null)) {
      this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(hwConnection, this.hwStructureTransformer.transform(parentHwStructure)));
    } else {
      this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(hwConnection));
    }
  }

  public String toH(final HwConnection connection) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("//framework");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.append("std::shared_ptr<Connection> ");
    String _call = HwConnectionTransformer.getCall(connection);
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String toCpp(final HwConnection connection) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = HwConnectionTransformer.getModulePath(connection);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    final String name = HwConnectionTransformer.getName(connection);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<Connection> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("//for usage on hw model top level (eg. in flat hierarchies)");
    _builder.newLine();
    _builder.append("std::shared_ptr<Connection> ");
    String _call = HwConnectionTransformer.getCall(connection);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" =  std::make_shared<Connection>(\"");
    _builder.append(name, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    CharSequence _setParams = this.setParams(connection);
    _builder.append(_setParams, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String toCpp(final HwConnection connection, final TranslationUnit parentTu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = HwConnectionTransformer.getModulePath(connection);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module = parentTu.getModule();
    _builder.append(_module);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    final String name = HwConnectionTransformer.getName(connection);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<Connection> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("//for usage in structures");
    _builder.newLine();
    _builder.append("std::shared_ptr<Connection> ");
    String _call = HwConnectionTransformer.getCall(connection);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("std::shared_ptr<HwStructure> parent = ");
    String _call_1 = parentTu.getCall();
    _builder.append(_call_1, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = parent->createConnection(\"");
    _builder.append(name, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    CharSequence _setParams = this.setParams(connection);
    _builder.append(_setParams, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  private CharSequence setParams(final HwConnection connection) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = HwConnectionTransformer.getName(connection);
    _builder.newLineIfNotEmpty();
    {
      IDiscreteValueDeviation _writeLatency = connection.getWriteLatency();
      boolean _tripleNotEquals = (_writeLatency != null);
      if (_tripleNotEquals) {
        _builder.append(name);
        _builder.append("->setWriteLatency(");
        String _deviation = IDiscreteValueDeviationTransformer.getDeviation(connection.getWriteLatency());
        _builder.append(_deviation);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      IDiscreteValueDeviation _readLatency = connection.getReadLatency();
      boolean _tripleNotEquals_1 = (_readLatency != null);
      if (_tripleNotEquals_1) {
        _builder.append(name);
        _builder.append("->setReadLatency(");
        String _deviation_1 = IDiscreteValueDeviationTransformer.getDeviation(connection.getReadLatency());
        _builder.append(_deviation_1);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
