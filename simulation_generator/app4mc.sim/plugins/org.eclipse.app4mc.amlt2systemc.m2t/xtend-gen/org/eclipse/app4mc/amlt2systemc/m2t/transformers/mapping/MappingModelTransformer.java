/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.MemoryMapping;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;

@Singleton
@SuppressWarnings("all")
public class MappingModelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private MemoryMappingTransformer memoryMappingTransformer;

  @Inject
  private TaskAllocationTransformer taskAllocationTransformer;

  @Inject
  private SchedulerAllocationTransformer schedulerAllocationTransformer;

  protected static String getModulePath() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/mapModel");
  }

  private String getModuleName() {
    String _modulePath = MappingModelTransformer.getModulePath();
    return (_modulePath + "/mapModel");
  }

  private String getFunctionDef() {
    return "init_mappingModel()";
  }

  public TranslationUnit transform(final MappingModel[] mappingModels) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(mappingModels);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName();
      String _functionDef = this.getFunctionDef();
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, mappingModels);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final MappingModel[] mappingModels) {
    final Consumer<MappingModel> _function = (MappingModel mappingModel) -> {
      final Consumer<MemoryMapping> _function_1 = (MemoryMapping memMapping) -> {
        this.memoryMappingTransformer.transform(memMapping);
      };
      mappingModel.getMemoryMapping().forEach(_function_1);
      final Consumer<TaskAllocation> _function_2 = (TaskAllocation taskAllocation) -> {
        this.taskAllocationTransformer.transform(taskAllocation);
      };
      mappingModel.getTaskAllocation().forEach(_function_2);
      final Consumer<SchedulerAllocation> _function_3 = (SchedulerAllocation schedulerAllocation) -> {
        this.schedulerAllocationTransformer.transform(schedulerAllocation);
      };
      mappingModel.getSchedulerAllocation().forEach(_function_3);
    };
    ((List<MappingModel>)Conversions.doWrapArray(mappingModels)).forEach(_function);
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH());
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp());
    String _modulePath = MappingModelTransformer.getModulePath();
    String _plus = (_modulePath + "/CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus, this.getCMake(it.getModule()));
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("//include model elements");
    _builder.newLine();
    final HashSet<String> uniqueMemoryMappings = new HashSet<String>();
    _builder.newLineIfNotEmpty();
    final Consumer<TranslationUnit> _function = (TranslationUnit it) -> {
      uniqueMemoryMappings.add(it.getModule());
    };
    this.memoryMappingTransformer.getCache().values().forEach(_function);
    _builder.newLineIfNotEmpty();
    {
      for(final String module : uniqueMemoryMappings) {
        _builder.append("#include \"");
        _builder.append(module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.taskAllocationTransformer.getCache().values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("#include \"");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.schedulerAllocationTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("#include \"");
        String _module_1 = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module_1);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append(";\t\t");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byCall = TuSort.byCall(this.memoryMappingTransformer.getCache().values());
      for(final TranslationUnit obj : _byCall) {
        _builder.append("\t");
        String _call = ((TranslationUnit) obj).getCall();
        _builder.append(_call, "\t");
        _builder.append("(); ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byCall_1 = TuSort.byCall(this.taskAllocationTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byCall_1) {
        _builder.append("\t");
        String _call_1 = ((TranslationUnit) obj_1).getCall();
        _builder.append(_call_1, "\t");
        _builder.append("(); ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byCall_2 = TuSort.byCall(this.schedulerAllocationTransformer.getCache().values());
      for(final TranslationUnit obj_2 : _byCall_2) {
        _builder.append("\t");
        String _call_2 = ((TranslationUnit) obj_2).getCall();
        _builder.append(_call_2, "\t");
        _builder.append("(); ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake(final String thisModule) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: CMake project for MappingModel of \"");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append("\".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("# Add sources of MappingModel");
    _builder.newLine();
    _builder.append("target_sources (");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append("  PRIVATE");
    _builder.newLineIfNotEmpty();
    final HashSet<String> uniqueMemoryMappings = new HashSet<String>();
    _builder.newLineIfNotEmpty();
    final Consumer<TranslationUnit> _function = (TranslationUnit it) -> {
      uniqueMemoryMappings.add(it.getModule());
    };
    this.memoryMappingTransformer.getCache().values().forEach(_function);
    _builder.newLineIfNotEmpty();
    {
      for(final String module : uniqueMemoryMappings) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        _builder.append(module);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Collection<TranslationUnit> _values = this.memoryMappingTransformer.getCache().values();
      for(final TranslationUnit obj : _values) {
      }
    }
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.taskAllocationTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byModule) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.schedulerAllocationTransformer.getCache().values());
      for(final TranslationUnit obj_2 : _byModule_1) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_1 = ((TranslationUnit) obj_2).getModule();
        _builder.append(_module_1);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    _builder.append(thisModule);
    _builder.append(".cpp\")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
