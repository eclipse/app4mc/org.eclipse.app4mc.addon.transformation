/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.ITimeDeviation;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeConstant;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.ITimeDeviationTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class PeriodicStimulusTransformer extends StimulusBaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private ConditionTransformer conditionTransformer;

  public TranslationUnit transform(final PeriodicStimulus stim) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(stim);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(stim);
      String _functionDef = this.getFunctionDef(stim);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, stim);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final PeriodicStimulus stim) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(stim));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(stim));
  }

  private String toH(final PeriodicStimulus stim) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// This code was generated for simulation with app4mc.sim");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("#pragma once");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"Stimuli/PeriodicStimulus.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("//PeriodicStimulus runnableA----");
    _builder.newLine();
    _builder.append("std::shared_ptr<PeriodicStimulus> ");
    String _functionDef = this.getFunctionDef(stim);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final PeriodicStimulus stim) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// This code was generated for simulation with app4mc.sim");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(stim);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    final ConditionTransformer.ConditionBuffer conditionContent = this.conditionTransformer.transformCondition(
      stim.getExecutionCondition(), this.getName(stim), true);
    _builder.newLineIfNotEmpty();
    CharSequence _conditionIncludes = ConditionTransformer.ConditionBuffer.getConditionIncludes(conditionContent);
    _builder.append(_conditionIncludes);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<PeriodicStimulus> ");
    String _name = stim.getName();
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<PeriodicStimulus>  ");
    String _functionDef = this.getFunctionDef(stim);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = stim.getName();
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    _builder.append("\t\t");
    String _name_2 = stim.getName();
    _builder.append(_name_2, "\t\t");
    _builder.append(" = std::make_shared<PeriodicStimulus>(\"");
    String _name_3 = stim.getName();
    _builder.append(_name_3, "\t\t");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    String _transform = TimeUtil.transform(stim.getRecurrence());
    _builder.append(_transform, "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    _builder.append(",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    Time _elvis = null;
    Time _offset = stim.getOffset();
    if (_offset != null) {
      _elvis = _offset;
    } else {
      Time _createTime = FactoryUtil.createTime(0, TimeUnit.PS);
      _elvis = _createTime;
    }
    String _transform_1 = TimeUtil.transform(_elvis);
    _builder.append(_transform_1, "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    _builder.append(",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    ITimeDeviation _elvis_1 = null;
    ITimeDeviation _jitter = stim.getJitter();
    if (_jitter != null) {
      _elvis_1 = _jitter;
    } else {
      TimeConstant _createTimeConstant = FactoryUtil.createTimeConstant(FactoryUtil.createTime(0, TimeUnit.PS));
      _elvis_1 = _createTimeConstant;
    }
    String _deviation = ITimeDeviationTransformer.getDeviation(_elvis_1);
    _builder.append(_deviation, "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    _builder.append(",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    Time _elvis_2 = null;
    Time _minDistance = stim.getMinDistance();
    if (_minDistance != null) {
      _elvis_2 = _minDistance;
    } else {
      Time _createTime_1 = FactoryUtil.createTime(0, TimeUnit.PS);
      _elvis_2 = _createTime_1;
    }
    String _transform_2 = TimeUtil.transform(_elvis_2);
    _builder.append(_transform_2, "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    CharSequence _conditionSource = ConditionTransformer.ConditionBuffer.getConditionSource(conditionContent);
    _builder.append(_conditionSource, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_4 = stim.getName();
    _builder.append(_name_4, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
