/**
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.ITimeDeviation;
import org.eclipse.app4mc.amalthea.model.SamplingType;
import org.eclipse.app4mc.amalthea.model.TimeBetaDistribution;
import org.eclipse.app4mc.amalthea.model.TimeBoundaries;
import org.eclipse.app4mc.amalthea.model.TimeConstant;
import org.eclipse.app4mc.amalthea.model.TimeGaussDistribution;
import org.eclipse.app4mc.amalthea.model.TimeUniformDistribution;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.TimeWeibullEstimatorsDistribution;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class ITimeDeviationTransformer {
  protected static String _getDeviation(final ITimeDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ITimeDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ITimeDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviation(final TimeBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = ITimeDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final TimeBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TimeBetaDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final TimeBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    double _alpha = value.getAlpha();
    _builder.append(_alpha);
    _builder.append(",");
    double _beta = value.getBeta();
    _builder.append(_beta);
    _builder.append(",");
    String _transform = TimeUtil.transform(value.getLowerBound());
    _builder.append(_transform);
    _builder.append(",");
    String _transform_1 = TimeUtil.transform(value.getUpperBound());
    _builder.append(_transform_1);
    return _builder.toString();
  }

  private static String stype(final TimeBoundaries value) {
    SamplingType _samplingType = value.getSamplingType();
    if (_samplingType != null) {
      switch (_samplingType) {
        case BEST_CASE:
          return "BoundariesSamplingType::BestCase";
        case WORST_CASE:
          return "BoundariesSamplingType::WorstCase";
        case AVERAGE_CASE:
          return "BoundariesSamplingType::AverageCase";
        case CORNER_CASE:
          return "BoundariesSamplingType::CornerCase";
        case UNIFORM:
          return "BoundariesSamplingType::Uniform";
        default:
          return "BoundariesSamplingType::WorstCase";
      }
    } else {
      return "BoundariesSamplingType::WorstCase";
    }
  }

  protected static String _getDeviation(final TimeBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = ITimeDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final TimeBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TimeBoundaries");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final TimeBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    String _stype = ITimeDeviationTransformer.stype(value);
    _builder.append(_stype);
    _builder.append(",");
    String _transform = TimeUtil.transform(value.getLowerBound());
    _builder.append(_transform);
    _builder.append(",");
    String _transform_1 = TimeUtil.transform(value.getUpperBound());
    _builder.append(_transform_1);
    return _builder.toString();
  }

  protected static String _getDeviation(final TimeConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = ITimeDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final TimeConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TimeConstant");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final TimeConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    String _transform = TimeUtil.transform(value.getValue());
    _builder.append(_transform);
    return _builder.toString();
  }

  protected static String _getDeviation(final TimeGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = ITimeDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final TimeGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TimeGaussDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final TimeGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _transform = TimeUtil.transform(value.getMean());
    _builder.append(_transform);
    _builder.append(",");
    String _transform_1 = TimeUtil.transform(value.getSd());
    _builder.append(_transform_1);
    _builder.append(",");
    String _elvis = null;
    String _transform_2 = TimeUtil.transform(value.getLowerBound());
    if (_transform_2 != null) {
      _elvis = _transform_2;
    } else {
      String _transform_3 = TimeUtil.transform(FactoryUtil.createTime(Long.MIN_VALUE, TimeUnit.PS));
      _elvis = _transform_3;
    }
    _builder.append(_elvis);
    _builder.append(",");
    String _elvis_1 = null;
    String _transform_4 = TimeUtil.transform(value.getUpperBound());
    if (_transform_4 != null) {
      _elvis_1 = _transform_4;
    } else {
      String _transform_5 = TimeUtil.transform(FactoryUtil.createTime(Long.MAX_VALUE, TimeUnit.PS));
      _elvis_1 = _transform_5;
    }
    _builder.append(_elvis_1);
    return _builder.toString();
  }

  protected static String _getDeviation(final TimeUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = ITimeDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final TimeUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TimeUniformDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final TimeUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _transform = TimeUtil.transform(value.getLowerBound());
    _builder.append(_transform);
    _builder.append(",");
    String _transform_1 = TimeUtil.transform(value.getUpperBound());
    _builder.append(_transform_1);
    return _builder.toString();
  }

  protected static String _getDeviation(final TimeWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = ITimeDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final TimeWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TimeWeibullEstimatorsDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final TimeWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = ITimeDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("::findParameter(");
    String _transform = TimeUtil.transform(value.getAverage());
    _builder.append(_transform);
    _builder.append(",");
    double _pRemainPromille = value.getPRemainPromille();
    _builder.append(_pRemainPromille);
    _builder.append(",");
    String _transform_1 = TimeUtil.transform(value.getLowerBound());
    _builder.append(_transform_1);
    _builder.append(",");
    String _transform_2 = TimeUtil.transform(value.getUpperBound());
    _builder.append(_transform_2);
    _builder.append("),");
    String _transform_3 = TimeUtil.transform(value.getLowerBound());
    _builder.append(_transform_3);
    _builder.append(",");
    String _transform_4 = TimeUtil.transform(value.getUpperBound());
    _builder.append(_transform_4);
    return _builder.toString();
  }

  public static String getDeviation(final ITimeDeviation value) {
    if (value instanceof TimeBetaDistribution) {
      return _getDeviation((TimeBetaDistribution)value);
    } else if (value instanceof TimeBoundaries) {
      return _getDeviation((TimeBoundaries)value);
    } else if (value instanceof TimeGaussDistribution) {
      return _getDeviation((TimeGaussDistribution)value);
    } else if (value instanceof TimeUniformDistribution) {
      return _getDeviation((TimeUniformDistribution)value);
    } else if (value instanceof TimeWeibullEstimatorsDistribution) {
      return _getDeviation((TimeWeibullEstimatorsDistribution)value);
    } else if (value instanceof TimeConstant) {
      return _getDeviation((TimeConstant)value);
    } else if (value != null) {
      return _getDeviation(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }

  public static String getDeviationTemplate(final ITimeDeviation value) {
    if (value instanceof TimeBetaDistribution) {
      return _getDeviationTemplate((TimeBetaDistribution)value);
    } else if (value instanceof TimeBoundaries) {
      return _getDeviationTemplate((TimeBoundaries)value);
    } else if (value instanceof TimeGaussDistribution) {
      return _getDeviationTemplate((TimeGaussDistribution)value);
    } else if (value instanceof TimeUniformDistribution) {
      return _getDeviationTemplate((TimeUniformDistribution)value);
    } else if (value instanceof TimeWeibullEstimatorsDistribution) {
      return _getDeviationTemplate((TimeWeibullEstimatorsDistribution)value);
    } else if (value instanceof TimeConstant) {
      return _getDeviationTemplate((TimeConstant)value);
    } else if (value != null) {
      return _getDeviationTemplate(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }

  public static String getDeviationValue(final ITimeDeviation value) {
    if (value instanceof TimeBetaDistribution) {
      return _getDeviationValue((TimeBetaDistribution)value);
    } else if (value instanceof TimeBoundaries) {
      return _getDeviationValue((TimeBoundaries)value);
    } else if (value instanceof TimeGaussDistribution) {
      return _getDeviationValue((TimeGaussDistribution)value);
    } else if (value instanceof TimeUniformDistribution) {
      return _getDeviationValue((TimeUniformDistribution)value);
    } else if (value instanceof TimeWeibullEstimatorsDistribution) {
      return _getDeviationValue((TimeWeibullEstimatorsDistribution)value);
    } else if (value instanceof TimeConstant) {
      return _getDeviationValue((TimeConstant)value);
    } else if (value != null) {
      return _getDeviationValue(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }
}
