/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.AbstractMemoryElement;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryMapping;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.MemoryTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ChannelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.LabelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ModeLabelTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class MemoryMappingTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private MemoryTransformer memoryTransformer;

  @Inject
  private LabelTransformer labelTransformer;

  @Inject
  private ChannelTransformer channelTransformer;

  @Inject
  private ModeLabelTransformer modeLabelTransformer;

  public static String getModulePath() {
    String _modulePath = MappingModelTransformer.getModulePath();
    return (_modulePath + "/memoryMapping");
  }

  public String getModuleName(final AbstractMemoryElement element, final Memory memory) {
    String _modulePath = MemoryMappingTransformer.getModulePath();
    return (_modulePath + "/memoryMapping");
  }

  public String getCall(final AbstractMemoryElement element, final Memory memory) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("init_MemoryMapping_");
    String _name = element.getName();
    _builder.append(_name);
    _builder.append("_to_");
    String _name_1 = memory.getName();
    _builder.append(_name_1);
    return _builder.toString();
  }

  private String getImplementation(final String functionName, final String memoryElementGetter, final String memoryGetter, final Long memoryAddress) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void ");
    _builder.append(functionName);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("MappingModel::addMemoryMapping(");
    _builder.append(memoryElementGetter, "\t");
    _builder.append(", ");
    _builder.append(memoryGetter, "\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("MappingModel::addMemoryAddress(");
    _builder.append(memoryElementGetter, "\t");
    _builder.append(", ");
    _builder.append(memoryAddress, "\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public TranslationUnit transform(final MemoryMapping memoryMapping) {
    boolean _memoryElementIsUsed = this.memoryElementIsUsed(memoryMapping.getAbstractElement());
    boolean _not = (!_memoryElementIsUsed);
    if (_not) {
      return null;
    } else {
      return this.transformInternal(memoryMapping);
    }
  }

  private TranslationUnit transformInternal(final MemoryMapping memoryMapping) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(memoryMapping);
    final TranslationUnit _result;
    synchronized (_createCache_transformInternal) {
      if (_createCache_transformInternal.containsKey(_cacheKey)) {
        return _createCache_transformInternal.get(_cacheKey);
      }
      String _moduleName = this.getModuleName(memoryMapping.getAbstractElement(), memoryMapping.getMemory());
      String _call = this.getCall(memoryMapping.getAbstractElement(), memoryMapping.getMemory());
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _call);
      _result = _translationUnit;
      _createCache_transformInternal.put(_cacheKey, _result);
    }
    _init_transformInternal(_result, memoryMapping);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transformInternal = CollectionLiterals.newHashMap();

  private void _init_transformInternal(final TranslationUnit it, final MemoryMapping memoryMapping) {
    this.toH(memoryMapping.getAbstractElement(), memoryMapping.getMemory());
    this.toCpp(memoryMapping.getAbstractElement(), memoryMapping.getMemory(), memoryMapping.getMemoryPositionAddress());
  }

  protected boolean _memoryElementIsUsed(final Channel element) {
    final ArrayList<?> _cacheKey = CollectionLiterals.<Channel>newArrayList(element);
    return this.channelTransformer.getCache().containsKey(_cacheKey);
  }

  protected boolean _memoryElementIsUsed(final Label element) {
    final ArrayList<?> _cacheKey = CollectionLiterals.<Label>newArrayList(element);
    return this.labelTransformer.getCache().containsKey(_cacheKey);
  }

  protected boolean _memoryElementIsUsed(final ModeLabel element) {
    final ArrayList<?> _cacheKey = CollectionLiterals.<ModeLabel>newArrayList(element);
    return this.modeLabelTransformer.getCache().containsKey(_cacheKey);
  }

  protected boolean _memoryElementIsUsed(final AbstractMemoryElement element) {
    return false;
  }

  protected void _toH(final Label label, final Memory memory) {
    final String moduleName = this.getModuleName(label, memory);
    boolean _bufferExists = this.outputBuffer.bufferExists("INC", moduleName);
    boolean _not = (!_bufferExists);
    if (_not) {
      this.outputBuffer.appendTo("INC", moduleName, "#include \"Common.h\"\n");
      this.outputBuffer.appendTo("INC", moduleName, "#include \"MappingModel/MappingModel.h\"\n");
    }
    String _call = this.getCall(label, memory);
    String _plus = ("void " + _call);
    String _plus_1 = (_plus + "();\n");
    this.outputBuffer.appendTo("INC", moduleName, _plus_1);
  }

  protected void _toCpp(final Label label, final Memory memory, final long address) {
    final String moduleName = this.getModuleName(label, memory);
    this.outputBuffer.appendTo("SRC", moduleName, (("#include \"" + moduleName) + ".h\"\n"));
    final TranslationUnit tuLabel = this.labelTransformer.transform(label);
    final TranslationUnit tuMemory = this.memoryTransformer.transform(memory);
    String _module = tuLabel.getModule();
    String _plus = ("#include \"" + _module);
    String _plus_1 = (_plus + ".h\"\n");
    this.outputBuffer.appendTo("SRC", moduleName, _plus_1);
    String _module_1 = tuMemory.getModule();
    String _plus_2 = ("#include \"" + _module_1);
    String _plus_3 = (_plus_2 + ".h\"\n");
    this.outputBuffer.appendTo("SRC", moduleName, _plus_3);
    String _call = this.getCall(label, memory);
    String _call_1 = tuLabel.getCall();
    String _call_2 = tuMemory.getCall();
    String _plus_4 = (_call_2 + "()");
    this.outputBuffer.appendTo("SRC", moduleName, 
      this.getImplementation(_call, _call_1, _plus_4, Long.valueOf(address)));
  }

  protected void _toH(final Channel channel, final Memory memory) {
    final String moduleName = this.getModuleName(channel, memory);
    boolean _bufferExists = this.outputBuffer.bufferExists("INC", moduleName);
    boolean _not = (!_bufferExists);
    if (_not) {
      this.outputBuffer.appendTo("INC", moduleName, "#include \"Common.h\"\n");
      this.outputBuffer.appendTo("INC", moduleName, "#include \"MappingModel/MappingModel.h\"\n");
    }
    String _call = this.getCall(channel, memory);
    String _plus = ("void " + _call);
    String _plus_1 = (_plus + "();\n");
    this.outputBuffer.appendTo("INC", moduleName, _plus_1);
  }

  protected void _toCpp(final Channel channel, final Memory memory, final long address) {
    final String moduleName = this.getModuleName(channel, memory);
    final TranslationUnit tuChannel = this.channelTransformer.transform(channel);
    final TranslationUnit tuMemory = this.memoryTransformer.transform(memory);
    this.outputBuffer.appendTo("SRC", moduleName, (("#include \"" + moduleName) + ".h\"\n"));
    String _module = tuMemory.getModule();
    String _plus = ("#include \"" + _module);
    String _plus_1 = (_plus + ".h\"\n");
    this.outputBuffer.appendTo("SRC", moduleName, _plus_1);
    String _module_1 = tuChannel.getModule();
    String _plus_2 = ("#include \"" + _module_1);
    String _plus_3 = (_plus_2 + ".h\"\n");
    this.outputBuffer.appendTo("SRC", moduleName, _plus_3);
    String _call = this.getCall(channel, memory);
    String _call_1 = tuChannel.getCall();
    String _call_2 = tuMemory.getCall();
    String _plus_4 = (_call_2 + "()");
    this.outputBuffer.appendTo("SRC", moduleName, 
      this.getImplementation(_call, _call_1, _plus_4, Long.valueOf(address)));
  }

  protected void _toH(final ModeLabel modeModeLabel, final Memory memory) {
    final String moduleName = this.getModuleName(modeModeLabel, memory);
    boolean _bufferExists = this.outputBuffer.bufferExists("INC", moduleName);
    boolean _not = (!_bufferExists);
    if (_not) {
      this.outputBuffer.appendTo("INC", moduleName, "#include \"Common.h\"\n");
      this.outputBuffer.appendTo("INC", moduleName, "#include \"MappingModel/MappingModel.h\"\n");
    }
    String _call = this.getCall(modeModeLabel, memory);
    String _plus = ("void " + _call);
    String _plus_1 = (_plus + "();\n");
    this.outputBuffer.appendTo("INC", moduleName, _plus_1);
  }

  protected void _toCpp(final ModeLabel modeModeLabel, final Memory memory, final long address) {
    final String moduleName = this.getModuleName(modeModeLabel, memory);
    final TranslationUnit tuModeLabel = this.modeLabelTransformer.transform(modeModeLabel);
    final TranslationUnit tuMemory = this.memoryTransformer.transform(memory);
    this.outputBuffer.appendTo("SRC", moduleName, (("#include \"" + moduleName) + ".h\"\n"));
    String _module = tuMemory.getModule();
    String _plus = ("#include \"" + _module);
    String _plus_1 = (_plus + ".h\"\n");
    this.outputBuffer.appendTo("SRC", moduleName, _plus_1);
    String _module_1 = tuModeLabel.getModule();
    String _plus_2 = ("#include \"" + _module_1);
    String _plus_3 = (_plus_2 + ".h\"\n");
    this.outputBuffer.appendTo("SRC", moduleName, _plus_3);
    String _call = this.getCall(modeModeLabel, memory);
    String _call_1 = tuModeLabel.getCall();
    String _call_2 = tuMemory.getCall();
    String _plus_4 = (_call_2 + "()");
    this.outputBuffer.appendTo("SRC", moduleName, 
      this.getImplementation(_call, _call_1, _plus_4, Long.valueOf(address)));
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transformInternal;
  }

  public boolean memoryElementIsUsed(final AbstractMemoryElement element) {
    if (element instanceof Channel) {
      return _memoryElementIsUsed((Channel)element);
    } else if (element instanceof Label) {
      return _memoryElementIsUsed((Label)element);
    } else if (element instanceof ModeLabel) {
      return _memoryElementIsUsed((ModeLabel)element);
    } else if (element != null) {
      return _memoryElementIsUsed(element);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(element).toString());
    }
  }

  public void toH(final EObject channel, final Memory memory) {
    if (channel instanceof Channel) {
      _toH((Channel)channel, memory);
      return;
    } else if (channel instanceof Label) {
      _toH((Label)channel, memory);
      return;
    } else if (channel instanceof ModeLabel) {
      _toH((ModeLabel)channel, memory);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(channel, memory).toString());
    }
  }

  public void toCpp(final EObject channel, final Memory memory, final long address) {
    if (channel instanceof Channel) {
      _toCpp((Channel)channel, memory, address);
      return;
    } else if (channel instanceof Label) {
      _toCpp((Label)channel, memory, address);
      return;
    } else if (channel instanceof ModeLabel) {
      _toCpp((ModeLabel)channel, memory, address);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(channel, memory, address).toString());
    }
  }
}
