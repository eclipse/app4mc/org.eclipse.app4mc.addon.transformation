/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import java.util.Collection;
import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.PortType;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class HwPortsTransformer {
  public static String getName(final HwPort port) {
    return port.getQualifiedName().replace(".", "__");
  }

  public static CharSequence initialize(final String parentName, final Collection<HwPort> ports) {
    StringConcatenation _builder = new StringConcatenation();
    {
      for(final HwPort port : ports) {
        final String fullname = HwPortsTransformer.getName(port);
        _builder.newLineIfNotEmpty();
        {
          PortType _portType = port.getPortType();
          boolean _tripleEquals = (_portType == PortType.INITIATOR);
          if (_tripleEquals) {
            _builder.append(parentName);
            _builder.append("->addInitPort(\"");
            _builder.append(fullname);
            _builder.append("\");");
            _builder.newLineIfNotEmpty();
          } else {
            PortType _portType_1 = port.getPortType();
            boolean _tripleEquals_1 = (_portType_1 == PortType.RESPONDER);
            if (_tripleEquals_1) {
              _builder.append(parentName);
              _builder.append("->addTargetPort(\"");
              _builder.append(fullname);
              _builder.append("\");");
              _builder.newLineIfNotEmpty();
            } else {
              _builder.append("//port type not set for port ");
              String _qualifiedName = port.getQualifiedName();
              _builder.append(_qualifiedName);
              _builder.append(" on module ");
              _builder.append(parentName);
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
    }
    return _builder;
  }
}
