/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class OperatingSystemTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private TaskSchedulerTransformer taskSchedulerTransformer;

  private static String getModulePath(final OperatingSystem os) {
    if ((os != null)) {
      String _modulePath = OsModelTransformer.getModulePath();
      String _plus = (_modulePath + "/operatingSystems/");
      String _name = OperatingSystemTransformer.getName(os);
      return (_plus + _name);
    } else {
      return OsModelTransformer.getModulePath();
    }
  }

  protected static String getCall(final OperatingSystem os) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_OperatingSystem_");
    String _name = OperatingSystemTransformer.getName(os);
    _builder.append(_name);
    return _builder.toString();
  }

  private static String getName(final OperatingSystem os) {
    return os.getName();
  }

  public TranslationUnit transform(final OperatingSystem os) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(os);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = OperatingSystemTransformer.getModulePath(os);
      String _call = OperatingSystemTransformer.getCall(os);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, os);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final OperatingSystem os) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(os));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(os));
  }

  private String toH(final OperatingSystem os) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"OSModel.h\"");
    _builder.newLine();
    _builder.append("//include model elements");
    _builder.newLine();
    _builder.append("void ");
    String _call = OperatingSystemTransformer.getCall(os);
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  private String toCpp(final OperatingSystem os) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("#include \"");
    String _modulePath = OperatingSystemTransformer.getModulePath(os);
    _builder.append(_modulePath, "\t");
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    {
      EList<TaskScheduler> _taskSchedulers = os.getTaskSchedulers();
      for(final TaskScheduler ts : _taskSchedulers) {
        _builder.append("\t");
        _builder.append("#include \"");
        String _module = this.taskSchedulerTransformer.transform(ts).getModule();
        _builder.append(_module, "\t");
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* OS */");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("void ");
    String _call = OperatingSystemTransformer.getCall(os);
    _builder.append(_call, "\t");
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    {
      EList<TaskScheduler> _taskSchedulers_1 = os.getTaskSchedulers();
      for(final TaskScheduler ts_1 : _taskSchedulers_1) {
        _builder.append("\t\t");
        String _call_1 = this.taskSchedulerTransformer.transform(ts_1).getCall();
        _builder.append(_call_1, "\t\t");
        _builder.append("();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
