/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccessEnum;
import org.eclipse.app4mc.amalthea.model.WaitingBehaviour;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.SemaphoreTransformer;
import org.eclipse.xtend2.lib.StringConcatenation;

@Singleton
@SuppressWarnings("all")
public class SemaphoreAccessTransformer extends BaseTransformer {
  @Inject
  private SemaphoreTransformer semaphoreTransformer;

  public void transform(final SemaphoreAccess access, final AgiContainerBuffer content) {
    final TranslationUnit tuSemaphore = this.semaphoreTransformer.transform(access.getSemaphore());
    content.addInclude(tuSemaphore.getModule());
    content.addAsActivityGraphItem(this.transformSemaphoreAccess(tuSemaphore.getCall(), this.getAccessType(access), 
      this.getWaitingBehaviorType(access)));
  }

  private String getAccessType(final SemaphoreAccess access) {
    String _switchResult = null;
    SemaphoreAccessEnum _access = access.getAccess();
    if (_access != null) {
      switch (_access) {
        case REQUEST:
          _switchResult = "request";
          break;
        case RELEASE:
          _switchResult = "release";
          break;
        default:
          SemaphoreAccessEnum _access_1 = access.getAccess();
          String _plus = ("Unsupported semaphore access type: " + _access_1);
          throw new IllegalArgumentException(_plus);
      }
    } else {
      SemaphoreAccessEnum _access_1 = access.getAccess();
      String _plus = ("Unsupported semaphore access type: " + _access_1);
      throw new IllegalArgumentException(_plus);
    }
    return _switchResult;
  }

  private String getWaitingBehaviorType(final SemaphoreAccess access) {
    String _switchResult = null;
    WaitingBehaviour _waitingBehaviour = access.getWaitingBehaviour();
    if (_waitingBehaviour != null) {
      switch (_waitingBehaviour) {
        case ACTIVE:
          _switchResult = "active";
          break;
        default:
          WaitingBehaviour _waitingBehaviour_1 = access.getWaitingBehaviour();
          String _plus = ("Unsupported waiting behavior for semaphore access: " + _waitingBehaviour_1);
          throw new IllegalArgumentException(_plus);
      }
    } else {
      WaitingBehaviour _waitingBehaviour_1 = access.getWaitingBehaviour();
      String _plus = ("Unsupported waiting behavior for semaphore access: " + _waitingBehaviour_1);
      throw new IllegalArgumentException(_plus);
    }
    return _switchResult;
  }

  private String transformSemaphoreAccess(final String call, final String accessType, final String waitingBehaviorType) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<SemaphoreAccess>({");
    _builder.append(call);
    _builder.append(", SemaphoreAccessType::");
    _builder.append(accessType);
    _builder.append(", app4mc::WaitingBehaviour::");
    _builder.append(waitingBehaviorType);
    _builder.append("})");
    return _builder.toString();
  }
}
