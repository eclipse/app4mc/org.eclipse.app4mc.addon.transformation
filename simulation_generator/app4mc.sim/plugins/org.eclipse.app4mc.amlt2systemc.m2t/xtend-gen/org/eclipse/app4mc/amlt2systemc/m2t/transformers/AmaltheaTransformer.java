/**
 * Copyright (c) 2019-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.EventModel;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.event.EventModelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.HwModelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.MemoryTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.ProcessingUnitTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping.MappingModelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.OsModelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli.StimuliModelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.SWModelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@Singleton
@SuppressWarnings("all")
public class AmaltheaTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private SWModelTransformer swModelTransformer;

  @Inject
  private OsModelTransformer osModelTransformer;

  @Inject
  private MappingModelTransformer mappingModelTransformer;

  @Inject
  private HwModelTransformer hwModelTransformer;

  @Inject
  private EventModelTransformer eventModelTransformer;

  @Inject
  private StimuliModelTransformer stimuliModelTransformer;

  @Inject
  private ProcessingUnitTransformer processingUnitTransformer;

  @Inject
  private MemoryTransformer memoryTransformer;

  @Inject
  private SessionLogger logger;

  public static String getModulePath() {
    return "amaltheaTop";
  }

  public String getModuleName() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/amalthea");
  }

  public void transform(final Amalthea[] amaltheas) {
    if (((amaltheas == null) || ((List<Amalthea>)Conversions.doWrapArray(amaltheas)).isEmpty())) {
      this.logger.error("Input Amalthea model(s) invalid (received null).");
      return;
    }
    this.adjustModel(amaltheas[0]);
    final Function1<Amalthea, HWModel> _function = (Amalthea it) -> {
      return it.getHwModel();
    };
    this.hwModelTransformer.transform(((HWModel[])Conversions.unwrapArray(ListExtensions.<Amalthea, HWModel>map(((List<Amalthea>)Conversions.doWrapArray(amaltheas)), _function), HWModel.class)));
    final Function1<Amalthea, SWModel> _function_1 = (Amalthea it) -> {
      return it.getSwModel();
    };
    this.swModelTransformer.transform(((SWModel[])Conversions.unwrapArray(ListExtensions.<Amalthea, SWModel>map(((List<Amalthea>)Conversions.doWrapArray(amaltheas)), _function_1), SWModel.class)));
    final Function1<Amalthea, MappingModel> _function_2 = (Amalthea it) -> {
      return it.getMappingModel();
    };
    this.mappingModelTransformer.transform(((MappingModel[])Conversions.unwrapArray(ListExtensions.<Amalthea, MappingModel>map(((List<Amalthea>)Conversions.doWrapArray(amaltheas)), _function_2), MappingModel.class)));
    final Function1<Amalthea, OSModel> _function_3 = (Amalthea it) -> {
      return it.getOsModel();
    };
    this.osModelTransformer.transform(((OSModel[])Conversions.unwrapArray(ListExtensions.<Amalthea, OSModel>map(((List<Amalthea>)Conversions.doWrapArray(amaltheas)), _function_3), OSModel.class)));
    final Function1<Amalthea, EventModel> _function_4 = (Amalthea it) -> {
      return it.getEventModel();
    };
    this.eventModelTransformer.transform(((EventModel[])Conversions.unwrapArray(ListExtensions.<Amalthea, EventModel>map(((List<Amalthea>)Conversions.doWrapArray(amaltheas)), _function_4), EventModel.class)));
    final Function1<Amalthea, StimuliModel> _function_5 = (Amalthea it) -> {
      return it.getStimuliModel();
    };
    this.stimuliModelTransformer.transform(((StimuliModel[])Conversions.unwrapArray(ListExtensions.<Amalthea, StimuliModel>map(((List<Amalthea>)Conversions.doWrapArray(amaltheas)), _function_5), StimuliModel.class)));
    this.outputBuffer.appendTo("INC", this.getModuleName(), this.toH());
    this.outputBuffer.appendTo("SRC", this.getModuleName(), this.toCpp());
    String _modulePath = AmaltheaTransformer.getModulePath();
    String _plus = (_modulePath + "/CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus, this.getCMake());
    this.outputBuffer.appendTo("OTHER", "/build.bat", this.getBuildScript());
    this.outputBuffer.appendTo("OTHER", "/run.bat", this.getRunScript());
  }

  private void adjustModel(final Amalthea amalthea) {
    ModelUtil.getOrCreateSwModel(amalthea);
    ModelUtil.getOrCreateOsModel(amalthea);
    ModelUtil.getOrCreateHwModel(amalthea);
    ModelUtil.getOrCreateMappingModel(amalthea);
    ModelUtil.getOrCreateEventModel(amalthea);
    ModelUtil.getOrCreateStimuliModel(amalthea);
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("//top level header ");
    _builder.newLine();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.append("#include \"Tracer/TracerFactory.h\"");
    _builder.newLine();
    _builder.append("#include \"SimParamParser.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("//include model elements\t");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.hwModelTransformer.getCache().values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("#include \"");
        String _module = obj.getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.swModelTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("#include \"");
        String _module_1 = obj_1.getModule();
        _builder.append(_module_1);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_2 = TuSort.byModule(this.osModelTransformer.getCache().values());
      for(final TranslationUnit obj_2 : _byModule_2) {
        _builder.append("#include \"");
        String _module_2 = obj_2.getModule();
        _builder.append(_module_2);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_3 = TuSort.byModule(this.mappingModelTransformer.getCache().values());
      for(final TranslationUnit obj_3 : _byModule_3) {
        _builder.append("#include \"");
        String _module_3 = obj_3.getModule();
        _builder.append(_module_3);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_4 = TuSort.byModule(this.eventModelTransformer.getCache().values());
      for(final TranslationUnit obj_4 : _byModule_4) {
        _builder.append("#include \"");
        String _module_4 = obj_4.getModule();
        _builder.append(_module_4);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_5 = TuSort.byModule(this.stimuliModelTransformer.getCache().values());
      for(final TranslationUnit obj_5 : _byModule_5) {
        _builder.append("#include \"");
        String _module_5 = obj_5.getModule();
        _builder.append(_module_5);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.newLine();
    _builder.append("//include processing units and memories for tracing -----");
    _builder.newLine();
    {
      Collection<TranslationUnit> _values = this.processingUnitTransformer.getCache().values();
      for(final TranslationUnit tu : _values) {
        _builder.append("#include \"");
        String _module_6 = tu.getModule();
        _builder.append(_module_6);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Collection<TranslationUnit> _values_1 = this.memoryTransformer.getCache().values();
      for(final TranslationUnit tu_1 : _values_1) {
        _builder.append("#include \"");
        String _module_7 = tu_1.getModule();
        _builder.append(_module_7);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("// tracing includes -----------------");
    _builder.newLine();
    _builder.newLine();
    _builder.append("INITIALIZE_EASYLOGGINGPP;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("int sc_main(int argc, char *argv[]) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//static code ----------------------");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("sc_core::sc_set_time_resolution(1.0,sc_core::SC_NS);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//throw error if object already exists, default behavior will rename second object and merely issue a warning (W505)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("sc_core::sc_report_handler::set_actions(\"object already exists\",sc_core::SC_THROW);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("START_EASYLOGGINGPP(argc, argv);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("SimParam params;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("SimParamParser::parse(argc, argv, params);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//end static code ------------------\t\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("VLOG(0) << \"build simulation model\";");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* Hardware */\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//always initialize hardware model first, as it relies on ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//hierarchical top->down construction, cross-references ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//from other models\' instantiation might interfere with that pattern ");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_6 = TuSort.byModule(this.hwModelTransformer.getCache().values());
      for(final TranslationUnit obj_6 : _byModule_6) {
        _builder.append("\t");
        _builder.append(obj_6.call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("/* Software */");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_7 = TuSort.byModule(this.swModelTransformer.getCache().values());
      for(final TranslationUnit obj_7 : _byModule_7) {
        _builder.append("\t");
        _builder.append(obj_7.call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("/* OS */");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_8 = TuSort.byModule(this.osModelTransformer.getCache().values());
      for(final TranslationUnit obj_8 : _byModule_8) {
        _builder.append("\t");
        _builder.append(obj_8.call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("/* Mapping */");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_9 = TuSort.byModule(this.mappingModelTransformer.getCache().values());
      for(final TranslationUnit obj_9 : _byModule_9) {
        _builder.append("\t");
        _builder.append(obj_9.call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("/* Event */");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_10 = TuSort.byModule(this.eventModelTransformer.getCache().values());
      for(final TranslationUnit obj_10 : _byModule_10) {
        _builder.append("\t");
        _builder.append(obj_10.call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("/* Stimuli */");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_11 = TuSort.byModule(this.stimuliModelTransformer.getCache().values());
      for(final TranslationUnit obj_11 : _byModule_11) {
        _builder.append("\t");
        _builder.append(obj_11.call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("VLOG(0) << \"simulation model complete\";");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//static code ----------------------");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("auto vcdPath = TraceManager::createTraceFilePath(\"trace\").string();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("sc_core::sc_trace_file *tf = sc_core::sc_create_vcd_trace_file(vcdPath.c_str());");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//trace processing units and memories -----");
    _builder.newLine();
    {
      Collection<TranslationUnit> _values_2 = this.processingUnitTransformer.getCache().values();
      for(final TranslationUnit tu_2 : _values_2) {
        _builder.append("\t");
        _builder.append("sc_trace(tf, &*");
        _builder.append(tu_2.call, "\t");
        _builder.append("(), \"");
        String _replaceAll = tu_2.getModule().replaceAll("/", "_");
        _builder.append(_replaceAll, "\t");
        _builder.append("\");");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Collection<TranslationUnit> _values_3 = this.memoryTransformer.getCache().values();
      for(final TranslationUnit tu_3 : _values_3) {
        _builder.append("\t");
        _builder.append("sc_trace(tf, &*");
        _builder.append(tu_3.call, "\t");
        _builder.append("(), \"");
        String _replaceAll_1 = tu_3.getModule().replaceAll("/", "_");
        _builder.append(_replaceAll_1, "\t");
        _builder.append("\");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//run simulation");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("VLOG(0) << \"starting simulation\";");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("sc_core::sc_start(params.simulationTimeInMs, sc_core::SC_MS);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("catch (sc_core::sc_report e) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("const char* file = e.get_file_name();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("const int line = e.get_line_number();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("const char* msg = e.get_msg();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("VLOG(0) << msg << \"\\nin file: \" << file << \"\\nline: \" << line;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return -1;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("sc_core::sc_close_vcd_trace_file(tf);");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("VLOG(0) << \"simulation done \";");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return 0;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("include_directories(${PROJECT_SOURCE_DIR})");
    _builder.newLine();
    _builder.newLine();
    _builder.append("add_executable (");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName, "\t");
    _builder.append(".cpp\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t ");
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("target_link_libraries(");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append(" app4mc.sim_lib)");
    _builder.newLineIfNotEmpty();
    _builder.append("target_include_directories(");
    String _property_2 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_2);
    _builder.append(" PUBLIC app4mc.sim_lib)");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("add_subdirectory (\"./hwModel\")");
    _builder.newLine();
    _builder.append("add_subdirectory (\"./swModel\")");
    _builder.newLine();
    _builder.append("add_subdirectory (\"./mapModel\") ");
    _builder.newLine();
    _builder.append("add_subdirectory (\"./eventModel\")");
    _builder.newLine();
    _builder.append("add_subdirectory (\"./stimuliModel\")");
    _builder.newLine();
    _builder.append("add_subdirectory (\"./osModel\")");
    _builder.newLine();
    _builder.newLine();
    {
      int _parseInt = Integer.parseInt(super.getProperty("CmakeUnityBuildTUPerCore", "0"));
      boolean _greaterThan = (_parseInt > 0);
      if (_greaterThan) {
        _builder.append("# set the unity-batch-size to a Processorcount dependend value, for more efficient compiling");
        _builder.newLine();
        _builder.append("if(COMMAND calc_unity_batch_size)");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("calc_unity_batch_size(");
        String _property_3 = this.getProperty(PropertyKeys.PROJECT_NAME);
        _builder.append(_property_3, "\t");
        _builder.append(" ");
        String _property_4 = super.getProperty("CmakeUnityBuildTUPerCore");
        _builder.append(_property_4, "\t");
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("endif()");
        _builder.newLine();
      }
    }
    _builder.newLine();
    _builder.append("# use /bigobj to ensure big models to build with MSVC (e.g. FMTV WATERS Challenge 2017) ");
    _builder.newLine();
    _builder.append("if(MSVC)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("add_definitions(/bigobj)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("message(\"Set /bigobj for big object files in MSVC\")");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.append("if(MINGW)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("set(CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} -Wa,-mbig-obj\")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("message(\"Set -Wa,-mbig-obj for big object files in MINGW\")");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    _builder.append("add_custom_target(model_execution ");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("COMMAND ");
    String _property_5 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_5, "              ");
    _builder.append(" -t ");
    String _simulationDuration = this.getSimulationDuration();
    _builder.append(_simulationDuration, "              ");
    _builder.append(" -r ");
    String _traceFormats = this.getTraceFormats();
    _builder.append(_traceFormats, "              ");
    _builder.append(" -o ");
    String _replace = this.getTracePath().toString().replace("\\", "/");
    _builder.append(_replace, "              ");
    _builder.newLineIfNotEmpty();
    _builder.append("              ");
    _builder.append("WORKING_DIRECTORY \"${MODEL_WORKING_DIR}\"");
    _builder.newLine();
    _builder.append("  ");
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("install(");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("TARGETS ");
    String _property_6 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_6, "    ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("RUNTIME DESTINATION ${PROJECT_SOURCE_DIR}/bin/)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# create a project name independend link into bin folder");
    _builder.newLine();
    _builder.append("install(CODE \"file(CREATE_LINK \\\"${PROJECT_SOURCE_DIR}/bin/");
    String _property_7 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_7);
    _builder.append("${CMAKE_EXECUTABLE_SUFFIX}\\\" \\\"${PROJECT_SOURCE_DIR}/bin/model${CMAKE_EXECUTABLE_SUFFIX}\\\" COPY_ON_ERROR SYMBOLIC)\")\t");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String getBuildScript() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@ECHO OFF");
    _builder.newLine();
    _builder.newLine();
    _builder.append("SET doClean=0");
    _builder.newLine();
    _builder.append("SET cleanCmd=cmake --build ./build --config Debug --target clean");
    _builder.newLine();
    _builder.newLine();
    _builder.append("SET doConfigure=0");
    _builder.newLine();
    _builder.append("SET configCmd=cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -H./ -B./build");
    _builder.newLine();
    _builder.newLine();
    _builder.append("SET doBuild=0");
    _builder.newLine();
    _builder.append("SET buildCmd=cmake --build ./build --config Debug --target install");
    _builder.newLine();
    _builder.newLine();
    _builder.append("IF \"%1\"==\"\" (GOTO :help)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("::Loop through command line arguments");
    _builder.newLine();
    _builder.append(":loop");
    _builder.newLine();
    _builder.append("IF NOT \"%1\"==\"\" (");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("If \"%1\"==\"--clean\" (SET doClean=1)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("If \"%1\"==\"--configure\" (SET doConfigure=1)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("If \"%1\"==\"--build\" (SET doBuild=1)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("If \"%1\"==\"--help\" (GOTO :help)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("SHIFT");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("GOTO :loop");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append(":: cleaning");
    _builder.newLine();
    _builder.append("IF \"%doClean%\"==\"1\" (");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ECHO [proc] Build clean: Removing subdirectories \\build, \\lib, and \\bin");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("IF EXIST .\\build ( ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("DEL /f /S /Q .\\build 1>nul ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("RD /S /Q .\\build");
    _builder.newLine();
    _builder.append("\t");
    _builder.append(")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("IF EXIST .\\lib ( ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("DEL /f /S /Q .\\lib 1>nul");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("RD /S /Q .\\lib ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append(")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("IF EXIST .\\bin ( ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("DEL /f /S /Q .\\bin 1>nul");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("RD /S /Q .\\bin ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append(")");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append(":: configuring");
    _builder.newLine();
    _builder.append("IF \"%doConfigure%\"==\"1\" (");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ECHO [proc] Configure all: %configCmd%");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("%configCmd% )");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append(":: building");
    _builder.newLine();
    _builder.append("IF \"%doBuild%\"==\"1\" (");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ECHO [proc] Build all: %buildCmd%");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("%buildCmd% )");
    _builder.newLine();
    _builder.newLine();
    _builder.append("GOTO :EOF");
    _builder.newLine();
    _builder.newLine();
    _builder.append(":help");
    _builder.newLine();
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.append("ECHO Usage: build [--clean] [--configure] [--build] [--help]");
    _builder.newLine();
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.append("ECHO At least one command option must be passed.");
    _builder.newLine();
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.append("ECHO Options:");
    _builder.newLine();
    _builder.append("ECHO    --clean         performs build clean");
    _builder.newLine();
    _builder.append("ECHO    --configure     configures the simulation project");
    _builder.newLine();
    _builder.append("ECHO    --build         builds the simulation project");
    _builder.newLine();
    _builder.append("ECHO    --help          shows command help");
    _builder.newLine();
    _builder.append("ECHO.");
    _builder.newLine();
    _builder.newLine();
    _builder.append(":EOF");
    _builder.newLine();
    return _builder.toString();
  }

  public String getRunScript() {
    StringConcatenation _builder = new StringConcatenation();
    final String executableName = super.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@ECHO OFF");
    _builder.newLine();
    _builder.newLine();
    _builder.append("SET runCmd=.\\build\\");
    String _modulePath = AmaltheaTransformer.getModulePath();
    _builder.append(_modulePath);
    _builder.append("\\Debug\\");
    _builder.append(executableName);
    _builder.append(".exe -t ");
    String _simulationDuration = this.getSimulationDuration();
    _builder.append(_simulationDuration);
    _builder.append(" -r ");
    String _traceFormats = this.getTraceFormats();
    _builder.append(_traceFormats);
    _builder.append(" -o ");
    String _tracePath = this.getTracePath();
    _builder.append(_tracePath);
    _builder.newLineIfNotEmpty();
    _builder.append("ECHO [proc] Run simulation: %runCmd%");
    _builder.newLine();
    _builder.append("%runCmd% ");
    _builder.newLine();
    return _builder.toString();
  }

  private String getTraceFormats() {
    String result = "";
    boolean useDefault = true;
    PropertyKeys.TraceType[] _values = PropertyKeys.TraceType.values();
    for (final PropertyKeys.TraceType type : _values) {
      boolean _parseBoolean = Boolean.parseBoolean(super.getProperty(type.toString(), "false"));
      if (_parseBoolean) {
        String _result = result;
        String _xifexpression = null;
        if (useDefault) {
          _xifexpression = "";
        } else {
          _xifexpression = " ";
        }
        String _name = type.getName();
        String _plus = (_xifexpression + _name);
        result = (_result + _plus);
        useDefault = false;
      }
    }
    String _xifexpression_1 = null;
    if (useDefault) {
      _xifexpression_1 = PropertyKeys.TraceType.BTF.getName();
    } else {
      _xifexpression_1 = result;
    }
    return _xifexpression_1;
  }

  private String getSimulationDuration() {
    return super.getProperty(PropertyKeys.SIM_DURATION_IN_MS, "1000");
  }

  private String getTracePath() {
    return Paths.get(super.getProperty(PropertyKeys.TRACE_FOLDER, "./traces")).toString();
  }
}
