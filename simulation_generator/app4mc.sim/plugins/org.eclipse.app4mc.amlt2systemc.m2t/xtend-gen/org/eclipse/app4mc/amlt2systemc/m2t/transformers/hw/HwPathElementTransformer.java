/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.ConnectionHandler;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwPathElement;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;

@SuppressWarnings("all")
public class HwPathElementTransformer extends BaseTransformer {
  @Inject
  private ConnectionHandlerTransformer connectionHandlerTransformer;

  @Inject
  private HwConnectionTransformer hwConnectionTransformer;

  @Inject
  private ProcessingUnitTransformer processingUnitTransformer;

  protected TranslationUnit _transform(final ConnectionHandler elem) {
    return this.connectionHandlerTransformer.transform(elem);
  }

  protected TranslationUnit _transform(final HwConnection elem) {
    return this.hwConnectionTransformer.transform(elem);
  }

  protected TranslationUnit _transform(final ProcessingUnit elem) {
    return this.processingUnitTransformer.transform(elem);
  }

  protected TranslationUnit _transform(final HwPathElement elem) {
    return new TranslationUnit("/*HwPathElement not yet supported*/", "/*HwPathElement not yet supported*/");
  }

  public TranslationUnit transform(final HwPathElement elem) {
    if (elem instanceof ConnectionHandler) {
      return _transform((ConnectionHandler)elem);
    } else if (elem instanceof ProcessingUnit) {
      return _transform((ProcessingUnit)elem);
    } else if (elem instanceof HwConnection) {
      return _transform((HwConnection)elem);
    } else if (elem != null) {
      return _transform(elem);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(elem).toString());
    }
  }
}
