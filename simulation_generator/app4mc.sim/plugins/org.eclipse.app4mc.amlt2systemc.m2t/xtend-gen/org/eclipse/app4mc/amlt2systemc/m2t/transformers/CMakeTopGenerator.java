/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class CMakeTopGenerator extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  public Class<Boolean> transform() {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList();
    final Class<Boolean> _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      _result = boolean.class;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result);
    return _result;
  }

  private final HashMap<ArrayList<?>, Class<Boolean>> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final Class<Boolean> it) {
    this.outputBuffer.appendTo("OTHER", "CMakeLists.txt", this.getContent());
  }

  private String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: Top level CMake project file.");
    _builder.newLine();
    _builder.append("# Perform a global configuration and include subprojects.");
    _builder.newLine();
    _builder.append("#");
    _builder.newLine();
    _builder.append("cmake_minimum_required (VERSION 3.20)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("project (");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("# leave redundant settings below in place (visual studio seems to default to c++ 11 if statements below are missing)");
    _builder.newLine();
    _builder.append("set (CMAKE_CXX_STANDARD 17 CACHE STRING");
    _builder.newLine();
    _builder.append("     ");
    _builder.append("\"C++ standard to build all targets.\")");
    _builder.newLine();
    _builder.append("set (CMAKE_CXX_STANDARD_REQUIRED ON CACHE BOOL");
    _builder.newLine();
    _builder.append("     ");
    _builder.append("\"The with CMAKE_CXX_STANDARD selected C++ standard is a requirement.\")");
    _builder.newLine();
    _builder.append("mark_as_advanced (CMAKE_CXX_STANDARD_REQUIRED)");
    _builder.newLine();
    _builder.newLine();
    final String app4mc_loc = this.getProperty(PropertyKeys.APP4MCSIM_FOLDER, "$ENV{APP4MCSIM_LOC}").replace("\\", "//");
    _builder.newLineIfNotEmpty();
    _builder.append("set(APP4MCSIM_LOC ");
    _builder.append(app4mc_loc);
    _builder.append(" CACHE PATH \"Path for App4mc.sim library\")");
    _builder.newLineIfNotEmpty();
    _builder.append("set(MODEL_WORKING_DIR \".\" CACHE PATH \"Working directory for build system based execution\")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("if(NOT EXISTS ${APP4MCSIM_LOC})");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("message(FATAL_ERROR \"APP4MCSIM_LOC needs to be configured (either as CMakeParameter or environment variable) with app4mc.sim source location\")");
    _builder.newLine();
    _builder.append("endif()");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# include subprojects");
    _builder.newLine();
    _builder.append("add_subdirectory (${APP4MCSIM_LOC} \"./app4mc.sim\" EXCLUDE_FROM_ALL)");
    _builder.newLine();
    _builder.append("add_subdirectory(./");
    String _modulePath = AmaltheaTransformer.getModulePath();
    _builder.append(_modulePath);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
}
