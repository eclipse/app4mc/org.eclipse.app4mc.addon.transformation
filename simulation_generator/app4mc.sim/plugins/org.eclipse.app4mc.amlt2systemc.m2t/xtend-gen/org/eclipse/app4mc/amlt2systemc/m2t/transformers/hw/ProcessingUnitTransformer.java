/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class ProcessingUnitTransformer extends HwModuleTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private FrequencyDomainTransformer frequencyDomainTransformer;

  @Inject
  private ProcessingUnitDefinitionTransformer processingUnitDefinitionTransformer;

  @Inject
  private HwAccessElementTransformer hwAccessElementTransformer;

  public TranslationUnit transform(final ProcessingUnit pu) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(pu);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(pu);
      String _call = this.getCall(pu);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, pu);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit tu, final ProcessingUnit pu) {
    ProcessingUnitDefinition _definition = pu.getDefinition();
    boolean _tripleEquals = (_definition == null);
    if (_tripleEquals) {
      String _qualifiedName = pu.getQualifiedName();
      String _plus = ("Processing unit " + _qualifiedName);
      String _plus_1 = (_plus + " is not associated with a processing unit definition.");
      throw new RuntimeException(_plus_1);
    }
    FrequencyDomain _frequencyDomain = pu.getFrequencyDomain();
    boolean _tripleEquals_1 = (_frequencyDomain == null);
    if (_tripleEquals_1) {
      String _qualifiedName_1 = pu.getQualifiedName();
      String _plus_2 = ("Processing unit " + _qualifiedName_1);
      String _plus_3 = (_plus_2 + " is not associated with a frequency domain.");
      throw new RuntimeException(_plus_3);
    }
    this.outputBuffer.appendTo("INC", tu.getModule(), this.toH(pu));
    this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(pu));
  }

  private String toH(final ProcessingUnit pu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("//framework");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.append("std::shared_ptr<ProcessingUnit> ");
    String _call = this.getCall(pu);
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final ProcessingUnit pu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(pu);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    String _parentInclude = this.getParentInclude(pu);
    _builder.append(_parentInclude);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      FrequencyDomain _frequencyDomain = pu.getFrequencyDomain();
      boolean _tripleNotEquals = (_frequencyDomain != null);
      if (_tripleNotEquals) {
        _builder.append("#include \"");
        String _module = this.frequencyDomainTransformer.transform(pu.getFrequencyDomain()).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      ProcessingUnitDefinition _definition = pu.getDefinition();
      boolean _tripleNotEquals_1 = (_definition != null);
      if (_tripleNotEquals_1) {
        _builder.append("#include \"");
        String _module_1 = this.processingUnitDefinitionTransformer.transform(pu.getDefinition()).getModule();
        _builder.append(_module_1);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<HwAccessElement> _accessElements = pu.getAccessElements();
      for(final HwAccessElement accessElement : _accessElements) {
        _builder.append("#include \"");
        String _module_2 = this.hwAccessElementTransformer.transform(accessElement).getModule();
        _builder.append(_module_2);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    CharSequence _includesForConnections = this.getIncludesForConnections(pu);
    _builder.append(_includesForConnections);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    final String name = this.getName(pu);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<ProcessingUnit> ");
    String _name = this.getName(pu);
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.append("//for usage in structures");
    _builder.newLine();
    _builder.append("std::shared_ptr<ProcessingUnit> ");
    String _call = this.getCall(pu);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("auto parent = ");
    String _parentGetter = this.getParentGetter(pu);
    _builder.append(_parentGetter, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = this.getName(pu);
    _builder.append(_name_1, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _name_2 = this.getName(pu);
    _builder.append(_name_2, "\t\t");
    _builder.append(" = parent->createModule<ProcessingUnit>(\"");
    _builder.append(name, "\t\t");
    _builder.append("\", *");
    String _call_1 = this.processingUnitDefinitionTransformer.transform(pu.getDefinition()).getCall();
    _builder.append(_call_1, "\t\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _name_3 = this.getName(pu);
    _builder.append(_name_3, "\t\t");
    _builder.append("->clock_period= ");
    String _call_2 = this.frequencyDomainTransformer.transform(pu.getFrequencyDomain()).getCall();
    _builder.append(_call_2, "\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//ports & connections");
    _builder.newLine();
    _builder.append("\t\t");
    CharSequence _setPortsAndConnections = this.setPortsAndConnections(pu);
    _builder.append(_setPortsAndConnections, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//access elements");
    _builder.newLine();
    {
      EList<HwAccessElement> _accessElements_1 = pu.getAccessElements();
      for(final HwAccessElement accessElement_1 : _accessElements_1) {
        _builder.append("\t\t");
        String _name_4 = this.getName(pu);
        _builder.append(_name_4, "\t\t");
        _builder.append("->addHWAccessElement(");
        String _call_3 = this.hwAccessElementTransformer.transform(accessElement_1).getCall();
        _builder.append(_call_3, "\t\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
