/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.SemaphoreType;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class SemaphoreTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  public String getName(final Semaphore semaphore) {
    return semaphore.getName();
  }

  private String getModulePath(final Semaphore semaphore) {
    String _modulePath = OsModelTransformer.getModulePath();
    String _plus = (_modulePath + "/semaphores/");
    String _name = this.getName(semaphore);
    return (_plus + _name);
  }

  private String getCall(final Semaphore semaphore) {
    String _name = this.getName(semaphore);
    String _plus = ("get_" + _name);
    return (_plus + "()");
  }

  public TranslationUnit transform(final Semaphore semaphore) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(semaphore);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(semaphore);
      String _call = this.getCall(semaphore);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, semaphore);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final Semaphore semaphore) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(semaphore));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(semaphore));
  }

  private String toH(final Semaphore semaphore) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Semaphore.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("std::shared_ptr<Semaphore> ");
    String _call = this.getCall(semaphore);
    _builder.append(_call);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final Semaphore semaphore) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(semaphore);
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(semaphore);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<Semaphore> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<Semaphore> ");
    String _call = this.getCall(semaphore);
    _builder.append(_call);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append(" == nullptr){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//instantiate");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = Semaphore::createSemaphore(\"");
    _builder.append(name, "\t\t");
    _builder.append("\", ");
    int _maxValue = semaphore.getMaxValue();
    _builder.append(_maxValue, "\t\t");
    _builder.append(", ");
    int _initialValue = semaphore.getInitialValue();
    _builder.append(_initialValue, "\t\t");
    _builder.append(", SemaphoreType::");
    String _semaphoreType = this.getSemaphoreType(semaphore);
    _builder.append(_semaphoreType, "\t\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  private String getSemaphoreType(final Semaphore semaphore) {
    String _switchResult = null;
    SemaphoreType _semaphoreType = semaphore.getSemaphoreType();
    if (_semaphoreType != null) {
      switch (_semaphoreType) {
        case COUNTING_SEMAPHORE:
          _switchResult = "CountingSemaphore";
          break;
        case SPINLOCK:
          _switchResult = "Spinlock";
          break;
        default:
          SemaphoreType _semaphoreType_1 = semaphore.getSemaphoreType();
          String _plus = ("Unsupported semaphore type: " + _semaphoreType_1);
          throw new IllegalArgumentException(_plus);
      }
    } else {
      SemaphoreType _semaphoreType_1 = semaphore.getSemaphoreType();
      String _plus = ("Unsupported semaphore type: " + _semaphoreType_1);
      throw new IllegalArgumentException(_plus);
    }
    return _switchResult;
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
