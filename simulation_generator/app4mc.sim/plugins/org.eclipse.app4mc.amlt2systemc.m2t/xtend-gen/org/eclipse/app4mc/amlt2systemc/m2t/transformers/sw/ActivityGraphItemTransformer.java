/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.ChannelAccess;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.WhileLoop;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;

@SuppressWarnings("all")
public class ActivityGraphItemTransformer extends BaseTransformer {
  @Inject
  private SwitchTransformer switchTransformer;

  @Inject
  private RunnableTransformer runnableTransformer;

  @Inject
  private TicksTransformer ticksTransformer;

  @Inject
  private ChannelAccessTransformer channelAccessTransformer;

  @Inject
  private LabelAccessTransformer labelAccessTransformer;

  @Inject
  private ModeLabelAccessTransformer modeLabelAccessTransformer;

  @Inject
  private InterProcessTriggerTransformer interProcessTriggerTransformer;

  @Inject
  private WhileLoopTransformer whileLoopTransformer;

  @Inject
  private SemaphoreAccessTransformer semaphoreAccrssTransformer;

  @Inject
  private GroupTransformer groupTransformer;

  protected void _transform(final ActivityGraphItem activityGraphItem, final AgiContainerBuffer containerContent) {
    String _string = activityGraphItem.getClass().toString();
    String _plus = ("//WARNING: no transformation for activity item " + _string);
    containerContent.addBody(_plus);
    String _string_1 = activityGraphItem.getClass().toString();
    String _plus_1 = ("//WARNING: no transformation for activity item " + _string_1);
    throw new UnsupportedOperationException(_plus_1);
  }

  protected void _transform(final RunnableCall runnableCall, final AgiContainerBuffer containerContent) {
    final TranslationUnit tuRunnable = this.runnableTransformer.transform(runnableCall.getRunnable());
    containerContent.addInclude(tuRunnable.getModule());
    String _call = tuRunnable.getCall();
    String _plus = ("<RunnableCall>({" + _call);
    String _plus_1 = (_plus + "})");
    containerContent.addAsActivityGraphItem(_plus_1);
  }

  protected void _transform(final Ticks ticks, final AgiContainerBuffer containerContent) {
    String _transform = this.ticksTransformer.transform(ticks);
    String _plus = ("<Ticks>({" + _transform);
    String _plus_1 = (_plus + "})");
    containerContent.addAsActivityGraphItem(_plus_1);
  }

  protected void _transform(final ChannelAccess channelAccess, final AgiContainerBuffer containerContent) {
    this.channelAccessTransformer.transform(channelAccess, containerContent);
  }

  protected void _transform(final LabelAccess labelAccess, final AgiContainerBuffer containerContent) {
    this.labelAccessTransformer.transform(labelAccess, containerContent);
  }

  protected void _transform(final ModeLabelAccess modeLabelAccess, final AgiContainerBuffer containerContent) {
    this.modeLabelAccessTransformer.transform(modeLabelAccess, containerContent);
  }

  protected void _transform(final InterProcessTrigger interProcessTrigger, final AgiContainerBuffer containerContent) {
    this.interProcessTriggerTransformer.transform(interProcessTrigger, containerContent);
  }

  protected void _transform(final Switch _switch, final AgiContainerBuffer containerContent) {
    this.switchTransformer.transform(_switch, containerContent);
  }

  protected void _transform(final WhileLoop whileLoop, final AgiContainerBuffer containerContent) {
    this.whileLoopTransformer.transform(whileLoop, containerContent);
  }

  protected void _transform(final SemaphoreAccess semaphoreAccess, final AgiContainerBuffer containerContent) {
    this.semaphoreAccrssTransformer.transform(semaphoreAccess, containerContent);
  }

  protected void _transform(final Group group, final AgiContainerBuffer containerContent) {
    this.groupTransformer.transform(group, containerContent);
  }

  public void transform(final ActivityGraphItem labelAccess, final AgiContainerBuffer containerContent) {
    if (labelAccess instanceof LabelAccess) {
      _transform((LabelAccess)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof Ticks) {
      _transform((Ticks)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof ChannelAccess) {
      _transform((ChannelAccess)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof Group) {
      _transform((Group)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof InterProcessTrigger) {
      _transform((InterProcessTrigger)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof ModeLabelAccess) {
      _transform((ModeLabelAccess)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof RunnableCall) {
      _transform((RunnableCall)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof SemaphoreAccess) {
      _transform((SemaphoreAccess)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof Switch) {
      _transform((Switch)labelAccess, containerContent);
      return;
    } else if (labelAccess instanceof WhileLoop) {
      _transform((WhileLoop)labelAccess, containerContent);
      return;
    } else if (labelAccess != null) {
      _transform(labelAccess, containerContent);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(labelAccess, containerContent).toString());
    }
  }
}
