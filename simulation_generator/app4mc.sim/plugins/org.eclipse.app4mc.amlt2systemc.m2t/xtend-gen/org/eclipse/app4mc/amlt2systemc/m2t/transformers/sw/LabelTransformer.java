/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class LabelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  private String getLabelName(final Label label) {
    String uniqueName = label.getUniqueName();
    uniqueName = uniqueName.replaceAll("\\?", "_");
    uniqueName = uniqueName.replace("=", "_");
    uniqueName = uniqueName.replace("/", "_");
    return uniqueName;
  }

  private String getModuleName(final Label label) {
    String _modulePath = SWModelTransformer.getModulePath();
    String _plus = (_modulePath + "/labels/");
    String _labelName = this.getLabelName(label);
    return (_plus + _labelName);
  }

  private String getFunctionDef(final Label _label) {
    String _labelName = this.getLabelName(_label);
    String _plus = ("get_" + _labelName);
    return (_plus + "()");
  }

  public TranslationUnit transform(final Label label) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(label);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName(label);
      String _functionDef = this.getFunctionDef(label);
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, label);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final Label label) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(label));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(label));
  }

  private String toH(final Label label) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"DataLabel.h\"");
    _builder.newLine();
    _builder.append("#include \"SoftwareModel.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("std::shared_ptr<DataLabel> ");
    String _functionDef = this.getFunctionDef(label);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final Label label) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getLabelName(label);
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName(label);
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<DataLabel> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<DataLabel>  ");
    String _functionDef = this.getFunctionDef(label);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append(" == nullptr){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = std::make_shared<DataLabel>(\"");
    _builder.append(name, "\t\t");
    _builder.append("\", ");
    long _sizeInByte = this.getSizeInByte(label);
    _builder.append(_sizeInByte, "\t\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  private long getSizeInByte(final Label label) {
    final DataSize size = label.getSize();
    if ((size == null)) {
      return 0;
    } else {
      return size.getNumberBytes();
    }
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
