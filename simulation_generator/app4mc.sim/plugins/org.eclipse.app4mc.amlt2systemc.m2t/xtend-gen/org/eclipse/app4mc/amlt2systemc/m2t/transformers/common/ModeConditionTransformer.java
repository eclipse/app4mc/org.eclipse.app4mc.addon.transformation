/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common;

import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.Mode;
import org.eclipse.app4mc.amalthea.model.ModeLabelCondition;
import org.eclipse.app4mc.amalthea.model.ModeValueCondition;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ModeLabelTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ModeTransformer;
import org.eclipse.emf.ecore.EObject;

/**
 * Target code example
 * 
 * 	SwitchEntry caseA("case A");
 * 	caseA.addCondition(modelabel1, lita);
 * 	caseA.addActivityGraphItem<ModeLabelAccess>({modelabel1, litb});
 * 	caseA.addActivityGraphItem<RunnableCall>({ra});
 * 
 * 	ConditionConjunction conj_caseDandD;// = caseDandD.createConjunction();
 * 	conj_caseDandD.addCondition(modelabel1, litd);
 * 	conj_caseDandD.addCondition(modelabel2, litd)
 * 
 * 	usage in Switch:
 * 	switch.addEntry(caseA); OR switch.setDefaultEntry(caseA);
 */
@SuppressWarnings("all")
public class ModeConditionTransformer extends BaseTransformer {
  @Inject
  private ModeLabelTransformer modeLabelTransformer;

  @Inject
  private ModeTransformer modeTransformer;

  protected void _createCondition(final String parent, final ModeLabelCondition condition, final ConditionTransformer.ConditionBuffer content) {
    final TranslationUnit tuLabel1 = this.modeLabelTransformer.transform(condition.getLabel1());
    final TranslationUnit tuLabel2 = this.modeLabelTransformer.transform(condition.getLabel2());
    content.addInclude(tuLabel1.getModule());
    content.addInclude(tuLabel2.getModule());
    String _call = tuLabel1.getCall();
    String _plus = (_call + ", ");
    String _call_1 = tuLabel2.getCall();
    String _plus_1 = (_plus + _call_1);
    String _plus_2 = (_plus_1 + ", ");
    String _relationalOperator = ConditionTransformer.getRelationalOperator(condition.getRelation());
    String _plus_3 = (_plus_2 + _relationalOperator);
    content.addCondition(_plus_3);
  }

  protected void _createCondition(final String parent, final ModeValueCondition condition, final ConditionTransformer.ConditionBuffer content) {
    final TranslationUnit tuLabel = this.modeLabelTransformer.transform(condition.getLabel());
    content.addInclude(tuLabel.getModule());
    Mode _mode = condition.getLabel().getMode();
    if ((_mode instanceof EnumMode)) {
      final TranslationUnit tuMode = this.modeTransformer.transform(condition.getLabel().getMode());
      content.addInclude(tuMode.getModule());
      String _call = tuLabel.getCall();
      String _plus = (_call + ", ");
      String _call_1 = tuMode.getCall();
      String _plus_1 = (_plus + _call_1);
      String _plus_2 = (_plus_1 + "->getLiteral(\"");
      String _value = condition.getValue();
      String _plus_3 = (_plus_2 + _value);
      String _plus_4 = (_plus_3 + "\"), ");
      String _relationalOperator = ConditionTransformer.getRelationalOperator(condition.getRelation());
      String _plus_5 = (_plus_4 + _relationalOperator);
      content.addCondition(_plus_5);
    } else {
      String _call_2 = tuLabel.getCall();
      String _plus_6 = (_call_2 + ", ");
      String _value_1 = condition.getValue();
      String _plus_7 = (_plus_6 + _value_1);
      String _plus_8 = (_plus_7 + ", ");
      String _relationalOperator_1 = ConditionTransformer.getRelationalOperator(condition.getRelation());
      String _plus_9 = (_plus_8 + _relationalOperator_1);
      content.addCondition(_plus_9);
    }
  }

  public void createCondition(final String parent, final EObject condition, final ConditionTransformer.ConditionBuffer content) {
    if (condition instanceof ModeLabelCondition) {
      _createCondition(parent, (ModeLabelCondition)condition, content);
      return;
    } else if (condition instanceof ModeValueCondition) {
      _createCondition(parent, (ModeValueCondition)condition, content);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(parent, condition, content).toString());
    }
  }
}
