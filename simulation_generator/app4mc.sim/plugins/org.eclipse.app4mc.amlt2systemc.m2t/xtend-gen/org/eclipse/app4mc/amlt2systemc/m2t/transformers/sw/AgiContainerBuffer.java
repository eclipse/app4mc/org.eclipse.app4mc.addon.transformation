/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;

@SuppressWarnings("all")
public class AgiContainerBuffer {
  private final LinkedHashSet<String> activityItemIncludes;

  private final LinkedList<String> activityItemCalls;

  public final String module;

  public final String instanceName;

  public final boolean parentIsPointer;

  public AgiContainerBuffer(final String _name, final String _module, final boolean _parentIsPointer) {
    this.instanceName = _name;
    this.module = _module;
    this.parentIsPointer = _parentIsPointer;
    LinkedHashSet<String> _linkedHashSet = new LinkedHashSet<String>();
    this.activityItemIncludes = _linkedHashSet;
    LinkedList<String> _linkedList = new LinkedList<String>();
    this.activityItemCalls = _linkedList;
  }

  public AgiContainerBuffer(final AgiContainerBuffer other) {
    this.instanceName = other.instanceName;
    this.module = other.module;
    this.parentIsPointer = other.parentIsPointer;
    this.activityItemIncludes = other.activityItemIncludes;
    this.activityItemCalls = other.activityItemCalls;
  }

  public boolean addAll(final AgiContainerBuffer other) {
    boolean _xblockexpression = false;
    {
      this.activityItemIncludes.addAll(other.activityItemIncludes);
      _xblockexpression = this.activityItemCalls.addAll(other.activityItemCalls);
    }
    return _xblockexpression;
  }

  public boolean addAll(final ConditionTransformer.ConditionBuffer other) {
    boolean _xifexpression = false;
    if ((other != null)) {
      boolean _xblockexpression = false;
      {
        this.activityItemIncludes.addAll(other.getIncludes());
        _xblockexpression = this.activityItemCalls.addAll(0, other.getConditions());
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }

  public LinkedHashSet<String> getIncludes() {
    return this.activityItemIncludes;
  }

  public LinkedList<String> getSource() {
    return this.activityItemCalls;
  }

  public void addInclude(final String includePath) {
    this.activityItemIncludes.add((("#include \"" + includePath) + ".h\""));
  }

  public void addAsActivityGraphItem(final String itemConstructor) {
    if (this.parentIsPointer) {
      this.activityItemCalls.add((((this.instanceName + "->addActivityGraphItem") + itemConstructor) + ";"));
    } else {
      this.activityItemCalls.add((((this.instanceName + ".addActivityGraphItem") + itemConstructor) + ";"));
    }
  }

  public void addBody(final String body) {
    this.activityItemCalls.add(body);
  }
}
