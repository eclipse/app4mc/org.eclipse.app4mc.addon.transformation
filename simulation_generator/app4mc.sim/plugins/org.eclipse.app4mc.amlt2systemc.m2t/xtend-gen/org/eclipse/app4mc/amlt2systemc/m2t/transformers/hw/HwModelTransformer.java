/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;

@Singleton
@SuppressWarnings("all")
public class HwModelTransformer extends BaseTransformer {
  protected static String getModulePath() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/hwModel");
  }

  protected String getModuleName() {
    String _modulePath = HwModelTransformer.getModulePath();
    return (_modulePath + "/hwModel");
  }

  private String getFunctionDef() {
    return "init_hwModel()";
  }

  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private HwStructureTransformer hwStructureTransformer;

  @Inject
  private FrequencyDomainTransformer frequencyDomainTransformer;

  @Inject
  private ProcessingUnitDefinitionTransformer processingUnitDefinitionTransformer;

  @Inject
  private ProcessingUnitTransformer processingUnitTransformer;

  @Inject
  private MemoryTransformer memoryTransformer;

  @Inject
  private ConnectionHandlerTransformer connectionHandlerTransformer;

  @Inject
  private HwConnectionTransformer hwConnectionTransformer;

  @Inject
  private HwFeatureTransformer hwFeatureTransformer;

  @Inject
  private HwAccessElementTransformer hwAccessElementTransformer;

  private final LinkedList<HwStructure> topLevelStructures = new LinkedList<HwStructure>();

  public TranslationUnit transform(final HWModel[] hwModels) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(hwModels);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName();
      String _functionDef = this.getFunctionDef();
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, hwModels);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final HWModel[] hwModels) {
    final Consumer<HWModel> _function = (HWModel hwModel) -> {
      final Consumer<HwStructure> _function_1 = (HwStructure structure) -> {
        this.topLevelStructures.add(structure);
        this.hwStructureTransformer.transform(structure);
      };
      hwModel.getStructures().forEach(_function_1);
    };
    ((List<HWModel>)Conversions.doWrapArray(hwModels)).forEach(_function);
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH());
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp());
    final String buf = this.getCMake(it.getModule());
    String _modulePath = HwModelTransformer.getModulePath();
    String _plus = (_modulePath + "/CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus, buf);
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.append("//include model elements");
    _builder.newLine();
    {
      for(final HwStructure structure : this.topLevelStructures) {
        _builder.append("#include \"");
        String _module = this.hwStructureTransformer.transform(structure).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName);
    _builder.append(".h\"\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/* Hardware */");
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//INITIALIZE ONLY TOP LEVEL STRUCTURES HERE! ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//Substructures will be initialized automatically by their parent.");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//this is necessary to ensure that hierarchical hardware structures are built top->down");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//due to app4mc.sim\'s create methods, which are widely spread in the hardware model ");
    _builder.newLine();
    {
      for(final HwStructure structure : this.topLevelStructures) {
        _builder.append("\t");
        String _call = this.hwStructureTransformer.transform(structure).getCall();
        _builder.append(_call, "\t");
        _builder.append("();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake(final String thisModule) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: CMake project for HwModel of \"");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append("\".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Add sources of HwModel");
    _builder.newLine();
    _builder.append("target_sources (");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append("  PRIVATE");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("#features ");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.hwFeatureTransformer.getCache().values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#definitions");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.processingUnitDefinitionTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_1 = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module_1);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#domains\t");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_2 = TuSort.byModule(this.frequencyDomainTransformer.getCache().values());
      for(final TranslationUnit obj_2 : _byModule_2) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_2 = ((TranslationUnit) obj_2).getModule();
        _builder.append(_module_2);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#hw structure");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_3 = TuSort.byModule(this.hwStructureTransformer.getCache().values());
      for(final TranslationUnit obj_3 : _byModule_3) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_3 = ((TranslationUnit) obj_3).getModule();
        _builder.append(_module_3);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("# modules");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_4 = TuSort.byModule(this.memoryTransformer.getCache().values());
      for(final TranslationUnit obj_4 : _byModule_4) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_4 = ((TranslationUnit) obj_4).getModule();
        _builder.append(_module_4);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_5 = TuSort.byModule(this.processingUnitTransformer.getCache().values());
      for(final TranslationUnit obj_5 : _byModule_5) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_5 = ((TranslationUnit) obj_5).getModule();
        _builder.append(_module_5);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_6 = TuSort.byModule(this.connectionHandlerTransformer.getCache().values());
      for(final TranslationUnit obj_6 : _byModule_6) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_6 = ((TranslationUnit) obj_6).getModule();
        _builder.append(_module_6);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#connections");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_7 = TuSort.byModule(this.hwConnectionTransformer.getCache().values());
      for(final TranslationUnit obj_7 : _byModule_7) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_7 = ((TranslationUnit) obj_7).getModule();
        _builder.append(_module_7);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("#HW access elements");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_8 = TuSort.byModule(this.hwAccessElementTransformer.getCache().values());
      for(final TranslationUnit obj_8 : _byModule_8) {
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_8 = ((TranslationUnit) obj_8).getModule();
        _builder.append(_module_8);
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    _builder.append(thisModule);
    _builder.append(".cpp\")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
