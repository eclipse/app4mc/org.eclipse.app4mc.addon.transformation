/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.MinAvgMaxStatistic;
import org.eclipse.app4mc.amalthea.model.SingleValueStatistic;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;

@SuppressWarnings("all")
public class NumericStatisticTransformer extends BaseTransformer {
  protected String _transform(final MinAvgMaxStatistic minAvgMaxStatistic) {
    return "memory access statistic not yet supported";
  }

  protected String _transform(final SingleValueStatistic singleValueStatistic) {
    return "memory access statistic not yet supported";
  }

  protected String _transform(final Object stat) {
    if ((stat == null)) {
      return "1";
    }
    return "1";
  }

  public String transform(final Object minAvgMaxStatistic) {
    if (minAvgMaxStatistic instanceof MinAvgMaxStatistic) {
      return _transform((MinAvgMaxStatistic)minAvgMaxStatistic);
    } else if (minAvgMaxStatistic instanceof SingleValueStatistic) {
      return _transform((SingleValueStatistic)minAvgMaxStatistic);
    } else if (minAvgMaxStatistic != null) {
      return _transform(minAvgMaxStatistic);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(minAvgMaxStatistic).toString());
    }
  }
}
