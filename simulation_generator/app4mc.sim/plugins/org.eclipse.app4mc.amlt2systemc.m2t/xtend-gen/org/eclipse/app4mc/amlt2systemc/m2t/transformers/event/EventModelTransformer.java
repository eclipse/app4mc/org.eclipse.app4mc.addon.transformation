/**
 * Copyright (c) 2019 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.event;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.ChannelEvent;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventModel;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;

@Singleton
@SuppressWarnings("all")
public class EventModelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private EventTransformer eventsDispatcher;

  protected static String getModulePath() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/eventModel");
  }

  private String getModuleName() {
    String _modulePath = EventModelTransformer.getModulePath();
    return (_modulePath + "/eventModel");
  }

  private String getFunctionDef() {
    return "init_eventModel()";
  }

  public TranslationUnit transform(final EventModel[] eventModels) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(eventModels);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName();
      String _functionDef = this.getFunctionDef();
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, eventModels);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final EventModel[] eventModels) {
    final Consumer<EventModel> _function = (EventModel eventModel) -> {
      final Consumer<Event> _function_1 = (Event it_1) -> {
        this.eventsDispatcher.transform(it_1);
      };
      eventModel.getEvents().forEach(_function_1);
    };
    ((List<EventModel>)Conversions.doWrapArray(eventModels)).forEach(_function);
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH());
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp());
    String _modulePath = EventModelTransformer.getModulePath();
    String _plus = (_modulePath + "/CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus, this.getCMake(it.getModule()));
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"SoftwareModel.h\"");
    _builder.newLine();
    _builder.append("#include \"Channel.h\"");
    _builder.newLine();
    _builder.append("#include \"ChannelAccess.h\"");
    _builder.newLine();
    _builder.append("#include \"PriorityScheduler.h\"");
    _builder.newLine();
    _builder.append("//include model elements»");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.eventsDispatcher.getCache(ChannelEvent.class).values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("#include \"");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName);
    _builder.append(".h\"\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/* Software */");
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append("{\t\t");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byCall = TuSort.byCall(this.eventsDispatcher.getCache(ChannelEvent.class).values());
      for(final TranslationUnit obj : _byCall) {
        _builder.append("\t");
        String _call = ((TranslationUnit) obj).getCall();
        _builder.append(_call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake(final String thisModule) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: CMake project for EventModel of \"");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append("\".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("# Add sources of EventModel");
    _builder.newLine();
    _builder.append("target_sources (");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append("  PRIVATE");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.eventsDispatcher.getCache(ChannelEvent.class).values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    _builder.append(thisModule, "\t");
    _builder.append(".cpp\")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
