/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.DataRate;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryDefinition;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.DataRateUtil;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class MemoryTransformer extends HwModuleTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private FrequencyDomainTransformer frequencyDomainTransformer;

  public TranslationUnit transform(final Memory memory) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(memory);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(memory);
      String _call = this.getCall(memory);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, memory);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit tu, final Memory memory) {
    FrequencyDomain _frequencyDomain = memory.getFrequencyDomain();
    boolean _tripleEquals = (_frequencyDomain == null);
    if (_tripleEquals) {
      String _qualifiedName = memory.getQualifiedName();
      String _plus = ("Memory " + _qualifiedName);
      String _plus_1 = (_plus + " is not associated with a frequency domain.");
      throw new RuntimeException(_plus_1);
    }
    this.outputBuffer.appendTo("INC", tu.getModule(), this.toH(memory));
    this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(memory));
  }

  public String toH(final Memory module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("//framework");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"\t\t");
    _builder.newLine();
    _builder.append("std::shared_ptr<Memory> ");
    String _call = this.getCall(module);
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String toCpp(final Memory module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(module);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    String _parentInclude = this.getParentInclude(module);
    _builder.append(_parentInclude);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      FrequencyDomain _frequencyDomain = module.getFrequencyDomain();
      boolean _tripleNotEquals = (_frequencyDomain != null);
      if (_tripleNotEquals) {
        _builder.append("#include \"");
        String _module = this.frequencyDomainTransformer.transform(module.getFrequencyDomain()).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    CharSequence _includesForConnections = this.getIncludesForConnections(module);
    _builder.append(_includesForConnections);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    final String name = this.getName(module);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<Memory> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("//for usage in structures");
    _builder.newLine();
    _builder.append("std::shared_ptr<Memory> ");
    String _call = this.getCall(module);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("auto parent = ");
    String _parentGetter = this.getParentGetter(module);
    _builder.append(_parentGetter, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    {
      MemoryDefinition _definition = null;
      if (module!=null) {
        _definition=module.getDefinition();
      }
      DataSize _size = null;
      if (_definition!=null) {
        _size=_definition.getSize();
      }
      boolean _tripleNotEquals_1 = (_size != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t\t");
        _builder.append(name, "\t\t");
        _builder.append(" = parent->createModule<Memory>(\"");
        _builder.append(name, "\t\t");
        _builder.append("\", ");
        long _numberBytes = module.getDefinition().getSize().getNumberBytes();
        _builder.append(_numberBytes, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      } else {
        _builder.append("\t\t");
        _builder.append(name, "\t\t");
        _builder.append(" = parent->createModule<Memory>(\"");
        _builder.append(name, "\t\t");
        _builder.append("\", 0);");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append("->clock_period= ");
    String _call_1 = this.frequencyDomainTransformer.transform(module.getFrequencyDomain()).getCall();
    _builder.append(_call_1, "\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    CharSequence _setPortsAndConnections = this.setPortsAndConnections(module);
    _builder.append(_setPortsAndConnections, "\t\t");
    _builder.append("\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _setAccessLatency = this.setAccessLatency(module);
    _builder.append(_setAccessLatency, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _setDataRate = this.setDataRate(module);
    _builder.append(_setDataRate, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String setAccessLatency(final Memory module) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(module);
    _builder.newLineIfNotEmpty();
    MemoryDefinition _definition = null;
    if (module!=null) {
      _definition=module.getDefinition();
    }
    IDiscreteValueDeviation _accessLatency = null;
    if (_definition!=null) {
      _accessLatency=_definition.getAccessLatency();
    }
    final IDiscreteValueDeviation deviation = _accessLatency;
    _builder.newLineIfNotEmpty();
    {
      if ((deviation != null)) {
        _builder.append(name);
        _builder.append("->setAccessLatency(");
        String _deviation = IDiscreteValueDeviationTransformer.getDeviation(deviation);
        _builder.append(_deviation);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }

  public String setDataRate(final Memory module) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(module);
    _builder.newLineIfNotEmpty();
    {
      MemoryDefinition _definition = null;
      if (module!=null) {
        _definition=module.getDefinition();
      }
      DataRate _dataRate = null;
      if (_definition!=null) {
        _dataRate=_definition.getDataRate();
      }
      boolean _tripleNotEquals = (_dataRate != null);
      if (_tripleNotEquals) {
        _builder.append(name);
        _builder.append("->setDataRate(");
        String _transform = DataRateUtil.transform(module.getDefinition().getDataRate());
        _builder.append(_transform);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
