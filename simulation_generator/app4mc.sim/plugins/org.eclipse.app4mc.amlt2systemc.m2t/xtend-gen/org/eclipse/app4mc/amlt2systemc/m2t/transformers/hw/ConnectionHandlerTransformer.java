/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.ConnectionHandler;
import org.eclipse.app4mc.amalthea.model.ConnectionHandlerDefinition;
import org.eclipse.app4mc.amalthea.model.DataRate;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.DataRateUtil;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class ConnectionHandlerTransformer extends HwModuleTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private FrequencyDomainTransformer frequencyDomainTransformer;

  public TranslationUnit transform(final ConnectionHandler connectionHandler) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(connectionHandler);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(connectionHandler);
      String _call = this.getCall(connectionHandler);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, connectionHandler);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit tu, final ConnectionHandler connectionHandler) {
    FrequencyDomain _frequencyDomain = connectionHandler.getFrequencyDomain();
    boolean _tripleEquals = (_frequencyDomain == null);
    if (_tripleEquals) {
      String _qualifiedName = connectionHandler.getQualifiedName();
      String _plus = ("ConnecionHandler " + _qualifiedName);
      String _plus_1 = (_plus + " is not associated with a frequency domain.");
      throw new RuntimeException(_plus_1);
    }
    this.outputBuffer.appendTo("INC", tu.getModule(), this.toH(connectionHandler));
    this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(connectionHandler));
  }

  public String toH(final ConnectionHandler connectionHandler) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("//framework");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("std::shared_ptr<ConnectionHandler> ");
    String _call = this.getCall(connectionHandler);
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String toCpp(final ConnectionHandler connectionHandler) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(connectionHandler);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    String _parentInclude = this.getParentInclude(connectionHandler);
    _builder.append(_parentInclude);
    _builder.newLineIfNotEmpty();
    {
      FrequencyDomain _frequencyDomain = connectionHandler.getFrequencyDomain();
      boolean _tripleNotEquals = (_frequencyDomain != null);
      if (_tripleNotEquals) {
        _builder.append("#include \"");
        String _module = this.frequencyDomainTransformer.transform(connectionHandler.getFrequencyDomain()).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    CharSequence _includesForConnections = this.getIncludesForConnections(connectionHandler);
    _builder.append(_includesForConnections);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    final String name = this.getName(connectionHandler);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<ConnectionHandler> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("//for usage in structures");
    _builder.newLine();
    _builder.append("std::shared_ptr<ConnectionHandler> ");
    String _call = this.getCall(connectionHandler);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("auto parent = ");
    String _parentGetter = this.getParentGetter(connectionHandler);
    _builder.append(_parentGetter, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" =  parent->createModule<ConnectionHandler>(\"");
    _builder.append(name, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//parameters from definition");
    _builder.newLine();
    {
      ConnectionHandlerDefinition _definition = null;
      if (connectionHandler!=null) {
        _definition=connectionHandler.getDefinition();
      }
      boolean _tripleNotEquals_1 = (_definition != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t\t");
        String _setReadLatency = this.setReadLatency(connectionHandler);
        _builder.append(_setReadLatency, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        String _setWriteLatency = this.setWriteLatency(connectionHandler);
        _builder.append(_setWriteLatency, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        String _setDataRate = this.setDataRate(connectionHandler);
        _builder.append(_setDataRate, "\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("//ports");
    _builder.newLine();
    _builder.append("\t\t");
    CharSequence _setPortsAndConnections = this.setPortsAndConnections(connectionHandler);
    _builder.append(_setPortsAndConnections, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//frequency domain");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append("->clock_period= ");
    String _call_1 = this.frequencyDomainTransformer.transform(connectionHandler.getFrequencyDomain()).getCall();
    _builder.append(_call_1, "\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String setReadLatency(final ConnectionHandler module) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(module);
    _builder.newLineIfNotEmpty();
    ConnectionHandlerDefinition _definition = null;
    if (module!=null) {
      _definition=module.getDefinition();
    }
    IDiscreteValueDeviation _readLatency = null;
    if (_definition!=null) {
      _readLatency=_definition.getReadLatency();
    }
    final IDiscreteValueDeviation deviation = _readLatency;
    _builder.newLineIfNotEmpty();
    {
      if ((deviation != null)) {
        _builder.append(name);
        _builder.append("->setReadLatency(");
        String _deviation = IDiscreteValueDeviationTransformer.getDeviation(deviation);
        _builder.append(_deviation);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }

  public String setWriteLatency(final ConnectionHandler module) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(module);
    _builder.newLineIfNotEmpty();
    ConnectionHandlerDefinition _definition = null;
    if (module!=null) {
      _definition=module.getDefinition();
    }
    IDiscreteValueDeviation _writeLatency = null;
    if (_definition!=null) {
      _writeLatency=_definition.getWriteLatency();
    }
    final IDiscreteValueDeviation deviation = _writeLatency;
    _builder.newLineIfNotEmpty();
    {
      if ((deviation != null)) {
        _builder.append(name);
        _builder.append("->setWriteLatency(");
        String _deviation = IDiscreteValueDeviationTransformer.getDeviation(deviation);
        _builder.append(_deviation);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }

  public String setDataRate(final ConnectionHandler module) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(module);
    _builder.newLineIfNotEmpty();
    {
      ConnectionHandlerDefinition _definition = null;
      if (module!=null) {
        _definition=module.getDefinition();
      }
      DataRate _dataRate = null;
      if (_definition!=null) {
        _dataRate=_definition.getDataRate();
      }
      boolean _tripleNotEquals = (_dataRate != null);
      if (_tripleNotEquals) {
        _builder.append(name);
        _builder.append("->setDataRate(");
        String _transform = DataRateUtil.transform(module.getDefinition().getDataRate());
        _builder.append(_transform);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
