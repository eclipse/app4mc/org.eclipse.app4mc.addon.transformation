/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.Mode;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.NumericMode;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class ModeLabelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private ModeTransformer modeTransformer;

  private String getModeLabelName(final ModeLabel modeLabel) {
    return modeLabel.getName();
  }

  private String getModuleName(final ModeLabel modeLabel) {
    String _modulePath = SWModelTransformer.getModulePath();
    String _plus = (_modulePath + "/modeLabels/");
    String _modeLabelName = this.getModeLabelName(modeLabel);
    return (_plus + _modeLabelName);
  }

  private String getFunctionDef(final ModeLabel _modeLabel) {
    String _modeLabelName = this.getModeLabelName(_modeLabel);
    String _plus = ("get_" + _modeLabelName);
    return (_plus + "()");
  }

  public TranslationUnit transform(final ModeLabel modeLabel) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(modeLabel);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName(modeLabel);
      String _functionDef = this.getFunctionDef(modeLabel);
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, modeLabel);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final ModeLabel modeLabel) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(modeLabel));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(modeLabel));
  }

  private String toH(final ModeLabel modeLabel) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"ModeLabel.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("std::shared_ptr<ModeLabel> ");
    String _functionDef = this.getFunctionDef(modeLabel);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final ModeLabel modeLabel) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getModeLabelName(modeLabel);
    _builder.newLineIfNotEmpty();
    final TranslationUnit modeTu = this.modeTransformer.transform(modeLabel.getMode());
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName(modeLabel);
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module = modeTu.getModule();
    _builder.append(_module);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<ModeLabel> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<ModeLabel>  ");
    String _functionDef = this.getFunctionDef(modeLabel);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append(" == nullptr){");
    _builder.newLineIfNotEmpty();
    {
      Mode _mode = modeLabel.getMode();
      if ((_mode instanceof EnumMode)) {
        {
          DataSize _size = modeLabel.getSize();
          boolean _tripleNotEquals = (_size != null);
          if (_tripleNotEquals) {
            _builder.append("\t\t");
            _builder.append(name, "\t\t");
            _builder.append(" = std::make_shared<ModeLabel>(\"");
            _builder.append(name, "\t\t");
            _builder.append("\", ");
            String _call = modeTu.getCall();
            _builder.append(_call, "\t\t");
            _builder.append(", ");
            String _call_1 = modeTu.getCall();
            _builder.append(_call_1, "\t\t");
            _builder.append("->getLiteral(\"");
            String _initialValue = modeLabel.getInitialValue();
            _builder.append(_initialValue, "\t\t");
            _builder.append("\"), ");
            long _sizeInByte = this.getSizeInByte(modeLabel);
            _builder.append(_sizeInByte, "\t\t");
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append("\t\t");
            _builder.append(name, "\t\t");
            _builder.append(" = std::make_shared<ModeLabel>(\"");
            _builder.append(name, "\t\t");
            _builder.append("\", ");
            String _call_2 = modeTu.getCall();
            _builder.append(_call_2, "\t\t");
            _builder.append(", ");
            String _call_3 = modeTu.getCall();
            _builder.append(_call_3, "\t\t");
            _builder.append("->getLiteral(\"");
            String _initialValue_1 = modeLabel.getInitialValue();
            _builder.append(_initialValue_1, "\t\t");
            _builder.append("\"));");
            _builder.newLineIfNotEmpty();
          }
        }
      } else {
        Mode _mode_1 = modeLabel.getMode();
        if ((_mode_1 instanceof NumericMode)) {
          {
            DataSize _size_1 = modeLabel.getSize();
            boolean _tripleNotEquals_1 = (_size_1 != null);
            if (_tripleNotEquals_1) {
              _builder.append("\t\t");
              _builder.append(name, "\t\t");
              _builder.append(" = std::make_shared<ModeLabel>(\"");
              _builder.append(name, "\t\t");
              _builder.append("\", ");
              String _call_4 = modeTu.getCall();
              _builder.append(_call_4, "\t\t");
              _builder.append(", ");
              String _initialValue_2 = modeLabel.getInitialValue();
              _builder.append(_initialValue_2, "\t\t");
              _builder.append(", ");
              long _sizeInByte_1 = this.getSizeInByte(modeLabel);
              _builder.append(_sizeInByte_1, "\t\t");
              _builder.append(");");
              _builder.newLineIfNotEmpty();
            } else {
              _builder.append("\t\t");
              _builder.append(name, "\t\t");
              _builder.append(" = std::make_shared<ModeLabel>(\"");
              _builder.append(name, "\t\t");
              _builder.append("\", ");
              String _call_5 = modeTu.getCall();
              _builder.append(_call_5, "\t\t");
              _builder.append(", ");
              String _initialValue_3 = modeLabel.getInitialValue();
              _builder.append(_initialValue_3, "\t\t");
              _builder.append(");");
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  public String getInitializer(final EnumMode mode, final String initialValue) {
    String _functionDef = ModeTransformer.getFunctionDef(mode);
    String _plus = (_functionDef + "->getLiteral(\"");
    String _plus_1 = (_plus + initialValue);
    return (_plus_1 + "\")");
  }

  public String getInitializer(final NumericMode mode, final String initialValue) {
    return initialValue;
  }

  private long getSizeInByte(final ModeLabel modeLabel) {
    final DataSize size = modeLabel.getSize();
    if ((size == null)) {
      return 0;
    } else {
      return size.getNumberBytes();
    }
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
