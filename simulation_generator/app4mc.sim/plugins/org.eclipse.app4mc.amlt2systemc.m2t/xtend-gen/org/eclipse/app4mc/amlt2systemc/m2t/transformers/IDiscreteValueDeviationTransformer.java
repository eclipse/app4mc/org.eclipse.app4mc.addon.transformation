/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.DiscreteValueBetaDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueBoundaries;
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
import org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueUniformDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.SamplingType;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class IDiscreteValueDeviationTransformer {
  protected static String _getDeviation(final IDiscreteValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final IDiscreteValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final IDiscreteValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviation(final DiscreteValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IDiscreteValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final DiscreteValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DiscreteValueBetaDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final DiscreteValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    double _alpha = value.getAlpha();
    _builder.append(_alpha);
    _builder.append(",");
    double _beta = value.getBeta();
    _builder.append(_beta);
    _builder.append(",");
    Long _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Long _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }

  private static String stype(final DiscreteValueBoundaries value) {
    SamplingType _samplingType = value.getSamplingType();
    if (_samplingType != null) {
      switch (_samplingType) {
        case BEST_CASE:
          return "BoundariesSamplingType::BestCase";
        case WORST_CASE:
          return "BoundariesSamplingType::WorstCase";
        case AVERAGE_CASE:
          return "BoundariesSamplingType::AverageCase";
        case CORNER_CASE:
          return "BoundariesSamplingType::CornerCase";
        case UNIFORM:
          return "BoundariesSamplingType::Uniform";
        default:
          return "BoundariesSamplingType::WorstCase";
      }
    } else {
      return "BoundariesSamplingType::WorstCase";
    }
  }

  protected static String _getDeviation(final DiscreteValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IDiscreteValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final DiscreteValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DiscreteValueBoundaries");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final DiscreteValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    String _stype = IDiscreteValueDeviationTransformer.stype(value);
    _builder.append(_stype);
    _builder.append(",");
    Long _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Long _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }

  protected static String _getDeviation(final DiscreteValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IDiscreteValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final DiscreteValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DiscreteValueConstant");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final DiscreteValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    long _value = value.getValue();
    _builder.append(_value);
    return _builder.toString();
  }

  protected static String _getDeviation(final DiscreteValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IDiscreteValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final DiscreteValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DiscreteValueGaussDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final DiscreteValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    double _mean = value.getMean();
    _builder.append(_mean);
    _builder.append(",");
    double _sd = value.getSd();
    _builder.append(_sd);
    _builder.append(",");
    Long _elvis = null;
    Long _lowerBound = value.getLowerBound();
    if (_lowerBound != null) {
      _elvis = _lowerBound;
    } else {
      _elvis = Long.valueOf(Long.MIN_VALUE);
    }
    _builder.append(_elvis);
    _builder.append(",");
    Long _elvis_1 = null;
    Long _upperBound = value.getUpperBound();
    if (_upperBound != null) {
      _elvis_1 = _upperBound;
    } else {
      _elvis_1 = Long.valueOf(Long.MAX_VALUE);
    }
    _builder.append(_elvis_1);
    return _builder.toString();
  }

  protected static String _getDeviation(final DiscreteValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IDiscreteValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final DiscreteValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DiscreteValueUniformDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final DiscreteValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    Long _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Long _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }

  protected static String _getDeviation(final DiscreteValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IDiscreteValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final DiscreteValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DiscreteValueWeibullEstimatorsDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final DiscreteValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IDiscreteValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("::findParameter(");
    Double _average = value.getAverage();
    _builder.append(_average);
    _builder.append(",");
    double _pRemainPromille = value.getPRemainPromille();
    _builder.append(_pRemainPromille);
    _builder.append(",");
    Long _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Long _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    _builder.append("),");
    Long _lowerBound_1 = value.getLowerBound();
    _builder.append(_lowerBound_1);
    _builder.append(",");
    Long _upperBound_1 = value.getUpperBound();
    _builder.append(_upperBound_1);
    return _builder.toString();
  }

  public static String getDeviation(final IDiscreteValueDeviation value) {
    if (value instanceof DiscreteValueBetaDistribution) {
      return _getDeviation((DiscreteValueBetaDistribution)value);
    } else if (value instanceof DiscreteValueBoundaries) {
      return _getDeviation((DiscreteValueBoundaries)value);
    } else if (value instanceof DiscreteValueGaussDistribution) {
      return _getDeviation((DiscreteValueGaussDistribution)value);
    } else if (value instanceof DiscreteValueUniformDistribution) {
      return _getDeviation((DiscreteValueUniformDistribution)value);
    } else if (value instanceof DiscreteValueWeibullEstimatorsDistribution) {
      return _getDeviation((DiscreteValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof DiscreteValueConstant) {
      return _getDeviation((DiscreteValueConstant)value);
    } else if (value != null) {
      return _getDeviation(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }

  public static String getDeviationTemplate(final IDiscreteValueDeviation value) {
    if (value instanceof DiscreteValueBetaDistribution) {
      return _getDeviationTemplate((DiscreteValueBetaDistribution)value);
    } else if (value instanceof DiscreteValueBoundaries) {
      return _getDeviationTemplate((DiscreteValueBoundaries)value);
    } else if (value instanceof DiscreteValueGaussDistribution) {
      return _getDeviationTemplate((DiscreteValueGaussDistribution)value);
    } else if (value instanceof DiscreteValueUniformDistribution) {
      return _getDeviationTemplate((DiscreteValueUniformDistribution)value);
    } else if (value instanceof DiscreteValueWeibullEstimatorsDistribution) {
      return _getDeviationTemplate((DiscreteValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof DiscreteValueConstant) {
      return _getDeviationTemplate((DiscreteValueConstant)value);
    } else if (value != null) {
      return _getDeviationTemplate(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }

  public static String getDeviationValue(final IDiscreteValueDeviation value) {
    if (value instanceof DiscreteValueBetaDistribution) {
      return _getDeviationValue((DiscreteValueBetaDistribution)value);
    } else if (value instanceof DiscreteValueBoundaries) {
      return _getDeviationValue((DiscreteValueBoundaries)value);
    } else if (value instanceof DiscreteValueGaussDistribution) {
      return _getDeviationValue((DiscreteValueGaussDistribution)value);
    } else if (value instanceof DiscreteValueUniformDistribution) {
      return _getDeviationValue((DiscreteValueUniformDistribution)value);
    } else if (value instanceof DiscreteValueWeibullEstimatorsDistribution) {
      return _getDeviationValue((DiscreteValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof DiscreteValueConstant) {
      return _getDeviationValue((DiscreteValueConstant)value);
    } else if (value != null) {
      return _getDeviationValue(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }
}
