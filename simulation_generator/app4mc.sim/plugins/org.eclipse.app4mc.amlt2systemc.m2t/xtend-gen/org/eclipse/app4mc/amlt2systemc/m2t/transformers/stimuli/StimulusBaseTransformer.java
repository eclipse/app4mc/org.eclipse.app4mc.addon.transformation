/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli;

import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;

@SuppressWarnings("all")
public abstract class StimulusBaseTransformer extends BaseTransformer {
  public String getName(final Stimulus stim) {
    return stim.getName();
  }

  public String getModulePath(final Stimulus stim) {
    String _modulePath = AmaltheaTransformer.getModulePath();
    String _plus = (_modulePath + "/stimuliModel/");
    String _name = this.getName(stim);
    return (_plus + _name);
  }

  public String getFunctionDef(final Stimulus stim) {
    String _name = stim.getName();
    String _plus = ("get_" + _name);
    return (_plus + "()");
  }
}
