/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common;

import com.google.inject.Inject;
import org.eclipse.app4mc.amalthea.model.ChannelFillCondition;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ChannelTransformer;

@SuppressWarnings("all")
public class ChannelFillConditionTransformer extends BaseTransformer {
  @Inject
  private ChannelTransformer channelTransformer;

  public void createCondition(final String parent, final ChannelFillCondition condition, final ConditionTransformer.ConditionBuffer content) {
    final TranslationUnit tuChannel = this.channelTransformer.transform(condition.getChannel());
    final int fillLevel = condition.getFillLevel();
    content.addInclude(tuChannel.getModule());
    String _call = tuChannel.getCall();
    String _plus = (_call + ", ");
    String _plus_1 = (_plus + Integer.valueOf(fillLevel));
    String _plus_2 = (_plus_1 + ", ");
    String _plus_3 = (_plus_2 + "RelationalOperator::GREATER_THAN");
    content.addCondition(_plus_3);
    String _call_1 = tuChannel.getCall();
    String _plus_4 = (_call_1 + ", ");
    String _plus_5 = (_plus_4 + Integer.valueOf(fillLevel));
    String _plus_6 = (_plus_5 + ", ");
    String _plus_7 = (_plus_6 + "RelationalOperator::EQUAL");
    content.addCondition(_plus_7);
  }
}
