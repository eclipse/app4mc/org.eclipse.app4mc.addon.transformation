/**
 * Copyright (c) 2019-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common;

import org.eclipse.app4mc.amalthea.model.ParameterType;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.BooleanObjectImpl;
import org.eclipse.app4mc.amalthea.model.impl.FloatObjectImpl;
import org.eclipse.app4mc.amalthea.model.impl.IntegerObjectImpl;
import org.eclipse.app4mc.amalthea.model.impl.StringObjectImpl;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil;

@SuppressWarnings("all")
public class SchedulingParameterTransformer extends BaseTransformer {
  public String createSchedParam(final String attributeeName, final String functionName, final boolean attributeeIsPointer, final SchedulingParameterDefinition schedulingParameterDefinition, final Value value) {
    String _xifexpression = null;
    if (attributeeIsPointer) {
      _xifexpression = "->";
    } else {
      _xifexpression = ".";
    }
    String _plus = (attributeeName + _xifexpression);
    String _plus_1 = (_plus + functionName);
    String _plus_2 = (_plus_1 + "(\"");
    String _name = schedulingParameterDefinition.getName();
    String _plus_3 = (_plus_2 + _name);
    String _plus_4 = (_plus_3 + "\", ");
    String _valueString = this.getValueString(schedulingParameterDefinition.getType(), value);
    String _plus_5 = (_plus_4 + _valueString);
    return (_plus_5 + ");");
  }

  private String getValueString(final ParameterType paramType, final Value value) {
    if (paramType != null) {
      switch (paramType) {
        case INTEGER:
          return Integer.valueOf(((IntegerObjectImpl) value).getValue()).toString();
        case TIME:
          return TimeUtil.transform(((Time) value)).toString();
        case BOOL:
          return Boolean.valueOf(((BooleanObjectImpl) value).isValue()).toString();
        case FLOAT:
          return Float.valueOf(((FloatObjectImpl) value).getValue()).toString();
        case STRING:
          return ((StringObjectImpl) value).getValue().toString();
        default:
          throw new IllegalArgumentException((("scheduling parameter of type " + paramType) + " is not supported."));
      }
    } else {
      throw new IllegalArgumentException((("scheduling parameter of type " + paramType) + " is not supported."));
    }
  }
}
