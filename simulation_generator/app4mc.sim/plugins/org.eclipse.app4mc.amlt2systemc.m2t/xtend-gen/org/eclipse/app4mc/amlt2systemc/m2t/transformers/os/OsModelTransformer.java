/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;

@Singleton
@SuppressWarnings("all")
public class OsModelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private OperatingSystemTransformer operatingSystemTransformer;

  @Inject
  private TaskSchedulerTransformer taskSchedulerTransformer;

  @Inject
  private SemaphoreTransformer semaphoreTransformer;

  public static String getModulePath() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/osModel");
  }

  public String getModuleName() {
    String _modulePath = OsModelTransformer.getModulePath();
    return (_modulePath + "/osModel_");
  }

  public String getFunctionDef() {
    return "init_osModel()";
  }

  public String getLibName() {
    return "osModel_lib";
  }

  public TranslationUnit transform(final OSModel[] osModels) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(osModels);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName();
      String _functionDef = this.getFunctionDef();
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, osModels);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final OSModel[] osModels) {
    final Consumer<OSModel> _function = (OSModel osModel) -> {
      final Consumer<OperatingSystem> _function_1 = (OperatingSystem os) -> {
        this.operatingSystemTransformer.transform(os);
      };
      osModel.getOperatingSystems().forEach(_function_1);
    };
    ((List<OSModel>)Conversions.doWrapArray(osModels)).forEach(_function);
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH());
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp());
    String _modulePath = OsModelTransformer.getModulePath();
    String _plus = (_modulePath + "/");
    String _plus_1 = (_plus + "CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus_1, this.getCMake());
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"OSModel.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName, "\t");
    _builder.append(".h\"\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.operatingSystemTransformer.getCache().values());
      for(final TranslationUnit os : _byModule) {
        _builder.append("\t");
        _builder.append("#include \"");
        String _module = os.getModule();
        _builder.append(_module, "\t");
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/* OS */");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef, "\t");
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.operatingSystemTransformer.getCache().values());
      for(final TranslationUnit os_1 : _byModule_1) {
        _builder.append("\t\t");
        String _call = os_1.getCall();
        _builder.append(_call, "\t\t");
        _builder.append("();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: CMake project for OsModel of \"");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append("\".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.append("# Add sources of OsModel");
    _builder.newLine();
    _builder.append("target_sources (");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append("  PRIVATE");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.taskSchedulerTransformer.getCache().values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.semaphoreTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_1 = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module_1, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      List<TranslationUnit> _byModule_2 = TuSort.byModule(this.operatingSystemTransformer.getCache().values());
      for(final TranslationUnit obj_2 : _byModule_2) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_2 = ((TranslationUnit) obj_2).getModule();
        _builder.append(_module_2, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName, "\t");
    _builder.append(".cpp\")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
