/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;

@SuppressWarnings("all")
public abstract class IActivityGraphItemContainerTransformer extends BaseTransformer {
  @Inject
  private ActivityGraphItemTransformer activityGraphItemTransformer;

  protected abstract String getContainerDescription(final IActivityGraphItemContainer container);

  protected abstract void transformContainer(final IActivityGraphItemContainer container, final AgiContainerBuffer parentContent);

  private String getStartOfContainer(final IActivityGraphItemContainer container) {
    String _containerDescription = this.getContainerDescription(container);
    String _plus = (("/*----" + "Begin ") + _containerDescription);
    return (_plus + "----*/");
  }

  private String getEndOfContainerComment(final IActivityGraphItemContainer container) {
    String _containerDescription = this.getContainerDescription(container);
    String _plus = (("/*----" + "End ") + _containerDescription);
    return (_plus + "----*/");
  }

  public final void transform(final IActivityGraphItemContainer container, final AgiContainerBuffer parentContent) {
    parentContent.addBody(this.getStartOfContainer(container));
    this.transformContainer(container, parentContent);
    parentContent.addBody(this.getEndOfContainerComment(container));
  }

  protected AgiContainerBuffer transformItems(final IActivityGraphItemContainer container, final String parentInstanceName, final String parentModule, final boolean parentIsPointer) {
    final AgiContainerBuffer containerContent = new AgiContainerBuffer(parentInstanceName, parentModule, parentIsPointer);
    if ((container == null)) {
      return containerContent;
    }
    final Consumer<ActivityGraphItem> _function = (ActivityGraphItem item) -> {
      this.activityGraphItemTransformer.transform(((ActivityGraphItem) item), containerContent);
    };
    container.getItems().forEach(_function);
    return containerContent;
  }
}
