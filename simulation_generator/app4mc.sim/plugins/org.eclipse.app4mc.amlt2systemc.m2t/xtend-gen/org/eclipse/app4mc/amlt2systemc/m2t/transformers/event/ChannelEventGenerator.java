/**
 * Copyright (c) 2019-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.event;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.app4mc.amalthea.model.ChannelEvent;
import org.eclipse.app4mc.amalthea.model.ChannelEventType;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ChannelTransformer;
import org.eclipse.xtend2.lib.StringConcatenation;

@Singleton
@SuppressWarnings("all")
public class ChannelEventGenerator {
  @Inject
  private ChannelTransformer channelTransformer;

  public static String getName(final ChannelEvent channelEvent) {
    return channelEvent.getName();
  }

  public static String getModulePath(final ChannelEvent channelEvent) {
    String _modulePath = EventModelTransformer.getModulePath();
    String _plus = (_modulePath + "/channelEvents/");
    String _name = ChannelEventGenerator.getName(channelEvent);
    return (_plus + _name);
  }

  public static String getFunctionDef(final ChannelEvent channelEvent) {
    String _name = ChannelEventGenerator.getName(channelEvent);
    String _plus = ("get_" + _name);
    return (_plus + "()");
  }

  public String toH(final ChannelEvent channelEvent) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"Event.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("//ChannelEvent----");
    _builder.newLine();
    _builder.append("std::shared_ptr<Event> ");
    String _functionDef = ChannelEventGenerator.getFunctionDef(channelEvent);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String toCpp(final ChannelEvent channelEvent) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"Channel.h\"");
    _builder.newLine();
    _builder.append("//#include \"GenericQueueAccess.h\"");
    _builder.newLine();
    _builder.append("//#include \"Deviation.h\"");
    _builder.newLine();
    _builder.append("#include \"");
    String _modulePath = ChannelEventGenerator.getModulePath(channelEvent);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module = this.channelTransformer.transform(channelEvent.getEntity()).getModule();
    _builder.append(_module);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("std::shared_ptr<Event> ");
    String _name = ChannelEventGenerator.getName(channelEvent);
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<Event>  ");
    String _functionDef = ChannelEventGenerator.getFunctionDef(channelEvent);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = ChannelEventGenerator.getName(channelEvent);
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    String _name_2 = ChannelEventGenerator.getName(channelEvent);
    _builder.append(_name_2, "\t\t");
    _builder.append(" = ");
    String _call = this.channelTransformer.transform(channelEvent.getEntity()).getCall();
    _builder.append(_call, "\t\t");
    _builder.append("->addEvent(\"");
    String _name_3 = ChannelEventGenerator.getName(channelEvent);
    _builder.append(_name_3, "\t\t");
    _builder.append("\", EventType::");
    String _type = ChannelEventGenerator.getType(channelEvent);
    _builder.append(_type, "\t\t");
    _builder.append(", StateType::DONE);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_4 = ChannelEventGenerator.getName(channelEvent);
    _builder.append(_name_4, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }

  private static String getType(final ChannelEvent channelEvent) {
    ChannelEventType _eventType = channelEvent.getEventType();
    if (_eventType != null) {
      switch (_eventType) {
        case SEND:
          return "WRITE_EVENT";
        case RECEIVE:
          return "READ_EVENT";
        default:
          return "ACCESS_EVENT";
      }
    } else {
      return "ACCESS_EVENT";
    }
  }
}
