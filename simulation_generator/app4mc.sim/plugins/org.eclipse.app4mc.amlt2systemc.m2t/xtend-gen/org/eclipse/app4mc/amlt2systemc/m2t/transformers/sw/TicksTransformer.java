/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer;

@SuppressWarnings("all")
public class TicksTransformer extends BaseTransformer {
  public String transform(final Ticks ticks) {
    IDiscreteValueDeviation _default = ticks.getDefault();
    boolean _tripleNotEquals = (_default != null);
    if (_tripleNotEquals) {
      return IDiscreteValueDeviationTransformer.getDeviation(ticks.getDefault());
    } else {
      return "";
    }
  }
}
