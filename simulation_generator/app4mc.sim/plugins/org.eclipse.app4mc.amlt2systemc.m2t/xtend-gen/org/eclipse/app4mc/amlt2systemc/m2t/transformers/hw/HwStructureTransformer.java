/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class HwStructureTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private HwModuleTransformer hwModuleTransformer;

  @Inject
  private HwConnectionTransformer hwConnectionTransformer;

  public static HwStructure getParentHwStructureOrNull(final EObject obj) {
    final EObject ret = obj.eContainer();
    if ((ret instanceof HwStructure)) {
      return ((HwStructure) ret);
    } else {
      return null;
    }
  }

  private String getModulePath(final HwStructure hwStructure) {
    if ((hwStructure != null)) {
      String _modulePath = HwModelTransformer.getModulePath();
      String _plus = (_modulePath + "/structures/");
      String _name = HwStructureTransformer.getName(hwStructure);
      return (_plus + _name);
    } else {
      return HwModelTransformer.getModulePath();
    }
  }

  protected static String getCall(final HwStructure hwStructure) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_HwStructure_");
    String _name = HwStructureTransformer.getName(hwStructure);
    _builder.append(_name);
    return _builder.toString();
  }

  private static String getName(final HwStructure hwStructure) {
    return hwStructure.getName();
  }

  public TranslationUnit transform(final HwStructure hwStructure) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(hwStructure);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(hwStructure);
      String _call = HwStructureTransformer.getCall(hwStructure);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, hwStructure);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit tu, final HwStructure hwStructure) {
    this.outputBuffer.appendTo("INC", tu.getModule(), this.toH(hwStructure, tu));
    final HwStructure containingHwStructure = HwStructureTransformer.getParentHwStructureOrNull(hwStructure);
    if ((containingHwStructure != null)) {
      this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(hwStructure, tu, this.transform(containingHwStructure)));
    } else {
      this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(hwStructure, tu));
    }
  }

  public String toH(final HwStructure hwStructure, final TranslationUnit tu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("std::shared_ptr<HwStructure> ");
    String _call = tu.getCall();
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  public String toCpp(final HwStructure hwStructure, final TranslationUnit tu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _module = tu.getModule();
    _builder.append(_module);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    {
      EList<HwStructure> _structures = hwStructure.getStructures();
      for(final HwStructure structure : _structures) {
        _builder.append("#include \"");
        String _module_1 = this.transform(structure).getModule();
        _builder.append(_module_1);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<HwModule> _modules = hwStructure.getModules();
      for(final HwModule module : _modules) {
        _builder.append("#include \"");
        String _module_2 = this.hwModuleTransformer.transform(module).getModule();
        _builder.append(_module_2);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<HwConnection> _connections = hwStructure.getConnections();
      for(final HwConnection connection : _connections) {
        _builder.append("#include \"");
        String _module_3 = this.hwConnectionTransformer.transform(connection).getModule();
        _builder.append(_module_3);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.newLine();
    final String name = HwStructureTransformer.getName(hwStructure);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<HwStructure> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("//for usage on hw model top level (eg. in flat hierarchies)");
    _builder.newLine();
    _builder.append("std::shared_ptr<HwStructure> ");
    String _call = tu.getCall();
    _builder.append(_call);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = std::make_shared<HwStructure>(\"");
    _builder.append(name, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    CharSequence _setParams = this.setParams(hwStructure);
    _builder.append(_setParams, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String toCpp(final HwStructure hwStructure, final TranslationUnit tu, final TranslationUnit parentTu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _module = tu.getModule();
    _builder.append(_module);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module_1 = parentTu.getModule();
    _builder.append(_module_1);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    {
      EList<HwStructure> _structures = hwStructure.getStructures();
      for(final HwStructure structure : _structures) {
        _builder.append("#include \"");
        String _module_2 = this.transform(structure).getModule();
        _builder.append(_module_2);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<HwModule> _modules = hwStructure.getModules();
      for(final HwModule module : _modules) {
        _builder.append("#include \"");
        String _module_3 = this.hwModuleTransformer.transform(module).getModule();
        _builder.append(_module_3);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<HwConnection> _connections = hwStructure.getConnections();
      for(final HwConnection connection : _connections) {
        _builder.append("#include \"");
        String _module_4 = this.hwConnectionTransformer.transform(connection).getModule();
        _builder.append(_module_4);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.newLine();
    final String name = HwStructureTransformer.getName(hwStructure);
    _builder.newLineIfNotEmpty();
    _builder.append("std::shared_ptr<HwStructure> ");
    _builder.append(name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.append("//for usage in structures");
    _builder.newLine();
    _builder.append("std::shared_ptr<HwStructure> ");
    String _call = tu.getCall();
    _builder.append(_call);
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("std::shared_ptr<HwStructure> parent = ");
    String _call_1 = parentTu.getCall();
    _builder.append(_call_1, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = parent->createSubStructure(\"");
    _builder.append(name, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    CharSequence _setParams = this.setParams(hwStructure);
    _builder.append(_setParams, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  private CharSequence setParams(final HwStructure hwStructure) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t\t");
    _builder.append("//Sub-Structures");
    _builder.newLine();
    {
      EList<HwStructure> _structures = hwStructure.getStructures();
      for(final HwStructure structure : _structures) {
        _builder.append("\t");
        String _call = this.transform(structure).getCall();
        _builder.append(_call, "\t");
        _builder.append("();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("//Sub-Modules");
    _builder.newLine();
    {
      EList<HwModule> _modules = hwStructure.getModules();
      for(final HwModule module : _modules) {
        String _call_1 = this.hwModuleTransformer.transform(module).getCall();
        _builder.append(_call_1);
        _builder.append("();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("//HW Ports\t");
    _builder.newLine();
    String _call_2 = HwStructureTransformer.getCall(hwStructure);
    String _plus = (_call_2 + "()");
    CharSequence _initialize = HwPortsTransformer.initialize(_plus, hwStructure.getPorts());
    _builder.append(_initialize);
    _builder.newLineIfNotEmpty();
    _builder.append("//Connections (Deferred Bindings of delegate ports)");
    _builder.newLine();
    return _builder;
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
