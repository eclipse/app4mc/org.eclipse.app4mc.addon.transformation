/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amalthea.model.SwitchDefault;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer;

/**
 * Target code example
 * 
 * SwitchEntry caseA("case A");
 * caseA.addCondition(modelabel1, lita);
 * caseA.addActivityGraphItem<ModeLabelAccess>({modelabel1, litb});
 * caseA.addActivityGraphItem<RunnableCall>({ra});
 * 
 * ConditionConjunction conj_caseDandD;// = caseDandD.createConjunction();
 * conj_caseDandD.addCondition(modelabel1, litd);
 * conj_caseDandD.addCondition(modelabel2, litd)
 * 
 * usage in Switch:
 * switch.addEntry(caseA); OR switch.setDefaultEntry(caseA);
 */
@SuppressWarnings("all")
public class SwitchEntryTransformer extends IActivityGraphItemContainerTransformer {
  @Inject
  private ConditionTransformer conditionTransformer;

  private static int defaultCnt = 0;

  private static String getName(final SwitchEntry switchEntry) {
    return switchEntry.getName();
  }

  private static String getName(final SwitchDefault switchDefault) {
    return ("switchDefault" + Integer.valueOf(SwitchEntryTransformer.defaultCnt));
  }

  private void transformContainer(final SwitchEntry switchEntry, final AgiContainerBuffer content) {
    final String name = SwitchEntryTransformer.getName(switchEntry);
    content.addBody((((("SwitchEntry " + name) + "(\"") + name) + "\");\n"));
    final ConditionTransformer.ConditionBuffer conditioContents = this.conditionTransformer.transformCondition(switchEntry.getCondition(), name, false);
    final AgiContainerBuffer activityGraphContents = this.transformItems(switchEntry, name, content.module, content.parentIsPointer);
    activityGraphContents.addAll(conditioContents);
    content.getIncludes().addAll(activityGraphContents.getIncludes());
    content.getSource().addAll(activityGraphContents.getSource());
    content.addBody((((content.instanceName + ".addEntry(") + name) + ");"));
  }

  private void transformContainer(final SwitchDefault switchDefault, final AgiContainerBuffer content) {
    final String name = SwitchEntryTransformer.getName(switchDefault);
    content.addBody((((("SwitchEntry " + name) + "(\"") + name) + "\");\n"));
    final AgiContainerBuffer activityGraphContents = this.transformItems(switchDefault, name, content.module, content.parentIsPointer);
    content.getIncludes().addAll(activityGraphContents.getIncludes());
    content.getSource().addAll(activityGraphContents.getSource());
    content.addBody((((content.instanceName + ".setDefaultEntry(") + name) + ");"));
    SwitchEntryTransformer.defaultCnt++;
  }

  @Override
  protected void transformContainer(final IActivityGraphItemContainer container, final AgiContainerBuffer parentContent) {
    if ((container instanceof SwitchEntry)) {
      this.transformContainer(((SwitchEntry) container), parentContent);
    } else {
      if ((container instanceof SwitchDefault)) {
        this.transformContainer(((SwitchDefault) container), parentContent);
      } else {
        throw new UnsupportedOperationException("Container type not supported");
      }
    }
  }

  @Override
  protected String getContainerDescription(final IActivityGraphItemContainer container) {
    String _xifexpression = null;
    if ((container instanceof SwitchEntry)) {
      String _name = SwitchEntryTransformer.getName(((SwitchEntry) container));
      _xifexpression = ("SwitchEntry: " + _name);
    } else {
      String _xifexpression_1 = null;
      if ((container instanceof SwitchDefault)) {
        String _name_1 = SwitchEntryTransformer.getName(((SwitchDefault) container));
        _xifexpression_1 = ("SwitchDefault: " + _name_1);
      } else {
        throw new UnsupportedOperationException("Container type not supported");
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
}
