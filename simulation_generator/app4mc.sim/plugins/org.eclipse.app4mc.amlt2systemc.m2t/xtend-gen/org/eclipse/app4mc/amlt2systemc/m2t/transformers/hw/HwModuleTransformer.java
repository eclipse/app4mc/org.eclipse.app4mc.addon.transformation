/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.Cache;
import org.eclipse.app4mc.amalthea.model.ConnectionHandler;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwDestination;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.PortType;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;

@Singleton
@SuppressWarnings("all")
public class HwModuleTransformer extends BaseTransformer {
  @Inject
  private HwConnectionTransformer hwConnectionTransformer;

  @Inject
  private HwStructureTransformer hwStructureTransformer;

  public static HwModule getParentHwModuleOrNull(final EObject obj) {
    final EObject ret = obj.eContainer();
    if ((ret instanceof HwModule)) {
      return ((HwModule) ret);
    } else {
      return null;
    }
  }

  public String getName(final HwModule hwModule) {
    return hwModule.getName();
  }

  protected String getCall(final HwModule hwModule) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_");
    String _name = this.getName(hwModule);
    _builder.append(_name);
    return _builder.toString();
  }

  protected String getModulePath(final HwModule hwModule) {
    String _modulePath = HwModelTransformer.getModulePath();
    String _plus = (_modulePath + "/modules/");
    String _name = this.getName(hwModule);
    return (_plus + _name);
  }

  protected CharSequence getIncludesForConnections(final HwModule hwModule) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("//Connections");
    _builder.newLine();
    {
      EList<HwPort> _ports = hwModule.getPorts();
      for(final HwPort port : _ports) {
        {
          EList<HwConnection> _connections = port.getConnections();
          for(final HwConnection connection : _connections) {
            _builder.append("#include \"");
            String _module = this.hwConnectionTransformer.transform(connection).getModule();
            _builder.append(_module);
            _builder.append(".h\"\t\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  protected CharSequence setPortsAndConnections(final HwModule hwModule) {
    StringConcatenation _builder = new StringConcatenation();
    final String name = this.getName(hwModule);
    _builder.newLineIfNotEmpty();
    _builder.append("//HW Ports\t");
    _builder.newLine();
    CharSequence _initialize = HwPortsTransformer.initialize(name, hwModule.getPorts());
    _builder.append(_initialize);
    _builder.newLineIfNotEmpty();
    _builder.append("//Connections");
    _builder.newLine();
    {
      EList<HwPort> _ports = hwModule.getPorts();
      for(final HwPort port : _ports) {
        {
          EList<HwConnection> _connections = port.getConnections();
          for(final HwConnection connection : _connections) {
            final TranslationUnit tuConnection = this.hwConnectionTransformer.transform(connection);
            _builder.newLineIfNotEmpty();
            _builder.append(name);
            _builder.append("->bindConnection(");
            String _call = tuConnection.getCall();
            _builder.append(_call);
            _builder.append("(), \"");
            String _name = HwPortsTransformer.getName(port);
            _builder.append(_name);
            _builder.append("\");");
            _builder.newLineIfNotEmpty();
            {
              PortType _portType = port.getPortType();
              boolean _tripleEquals = (_portType == PortType.INITIATOR);
              if (_tripleEquals) {
                String _call_1 = tuConnection.getCall();
                _builder.append(_call_1);
                _builder.append("()->setFreqDomain(");
                _builder.append(name);
                _builder.append(");");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
      }
    }
    return _builder;
  }

  protected String getParentGetter(final HwModule module) {
    final HwStructure parentHwStructure = HwStructureTransformer.getParentHwStructureOrNull(module);
    if ((parentHwStructure != null)) {
      final TranslationUnit parentTu = this.hwStructureTransformer.transform(parentHwStructure);
      return parentTu.getCall();
    } else {
      String _qualifiedName = module.getQualifiedName();
      String _plus = ("Module " + _qualifiedName);
      String _plus_1 = (_plus + " is not embedded in a HwStructure.");
      throw new RuntimeException(_plus_1);
    }
  }

  protected String getParentInclude(final HwModule module) {
    final HwStructure parentHwStructure = HwStructureTransformer.getParentHwStructureOrNull(module);
    if ((parentHwStructure != null)) {
      final TranslationUnit parentTu = this.hwStructureTransformer.transform(parentHwStructure);
      String _module = parentTu.getModule();
      String _plus = ("#include \"" + _module);
      return (_plus + ".h\"");
    } else {
      String _qualifiedName = module.getQualifiedName();
      String _plus_1 = ("Module " + _qualifiedName);
      String _plus_2 = (_plus_1 + " is not embedded in a HwStructure.");
      throw new RuntimeException(_plus_2);
    }
  }

  protected TranslationUnit _transform(final HwDestination hwDestination) {
    if ((hwDestination instanceof ProcessingUnit)) {
      return this.transform(((ProcessingUnit) hwDestination));
    } else {
      if ((hwDestination instanceof Memory)) {
        return this.transform(((Memory) hwDestination));
      }
    }
    return null;
  }

  @Inject
  private MemoryTransformer memoryTransformer;

  protected TranslationUnit _transform(final Memory memory) {
    return this.memoryTransformer.transform(memory);
  }

  protected TranslationUnit _transform(final Cache cache) {
    return new TranslationUnit("Transformation of Cache transformation not yet implemented", 
      "Transformation of Cache transformation not yet implemented");
  }

  @Inject
  private ConnectionHandlerTransformer connectionHandlerTransformer;

  protected TranslationUnit _transform(final ConnectionHandler connectionHandler) {
    return this.connectionHandlerTransformer.transform(connectionHandler);
  }

  @Inject
  private ProcessingUnitTransformer processingUnitTransformer;

  protected TranslationUnit _transform(final ProcessingUnit processingUnit) {
    return this.processingUnitTransformer.transform(processingUnit);
  }

  public TranslationUnit transform(final IReferable cache) {
    if (cache instanceof Cache) {
      return _transform((Cache)cache);
    } else if (cache instanceof ConnectionHandler) {
      return _transform((ConnectionHandler)cache);
    } else if (cache instanceof Memory) {
      return _transform((Memory)cache);
    } else if (cache instanceof ProcessingUnit) {
      return _transform((ProcessingUnit)cache);
    } else if (cache instanceof HwDestination) {
      return _transform((HwDestination)cache);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(cache).toString());
    }
  }
}
