/**
 * Copyright (c) 2019 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;

@Singleton
@SuppressWarnings("all")
public class SWModelTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private TaskTransformer taskTransformer;

  @Inject
  private RunnableTransformer runnableTransformer;

  @Inject
  private ChannelTransformer channelTransformer;

  @Inject
  private LabelTransformer labelTransformer;

  @Inject
  private ModeLabelTransformer modeLabelTransformer;

  @Inject
  private ModeTransformer modeTransformer;

  protected static String getModulePath() {
    String _modulePath = AmaltheaTransformer.getModulePath();
    return (_modulePath + "/swModel");
  }

  private String getModuleName() {
    String _modulePath = SWModelTransformer.getModulePath();
    return (_modulePath + "/swModel");
  }

  private String getFunctionDef() {
    return "init_swModel()";
  }

  public TranslationUnit transform(final SWModel[] swModels) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(swModels);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName();
      String _functionDef = this.getFunctionDef();
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _functionDef);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, swModels);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final SWModel[] swModels) {
    final Consumer<SWModel> _function = (SWModel swModel) -> {
      final Consumer<Task> _function_1 = (Task it_1) -> {
        this.taskTransformer.transform(it_1);
      };
      swModel.getTasks().forEach(_function_1);
    };
    ((List<SWModel>)Conversions.doWrapArray(swModels)).forEach(_function);
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH());
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp());
    String _modulePath = SWModelTransformer.getModulePath();
    String _plus = (_modulePath + "/CMakeLists.txt");
    this.outputBuffer.appendTo("OTHER", _plus, this.getCMake(it.getModule()));
  }

  private String toH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.append("#include \"");
    String _moduleName = this.getModuleName();
    _builder.append(_moduleName);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.taskTransformer.getCache().values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("#include \"");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("/* Software */");
    _builder.newLine();
    _builder.append("void ");
    String _functionDef = this.getFunctionDef();
    _builder.append(_functionDef);
    _builder.append("{\t\t");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byCall = TuSort.byCall(this.taskTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byCall) {
        _builder.append("\t");
        String _call = ((TranslationUnit) obj_1).getCall();
        _builder.append(_call, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public String getCMake(final String thisModule) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# CMakeList.txt: CMake project for SoftwareModel of \"");
    String _property = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property);
    _builder.append("\".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("# Add sources of SoftwareModel");
    _builder.newLine();
    _builder.append("target_sources (");
    String _property_1 = this.getProperty(PropertyKeys.PROJECT_NAME);
    _builder.append(_property_1);
    _builder.append("  PRIVATE");
    _builder.newLineIfNotEmpty();
    {
      List<TranslationUnit> _byModule = TuSort.byModule(this.modeTransformer.getCache().values());
      for(final TranslationUnit obj : _byModule) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module = ((TranslationUnit) obj).getModule();
        _builder.append(_module, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    {
      List<TranslationUnit> _byModule_1 = TuSort.byModule(this.channelTransformer.getCache().values());
      for(final TranslationUnit obj_1 : _byModule_1) {
        _builder.append("\t\"${PROJECT_SOURCE_DIR}/");
        String _module_1 = ((TranslationUnit) obj_1).getModule();
        _builder.append(_module_1, "\t");
        _builder.append(".cpp\"");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_2 = TuSort.byModule(this.labelTransformer.getCache().values());
      for(final TranslationUnit obj_2 : _byModule_2) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_2 = ((TranslationUnit) obj_2).getModule();
        _builder.append(_module_2, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_3 = TuSort.byModule(this.modeLabelTransformer.getCache().values());
      for(final TranslationUnit obj_3 : _byModule_3) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_3 = ((TranslationUnit) obj_3).getModule();
        _builder.append(_module_3, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_4 = TuSort.byModule(this.runnableTransformer.getCache().values());
      for(final TranslationUnit obj_4 : _byModule_4) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_4 = ((TranslationUnit) obj_4).getModule();
        _builder.append(_module_4, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    {
      List<TranslationUnit> _byModule_5 = TuSort.byModule(this.taskTransformer.getCache().values());
      for(final TranslationUnit obj_5 : _byModule_5) {
        _builder.append("\t");
        _builder.append("\"${PROJECT_SOURCE_DIR}/");
        String _module_5 = ((TranslationUnit) obj_5).getModule();
        _builder.append(_module_5, "\t");
        _builder.append(".cpp\" ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.append("\"${PROJECT_SOURCE_DIR}/");
    _builder.append(thisModule, "\t");
    _builder.append(".cpp\")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
