/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common;

import com.google.inject.Inject;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.ChannelFillCondition;
import org.eclipse.app4mc.amalthea.model.Condition;
import org.eclipse.app4mc.amalthea.model.ConditionConjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunctionEntry;
import org.eclipse.app4mc.amalthea.model.ModeLabelCondition;
import org.eclipse.app4mc.amalthea.model.ModeValueCondition;
import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class ConditionTransformer extends BaseTransformer {
  public static class ConditionBuffer {
    private final LinkedHashSet<String> includes;

    private final LinkedList<String> conditions;

    public final String instanceName;

    public final boolean parentIsPointer;

    public ConditionBuffer(final String _name, final boolean _parentIsPointer) {
      this.instanceName = _name;
      this.parentIsPointer = _parentIsPointer;
      LinkedHashSet<String> _linkedHashSet = new LinkedHashSet<String>();
      this.includes = _linkedHashSet;
      LinkedList<String> _linkedList = new LinkedList<String>();
      this.conditions = _linkedList;
    }

    public ConditionBuffer(final ConditionTransformer.ConditionBuffer other) {
      this.instanceName = other.instanceName;
      this.parentIsPointer = other.parentIsPointer;
      this.includes = other.includes;
      this.conditions = other.conditions;
    }

    public boolean addAll(final ConditionTransformer.ConditionBuffer other) {
      boolean _xblockexpression = false;
      {
        this.includes.addAll(other.includes);
        _xblockexpression = this.conditions.addAll(other.conditions);
      }
      return _xblockexpression;
    }

    public LinkedHashSet<String> getIncludes() {
      return this.includes;
    }

    public LinkedList<String> getConditions() {
      return this.conditions;
    }

    public void addInclude(final String includePath) {
      this.includes.add((("#include \"" + includePath) + ".h\""));
    }

    public void addCondition(final String params) {
      String _xifexpression = null;
      if (this.parentIsPointer) {
        _xifexpression = "->";
      } else {
        _xifexpression = ".";
      }
      String _plus = (this.instanceName + _xifexpression);
      String _plus_1 = (_plus + "addCondition(");
      String _plus_2 = (_plus_1 + params);
      String _plus_3 = (_plus_2 + ");");
      this.conditions.add(_plus_3);
    }

    public void addConjunction(final String conjunctionName, final ConditionTransformer.ConditionBuffer conjunctiveConditions) {
      this.conditions.add((("ConditionConjunction " + conjunctionName) + ";\n"));
      this.addAll(conjunctiveConditions);
      String _xifexpression = null;
      if (this.parentIsPointer) {
        _xifexpression = "->";
      } else {
        _xifexpression = ".";
      }
      String _plus = (this.instanceName + _xifexpression);
      String _plus_1 = (_plus + "addConjunction(");
      String _plus_2 = (_plus_1 + conjunctionName);
      String _plus_3 = (_plus_2 + ");");
      this.conditions.add(_plus_3);
    }

    public static CharSequence getConditionIncludes(final ConditionTransformer.ConditionBuffer _content) {
      StringConcatenation _builder = new StringConcatenation();
      {
        if ((_content != null)) {
          {
            for(final String include : _content.includes) {
              _builder.append(include);
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
      return _builder;
    }

    public static CharSequence getConditionSource(final ConditionTransformer.ConditionBuffer _content) {
      StringConcatenation _builder = new StringConcatenation();
      {
        if ((_content != null)) {
          {
            for(final String source : _content.conditions) {
              _builder.append(source);
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
      return _builder;
    }
  }

  @Inject
  private ModeConditionTransformer modeConditionTransformer;

  @Inject
  private ChannelFillConditionTransformer channelFillConditionTransformer;

  public static String getRelationalOperator(final RelationalOperator relation) {
    if (relation != null) {
      switch (relation) {
        case EQUAL:
          return "RelationalOperator::EQUAL";
        case NOT_EQUAL:
          return "RelationalOperator::NOT_EQUAL";
        case GREATER_THAN:
          return "RelationalOperator::GREATER_THAN";
        case LESS_THAN:
          return "RelationalOperator::LESS_THAN";
        default:
          throw new RuntimeException((("RelationalOperator " + relation) + " is not supported."));
      }
    } else {
      throw new RuntimeException((("RelationalOperator " + relation) + " is not supported."));
    }
  }

  public ConditionTransformer.ConditionBuffer transformCondition(final ConditionDisjunction disjunction, final String parentName, final boolean parentIsPointer) {
    if ((disjunction == null)) {
      return null;
    }
    final ConditionTransformer.ConditionBuffer content = new ConditionTransformer.ConditionBuffer(parentName, parentIsPointer);
    EList<ConditionDisjunctionEntry> _entries = disjunction.getEntries();
    for (final ConditionDisjunctionEntry disjunctionEntry : _entries) {
      if ((disjunctionEntry instanceof ConditionConjunction)) {
        this.createConjunction(content.instanceName, ((ConditionConjunction) disjunctionEntry), content);
      } else {
        if ((disjunctionEntry instanceof Condition)) {
          this.createCondition(content.instanceName, ((Condition)disjunctionEntry), content);
        }
      }
    }
    return content;
  }

  private static int conjunctionCnt = 0;

  private void createConjunction(final String parent, final ConditionConjunction conjunction, final ConditionTransformer.ConditionBuffer content) {
    final String name = ((parent + "_AND") + Integer.valueOf(ConditionTransformer.conjunctionCnt));
    final ConditionTransformer.ConditionBuffer conjunctiveConditions = new ConditionTransformer.ConditionBuffer(name, false);
    final Consumer<Condition> _function = (Condition condition) -> {
      this.createCondition(name, condition, conjunctiveConditions);
      ConditionTransformer.conjunctionCnt++;
    };
    conjunction.getEntries().forEach(_function);
    content.addConjunction(name, conjunctiveConditions);
  }

  protected void _createCondition(final String parent, final Condition condition, final ConditionTransformer.ConditionBuffer content) {
    String _string = condition.getClass().toString();
    String _plus = ("no transformation for conditions of type " + _string);
    throw new IllegalArgumentException(_plus);
  }

  protected void _createCondition(final String parent, final ModeLabelCondition modeLabelCondition, final ConditionTransformer.ConditionBuffer content) {
    this.modeConditionTransformer.createCondition(parent, modeLabelCondition, content);
  }

  protected void _createCondition(final String parent, final ModeValueCondition modeValueCondition, final ConditionTransformer.ConditionBuffer content) {
    this.modeConditionTransformer.createCondition(parent, modeValueCondition, content);
  }

  protected void _createCondition(final String parent, final ChannelFillCondition channelFillCondition, final ConditionTransformer.ConditionBuffer content) {
    this.channelFillConditionTransformer.createCondition(parent, channelFillCondition, content);
  }

  public void createCondition(final String parent, final Condition modeLabelCondition, final ConditionTransformer.ConditionBuffer content) {
    if (modeLabelCondition instanceof ModeLabelCondition) {
      _createCondition(parent, (ModeLabelCondition)modeLabelCondition, content);
      return;
    } else if (modeLabelCondition instanceof ModeValueCondition) {
      _createCondition(parent, (ModeValueCondition)modeLabelCondition, content);
      return;
    } else if (modeLabelCondition instanceof ChannelFillCondition) {
      _createCondition(parent, (ChannelFillCondition)modeLabelCondition, content);
      return;
    } else if (modeLabelCondition != null) {
      _createCondition(parent, modeLabelCondition, content);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(parent, modeLabelCondition, content).toString());
    }
  }
}
