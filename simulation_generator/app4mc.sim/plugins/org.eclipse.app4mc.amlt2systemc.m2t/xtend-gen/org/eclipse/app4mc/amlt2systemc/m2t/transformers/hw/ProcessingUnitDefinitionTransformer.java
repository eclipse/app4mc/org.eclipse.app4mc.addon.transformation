/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.HwFeature;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class ProcessingUnitDefinitionTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private HwFeatureTransformer hwFeatureTransformer;

  public static String getModulePath(final ProcessingUnitDefinition processingUnitDefinition) {
    String _modulePath = HwModelTransformer.getModulePath();
    String _plus = (_modulePath + "/definitions/");
    String _name = ProcessingUnitDefinitionTransformer.getName(processingUnitDefinition);
    return (_plus + _name);
  }

  public static String getCall(final ProcessingUnitDefinition processingUnitDefinition) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_ProcessingUnitDefinition_");
    String _name = ProcessingUnitDefinitionTransformer.getName(processingUnitDefinition);
    _builder.append(_name);
    _builder.append("()");
    return _builder.toString();
  }

  public static String getName(final ProcessingUnitDefinition processingUnitDefinition) {
    return processingUnitDefinition.getName();
  }

  public TranslationUnit transform(final ProcessingUnitDefinition processingUnitDefinition) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(processingUnitDefinition);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = ProcessingUnitDefinitionTransformer.getModulePath(processingUnitDefinition);
      String _call = ProcessingUnitDefinitionTransformer.getCall(processingUnitDefinition);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, processingUnitDefinition);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final ProcessingUnitDefinition processingUnitDefinition) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(processingUnitDefinition));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(processingUnitDefinition));
  }

  public String toH(final ProcessingUnitDefinition processingUnitDefinition) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("ProcessingUnitDefinition* ");
    String _call = ProcessingUnitDefinitionTransformer.getCall(processingUnitDefinition);
    _builder.append(_call);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public String toCpp(final ProcessingUnitDefinition processingUnitDefinition) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = ProcessingUnitDefinitionTransformer.getModulePath(processingUnitDefinition);
    _builder.append(_modulePath);
    _builder.append(".h\"\t");
    _builder.newLineIfNotEmpty();
    {
      EList<HwFeature> _features = processingUnitDefinition.getFeatures();
      for(final HwFeature feature : _features) {
        _builder.append("#include \"");
        String _module = this.hwFeatureTransformer.transform(feature).getModule();
        _builder.append(_module);
        _builder.append(".h\"");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    final String name = ProcessingUnitDefinitionTransformer.getName(processingUnitDefinition);
    _builder.newLineIfNotEmpty();
    _builder.append("ProcessingUnitDefinition* ");
    _builder.append(name);
    _builder.append(" = NULL;");
    _builder.newLineIfNotEmpty();
    _builder.append("ProcessingUnitDefinition* ");
    String _call = ProcessingUnitDefinitionTransformer.getCall(processingUnitDefinition);
    _builder.append(_call);
    _builder.append("{");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (");
    _builder.append(name, "\t");
    _builder.append("==NULL){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append(name, "\t\t");
    _builder.append(" = new ProcessingUnitDefinition(\"");
    _builder.append(name, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    {
      EList<HwFeature> _features_1 = processingUnitDefinition.getFeatures();
      for(final HwFeature feature_1 : _features_1) {
        _builder.append("\t\t");
        _builder.append(name, "\t\t");
        _builder.append("->addFeature(*");
        String _call_1 = this.hwFeatureTransformer.transform(feature_1).getCall();
        _builder.append(_call_1, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    _builder.append(name, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
