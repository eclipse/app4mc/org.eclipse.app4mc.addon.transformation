/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;

@SuppressWarnings("all")
public class ActivityGraphTransformer extends IActivityGraphItemContainerTransformer {
  @Override
  protected String getContainerDescription(final IActivityGraphItemContainer container) {
    return "ActivityGraph";
  }

  @Override
  protected void transformContainer(final IActivityGraphItemContainer container, final AgiContainerBuffer parentContent) {
    final AgiContainerBuffer activityGraphContents = this.transformItems(((ActivityGraph) container), parentContent.instanceName, parentContent.module, parentContent.parentIsPointer);
    parentContent.getIncludes().addAll(activityGraphContents.getIncludes());
    parentContent.getSource().addAll(activityGraphContents.getSource());
  }
}
