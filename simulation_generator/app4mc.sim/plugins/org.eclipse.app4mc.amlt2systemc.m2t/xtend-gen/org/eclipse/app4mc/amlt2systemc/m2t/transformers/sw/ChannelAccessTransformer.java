/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import com.google.inject.Inject;
import org.eclipse.app4mc.amalthea.model.ChannelAccess;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class ChannelAccessTransformer extends BaseTransformer {
  @Inject
  private ChannelTransformer channelTransformer;

  public void transform(final ChannelAccess _access, final AgiContainerBuffer content) {
    final TranslationUnit tu = this.channelTransformer.transform(_access.getData());
    content.addInclude(tu.getModule());
    if ((_access instanceof ChannelSend)) {
      content.addAsActivityGraphItem(this.transformSend(tu.getCall(), ((ChannelSend)_access).getElements()));
    } else {
      if ((_access instanceof ChannelReceive)) {
        content.addAsActivityGraphItem(this.transformReceive(tu.getCall(), ((ChannelReceive)_access).getElements()));
      }
    }
  }

  private String transformReceive(final String call, final int elements) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ChannelRead>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(elements);
    _builder.append("})");
    return _builder.toString();
  }

  private String transformSend(final String call, final int elements) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ChannelWrite>({");
    _builder.append(call);
    _builder.append(", ");
    _builder.append(elements);
    _builder.append("})");
    return _builder.toString();
  }
}
