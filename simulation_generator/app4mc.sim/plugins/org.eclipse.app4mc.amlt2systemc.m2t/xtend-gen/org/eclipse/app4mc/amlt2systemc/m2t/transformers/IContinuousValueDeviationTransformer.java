/**
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

import java.util.Arrays;
import org.eclipse.app4mc.amalthea.model.ContinuousValueBetaDistribution;
import org.eclipse.app4mc.amalthea.model.ContinuousValueBoundaries;
import org.eclipse.app4mc.amalthea.model.ContinuousValueConstant;
import org.eclipse.app4mc.amalthea.model.ContinuousValueGaussDistribution;
import org.eclipse.app4mc.amalthea.model.ContinuousValueUniformDistribution;
import org.eclipse.app4mc.amalthea.model.ContinuousValueWeibullEstimatorsDistribution;
import org.eclipse.app4mc.amalthea.model.IContinuousValueDeviation;
import org.eclipse.app4mc.amalthea.model.SamplingType;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class IContinuousValueDeviationTransformer {
  protected static String _getDeviation(final IContinuousValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final IContinuousValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final IContinuousValueDeviation value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("WARNING:VALUE_FORMAT_NOT_SUPPORTED");
    return _builder.toString();
  }

  protected static String _getDeviation(final ContinuousValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IContinuousValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ContinuousValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ContinuousValueBetaDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ContinuousValueBetaDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    double _alpha = value.getAlpha();
    _builder.append(_alpha);
    _builder.append(",");
    double _beta = value.getBeta();
    _builder.append(_beta);
    _builder.append(",");
    Double _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Double _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }

  private static String stype(final ContinuousValueBoundaries value) {
    SamplingType _samplingType = value.getSamplingType();
    if (_samplingType != null) {
      switch (_samplingType) {
        case BEST_CASE:
          return "BoundariesSamplingType::BestCase";
        case WORST_CASE:
          return "BoundariesSamplingType::WorstCase";
        case AVERAGE_CASE:
          return "BoundariesSamplingType::AverageCase";
        case CORNER_CASE:
          return "BoundariesSamplingType::CornerCase";
        case UNIFORM:
          return "BoundariesSamplingType::Uniform";
        default:
          return "BoundariesSamplingType::WorstCase";
      }
    } else {
      return "BoundariesSamplingType::WorstCase";
    }
  }

  protected static String _getDeviation(final ContinuousValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IContinuousValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ContinuousValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ContinuousValueBoundaries");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ContinuousValueBoundaries value) {
    StringConcatenation _builder = new StringConcatenation();
    String _stype = IContinuousValueDeviationTransformer.stype(value);
    _builder.append(_stype);
    _builder.append(",");
    Double _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Double _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }

  protected static String _getDeviation(final ContinuousValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IContinuousValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ContinuousValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ContinuousValueConstant");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ContinuousValueConstant value) {
    StringConcatenation _builder = new StringConcatenation();
    double _value = value.getValue();
    _builder.append(_value);
    return _builder.toString();
  }

  protected static String _getDeviation(final ContinuousValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IContinuousValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ContinuousValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ContinuousValueGaussDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ContinuousValueGaussDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    double _mean = value.getMean();
    _builder.append(_mean);
    _builder.append(",");
    double _sd = value.getSd();
    _builder.append(_sd);
    _builder.append(",");
    Double _elvis = null;
    Double _lowerBound = value.getLowerBound();
    if (_lowerBound != null) {
      _elvis = _lowerBound;
    } else {
      _elvis = Double.valueOf((-Double.MAX_VALUE));
    }
    _builder.append(_elvis);
    _builder.append(",");
    Double _elvis_1 = null;
    Double _upperBound = value.getUpperBound();
    if (_upperBound != null) {
      _elvis_1 = _upperBound;
    } else {
      _elvis_1 = Double.valueOf(Double.MAX_VALUE);
    }
    _builder.append(_elvis_1);
    return _builder.toString();
  }

  protected static String _getDeviation(final ContinuousValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IContinuousValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ContinuousValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ContinuousValueUniformDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ContinuousValueUniformDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    Double _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Double _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    return _builder.toString();
  }

  protected static String _getDeviation(final ContinuousValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("(");
    String _deviationValue = IContinuousValueDeviationTransformer.getDeviationValue(value);
    _builder.append(_deviationValue);
    _builder.append(")");
    return _builder.toString();
  }

  protected static String _getDeviationTemplate(final ContinuousValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ContinuousValueWeibullEstimatorsDistribution");
    return _builder.toString();
  }

  protected static String _getDeviationValue(final ContinuousValueWeibullEstimatorsDistribution value) {
    StringConcatenation _builder = new StringConcatenation();
    String _deviationTemplate = IContinuousValueDeviationTransformer.getDeviationTemplate(value);
    _builder.append(_deviationTemplate);
    _builder.append("::findParameter(");
    Double _average = value.getAverage();
    _builder.append(_average);
    _builder.append(",");
    double _pRemainPromille = value.getPRemainPromille();
    _builder.append(_pRemainPromille);
    _builder.append(",");
    Double _lowerBound = value.getLowerBound();
    _builder.append(_lowerBound);
    _builder.append(",");
    Double _upperBound = value.getUpperBound();
    _builder.append(_upperBound);
    _builder.append("),");
    Double _lowerBound_1 = value.getLowerBound();
    _builder.append(_lowerBound_1);
    _builder.append(",");
    Double _upperBound_1 = value.getUpperBound();
    _builder.append(_upperBound_1);
    return _builder.toString();
  }

  public static String getDeviation(final IContinuousValueDeviation value) {
    if (value instanceof ContinuousValueBetaDistribution) {
      return _getDeviation((ContinuousValueBetaDistribution)value);
    } else if (value instanceof ContinuousValueBoundaries) {
      return _getDeviation((ContinuousValueBoundaries)value);
    } else if (value instanceof ContinuousValueGaussDistribution) {
      return _getDeviation((ContinuousValueGaussDistribution)value);
    } else if (value instanceof ContinuousValueUniformDistribution) {
      return _getDeviation((ContinuousValueUniformDistribution)value);
    } else if (value instanceof ContinuousValueWeibullEstimatorsDistribution) {
      return _getDeviation((ContinuousValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof ContinuousValueConstant) {
      return _getDeviation((ContinuousValueConstant)value);
    } else if (value != null) {
      return _getDeviation(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }

  public static String getDeviationTemplate(final IContinuousValueDeviation value) {
    if (value instanceof ContinuousValueBetaDistribution) {
      return _getDeviationTemplate((ContinuousValueBetaDistribution)value);
    } else if (value instanceof ContinuousValueBoundaries) {
      return _getDeviationTemplate((ContinuousValueBoundaries)value);
    } else if (value instanceof ContinuousValueGaussDistribution) {
      return _getDeviationTemplate((ContinuousValueGaussDistribution)value);
    } else if (value instanceof ContinuousValueUniformDistribution) {
      return _getDeviationTemplate((ContinuousValueUniformDistribution)value);
    } else if (value instanceof ContinuousValueWeibullEstimatorsDistribution) {
      return _getDeviationTemplate((ContinuousValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof ContinuousValueConstant) {
      return _getDeviationTemplate((ContinuousValueConstant)value);
    } else if (value != null) {
      return _getDeviationTemplate(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }

  public static String getDeviationValue(final IContinuousValueDeviation value) {
    if (value instanceof ContinuousValueBetaDistribution) {
      return _getDeviationValue((ContinuousValueBetaDistribution)value);
    } else if (value instanceof ContinuousValueBoundaries) {
      return _getDeviationValue((ContinuousValueBoundaries)value);
    } else if (value instanceof ContinuousValueGaussDistribution) {
      return _getDeviationValue((ContinuousValueGaussDistribution)value);
    } else if (value instanceof ContinuousValueUniformDistribution) {
      return _getDeviationValue((ContinuousValueUniformDistribution)value);
    } else if (value instanceof ContinuousValueWeibullEstimatorsDistribution) {
      return _getDeviationValue((ContinuousValueWeibullEstimatorsDistribution)value);
    } else if (value instanceof ContinuousValueConstant) {
      return _getDeviationValue((ContinuousValueConstant)value);
    } else if (value != null) {
      return _getDeviationValue(value);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(value).toString());
    }
  }
}
