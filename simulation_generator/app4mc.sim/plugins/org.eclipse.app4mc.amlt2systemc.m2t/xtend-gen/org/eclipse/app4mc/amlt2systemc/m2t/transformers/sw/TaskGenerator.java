/**
 * Copyright (c) 2019-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class TaskGenerator {
  private TaskGenerator() {
    throw new IllegalStateException("Utility class");
  }

  public static String getName(final Task task) {
    return task.getName();
  }

  public static String getModulePath(final Task task) {
    String _modulePath = SWModelTransformer.getModulePath();
    String _plus = (_modulePath + "/tasks/");
    String _name = TaskGenerator.getName(task);
    return (_plus + _name);
  }

  public static String getFunctionDef(final Task task) {
    String _name = task.getName();
    String _plus = ("get_" + _name);
    return (_plus + "()");
  }

  public static String toH(final Task task) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("//Task runnableA----");
    _builder.newLine();
    _builder.append("std::shared_ptr<Task> ");
    String _functionDef = TaskGenerator.getFunctionDef(task);
    _builder.append(_functionDef);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  public static String toCpp(final Task task, final LinkedList<String> stimuliCalls, final LinkedHashSet<String> activityItemIncludes, final LinkedList<String> activityItemCalls) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include \"APP4MCsim.h\"");
    _builder.newLine();
    _builder.append("#include \"");
    String _modulePath = TaskGenerator.getModulePath(task);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      for(final String include : activityItemIncludes) {
        _builder.append(include);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("std::shared_ptr<Task> ");
    String _name = task.getName();
    _builder.append(_name);
    _builder.append(" = nullptr;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("std::shared_ptr<Task>  ");
    String _functionDef = TaskGenerator.getFunctionDef(task);
    _builder.append(_functionDef);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (");
    String _name_1 = task.getName();
    _builder.append(_name_1, "\t");
    _builder.append(" == nullptr) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("//initialize");
    _builder.newLine();
    _builder.append("\t\t");
    String _name_2 = task.getName();
    _builder.append(_name_2, "\t\t");
    _builder.append(" = Task::createTask(\"");
    String _name_3 = task.getName();
    _builder.append(_name_3, "\t\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    {
      int _multipleTaskActivationLimit = task.getMultipleTaskActivationLimit();
      boolean _greaterEqualsThan = (_multipleTaskActivationLimit >= 1);
      if (_greaterEqualsThan) {
        _builder.append("\t\t");
        String _name_4 = task.getName();
        _builder.append(_name_4, "\t\t");
        _builder.append("->setActivationLimit(");
        int _multipleTaskActivationLimit_1 = task.getMultipleTaskActivationLimit();
        _builder.append(_multipleTaskActivationLimit_1, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final String activityItemCall : activityItemCalls) {
        {
          if ((activityItemCall.startsWith("//") || activityItemCall.startsWith("/*"))) {
            _builder.append("\t\t");
            _builder.append("//");
            _builder.append(activityItemCall, "\t\t");
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append("\t\t");
            _builder.append(activityItemCall, "\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t\t");
    _builder.newLine();
    {
      for(final String stimulusCall : stimuliCalls) {
        _builder.append("\t\t");
        String _name_5 = task.getName();
        _builder.append(_name_5, "\t\t");
        _builder.append("->addStimulus(");
        _builder.append(stimulusCall, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return ");
    String _name_6 = task.getName();
    _builder.append(_name_6, "\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
}
