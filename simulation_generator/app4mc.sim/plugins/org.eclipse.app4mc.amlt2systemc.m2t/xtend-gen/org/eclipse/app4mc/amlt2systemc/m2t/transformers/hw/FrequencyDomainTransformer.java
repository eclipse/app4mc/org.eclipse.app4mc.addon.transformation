/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.app4mc.amalthea.model.Frequency;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.FrequencyUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@Singleton
@SuppressWarnings("all")
public class FrequencyDomainTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  public static String getName(final FrequencyDomain frequencyDomain) {
    return frequencyDomain.getName();
  }

  public static String getModulePath(final FrequencyDomain frequencyDomain) {
    String _modulePath = HwModelTransformer.getModulePath();
    String _plus = (_modulePath + "/domains/");
    String _name = FrequencyDomainTransformer.getName(frequencyDomain);
    return (_plus + _name);
  }

  public static String getCall(final FrequencyDomain frequencyDomain) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_FrequencyDomain_");
    String _name = FrequencyDomainTransformer.getName(frequencyDomain);
    _builder.append(_name);
    _builder.append("()");
    return _builder.toString();
  }

  public TranslationUnit transform(final FrequencyDomain frequencyDomain) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(frequencyDomain);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = FrequencyDomainTransformer.getModulePath(frequencyDomain);
      String _call = FrequencyDomainTransformer.getCall(frequencyDomain);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, frequencyDomain);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final FrequencyDomain frequencyDomain) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(frequencyDomain));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(frequencyDomain));
  }

  public String toH(final FrequencyDomain frequencyDomain) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("sc_core::sc_time ");
    String _call = FrequencyDomainTransformer.getCall(frequencyDomain);
    _builder.append(_call);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }

  public String toCpp(final FrequencyDomain frequencyDomain) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = FrequencyDomainTransformer.getModulePath(frequencyDomain);
    _builder.append(_modulePath);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("sc_core::sc_time ");
    String _call = FrequencyDomainTransformer.getCall(frequencyDomain);
    _builder.append(_call);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("return sc_core::sc_time(");
    double _periodInPS = this.getPeriodInPS(frequencyDomain.getDefaultValue());
    _builder.append(_periodInPS, "\t");
    _builder.append(", sc_core::SC_PS);");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public double getPeriodInPS(final Frequency frequency) {
    FrequencyUnit _unit = null;
    if (frequency!=null) {
      _unit=frequency.getUnit();
    }
    if (_unit != null) {
      switch (_unit) {
        case GHZ:
          double _pow = Math.pow(10, 3);
          double _value = frequency.getValue();
          return (_pow / _value);
        case MHZ:
          double _pow_1 = Math.pow(10, 6);
          double _value_1 = frequency.getValue();
          return (_pow_1 / _value_1);
        case KHZ:
          double _pow_2 = Math.pow(10, 9);
          double _value_2 = frequency.getValue();
          return (_pow_2 / _value_2);
        case HZ:
          double _pow_3 = Math.pow(10, 12);
          double _value_3 = frequency.getValue();
          return (_pow_3 / _value_3);
        default:
          return 0.0;
      }
    } else {
      return 0.0;
    }
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
