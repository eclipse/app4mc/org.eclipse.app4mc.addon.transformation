/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.SchedulingParameterTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.TaskSchedulerTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.TaskGenerator;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.TaskTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class TaskAllocationTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private TaskTransformer taskTransformer;

  @Inject
  private TaskSchedulerTransformer taskSchedulerTransformer;

  @Inject
  private SchedulingParameterTransformer schedulingParameterTransformer;

  private String getModulePath() {
    String _modulePath = MappingModelTransformer.getModulePath();
    return (_modulePath + "/taskAllocation");
  }

  private String getModuleName(final TaskAllocation taskAllocation) {
    StringConcatenation _builder = new StringConcatenation();
    String _modulePath = this.getModulePath();
    _builder.append(_modulePath);
    _builder.append("/taskAllocation_");
    String _name = taskAllocation.getScheduler().getName();
    _builder.append(_name);
    _builder.append("_");
    String _name_1 = taskAllocation.getTask().getName();
    _builder.append(_name_1);
    return _builder.toString();
  }

  private String getCall(final TaskAllocation taskAllocation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("init_TaskAllocation_");
    String _name = this.taskSchedulerTransformer.getName(taskAllocation.getScheduler());
    _builder.append(_name);
    _builder.append("_");
    String _name_1 = TaskGenerator.getName(taskAllocation.getTask());
    _builder.append(_name_1);
    return _builder.toString();
  }

  public TranslationUnit transform(final TaskAllocation taskAllocation) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(taskAllocation);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _moduleName = this.getModuleName(taskAllocation);
      String _call = this.getCall(taskAllocation);
      TranslationUnit _translationUnit = new TranslationUnit(_moduleName, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, taskAllocation);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit it, final TaskAllocation taskAllocation) {
    this.outputBuffer.appendTo("INC", it.getModule(), this.toH(it));
    this.outputBuffer.appendTo("SRC", it.getModule(), this.toCpp(taskAllocation));
  }

  private String toH(final TranslationUnit tu) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("void ");
    String _call = tu.getCall();
    _builder.append(_call);
    _builder.append("();\t\t");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final TaskAllocation taskAllocation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t\t");
    final TranslationUnit tuScheduler = this.taskSchedulerTransformer.transform(taskAllocation.getScheduler());
    _builder.newLineIfNotEmpty();
    final TranslationUnit tuTask = this.taskTransformer.transform(taskAllocation.getTask());
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module = tuScheduler.getModule();
    _builder.append(_module);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module_1 = tuTask.getModule();
    _builder.append(_module_1);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("void ");
    String _call = this.getCall(taskAllocation);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    {
      EMap<SchedulingParameterDefinition, Value> _schedulingParameters = null;
      if (taskAllocation!=null) {
        _schedulingParameters=taskAllocation.getSchedulingParameters();
      }
      boolean _tripleNotEquals = (_schedulingParameters != null);
      if (_tripleNotEquals) {
        _builder.append("\t");
        _builder.append("TaskAllocation ta;");
        _builder.newLine();
        {
          Set<Map.Entry<SchedulingParameterDefinition, Value>> _entrySet = taskAllocation.getSchedulingParameters().entrySet();
          for(final Map.Entry<SchedulingParameterDefinition, Value> paramEntry : _entrySet) {
            _builder.append("\t");
            String _createSchedParam = this.schedulingParameterTransformer.createSchedParam("ta", "setSchedulingParameter", false, paramEntry.getKey(), paramEntry.getValue());
            _builder.append(_createSchedParam, "\t");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        String _call_1 = tuTask.getCall();
        _builder.append(_call_1, "\t");
        _builder.append("->setTaskAllocation(ta);");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    String _call_2 = tuScheduler.getCall();
    _builder.append(_call_2, "\t");
    _builder.append("()->addTaskMapping(");
    String _call_3 = tuTask.getCall();
    _builder.append(_call_3, "\t");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
