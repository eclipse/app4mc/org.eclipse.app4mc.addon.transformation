/**
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.DataRate;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwAccessPath;
import org.eclipse.app4mc.amalthea.model.HwPathElement;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.utils.DataRateUtil;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@Singleton
@SuppressWarnings("all")
public class HwAccessElementTransformer extends BaseTransformer {
  @Inject
  private OutputBuffer outputBuffer;

  @Inject
  private HwPathElementTransformer hwPathElementTransformer;

  @Inject
  private HwModuleTransformer hwModuleTransformer;

  private String getModulePath(final HwAccessElement hwAccessElement) {
    if ((hwAccessElement != null)) {
      String _modulePath = HwModelTransformer.getModulePath();
      String _plus = (_modulePath + "/hwAccessElements/");
      String _name = this.getName(hwAccessElement);
      return (_plus + _name);
    } else {
      return HwModelTransformer.getModulePath();
    }
  }

  protected String getCall(final HwAccessElement hwAccessElement) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("get_HwStructure_");
    String _name = this.getName(hwAccessElement);
    _builder.append(_name);
    return _builder.toString();
  }

  private String getName(final HwAccessElement hwAccessElement) {
    return hwAccessElement.getName();
  }

  public TranslationUnit transform(final HwAccessElement hwAccessElement) {
    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(hwAccessElement);
    final TranslationUnit _result;
    synchronized (_createCache_transform) {
      if (_createCache_transform.containsKey(_cacheKey)) {
        return _createCache_transform.get(_cacheKey);
      }
      String _modulePath = this.getModulePath(hwAccessElement);
      String _call = this.getCall(hwAccessElement);
      TranslationUnit _translationUnit = new TranslationUnit(_modulePath, _call);
      _result = _translationUnit;
      _createCache_transform.put(_cacheKey, _result);
    }
    _init_transform(_result, hwAccessElement);
    return _result;
  }

  private final HashMap<ArrayList<?>, TranslationUnit> _createCache_transform = CollectionLiterals.newHashMap();

  private void _init_transform(final TranslationUnit tu, final HwAccessElement hwAccessElement) {
    this.outputBuffer.appendTo("INC", tu.getModule(), this.toH(hwAccessElement));
    this.outputBuffer.appendTo("SRC", tu.getModule(), this.toCpp(hwAccessElement));
  }

  private String toH(final HwAccessElement hwAccessElement) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include <systemc>");
    _builder.newLine();
    _builder.append("#include <memory>");
    _builder.newLine();
    _builder.append("#include \"Common.h\"");
    _builder.newLine();
    _builder.append("#include \"HardwareModel.h\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("HwAccessElement ");
    String _call = this.getCall(hwAccessElement);
    _builder.append(_call);
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }

  private String toCpp(final HwAccessElement hwAccessElement) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#include \"");
    String _modulePath = this.getModulePath(hwAccessElement);
    _builder.append(_modulePath);
    _builder.append(".h\"\t");
    _builder.newLineIfNotEmpty();
    _builder.append("//include path elementss");
    _builder.newLine();
    {
      HwAccessPath _accessPath = hwAccessElement.getAccessPath();
      boolean _tripleNotEquals = (_accessPath != null);
      if (_tripleNotEquals) {
        {
          EList<HwPathElement> _pathElements = hwAccessElement.getAccessPath().getPathElements();
          for(final HwPathElement pathElement : _pathElements) {
            _builder.append("#include \"");
            String _module = this.hwPathElementTransformer.transform(pathElement).getModule();
            _builder.append(_module);
            _builder.append(".h\"");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("#include \"");
    String _module_1 = this.hwModuleTransformer.transform(hwAccessElement.getSource()).getModule();
    _builder.append(_module_1);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.append("#include \"");
    String _module_2 = this.hwModuleTransformer.transform(hwAccessElement.getDestination()).getModule();
    _builder.append(_module_2);
    _builder.append(".h\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/* HwAccessWlement not contained in structure, eg. in flat hardware model */");
    _builder.newLine();
    _builder.append("HwAccessElement ");
    String _call = this.getCall(hwAccessElement);
    _builder.append(_call);
    _builder.append("(){");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("//utilize RTO here");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("HwAccessElement returnVal;");
    _builder.newLine();
    {
      IDiscreteValueDeviation _readLatency = hwAccessElement.getReadLatency();
      boolean _tripleNotEquals_1 = (_readLatency != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t");
        _builder.append("returnVal.setReadLatency(");
        String _deviation = IDiscreteValueDeviationTransformer.getDeviation(hwAccessElement.getReadLatency());
        _builder.append(_deviation, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      IDiscreteValueDeviation _writeLatency = hwAccessElement.getWriteLatency();
      boolean _tripleNotEquals_2 = (_writeLatency != null);
      if (_tripleNotEquals_2) {
        _builder.append("\t");
        _builder.append("returnVal.setWriteLatency(");
        String _deviation_1 = IDiscreteValueDeviationTransformer.getDeviation(hwAccessElement.getWriteLatency());
        _builder.append(_deviation_1, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      DataRate _dataRate = hwAccessElement.getDataRate();
      boolean _tripleNotEquals_3 = (_dataRate != null);
      if (_tripleNotEquals_3) {
        _builder.append("\t");
        _builder.append("returnVal.setDataRate(");
        String _transform = DataRateUtil.transform(hwAccessElement.getDataRate());
        _builder.append(_transform, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    {
      HwAccessPath _accessPath_1 = hwAccessElement.getAccessPath();
      boolean _tripleNotEquals_4 = (_accessPath_1 != null);
      if (_tripleNotEquals_4) {
        _builder.append("\t");
        final LinkedList<String> calls = this.getAccessElementCalls(hwAccessElement);
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("returnVal.path.elements.assign({");
        {
          for(final String call : calls) {
            _builder.append(" ");
            _builder.append(call, "\t");
            _builder.append(" ");
          }
        }
        _builder.append("});");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//set source");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("returnVal.source = ");
    String _call_1 = this.hwModuleTransformer.transform(hwAccessElement.getSource()).getCall();
    _builder.append(_call_1, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//set destination");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("returnVal.dest = ");
    String _call_2 = this.hwModuleTransformer.transform(hwAccessElement.getDestination()).getCall();
    _builder.append(_call_2, "\t");
    _builder.append("();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return returnVal;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }

  public LinkedList<String> getAccessElementCalls(final HwAccessElement hwAccessElement) {
    final LinkedList<String> calls = new LinkedList<String>();
    int _size = hwAccessElement.getAccessPath().getPathElements().size();
    boolean _greaterThan = (_size > 1);
    if (_greaterThan) {
      EList<HwPathElement> _pathElements = hwAccessElement.getAccessPath().getPathElements();
      int _size_1 = hwAccessElement.getAccessPath().getPathElements().size();
      int _minus = (_size_1 - 1);
      final List<HwPathElement> elements = _pathElements.subList(0, _minus);
      final Function1<HwPathElement, TranslationUnit> _function = (HwPathElement it) -> {
        return this.hwPathElementTransformer.transform(it);
      };
      final List<TranslationUnit> tus = ListExtensions.<HwPathElement, TranslationUnit>map(elements, _function);
      final Consumer<TranslationUnit> _function_1 = (TranslationUnit it) -> {
        String _call = it.getCall();
        String _plus = (_call + "(),");
        calls.add(_plus);
      };
      tus.forEach(_function_1);
    }
    String _call = this.hwPathElementTransformer.transform(IterableExtensions.<HwPathElement>last(hwAccessElement.getAccessPath().getPathElements())).getCall();
    String _plus = (_call + "()");
    calls.add(_plus);
    return calls;
  }

  public HashMap<ArrayList<?>, TranslationUnit> getCache() {
    return this._createCache_transform;
  }
}
