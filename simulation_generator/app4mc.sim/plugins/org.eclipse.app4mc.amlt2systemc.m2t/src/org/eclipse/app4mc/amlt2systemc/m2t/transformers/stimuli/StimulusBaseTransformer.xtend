/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer

abstract class StimulusBaseTransformer extends BaseTransformer {
	
	
	def getName(Stimulus stim){
		return stim.name
	}

	def getModulePath(Stimulus stim) {
		return AmaltheaTransformer.getModulePath() + "/stimuliModel/" + getName(stim)
	}

	def getFunctionDef(Stimulus stim) {
		return "get_" + stim.name + "()"
	}
		

}
