/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaModel2TextTransformer;
import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.transformers.Model2TextRootTransformer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.google.inject.Guice;
import com.google.inject.Injector;

@Component(
		configurationPid = ServiceConstants.SESSION_CONFIGURATION_PID,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = { ServiceConstants.TRANSFORMATION_PROPERTY + "=APP4MCSIM" },
		service = Model2TextRootTransformer.class)

public class SystemCRootTransformer extends Model2TextRootTransformer {

	@Reference
	SystemCGuiceModuleFactory factory;

	Properties properties;

	@Activate
	@Override
	protected void activate(Map<String, ?> properties) {
		super.activate(properties);
		this.properties = new Properties();
		this.properties.putAll(properties);
		
		// set the output directory to the local properties
		this.properties.put(TransformationConstants.TRANSFORMATION_OUTPUT_FOLDER, getOutputFolder());
	}
	
	@Override
	public void m2tTransformation() {

		Injector injector = Guice.createInjector(factory.getModule(logger, properties));

		AmaltheaModel2TextTransformer instance = injector.getInstance(AmaltheaModel2TextTransformer.class);

		ResourceSet inputResourceSet = getInputResourceSet(getInputFolder(), logger);
		setProjectName(inputResourceSet, this.properties);

		instance.m2tTransformation(inputResourceSet);

	}

	private void setProjectName(ResourceSet inputResourceSet, Properties props) {
		if (!props.containsKey(PropertyKeys.PROJECT_NAME)) {

			EList<Resource> resources = inputResourceSet.getResources();
			if (! resources.isEmpty()) {

				String devicePath = resources.get(0).getURI().toFileString();
				String fragment = devicePath.substring(devicePath.lastIndexOf(File.separator) + 1,
						devicePath.lastIndexOf("."));
				fragment = fragment.replace(".", "_");
				props.put(PropertyKeys.PROJECT_NAME, (Object) fragment);
			}
		}
	}

}
