/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem
import org.eclipse.app4mc.amalthea.model.ChannelAccess
import org.eclipse.app4mc.amalthea.model.Group
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger
import org.eclipse.app4mc.amalthea.model.LabelAccess
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess
import org.eclipse.app4mc.amalthea.model.RunnableCall
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess
import org.eclipse.app4mc.amalthea.model.Switch
import org.eclipse.app4mc.amalthea.model.Ticks
import org.eclipse.app4mc.amalthea.model.WhileLoop
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer

class ActivityGraphItemTransformer extends BaseTransformer {
	
	//@Inject OutputBuffer outputBuffer
	@Inject SwitchTransformer switchTransformer
	@Inject RunnableTransformer runnableTransformer
	@Inject TicksTransformer ticksTransformer
	@Inject ChannelAccessTransformer channelAccessTransformer
	@Inject LabelAccessTransformer labelAccessTransformer
	@Inject ModeLabelAccessTransformer modeLabelAccessTransformer
	@Inject InterProcessTriggerTransformer interProcessTriggerTransformer
	@Inject WhileLoopTransformer whileLoopTransformer
	@Inject SemaphoreAccessTransformer semaphoreAccrssTransformer
	@Inject GroupTransformer groupTransformer

	dispatch def void transform(ActivityGraphItem activityGraphItem, 
		AgiContainerBuffer containerContent) {	
		containerContent.addBody("//WARNING: no transformation for activity item " + activityGraphItem.class.toString)
		throw new UnsupportedOperationException("//WARNING: no transformation for activity item " + activityGraphItem.class.toString)
	}

	dispatch def void transform(RunnableCall runnableCall, 
		AgiContainerBuffer containerContent) {
		val tuRunnable = runnableTransformer.transform(runnableCall.runnable)
		containerContent.addInclude(tuRunnable.module)
		containerContent.addAsActivityGraphItem("<RunnableCall>({" + tuRunnable.call + "})")
	}

	dispatch def void transform(Ticks ticks, 
		AgiContainerBuffer containerContent) {
		// example: r2->addActivityGraphItem<Ticks>({IValueDeviationConstant<ELong>(100000)});
		containerContent.addAsActivityGraphItem("<Ticks>({" + ticksTransformer.transform(ticks) + "})");
	}

	dispatch def void transform(ChannelAccess channelAccess, 
		AgiContainerBuffer containerContent) {
		channelAccessTransformer.transform(channelAccess, containerContent);
	}

	dispatch def void transform(LabelAccess labelAccess, 
		AgiContainerBuffer containerContent) {
		labelAccessTransformer.transform(labelAccess, containerContent);
	}

	dispatch def void transform(ModeLabelAccess modeLabelAccess, 
		AgiContainerBuffer containerContent) {
		modeLabelAccessTransformer.transform(modeLabelAccess, containerContent);
	}
	
	dispatch def void transform(InterProcessTrigger interProcessTrigger, 
		AgiContainerBuffer containerContent) {
		interProcessTriggerTransformer.transform(interProcessTrigger, containerContent);
	}
	
	 dispatch def void transform(Switch _switch, 
		AgiContainerBuffer containerContent) {	 
		switchTransformer.transform(_switch, containerContent);	
	}
	
	 dispatch def void transform(WhileLoop whileLoop, 
		AgiContainerBuffer containerContent) {	 
		whileLoopTransformer.transform(whileLoop, containerContent);	
	}

	dispatch def void transform(SemaphoreAccess semaphoreAccess, 
		AgiContainerBuffer containerContent) {	 
		semaphoreAccrssTransformer.transform(semaphoreAccess, containerContent);	
	}

	dispatch def void transform(Group group, 
		AgiContainerBuffer containerContent) {	 
		groupTransformer.transform(group, containerContent);	
	}

}
