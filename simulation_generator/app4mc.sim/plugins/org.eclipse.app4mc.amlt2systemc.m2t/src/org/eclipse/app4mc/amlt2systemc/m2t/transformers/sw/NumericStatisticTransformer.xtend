/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import org.eclipse.app4mc.amalthea.model.MinAvgMaxStatistic
import org.eclipse.app4mc.amalthea.model.SingleValueStatistic
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer

class NumericStatisticTransformer extends BaseTransformer {

	def dispatch transform(MinAvgMaxStatistic minAvgMaxStatistic) {
		return "memory access statistic not yet supported"
	}

	def dispatch transform(SingleValueStatistic singleValueStatistic) {
		return "memory access statistic not yet supported"
	}

	def dispatch transform(Object stat) {
		if(stat === null) return "1"
		return "1"
	}

}
