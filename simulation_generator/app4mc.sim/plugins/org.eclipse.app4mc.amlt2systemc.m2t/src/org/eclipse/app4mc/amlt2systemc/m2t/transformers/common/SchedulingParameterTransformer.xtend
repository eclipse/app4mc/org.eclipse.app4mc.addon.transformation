/**
 * *******************************************************************************
 * Copyright (c) 2019-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common

import org.eclipse.app4mc.amalthea.model.ParameterType
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition
import org.eclipse.app4mc.amalthea.model.Time
import org.eclipse.app4mc.amalthea.model.Value
import org.eclipse.app4mc.amalthea.model.impl.BooleanObjectImpl
import org.eclipse.app4mc.amalthea.model.impl.FloatObjectImpl
import org.eclipse.app4mc.amalthea.model.impl.IntegerObjectImpl
import org.eclipse.app4mc.amalthea.model.impl.StringObjectImpl
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil

class SchedulingParameterTransformer  extends BaseTransformer {

	def String createSchedParam(String attributeeName, String functionName, boolean attributeeIsPointer, SchedulingParameterDefinition schedulingParameterDefinition, Value value) {
		return attributeeName + (attributeeIsPointer ? "->" : ".") + functionName + "(\"" + schedulingParameterDefinition.name + "\", " + getValueString(schedulingParameterDefinition.getType() , value) + ");";
	}

	private def getValueString(ParameterType paramType, Value value){
		switch(paramType){
			case INTEGER:
				return (value as IntegerObjectImpl).getValue().toString()
			case TIME: 
				return TimeUtil.transform(value as Time).toString()
			case BOOL:
				return (value as BooleanObjectImpl).isValue().toString()
			case FLOAT:
				return (value as FloatObjectImpl).getValue().toString()
			case STRING:
				return (value as StringObjectImpl).getValue().toString()
			default:
				throw new IllegalArgumentException("scheduling parameter of type " + paramType + " is not supported.")
		}
	}
}