/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import org.eclipse.app4mc.amalthea.model.Group
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer

class GroupTransformer extends IActivityGraphItemContainerTransformer {
	
	
	override protected getContainerDescription(IActivityGraphItemContainer container) {
		return "Group: " + (container as Group).name;
	}
	
	override protected transformContainer(IActivityGraphItemContainer container, AgiContainerBuffer parentContent) {
		val groupContents = transformItems((container as Group), parentContent.instanceName, parentContent.module, parentContent.parentIsPointer)
		
		parentContent.getIncludes().addAll(groupContents.getIncludes())
		parentContent.getSource().addAll(groupContents.getSource())
	}

}
