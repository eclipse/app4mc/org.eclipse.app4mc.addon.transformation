/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.EventStimulus
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.event.EventTransformer
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer

@Singleton
class EventStimulusTransformer extends StimulusBaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject EventTransformer eventsDispatcher
	@Inject ConditionTransformer conditionTransformer;
		
	def create new TranslationUnit(
		getModulePath(stim),
		getFunctionDef(stim)
		
	) transform(EventStimulus stim) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(stim))
		// write implementation file
		outputBuffer.appendTo("SRC", it.module, toCpp(stim))
	}

	private def String toH(EventStimulus stim) '''
		// This code was generated for simulation with app4mc.sim
			
		#pragma once
		#include "Common.h"
		#include "Stimuli/EventStimulus.h"
		
		//stim runnableA----
		std::shared_ptr<EventStimulus> «getFunctionDef(stim)»;
	'''

	private def String toCpp(EventStimulus stim) '''
		// This code was generated for simulation with app4mc.sim
			
		#include "«getModulePath(stim)».h"
		«val conditionContent = conditionTransformer.transformCondition(
			stim.executionCondition, getName(stim), true)»
		«ConditionBuffer.getConditionIncludes(conditionContent)»
				
		«FOR event: stim.triggeringEvents»
			#include "«eventsDispatcher.transform(event).module».h"
		«ENDFOR»
		
		std::shared_ptr<EventStimulus> «stim.name» = nullptr;
		
		std::shared_ptr<EventStimulus>  «getFunctionDef(stim)» {
		
			if («stim.name» == nullptr) {
				//initialize
				«FOR event: stim.triggeringEvents»
					«stim.name» = std::make_shared<EventStimulus>("«stim.name»", «eventsDispatcher.transform(event).call»);
				«ENDFOR»
				«ConditionBuffer.getConditionSource(conditionContent)»
			}
			return «stim.name»;
		}
		
	'''

	def getCache() { return this._createCache_transform }

}
