/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping

import com.google.inject.Inject
import com.google.inject.Singleton
import java.util.HashSet
import org.eclipse.app4mc.amalthea.model.MappingModel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys

@Singleton
class MappingModelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject MemoryMappingTransformer memoryMappingTransformer
	@Inject TaskAllocationTransformer taskAllocationTransformer
	@Inject SchedulerAllocationTransformer schedulerAllocationTransformer

	protected static def getModulePath() {
		return AmaltheaTransformer.getModulePath() + "/mapModel"
	}

	private def getModuleName() {
		return getModulePath + "/mapModel"
	}

	private def getFunctionDef() {
		return "init_mappingModel()"
	}

	def create new TranslationUnit(
		getModuleName(),
		getFunctionDef()		
	) transform(MappingModel[] mappingModels) {
		mappingModels.forEach[mappingModel | {	
			// start transformation @ task level, 
			// Note: generate referenced object (e.g. runnables) when referenced (eg. in a runnable call within the call graph)
			mappingModel.memoryMapping.forEach[memMapping|memoryMappingTransformer.transform(memMapping)]
			mappingModel.taskAllocation.forEach[taskAllocation|taskAllocationTransformer.transform(taskAllocation)]
			mappingModel.schedulerAllocation.forEach [ schedulerAllocation |
				schedulerAllocationTransformer.transform(schedulerAllocation)
			]
		}]

		outputBuffer.appendTo("INC", it.module, toH())
		outputBuffer.appendTo("SRC", it.module, toCpp())
		outputBuffer.appendTo("OTHER", getModulePath + "/CMakeLists.txt", getCMake(it.module));
	}

	private def String toH() '''
		#include <systemc>
		#include <memory>
		#include "APP4MCsim.h"
		
		
		//include model elements
		«val uniqueMemoryMappings = new HashSet<String>()»
		«memoryMappingTransformer.cache.values.forEach[uniqueMemoryMappings.add(it.module)]»
		«FOR module : uniqueMemoryMappings»
			#include "«module».h"
		«ENDFOR»	
		«FOR obj : TuSort.byModule(taskAllocationTransformer.cache.values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»	
		«FOR obj : TuSort.byModule(schedulerAllocationTransformer.cache.values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»	
		void «getFunctionDef()»;		
	'''

	private def String toCpp() '''
		#include "«getModuleName».h"
		
		void «getFunctionDef()»{
			«FOR obj : TuSort.byCall(memoryMappingTransformer.cache.values)»
				«(obj as TranslationUnit).call»(); 
			«ENDFOR»
			«FOR obj : TuSort.byCall(taskAllocationTransformer.cache.values)»
				«(obj as TranslationUnit).call»(); 
			«ENDFOR»
			«FOR obj : TuSort.byCall(schedulerAllocationTransformer.cache.values)»
				«(obj as TranslationUnit).call»(); 
			«ENDFOR»
		}
	'''

	def String getCMake(String thisModule) '''	
		# CMakeList.txt: CMake project for MappingModel of "«getProperty(PropertyKeys.PROJECT_NAME)»".
						
				
		# Add sources of MappingModel
		target_sources («getProperty(PropertyKeys.PROJECT_NAME)»  PRIVATE
		«val uniqueMemoryMappings = new HashSet<String>()»
		«memoryMappingTransformer.cache.values.forEach[uniqueMemoryMappings.add(it.module)]»
		«FOR module : uniqueMemoryMappings»
			"${PROJECT_SOURCE_DIR}/«module».cpp" 
		«ENDFOR»
		«FOR obj : memoryMappingTransformer.cache.values» 
		«ENDFOR»
		«FOR obj : TuSort.byModule(taskAllocationTransformer.cache.values)» 
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		«FOR obj : TuSort.byModule(schedulerAllocationTransformer.cache.values)» 
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		"${PROJECT_SOURCE_DIR}/«thisModule».cpp")
	'''
	
	def getCache(){
		return this._createCache_transform
	}
}
