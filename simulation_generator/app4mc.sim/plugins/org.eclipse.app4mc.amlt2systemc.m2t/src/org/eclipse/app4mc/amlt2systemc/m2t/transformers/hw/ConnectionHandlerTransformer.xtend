 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.ConnectionHandler
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.utils.DataRateUtil
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class ConnectionHandlerTransformer extends HwModuleTransformer {

	@Inject OutputBuffer outputBuffer;
	@Inject FrequencyDomainTransformer frequencyDomainTransformer

	def create tu : new TranslationUnit( getModulePath(connectionHandler),getCall(connectionHandler) ) transform(
		ConnectionHandler connectionHandler) {
		//sanity check
		if (connectionHandler.frequencyDomain === null)
			throw new RuntimeException("ConnecionHandler " + connectionHandler.qualifiedName + " is not associated with a frequency domain.")
		outputBuffer.appendTo("INC", tu.module, toH(connectionHandler))
		outputBuffer.appendTo("SRC", tu.module, toCpp(connectionHandler))

	}

	def String toH(ConnectionHandler connectionHandler) '''
		//framework
		#include "HardwareModel.h"
		
		std::shared_ptr<ConnectionHandler> «getCall(connectionHandler)»();
	'''

	def String toCpp(ConnectionHandler connectionHandler) '''
		#include "«getModulePath(connectionHandler)».h"
		«getParentInclude(connectionHandler)»
		«IF (connectionHandler.frequencyDomain !== null)»
			#include "«frequencyDomainTransformer.transform(connectionHandler.frequencyDomain).module».h"
		«ENDIF»
		«getIncludesForConnections(connectionHandler)»
		
		«val name = getName(connectionHandler)»
		std::shared_ptr<ConnectionHandler> «name» = nullptr;
		
		//for usage in structures
		std::shared_ptr<ConnectionHandler> «getCall(connectionHandler)»(){
			auto parent = «getParentGetter(connectionHandler)»();
			if («name»==NULL){
				«name» =  parent->createModule<ConnectionHandler>("«name»");
				//parameters from definition
				«IF (connectionHandler?.definition !== null)»
					«setReadLatency(connectionHandler)»
					«setWriteLatency(connectionHandler)»
					«setDataRate(connectionHandler)»
				«ENDIF»
				//ports
				«setPortsAndConnections(connectionHandler)»
				//frequency domain
				«name»->clock_period= «frequencyDomainTransformer.transform(connectionHandler.frequencyDomain).call»;
			}
			return «name»;
		}
	'''

	def String setReadLatency(ConnectionHandler module) '''
		«val name = getName(module)»
		«val deviation = module?.definition?.readLatency»
		«IF ( deviation !== null)»
			«name»->setReadLatency(«IDiscreteValueDeviationTransformer.getDeviation(deviation)»);
		«ENDIF»
	'''

	def String setWriteLatency(ConnectionHandler module) '''
		«val name = getName(module)»
		«val deviation = module?.definition?.writeLatency»
		«IF ( deviation !== null)»
			«name»->setWriteLatency(«IDiscreteValueDeviationTransformer.getDeviation(deviation)»);
		«ENDIF»
	'''

	def String setDataRate(ConnectionHandler module) '''
		«val name = getName(module)»
		«IF (module?.definition?.dataRate !== null)»
			«name»->setDataRate(«DataRateUtil.transform(module.definition.dataRate)»);
		«ENDIF»
	'''

	def getCache() { return this._createCache_transform }

}
