/**
 * *******************************************************************************
 * Copyright (c) 2019-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.event;

import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.ChannelEvent;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class EventTransformer extends BaseTransformer {

	@Inject ChannelEventTransformer channelEventTransformer;

	public TranslationUnit transform(Event event){
		if (event instanceof ChannelEvent){
			return channelEventTransformer.transform((ChannelEvent)event);
		} 
		else{
			return new TranslationUnit("//unsupported stimulus " + event.getName(), "//unsupported event " + event.getName());			
		}
	}
	
	public Map<List<Object>, TranslationUnit> getCache(Class<? extends Event> type){
		if (type == ChannelEvent.class){
			return channelEventTransformer.getCache();
		} 
		else{
			return null;			
		}
	}
	
}
