/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.EnumMode
import org.eclipse.app4mc.amalthea.model.ModeLabelCondition
import org.eclipse.app4mc.amalthea.model.ModeValueCondition
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ModeLabelTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ModeTransformer

/** Target code example
 * 
 * 	SwitchEntry caseA("case A");
 * 	caseA.addCondition(modelabel1, lita);
 * 	caseA.addActivityGraphItem<ModeLabelAccess>({modelabel1, litb});
 * 	caseA.addActivityGraphItem<RunnableCall>({ra});
 * 
 * 	ConditionConjunction conj_caseDandD;// = caseDandD.createConjunction();
 * 	conj_caseDandD.addCondition(modelabel1, litd);
 * 	conj_caseDandD.addCondition(modelabel2, litd)
 * 
 * 	usage in Switch:
 * 	switch.addEntry(caseA); OR switch.setDefaultEntry(caseA);
 * 	
 */
class ModeConditionTransformer extends BaseTransformer {

	@Inject ModeLabelTransformer modeLabelTransformer
	@Inject ModeTransformer modeTransformer

	def dispatch createCondition(String parent, ModeLabelCondition condition, ConditionBuffer content) {
		val tuLabel1 = modeLabelTransformer.transform(condition.label1)
		val tuLabel2 = modeLabelTransformer.transform(condition.label2)
		content.addInclude(tuLabel1.module)
		content.addInclude(tuLabel2.module)
		content.addCondition(tuLabel1.call + ", " + tuLabel2.call + ", " +
			ConditionTransformer.getRelationalOperator(condition.relation))
	}

	def dispatch createCondition(String parent, ModeValueCondition condition, ConditionBuffer content) {
		val tuLabel = modeLabelTransformer.transform(condition.label)
		content.addInclude(tuLabel.module)

		if (condition.label.mode instanceof EnumMode) {
			val tuMode = modeTransformer.transform(condition.label.mode)
			content.addInclude(tuMode.module)
			content.addCondition(
				tuLabel.call + ", " + tuMode.call + "->getLiteral(\"" + condition.value + "\"), " +
					ConditionTransformer.getRelationalOperator(condition.relation)
			)
		} else { // numeric: use string
			content.addCondition(tuLabel.call + ", " + condition.value + ", " +
				ConditionTransformer.getRelationalOperator(condition.relation))
		}
	}

}
