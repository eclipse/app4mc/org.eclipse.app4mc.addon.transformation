/**
 * Copyright (c) 2019-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.event;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.ChannelEvent;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ChannelEventTransformer{

	@Inject private OutputBuffer outputBuffer;
	@Inject private ChannelEventGenerator channelEventGenerator;

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, TranslationUnit> transformCache = new HashMap<>();

	public Map<List<Object>, TranslationUnit> getCache() {
		return this.transformCache;
	}

	public TranslationUnit transform(final ChannelEvent event) {
		final List<Object> key = List.of(event);
		final TranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(event);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, event);
		}

		return tu;
	}

	// ---------------------------------------------------

	private TranslationUnit createTranslationUnit(final ChannelEvent event) {
		if ((event == null)) {
			return new TranslationUnit("UNSPECIFIED_C");
		} else {
			String module = ChannelEventGenerator.getModulePath(event);
			String call = ChannelEventGenerator.getFunctionDef(event);
			return new TranslationUnit(module, call);
		}
	}

	private void doTransform(final TranslationUnit eventTU, final ChannelEvent event) {

		this.outputBuffer.appendTo("INC", eventTU.getModule(), channelEventGenerator.toH(event));
		this.outputBuffer.appendTo("SRC", eventTU.getModule(), channelEventGenerator.toCpp(event));
	}

}
