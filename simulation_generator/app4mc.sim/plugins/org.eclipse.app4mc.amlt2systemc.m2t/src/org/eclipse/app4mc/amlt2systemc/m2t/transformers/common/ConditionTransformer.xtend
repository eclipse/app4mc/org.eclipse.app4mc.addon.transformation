/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common

import com.google.inject.Inject
import java.util.LinkedHashSet
import java.util.LinkedList
import org.eclipse.app4mc.amalthea.model.Condition
import org.eclipse.app4mc.amalthea.model.ConditionConjunction
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction
import org.eclipse.app4mc.amalthea.model.ModeLabelCondition
import org.eclipse.app4mc.amalthea.model.ModeValueCondition
import org.eclipse.app4mc.amalthea.model.RelationalOperator
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amalthea.model.ChannelFillCondition

class ConditionTransformer extends BaseTransformer {
	
	@Inject ModeConditionTransformer modeConditionTransformer
	@Inject ChannelFillConditionTransformer channelFillConditionTransformer

	static class ConditionBuffer{
		val LinkedHashSet<String> includes
		val LinkedList<String> conditions

		public val String instanceName
		public val boolean parentIsPointer
		
		new(String _name, boolean _parentIsPointer){
			instanceName =_name
			parentIsPointer = _parentIsPointer
			includes = new  LinkedHashSet<String>
			conditions = new LinkedList<String>()
		}
				
		new(ConditionBuffer other){
			instanceName = other.instanceName
			parentIsPointer = other.parentIsPointer
			includes = other.includes
			conditions = other.conditions
		}
		
		def addAll(ConditionBuffer other){
			includes.addAll(other.includes)
			conditions.addAll(other.conditions)
		}
		
		def LinkedHashSet<String> getIncludes(){
			return includes
		}

		def LinkedList<String> getConditions(){
			return conditions
		}
		
		def void addInclude(String includePath){
			includes.add("#include \"" + includePath + ".h\"")
		}
		
		def void addCondition(String params){
			conditions.add(instanceName + (parentIsPointer ? "->" : ".") + "addCondition(" + params + ");")
		}
		
		
		def void addConjunction(String conjunctionName, ConditionBuffer conjunctiveConditions){
			conditions.add("ConditionConjunction " + conjunctionName + ";\n")
			addAll(conjunctiveConditions)
			conditions.add(instanceName + (parentIsPointer ? "->" : ".") + "addConjunction(" + conjunctionName + ");")	
		}
			
		static def getConditionIncludes(ConditionBuffer _content)'''
			«IF _content !== null»
				«FOR include : _content.includes»
					«include»
				«ENDFOR»
			«ENDIF»
		'''
	
		static def getConditionSource(ConditionBuffer _content)'''
			«IF _content !== null»
				«FOR source : _content.conditions»
					«source»
				«ENDFOR»
			«ENDIF»
		'''		
	}
	
	static def getRelationalOperator(RelationalOperator relation){
		switch(relation){
			case RelationalOperator.EQUAL: return "RelationalOperator::EQUAL"
			case RelationalOperator.NOT_EQUAL: return "RelationalOperator::NOT_EQUAL"
			case RelationalOperator.GREATER_THAN: return "RelationalOperator::GREATER_THAN"
			case RelationalOperator.LESS_THAN: return "RelationalOperator::LESS_THAN"
			default: throw new RuntimeException("RelationalOperator " + relation + " is not supported.")
		}
	}
	
	def transformCondition(ConditionDisjunction disjunction, String parentName, boolean parentIsPointer){
		if (disjunction === null) return null
		val content = new ConditionBuffer(parentName, parentIsPointer);
		for (disjunctionEntry : disjunction.entries){
			if (disjunctionEntry instanceof ConditionConjunction)
				createConjunction(content.instanceName, disjunctionEntry as ConditionConjunction, content)
			else if (disjunctionEntry instanceof Condition)
				createCondition(content.instanceName, disjunctionEntry, content)
		}	
		return content
	}
	
	static int conjunctionCnt =0
	private def createConjunction(String parent, ConditionConjunction conjunction, ConditionBuffer content){
		val name = parent + "_AND" + conjunctionCnt
		val conjunctiveConditions = new ConditionBuffer(name, false)
		conjunction.entries.forEach[condition |{
			createCondition(name, condition, conjunctiveConditions)
			conjunctionCnt++
		}]		
		content.addConjunction(name, conjunctiveConditions)
	}
	
	def dispatch createCondition(String parent, Condition condition, ConditionBuffer content){
		throw new IllegalArgumentException("no transformation for conditions of type " + condition.class.toString)
	}
	
	def dispatch createCondition(String parent, ModeLabelCondition modeLabelCondition, ConditionBuffer content){
		modeConditionTransformer.createCondition(parent, modeLabelCondition, content)
	}
	
	def dispatch createCondition(String parent, ModeValueCondition modeValueCondition, ConditionBuffer content){
		modeConditionTransformer.createCondition(parent, modeValueCondition, content)
	}
	
	def dispatch createCondition(String parent, ChannelFillCondition channelFillCondition, ConditionBuffer content){
		channelFillConditionTransformer.createCondition(parent, channelFillCondition, content)
	}

}
