/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer
import org.eclipse.app4mc.amalthea.model.WhileLoop
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer

class WhileLoopTransformer extends IActivityGraphItemContainerTransformer {

	@Inject ConditionTransformer conditionTransformer
	
	static int cnt =0;
	
	static def getName(){
		return "whileLoop_" + cnt;
	}	
	
	override protected getContainerDescription(IActivityGraphItemContainer container) {
		return "WhileLoop"
	}

	override protected transformContainer(IActivityGraphItemContainer container, AgiContainerBuffer parentContent) {
		val name = getName()
		cnt++;	
		
		parentContent.addBody("WhileLoop " + name + ";")
		
		val content = new AgiContainerBuffer(name, parentContent.module, false)
		
		val conditionContent = conditionTransformer.transformCondition((container as WhileLoop).condition, name, false)
		val whileLoopContents = transformItems((container as WhileLoop), name, content.module, content.parentIsPointer)
		whileLoopContents.addAll(conditionContent)
		
		parentContent.getIncludes().addAll(whileLoopContents.getIncludes())
		parentContent.getSource().addAll(whileLoopContents.getSource())
			
		parentContent.addAsActivityGraphItem("<WhileLoop>(" + name + ")")
	}

}


