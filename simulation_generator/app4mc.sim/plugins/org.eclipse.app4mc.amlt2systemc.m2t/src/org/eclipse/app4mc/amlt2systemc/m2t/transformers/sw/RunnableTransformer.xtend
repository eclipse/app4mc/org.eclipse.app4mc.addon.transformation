/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class RunnableTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject ConditionTransformer conditionTransformer
	@Inject ActivityGraphTransformer activityGraphTransformer

	static def String getName(Runnable runnable) {
		return runnable.name; // in case we want to do fancy name (e.g. use unique ID) we can modify this function
	}
	
	static def String getModuleName(Runnable runnable) {
		return SWModelTransformer.getModulePath() + "/runnables/" + getName(runnable)
	}

	static def String getFunctionDef(Runnable runnable) {
		return "get_" + getName(runnable) + "()"
		
	}

	def create new TranslationUnit(
		getModuleName(runnable),
		getFunctionDef(runnable)
		
	) transform(Runnable runnable) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(runnable))
		// write implementation file
		
		val runnableContents = new AgiContainerBuffer(getName(runnable), getModuleName(runnable), true)
		activityGraphTransformer.transform(runnable.activityGraph, runnableContents);
		
		val conditionContent = 
			conditionTransformer.transformCondition(runnable.executionCondition, getName(runnable), true)
		runnableContents.addAll(conditionContent)
		outputBuffer.appendTo("SRC", it.module, toCpp(runnable, runnableContents))
	}

	private def String toH(Runnable runnable) '''
		#include "SoftwareModel.h"
		std::shared_ptr<Runnable> «getFunctionDef(runnable)»;
	'''

	private def String toCpp(Runnable runnable, AgiContainerBuffer graphContent) '''
		#include <systemc>
		#include "APP4MCsim.h"
		#include "«getModuleName(runnable)».h"	
		«FOR include : graphContent.includes»
			«include»
		«ENDFOR»
		
		std::shared_ptr<Runnable> «getName(runnable)» = nullptr;
		
		std::shared_ptr<Runnable>  «getFunctionDef(runnable)» {
		
			if («getName(runnable)» == nullptr) {
				//initialize
				«getName(runnable)» = std::make_shared<Runnable>("«getName(runnable)»");
				«FOR source : graphContent.getSource()»
					«source»
				«ENDFOR»
			}
			return «getName(runnable)»;
		}
		
	'''

	def getCache() {
		this._createCache_transform
	}
}
