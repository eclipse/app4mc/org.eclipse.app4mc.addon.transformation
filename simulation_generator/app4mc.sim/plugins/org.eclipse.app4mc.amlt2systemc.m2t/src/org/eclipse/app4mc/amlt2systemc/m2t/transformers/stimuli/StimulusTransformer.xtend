/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.EventStimulus
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus
import org.eclipse.app4mc.amalthea.model.SingleStimulus
import org.eclipse.app4mc.amalthea.model.Stimulus
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit

@Singleton
class StimulusTransformer extends BaseTransformer {

	@Inject PeriodicStimulusTransformer periodicStimulusTransformer
	@Inject SingleStimulusTransformer singleStimulusTransformer
	@Inject RelativePeriodicStimulusTransformer relativePeriodicStimulusTransformer
	@Inject InterProcessStimulusTransformer interProcessStimulusTransformer
	@Inject EventStimulusTransformer eventStimulusTransformer;

	def dispatch TranslationUnit transform(PeriodicStimulus stimulus) {
		return periodicStimulusTransformer.transform(stimulus)
	}
	
	def dispatch TranslationUnit transform(SingleStimulus stimulus) {
		return singleStimulusTransformer.transform(stimulus)
	}
	
	def dispatch TranslationUnit transform(RelativePeriodicStimulus stimulus) {
		return relativePeriodicStimulusTransformer.transform(stimulus)
	}
	
	def dispatch TranslationUnit transform(InterProcessStimulus stimulus) {
		return interProcessStimulusTransformer.transform(stimulus)
	}
	
	def dispatch TranslationUnit transform(Stimulus stimulus) {
		return new TranslationUnit("//unsupported stimulus " + stimulus.name, "//unsupported stimulus " + stimulus.name)
	}
	
	def dispatch TranslationUnit transform(EventStimulus stimulus) {
		return eventStimulusTransformer.transform(stimulus)
	}
	
	def getCache(Class<? extends Stimulus> type){
		switch type{
			case typeof(SingleStimulus) : return singleStimulusTransformer.cache
			case typeof(PeriodicStimulus) : return periodicStimulusTransformer.cache
			case typeof(InterProcessStimulus) : return interProcessStimulusTransformer.cache 
			case typeof(EventStimulus) : return eventStimulusTransformer.cache
			case typeof(RelativePeriodicStimulus) : return relativePeriodicStimulusTransformer.cache
			default: return null
		}
	}

		
}
