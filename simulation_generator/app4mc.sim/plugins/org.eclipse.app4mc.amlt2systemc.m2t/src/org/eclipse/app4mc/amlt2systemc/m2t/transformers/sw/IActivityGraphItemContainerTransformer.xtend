/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer

abstract class IActivityGraphItemContainerTransformer extends BaseTransformer {

	@Inject ActivityGraphItemTransformer activityGraphItemTransformer
	
	def protected String getContainerDescription(IActivityGraphItemContainer container)
	
	def protected void transformContainer(IActivityGraphItemContainer container, AgiContainerBuffer parentContent)
	
	private def getStartOfContainer(IActivityGraphItemContainer container){
		return "/*----" + "Begin " + getContainerDescription(container) + "----*/"
	}
	
	private def getEndOfContainerComment(IActivityGraphItemContainer container){
		return "/*----" + "End " + getContainerDescription(container) + "----*/"
	}
		
	def final void transform(IActivityGraphItemContainer container, AgiContainerBuffer parentContent){
		parentContent.addBody(getStartOfContainer(container))
		transformContainer(container, parentContent)
		parentContent.addBody(getEndOfContainerComment(container))
	}
	
	protected def AgiContainerBuffer transformItems(IActivityGraphItemContainer container, String parentInstanceName, String parentModule, boolean parentIsPointer){
		val  containerContent = new AgiContainerBuffer(parentInstanceName, parentModule, parentIsPointer)
		
		if (container === null){ 
			return containerContent
		}
		//run through items
		container.items.forEach [ item | {
			activityGraphItemTransformer.transform((item as ActivityGraphItem), containerContent)
		}]
		return containerContent
	}
	
}
