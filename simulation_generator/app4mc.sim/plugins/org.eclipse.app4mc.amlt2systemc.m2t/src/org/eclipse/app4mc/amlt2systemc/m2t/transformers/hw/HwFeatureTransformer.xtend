 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.HwFeature
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class HwFeatureTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer

	static def String getModulePath(HwFeature hwFeature) {
		return HwModelTransformer.getModulePath() + "/features/" + getName(hwFeature);
	}

	static def String getCall(HwFeature hwFeature) '''
	get_HwFeature_«getName(hwFeature)»()'''

	static def String getName(HwFeature hwFeature) {
		return hwFeature.containingCategory.name + "_" + hwFeature.name // or uniqueName
	}

	def create new TranslationUnit(
		getModulePath(hwFeature),
		getCall(hwFeature)		
	) transform(HwFeature hwFeature) {

		outputBuffer.appendTo("INC", it.module, toH(hwFeature))
		outputBuffer.appendTo("SRC", it.module, toCpp(hwFeature))

	}

	def String toH(HwFeature hwFeature) '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "ProcessingUnitDefinition.h"
		
		HwFeature* «getCall(hwFeature)»;
	'''

	def String toCpp(HwFeature hwFeature) '''
		#include "«getModulePath(hwFeature)».h"
			
		«val name = getName(hwFeature)»
		HwFeature*  «name» = nullptr;
		HwFeature* «getCall(hwFeature)»{
			if («name»==NULL){
				«name» = new HwFeature("«hwFeature.name»", «hwFeature.value.toString»);
			}
			return «name»;
		}
	'''

	def getCache() { return this._createCache_transform }
}
