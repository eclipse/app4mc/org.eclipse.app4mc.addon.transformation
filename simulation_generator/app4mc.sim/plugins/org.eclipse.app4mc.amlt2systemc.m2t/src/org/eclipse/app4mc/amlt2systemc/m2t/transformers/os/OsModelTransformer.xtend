/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.OSModel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys

@Singleton
class OsModelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer;
	@Inject OperatingSystemTransformer operatingSystemTransformer
	@Inject TaskSchedulerTransformer taskSchedulerTransformer
	@Inject SemaphoreTransformer semaphoreTransformer

//------------------------------
// OS
//------------------------------
	static def String getModulePath() {
		return AmaltheaTransformer.getModulePath() + "/osModel"
	}

	def String getModuleName() {
		return getModulePath + "/osModel_"
	}

	def getFunctionDef() {
		return "init_osModel()"
	}

	def getLibName() {
		return "osModel_lib"
	}

	def create new TranslationUnit(getModuleName(), getFunctionDef()) 
	transform(OSModel[] osModels) {
		osModels.forEach[osModel |{
			osModel.operatingSystems.forEach[os | 
				operatingSystemTransformer.transform(os)
			]
		}]
		// start transformation @ task level, 
		// Note: generate referenced object (e.g. runnables) when referenced (eg. in a runnable call within the call graph)
		outputBuffer.appendTo("INC", it.module, toH())
		outputBuffer.appendTo("SRC", it.module, toCpp())
		outputBuffer.appendTo("OTHER", modulePath + "/" + "CMakeLists.txt", getCMake())
	}

	private def String toH() '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "OSModel.h"
		
		void «getFunctionDef()»;
		
	'''

	private def String toCpp() '''
			#include "«getModuleName».h"	
			
			«FOR os : TuSort.byModule(operatingSystemTransformer.cache.values)»
			#include "«os.module».h"
			«ENDFOR»
		
			
			/* OS */
			void «getFunctionDef()»{
				«FOR os : TuSort.byModule(operatingSystemTransformer.cache.values)»
					«os.call»();
				«ENDFOR»
			}
	'''

	def String getCMake() '''	
		# CMakeList.txt: CMake project for OsModel of "«getProperty(PropertyKeys.PROJECT_NAME)»".
								
						
		# Add sources of OsModel
		target_sources («getProperty(PropertyKeys.PROJECT_NAME)»  PRIVATE
			«FOR obj : TuSort.byModule(taskSchedulerTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			«FOR obj : TuSort.byModule(semaphoreTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			«FOR obj : TuSort.byModule(operatingSystemTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			"${PROJECT_SOURCE_DIR}/«moduleName».cpp")
	'''
	
	def getCache(){
		return this._createCache_transform
	}

}
