/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.ModeLiteral
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit

@Singleton
class ModeLiteralTransformer extends BaseTransformer {

	@Inject ModeTransformer modeTransformer

	def create new TranslationUnit(
		modeTransformer.transform(literal.containingMode).module,
		modeTransformer.transform(literal.containingMode).call + "::" + literal.name
	) transform (ModeLiteral literal){	}
	
	
	
	

}
