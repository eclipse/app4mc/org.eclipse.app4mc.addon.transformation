 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import java.util.LinkedList
import org.eclipse.app4mc.amalthea.model.HwAccessElement
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.utils.DataRateUtil
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class HwAccessElementTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer;
	@Inject HwPathElementTransformer hwPathElementTransformer
	@Inject HwModuleTransformer hwModuleTransformer

	private def String getModulePath(HwAccessElement hwAccessElement) {
		if (hwAccessElement !== null) {
			return HwModelTransformer.getModulePath() + "/hwAccessElements/" + getName(hwAccessElement);
		} else {
			return HwModelTransformer.getModulePath();
		}
	}

	protected def String getCall(HwAccessElement hwAccessElement) '''
	get_HwStructure_«getName(hwAccessElement)»'''

	private def String getName(HwAccessElement hwAccessElement) {
		return hwAccessElement.name // or uniqueName
	}

	def create tu: new TranslationUnit(getModulePath(hwAccessElement), getCall(hwAccessElement)) transform(
		HwAccessElement hwAccessElement) {
		// start transformation @ task level, 
		// Note: generate referenced object (e.g. runnables) when referenced (eg. in a runnable call within the call graph) 
		outputBuffer.appendTo("INC", tu.module, toH(hwAccessElement))
		outputBuffer.appendTo("SRC", tu.module, toCpp(hwAccessElement))
	}

	private def String toH(HwAccessElement hwAccessElement) '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "HardwareModel.h"
		
		HwAccessElement «getCall(hwAccessElement)»();
	'''

	private def String toCpp(HwAccessElement hwAccessElement) '''
		#include "«getModulePath(hwAccessElement)».h"	
		//include path elementss
		«IF hwAccessElement.accessPath !== null »
			«FOR pathElement : hwAccessElement.accessPath.pathElements»
				#include "«hwPathElementTransformer.transform(pathElement).module».h"
			«ENDFOR»
		«ENDIF»
		#include "«hwModuleTransformer.transform(hwAccessElement.source).module».h"
		#include "«hwModuleTransformer.transform(hwAccessElement.destination).module».h"
		
		/* HwAccessWlement not contained in structure, eg. in flat hardware model */
		HwAccessElement «getCall(hwAccessElement)»(){
			//utilize RTO here
			HwAccessElement returnVal;
			«IF hwAccessElement.readLatency !== null »
				 returnVal.setReadLatency(«IDiscreteValueDeviationTransformer.getDeviation(hwAccessElement.readLatency)»);
			«ENDIF»
			«IF hwAccessElement.writeLatency !== null »
				returnVal.setWriteLatency(«IDiscreteValueDeviationTransformer.getDeviation(hwAccessElement.writeLatency)»);
			«ENDIF»
			«IF hwAccessElement.dataRate !== null »
				returnVal.setDataRate(«DataRateUtil.transform(hwAccessElement.dataRate)»);
			«ENDIF»
			
			
			«IF hwAccessElement.accessPath !== null»
				«val calls = getAccessElementCalls(hwAccessElement)»
				returnVal.path.elements.assign({«FOR call : calls» «call» «ENDFOR»});
			«ENDIF»
			
			//set source
			returnVal.source = «hwModuleTransformer.transform(hwAccessElement.source).call»();
			
			//set destination
			returnVal.dest = «hwModuleTransformer.transform(hwAccessElement.destination).call»();
				
			return returnVal;
		}
	'''

	def getAccessElementCalls(HwAccessElement hwAccessElement) {
		val calls = new LinkedList<String>()
		if (hwAccessElement.accessPath.pathElements.size > 1) {
			val elements = hwAccessElement.accessPath.pathElements.subList(0,
				hwAccessElement.accessPath.pathElements.size - 1)
			val tus = elements.map[hwPathElementTransformer.transform(it)]
			tus.forEach[calls.add(it.call + "(),")]
		}
		calls.add(hwPathElementTransformer.transform(hwAccessElement.accessPath.pathElements.last).call + "()")
		return calls;
	}

	def getCache() { return this._createCache_transform }

}
