/**
* *******************************************************************************
* Copyright (c) 2019-2023 Robert Bosch GmbH.
* 
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
* 
* SPDX-License-Identifier: EPL-2.0
* 
* Contributors:
*     Robert Bosch GmbH - initial API and implementation
* *******************************************************************************
*/

package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.google.inject.Inject;

public class AmaltheaModel2TextTransformer extends BaseTransformer {

	@Inject private SessionLogger logger;
	@Inject private OutputBuffer outputBuffer;
	@Inject private AmaltheaTransformer amaltheaTransformer;
	@Inject private CMakeTopGenerator cmakeTopGenerator;

	public void m2tTransformation(final ResourceSet inputResourceSet) {
		// configure output buffer
		this.outputBuffer.initialize(this.getProperty(TransformationConstants.TRANSFORMATION_OUTPUT_FOLDER));
		this.outputBuffer.configureFiletype("SRC", ".cpp", "// This code was generated for simulation with app4mc.sim\n\n", null);
		this.outputBuffer.configureFiletype("INC", ".h", "// This code was generated for simulation with app4mc.sim\n\n#pragma once\n\n", null);
		this.outputBuffer.configureFiletype("OTHER", "", "", "");

		// collect Amalthea models
		// for APP4MC > 1.1.0 the AmaltheaLoader has a method collectAmaltheaModels(ResourceSet resourceSet)

		Collection<Amalthea> models = new ArrayList<>();
		for (final Resource resource : inputResourceSet.getResources()) {
			for (final EObject model : resource.getContents()) {

				logger.info("Processing file : {0}", resource.getURI());

				if ((model instanceof Amalthea)) {
					models.add((Amalthea) model);
				}
			}
		}
		
		// if you want to check consistency of model structure:
		// for APP4MC > 1.1.0 the method ModelStructureCheck.checkModels(...) can be used

		//transform models
		this.amaltheaTransformer.transform(models.toArray(new Amalthea[models.size()]));

		logger.info("Generating cmake");

		this.cmakeTopGenerator.transform();

		// exclude build path
		List<Path> buildPathList = new ArrayList<>();
		Path buildPath = Paths.get(outputBuffer.getOutputFolder(),"build");
		if(Files.isDirectory(buildPath)) {
			buildPathList.add(buildPath);
		}

		// write output buffer to disk
		this.outputBuffer.setExcludePathsOfResult(buildPathList);
		this.outputBuffer.finish(true, true);
		
		logger.info("Script file generated at : {0}", this.outputBuffer.getOutputFolder());
	}

}
