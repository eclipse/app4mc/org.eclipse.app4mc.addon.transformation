/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.EnumMode
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum
import org.eclipse.app4mc.amalthea.model.NumericMode
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer

@Singleton
class ModeLabelAccessTransformer extends BaseTransformer {

	@Inject ModeLabelTransformer labelTransformer
	@Inject ModeTransformer modeTransformer

	def void transform(ModeLabelAccess _access, AgiContainerBuffer content) {
		val tuLabel = labelTransformer.transform(_access.data)
		content.addInclude(tuLabel.module)
		
		// GENERAL 
		if (_access.access === ModeLabelAccessEnum.READ){
			content.addAsActivityGraphItem(transformRead(tuLabel.call))
		}
		//ENUM MODES
		else if (_access.data.mode instanceof EnumMode){
			if (_access.access === ModeLabelAccessEnum.SET){
				val tuMode = modeTransformer.transform(_access.data.mode)
				content.addInclude(tuMode.module)
				content.addAsActivityGraphItem(transformSetEnum(tuLabel.call, tuMode.call, _access.value))
			}	
		}	
		//NUMERIC MODES
		else if (_access.data.mode instanceof NumericMode){
			if (_access.access === ModeLabelAccessEnum.SET){
				content.addAsActivityGraphItem(transformSetNumeric(tuLabel.call, _access.value))
			} else if (_access.access === ModeLabelAccessEnum.INCREMENT){
				content.addAsActivityGraphItem(transformIncrement(tuLabel.call, _access.step))
			} else if (_access.access === ModeLabelAccessEnum.DECREMENT){
				content.addAsActivityGraphItem(transformDecrement(tuLabel.call, _access.step))
			}
		} else {
			throw new RuntimeException("ModeLabelAccess of " + _access.data.name + " has specified neither read nor write")
		}
	}

	private def String transformSetEnum(String call, String mode, String value) '''
	<ModeLabelAccess>({«call», «mode»->getLiteral("«value»"), ModeAccessType::SET})'''
	
	private def String transformSetNumeric(String call, String value) '''
	<ModeLabelAccess>({«call», «value», ModeAccessType::SET})'''
	
	private def String transformIncrement(String call, int value) '''
	<ModeLabelAccess>({«call», «value», ModeAccessType::INCREMENT})'''
	
	private def String transformDecrement(String call, int value) '''
	<ModeLabelAccess>({«call», «value», ModeAccessType::DECREMENT})'''

	private def String transformRead(String call) '''
	<ModeLabelAccess>({«call», ModeAccessType::READ})'''

}
