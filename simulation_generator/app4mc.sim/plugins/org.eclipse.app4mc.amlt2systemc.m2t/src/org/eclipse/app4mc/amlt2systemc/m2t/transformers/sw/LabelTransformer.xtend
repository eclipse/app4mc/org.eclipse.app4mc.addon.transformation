/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class LabelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer

	private def String getLabelName(Label label) {
		var uniqueName = label.uniqueName
		uniqueName = uniqueName.replaceAll("\\?", "_")
		uniqueName = uniqueName.replace("=", "_")
		uniqueName = uniqueName.replace("/", "_")
		return uniqueName
	}

	private def String getModuleName(Label label) {
		return SWModelTransformer.getModulePath() + "/labels/" + getLabelName(label)
	}

	private def getFunctionDef(Label _label) {
		return "get_" + getLabelName(_label) + "()"
	}

	def create new TranslationUnit(
		getModuleName(label),
		getFunctionDef(label)
	) transform(Label label) {
		outputBuffer.appendTo("INC", it.module, toH(label))
		outputBuffer.appendTo("SRC", it.module, toCpp(label))
	}

	private def String toH(Label label) '''
		#include "Common.h"
		#include "DataLabel.h"
		#include "SoftwareModel.h"
		
		std::shared_ptr<DataLabel> «getFunctionDef(label)»;
	'''

	private def String toCpp(Label label) '''
		«val name = getLabelName(label)»
		#include "«getModuleName(label)».h"
		
		std::shared_ptr<DataLabel> «name» = nullptr;
		
		std::shared_ptr<DataLabel>  «getFunctionDef(label)» {
			if («name» == nullptr){
				«name» = std::make_shared<DataLabel>("«name»", «getSizeInByte(label)»);
			}
			return «name»;
		}
		
	'''

	private def getSizeInByte(Label label) {
		val size = label.size
		if (size === null)
			return 0
		else
			return size.numberBytes
	}

	def getCache() {
		this._createCache_transform
	}

}
