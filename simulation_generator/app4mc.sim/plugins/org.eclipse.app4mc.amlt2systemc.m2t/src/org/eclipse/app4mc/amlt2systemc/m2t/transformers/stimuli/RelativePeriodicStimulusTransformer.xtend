/**
 * *******************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API
 * 	   University of Rostock - Benjamin.Beichler@uni-rostock.de
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.ITimeDeviationTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class RelativePeriodicStimulusTransformer extends StimulusBaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject ConditionTransformer conditionTransformer;


	def create new TranslationUnit(
		getModulePath(stim),
		getFunctionDef(stim)		
	) transform(RelativePeriodicStimulus stim) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(stim))
		// write implementation file
		outputBuffer.appendTo("SRC", it.module, toCpp(stim))
	}

	private def String toH(RelativePeriodicStimulus stim) '''
		// This code was generated for simulation with app4mc.sim
			
		#pragma once
		#include "Common.h"
		#include "Stimuli/PeriodicStimulus.h"
		
		//RelativePeriodicStimulus runnableA----
		std::shared_ptr<RelativePeriodicStimulus> «getFunctionDef(stim)»;
	'''

	private def String toCpp(RelativePeriodicStimulus stim) '''
		// This code was generated for simulation with app4mc.sim
			
		#include "«getModulePath(stim)».h"
		«val conditionContent = conditionTransformer.transformCondition(
			stim.executionCondition, getName(stim), true)»
		«ConditionBuffer.getConditionIncludes(conditionContent)»
		
		std::shared_ptr<RelativePeriodicStimulus> «stim.name» = nullptr;
		
		std::shared_ptr<RelativePeriodicStimulus>  «getFunctionDef(stim)» {
		
			if («stim.name» == nullptr) {
				//initialize
				«stim.name» = std::make_shared<RelativePeriodicStimulus>("«stim.name»",
																			 «ITimeDeviationTransformer.getDeviation(stim.nextOccurrence)»,
																			 «TimeUtil.transform(stim.offset?:FactoryUtil.createTime(0,TimeUnit.PS))»);
				«ConditionBuffer.getConditionSource(conditionContent)»
			}
			return «stim.name»;
		}
		
	'''

	def getCache() { return this._createCache_transform }

}
