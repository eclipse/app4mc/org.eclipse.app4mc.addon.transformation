 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.HwConnection
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class HwConnectionTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer;
	@Inject HwStructureTransformer hwStructureTransformer

	protected static def String getName(HwConnection hwConnection) {
		return hwConnection.name // or uniqueName
	}

	protected static def String getCall(HwConnection hwConnection) '''
	get_«getName(hwConnection)»'''

	protected static def String getModulePath(HwConnection hwConnection) {
		// hw modules are created within the files of their containing hw structure
		HwModelTransformer.getModulePath() + "/connections/" + getName(hwConnection);
	}

	def create tu : new TranslationUnit(getModulePath(hwConnection), getCall(hwConnection)) transform(
		HwConnection hwConnection) {
		outputBuffer.appendTo("INC", tu.module, toH(hwConnection))
		val parentHwStructure = HwStructureTransformer.getParentHwStructureOrNull(hwConnection)
		if (parentHwStructure !== null) {
			outputBuffer.appendTo("SRC", tu.module, toCpp(hwConnection, hwStructureTransformer.transform(parentHwStructure)))
		} else {
			outputBuffer.appendTo("SRC", tu.module, toCpp(hwConnection))
		}
	}

	def String toH(HwConnection connection) '''
		//framework
		#include "HardwareModel.h"
		std::shared_ptr<Connection> «getCall(connection)»();
	'''

	def String toCpp(HwConnection connection) '''
		#include "«getModulePath(connection)».h"
		
		«val name = getName(connection)»
		std::shared_ptr<Connection> «name» = nullptr;
		
		//for usage on hw model top level (eg. in flat hierarchies)
		std::shared_ptr<Connection> «getCall(connection)»(){
			if («name»==NULL){
				«name» =  std::make_shared<Connection>("«name»");
				«setParams(connection)»
			}
			return «name»;
		}
	'''

	def String toCpp(HwConnection connection, TranslationUnit parentTu) '''
		#include "«getModulePath(connection)».h"
		#include "«parentTu.module».h"
		
		«val name = getName(connection)»
		std::shared_ptr<Connection> «name» = nullptr;
		
		//for usage in structures
		std::shared_ptr<Connection> «getCall(connection)»(){
			std::shared_ptr<HwStructure> parent = «parentTu.call»();
			if («name»==NULL){
				«name» = parent->createConnection("«name»");
				«setParams(connection)»
			}
			return «name»;
		}
		
		
	'''

	private def CharSequence setParams(HwConnection connection) '''
		«val name = getName(connection)»
		«IF connection.writeLatency !== null»
			«name»->setWriteLatency(«IDiscreteValueDeviationTransformer.getDeviation(connection.writeLatency)»);
		«ENDIF»
		«IF connection.readLatency !== null»
			«name»->setReadLatency(«IDiscreteValueDeviationTransformer.getDeviation(connection.readLatency)»);
		«ENDIF»
	'''

	def getCache() { return this._createCache_transform }

}
