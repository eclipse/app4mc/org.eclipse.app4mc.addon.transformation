/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.LabelAccess
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer

@Singleton
class LabelAccessTransformer extends BaseTransformer {

	@Inject LabelTransformer labelTransformer
	@Inject NumericStatisticTransformer numericStatisticTransformer

	def void transform(LabelAccess _access, AgiContainerBuffer content) {
		val tuLabel = labelTransformer.transform(_access.data)
		
		content.addInclude(tuLabel.module)
		
		var statistics = "1"
		if (_access.statistic !== null)
			statistics = numericStatisticTransformer.transform(_access.statistic)
		// read/write access
		if (_access.access === LabelAccessEnum.WRITE)	
			content.addAsActivityGraphItem(transformWrite(tuLabel.call, statistics))
			//return new TranslationUnit(tuLabel.module, transformWrite(tuLabel.call, statistics))
		else if (_access.access === LabelAccessEnum.READ)
			content.addAsActivityGraphItem(transformRead(tuLabel.call, statistics))
			//return new TranslationUnit(tuLabel.module, transformRead(tuLabel.call, statistics))
		else
			content.addBody("//Access of " + _access.data.name + " has specified neither read nor write")
			//return new TranslationUnit(tuLabel.module, "Label access has specified neither read nor write")
	}

	private def String transformWrite(String call, String statistics) '''
	<LabelAccess>({«call», «statistics», tlm::TLM_WRITE_COMMAND})'''

	private def String transformRead(String call, String statistics) '''
	<LabelAccess>({«call», «statistics», tlm::TLM_READ_COMMAND})'''

}
