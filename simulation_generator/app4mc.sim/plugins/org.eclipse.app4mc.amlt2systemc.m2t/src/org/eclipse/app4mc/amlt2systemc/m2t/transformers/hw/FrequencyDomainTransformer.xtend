 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Frequency
import org.eclipse.app4mc.amalthea.model.FrequencyDomain
import org.eclipse.app4mc.amalthea.model.FrequencyUnit
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class FrequencyDomainTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer;

	static def String getName(FrequencyDomain frequencyDomain) {
		return frequencyDomain.name // or uniqueName
	}

	static def String getModulePath(FrequencyDomain frequencyDomain) {
		return HwModelTransformer.getModulePath() + "/domains/" + getName(frequencyDomain);
	}

	static def String getCall(FrequencyDomain frequencyDomain) '''
	get_FrequencyDomain_«getName(frequencyDomain)»()'''

	def create new TranslationUnit(getModulePath(frequencyDomain), getCall(frequencyDomain)) transform(
		FrequencyDomain frequencyDomain) {
		outputBuffer.appendTo("INC", it.module, toH(frequencyDomain))
		outputBuffer.appendTo("SRC", it.module, toCpp(frequencyDomain))
	}

	def String toH(FrequencyDomain frequencyDomain) '''
		#include <systemc>
		sc_core::sc_time «getCall(frequencyDomain)»;
		
	'''

	def String toCpp(FrequencyDomain frequencyDomain) '''
		#include "«getModulePath(frequencyDomain)».h"
		
		sc_core::sc_time «getCall(frequencyDomain)» {
			return sc_core::sc_time(«getPeriodInPS(frequencyDomain.defaultValue)», sc_core::SC_PS);
		}
	'''

	def double getPeriodInPS(Frequency frequency) {
		switch (frequency?.unit) {
			case FrequencyUnit.GHZ:
				return Math.pow(10, 3) / frequency.value // 10^9 (GHz) --> 10^-12 (ps)
			case FrequencyUnit.MHZ:
				return Math.pow(10, 6) / frequency.value // 10^6 (MHz) --> 10^-12 (ps)
			case FrequencyUnit.KHZ:
				return Math.pow(10, 9) / frequency.value // 10^9 (GHz) --> 10^-12 (ps)
			case FrequencyUnit.HZ:
				return Math.pow(10, 12) / frequency.value
			default:
				return 0.0
		}
	}

	def getCache() { return this._createCache_transform }

}
