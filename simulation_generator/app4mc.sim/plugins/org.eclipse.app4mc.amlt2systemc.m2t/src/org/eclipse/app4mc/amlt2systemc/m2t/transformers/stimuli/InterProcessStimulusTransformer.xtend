/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class InterProcessStimulusTransformer extends StimulusBaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject ConditionTransformer conditionTransformer;

	def create new TranslationUnit(
		getModulePath(stim),
		getFunctionDef(stim)		
	) transform(InterProcessStimulus stim) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(stim))

		// write implementation file
		outputBuffer.appendTo("SRC", it.module, toCpp(stim))
	}

	private def String toH(InterProcessStimulus stim) '''
		#include "Common.h"
		#include "Stimuli/Stimuli.h"
		
		//InterProcessStimulus runnableA----
		std::shared_ptr<InterProcessStimulus> «getFunctionDef(stim)»;
	'''

	private def String toCpp(InterProcessStimulus stim) '''
		// This code was generated for simulation with app4mc.sim
		#include "«getModulePath(stim)».h"
		«val conditionContent = conditionTransformer.transformCondition(
			stim.executionCondition, getName(stim), true)»
		«ConditionBuffer.getConditionIncludes(conditionContent)»
		
		std::shared_ptr<InterProcessStimulus> «stim.name» = nullptr;
		
		std::shared_ptr<InterProcessStimulus>  «getFunctionDef(stim)» {
		
			if («stim.name» == nullptr) {
				//initialize
				«stim.name» = std::make_shared<InterProcessStimulus>("«stim.name»");
				«ConditionBuffer.getConditionSource(conditionContent)»
			}
			return «stim.name»;
		}
		
	'''

	def getCache() { return this._createCache_transform }

}
