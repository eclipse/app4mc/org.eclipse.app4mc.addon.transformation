/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess
import org.eclipse.app4mc.amalthea.model.SemaphoreAccessEnum
import org.eclipse.app4mc.amalthea.model.WaitingBehaviour
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.SemaphoreTransformer

@Singleton
class SemaphoreAccessTransformer extends BaseTransformer {

	@Inject SemaphoreTransformer semaphoreTransformer
	
	def void transform(SemaphoreAccess access, AgiContainerBuffer content) {
		val tuSemaphore = semaphoreTransformer.transform(access.semaphore)
		content.addInclude(tuSemaphore.module)
		content.addAsActivityGraphItem(transformSemaphoreAccess(tuSemaphore.call, getAccessType(access),
				getWaitingBehaviorType(access)))
	}


	private def getAccessType(SemaphoreAccess access){
		switch access.access {
			case SemaphoreAccessEnum.REQUEST: "request"
			case SemaphoreAccessEnum.RELEASE: "release"
			default: throw new IllegalArgumentException("Unsupported semaphore access type: " + access.access)
		}
	}

	private def getWaitingBehaviorType(SemaphoreAccess access){
		switch access.waitingBehaviour {
			case WaitingBehaviour.ACTIVE: "active"
			default: throw new IllegalArgumentException("Unsupported waiting behavior for semaphore access: " + access.waitingBehaviour)
		}
	}
	
	private def String transformSemaphoreAccess(String call, String accessType, String waitingBehaviorType) '''
		<SemaphoreAccess>({«call», SemaphoreAccessType::«accessType», app4mc::WaitingBehaviour::«waitingBehaviorType»})'''
}
