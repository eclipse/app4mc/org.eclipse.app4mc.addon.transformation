 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Memory
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.IDiscreteValueDeviationTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.utils.DataRateUtil
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class MemoryTransformer extends HwModuleTransformer {

	@Inject OutputBuffer outputBuffer;
	@Inject FrequencyDomainTransformer frequencyDomainTransformer

	def create tu : new TranslationUnit(
		getModulePath(memory),
		getCall(memory)
	) transform(Memory memory) {
		//sanity check
		if (memory.frequencyDomain === null)
			throw new RuntimeException("Memory " + memory.qualifiedName + " is not associated with a frequency domain.")
		
		outputBuffer.appendTo("INC", tu.module, toH(memory))
		outputBuffer.appendTo("SRC", tu.module, toCpp(memory))
	}

	def String toH(Memory module) '''
		//framework
		#include "HardwareModel.h"		
		std::shared_ptr<Memory> «getCall(module)»();
	'''

	def String toCpp(Memory module) '''
		#include "«getModulePath(module)».h"
		«getParentInclude(module)»
		
		«IF (module.frequencyDomain !== null)»
			#include "«frequencyDomainTransformer.transform(module.frequencyDomain).module».h"
		«ENDIF»
		«getIncludesForConnections(module)»
		
		«val name = getName(module)»
		std::shared_ptr<Memory> «name» = nullptr;
		
		//for usage in structures
		std::shared_ptr<Memory> «getCall(module)»(){
			auto parent = «getParentGetter(module)»();
			if («name»==NULL){
				«IF (module?.definition?.size !== null)»
					«name» = parent->createModule<Memory>("«name»", «module.definition.size.numberBytes»);
				«ELSE»
					«name» = parent->createModule<Memory>("«name»", 0);
				«ENDIF»
				«name»->clock_period= «frequencyDomainTransformer.transform(module.frequencyDomain).call»;
				«setPortsAndConnections(module)»	
				«setAccessLatency(module)»
				«setDataRate(module)»
			}
			return «name»;
		}
	'''

	def String setAccessLatency(Memory module) '''
		«val name = getName(module)»
		«val deviation = module?.definition?.accessLatency»
		«IF ( deviation !== null)»
			«name»->setAccessLatency(«IDiscreteValueDeviationTransformer.getDeviation(deviation)»);
		«ENDIF»
	'''

	def String setDataRate(Memory module) '''
		«val name = getName(module)»
		«IF (module?.definition?.dataRate !== null)»
			«name»->setDataRate(«DataRateUtil.transform(module.definition.dataRate)»);
		«ENDIF»
	'''

	def getCache() { return this._createCache_transform }

}
