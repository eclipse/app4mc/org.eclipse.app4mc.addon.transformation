 /**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers

import org.eclipse.app4mc.amalthea.model.TimeConstant
import org.eclipse.app4mc.amalthea.model.TimeGaussDistribution
import org.eclipse.app4mc.amalthea.model.TimeWeibullEstimatorsDistribution
import org.eclipse.app4mc.amalthea.model.ITimeDeviation
import org.eclipse.app4mc.amalthea.model.TimeUniformDistribution
import org.eclipse.app4mc.amalthea.model.TimeBetaDistribution
import org.eclipse.app4mc.amalthea.model.TimeBoundaries
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.model.TimeUnit

class ITimeDeviationTransformer {
//ITimeDeviation
	static def dispatch String getDeviation(ITimeDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''
	static def dispatch String getDeviationTemplate(ITimeDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''
	static def dispatch String getDeviationValue(ITimeDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''

//TimeBetaDistribution
static def dispatch String getDeviation(TimeBetaDistribution value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(TimeBetaDistribution value)'''
		TimeBetaDistribution'''

	static def dispatch String getDeviationValue(TimeBetaDistribution value)'''
		«value.alpha»,«value.beta»,«TimeUtil.transform(value.lowerBound)»,«TimeUtil.transform(value.upperBound)»'''

private static def String stype(TimeBoundaries value) {
	switch(value.samplingType){
		case BEST_CASE: {
			return "BoundariesSamplingType::BestCase";
		}
		case WORST_CASE: {
			return "BoundariesSamplingType::WorstCase";
		}
		case AVERAGE_CASE: {
			return "BoundariesSamplingType::AverageCase";
		}
		case CORNER_CASE: {
			return "BoundariesSamplingType::CornerCase";
		}
		case UNIFORM: {
			return "BoundariesSamplingType::Uniform";
		}
		//TODO: what is DEFAULT enum case?
		default: {
			return "BoundariesSamplingType::WorstCase";
		}
	}
		
}
		
//TimeBoundaries
static def dispatch String getDeviation(TimeBoundaries value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(TimeBoundaries value)'''
		TimeBoundaries'''

	static def dispatch String getDeviationValue(TimeBoundaries value)'''
		«stype(value)»,«TimeUtil.transform(value.lowerBound)»,«TimeUtil.transform(value.upperBound)»'''

//TimeConstant
	static def dispatch String getDeviation(TimeConstant value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''

	static def dispatch String getDeviationTemplate(TimeConstant value)'''
		TimeConstant'''

	static def dispatch String getDeviationValue(TimeConstant value)'''
		«TimeUtil.transform(value.value)»'''

//TimeGaussDistribution
	static def dispatch String getDeviation(TimeGaussDistribution value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(TimeGaussDistribution value)'''
		TimeGaussDistribution'''

	static def dispatch String getDeviationValue(TimeGaussDistribution value)'''
		«TimeUtil.transform(value.mean)»,«TimeUtil.transform(value.sd)»,«TimeUtil.transform(value.lowerBound)?: TimeUtil.transform(FactoryUtil.createTime(Long.MIN_VALUE,TimeUnit.PS))»,«TimeUtil.transform(value.upperBound)?:TimeUtil.transform(FactoryUtil.createTime(Long.MAX_VALUE,TimeUnit.PS))»'''

//TODO: TimeHistogram
//TODO: TimeStatistics

//TimeUniformDistribution
	static def dispatch String getDeviation(TimeUniformDistribution value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(TimeUniformDistribution value)'''
		TimeUniformDistribution'''
	
	static def dispatch String getDeviationValue(TimeUniformDistribution value)'''
		«TimeUtil.transform(value.lowerBound)»,«TimeUtil.transform(value.upperBound)»'''

//TimeWeibullEstimatorsDistribution
	static def dispatch String getDeviation(TimeWeibullEstimatorsDistribution value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''

	static def dispatch String getDeviationTemplate(TimeWeibullEstimatorsDistribution value)'''
		TimeWeibullEstimatorsDistribution'''

	static def dispatch String getDeviationValue(TimeWeibullEstimatorsDistribution value)'''
		«ITimeDeviationTransformer.getDeviationTemplate(value)»::findParameter(«TimeUtil.transform(value.average)»,«value.PRemainPromille»,«TimeUtil.transform(value.lowerBound)»,«TimeUtil.transform(value.upperBound)»),«TimeUtil.transform(value.lowerBound)»,«TimeUtil.transform(value.upperBound)»'''
}