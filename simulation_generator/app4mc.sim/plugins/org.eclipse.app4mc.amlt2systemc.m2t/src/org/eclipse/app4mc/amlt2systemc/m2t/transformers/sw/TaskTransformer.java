/**
 * Copyright (c) 2019-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli.StimulusTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TaskTransformer extends BaseTransformer {

	@Inject private OutputBuffer outputBuffer;
	@Inject private StimulusTransformer stimulusTransformer;
	@Inject ActivityGraphTransformer activityGraphTransformer;
	

	// ---------- generic part "def create new transform(...)" ----------

	private final Map<List<Object>, TranslationUnit> transformCache = new HashMap<>();

	public Map<List<Object>, TranslationUnit> getCache() {
		return this.transformCache;
	}

	public TranslationUnit transform(final Task task) {
		final List<Object> key = List.of(task);
		final TranslationUnit tu;

		synchronized (transformCache) {
			if (transformCache.containsKey(key)) {
				return transformCache.get(key);
			}
			tu = createTranslationUnit(task);
			transformCache.put(key, tu);
		}

		// if translation unit is newly created and valid -> create files
		if (tu.isValid()) {
			doTransform(tu, task);
		}

		return tu;
	}

	// ---------------------------------------------------

	private TranslationUnit createTranslationUnit(final Task task) {
		if ((task == null)) {
			return new TranslationUnit("UNSPECIFIED_TASK");
		} else {
			String module = TaskGenerator.getModulePath(task);
			String call = TaskGenerator.getFunctionDef(task);
			return new TranslationUnit(module, call);
		}
	}

	private void doTransform(final TranslationUnit taskTU, final Task task) {

		final LinkedList<String> stimuliCalls = new LinkedList<>();
		
		AgiContainerBuffer taskContents = new AgiContainerBuffer(TaskGenerator.getName(task),TaskGenerator.getModulePath(task), true); 
		activityGraphTransformer.transform(task.getActivityGraph(), taskContents);
		
		for (Stimulus stimulus : task.getStimuli()) {
			final TranslationUnit stimulusTU = stimulusTransformer.transform(stimulus);

			String stimulusModule = stimulusTU.getModule();
			if (stimulusModule != null && !stimulusModule.isEmpty() && !taskTU.getModule().equals(stimulusModule)) {
				taskContents.addInclude(stimulusModule);
				//super.activityItemIncludes.add(stimulusModule);
			}
			String stimulusCall = stimulusTU.getCall();
			if (stimulusCall != null && !stimulusCall.isEmpty()) {
				stimuliCalls.add(stimulusCall);
			}
		}

		this.outputBuffer.appendTo("INC", taskTU.getModule(), TaskGenerator.toH(task));
		this.outputBuffer.appendTo("SRC", taskTU.getModule(), TaskGenerator.toCpp(task, stimuliCalls, taskContents.getIncludes(), taskContents.getSource()));
	}

}
