package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.EventStimulus
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus
import org.eclipse.app4mc.amalthea.model.SingleStimulus
import org.eclipse.app4mc.amalthea.model.StimuliModel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class StimuliModelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject StimulusTransformer stimulusDispatcher

	protected static def getModulePath() {
		return AmaltheaTransformer.getModulePath() + "/stimuliModel"
	}

	private def getModuleName() {
		return getModulePath + "/stimuliModel"
	}

	private def getFunctionDef() {
		return "init_stimuliModel()"
	}

	def create new TranslationUnit(
		getModuleName(),
		getFunctionDef()
	) transform(StimuliModel[] stimuliModels) {
		stimuliModels.forEach[stimuliModel|{
			stimuliModel.stimuli.forEach[stimulusDispatcher.transform(it)]
		}]
		outputBuffer.appendTo("INC", it.module, toH())
		outputBuffer.appendTo("SRC", it.module, toCpp())
		outputBuffer.appendTo("OTHER", getModulePath() + "/CMakeLists.txt", getCMake());
	}
	
	private def String toH() '''
		#include <systemc>
		#include <memory>
		
		void «getFunctionDef()»;
		
	'''

	private def String toCpp() '''
		#include "«getModuleName».h"
		#include "APP4MCsim.h"
		
		//include model elements»
		«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(SingleStimulus)).values)»
				#include "«(obj as TranslationUnit).module».h"
			«ENDFOR»
		«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(PeriodicStimulus)).values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»
		«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(RelativePeriodicStimulus)).values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»
		«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(InterProcessStimulus)).values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»
		«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(EventStimulus)).values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»	
		
		/* Software */
		void «getFunctionDef()»{		
			«FOR obj : TuSort.byCall(stimulusDispatcher.getCache(typeof(SingleStimulus)).values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
			«FOR obj : TuSort.byCall(stimulusDispatcher.getCache(typeof(PeriodicStimulus)).values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
			«FOR obj : TuSort.byCall(stimulusDispatcher.getCache(typeof(RelativePeriodicStimulus)).values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
			«FOR obj : TuSort.byCall(stimulusDispatcher.getCache(typeof(InterProcessStimulus)).values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
			«FOR obj : TuSort.byCall(stimulusDispatcher.getCache(typeof(EventStimulus)).values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
		}
	'''

	def String getCMake() '''	
		# CMakeList.txt: CMake project for StimuliModel of "«getProperty(PropertyKeys.PROJECT_NAME)»".
								
						
		# Add sources of Stimuli
		target_sources («getProperty(PropertyKeys.PROJECT_NAME)»  PRIVATE«
			FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(SingleStimulus)).values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(PeriodicStimulus)).values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(RelativePeriodicStimulus)).values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(InterProcessStimulus)).values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			«FOR obj : TuSort.byModule(stimulusDispatcher.getCache(typeof(EventStimulus)).values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			
			"${PROJECT_SOURCE_DIR}/«getModuleName()».cpp")
	'''
	
	def getCache(){
		return this._createCache_transform
	}
}
