/**
 * *******************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.SWModel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys

@Singleton
class SWModelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject TaskTransformer taskTransformer
	@Inject RunnableTransformer runnableTransformer
	@Inject ChannelTransformer channelTransformer
	@Inject LabelTransformer labelTransformer
	@Inject ModeLabelTransformer modeLabelTransformer
	@Inject ModeTransformer modeTransformer

	protected static def getModulePath() {
		return AmaltheaTransformer.getModulePath() + "/swModel"
	}

	private def getModuleName() {
		return getModulePath + "/swModel"
	}

	private def getFunctionDef() {
		return "init_swModel()"
	}

	def create new TranslationUnit(
		getModuleName(),
		getFunctionDef()		
	) transform(SWModel[] swModels) {
		
		swModels.forEach[swModel | {
		// start transformation @ task level, 
		// Note: generate referenced object (e.g. runnables) when referenced (eg. in a runnable call within the call graph) 
			swModel.tasks.forEach[taskTransformer.transform(it)]
		}]
		
		outputBuffer.appendTo("INC", it.module, toH())
		outputBuffer.appendTo("SRC", it.module, toCpp())
		outputBuffer.appendTo("OTHER", getModulePath() + "/CMakeLists.txt", getCMake(it.module));
	}

	private def String toH() '''
		void «getFunctionDef()»;
	'''

	private def String toCpp() '''
		#include "APP4MCsim.h"
		#include "«getModuleName».h"
		«FOR obj : TuSort.byModule(taskTransformer.cache.values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»	
		
		/* Software */
		void «getFunctionDef()»{		
			«FOR obj : TuSort.byCall(taskTransformer.cache.values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
		}
	'''

	def String getCMake(String thisModule) '''	
		# CMakeList.txt: CMake project for SoftwareModel of "«getProperty(PropertyKeys.PROJECT_NAME)»".
								
				
		# Add sources of SoftwareModel
		target_sources («getProperty(PropertyKeys.PROJECT_NAME)»  PRIVATE
			«FOR obj : TuSort.byModule(modeTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»			
			
			«FOR obj : TuSort.byModule(channelTransformer.cache.values)»« 
			»	"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp"«
			»«ENDFOR»

			«FOR obj : TuSort.byModule(labelTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»			

			«FOR obj : TuSort.byModule(modeLabelTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»			

			«FOR obj : TuSort.byModule(runnableTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			
			«FOR obj : TuSort.byModule(taskTransformer.cache.values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»

			"${PROJECT_SOURCE_DIR}/«thisModule».cpp")
	'''
	
	def getCache(){
		return this._createCache_transform
	}

}
