/**
 * *******************************************************************************
 * Copyright (c) 2019-2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Algorithm
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping.TaskAllocationTransformer
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.SchedulingParameterTransformer

@Singleton
class TaskSchedulerTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject SchedulingParameterTransformer schedulingParameterTransformer

	def getName(TaskScheduler taskScheduler) {
		return taskScheduler.name + "_" + chooseSchedulerBasedOnSchedulerDefinition(taskScheduler.definition).call
	}

	private def getModulePath(TaskScheduler taskScheduler) {
		return OsModelTransformer.getModulePath() + "/tasks/" + getName(taskScheduler)
	}

	private def getCall(TaskScheduler taskScheduler) {
		return "get_" + getName(taskScheduler)
	}

	def create new TranslationUnit(
		getModulePath(taskScheduler),
		getCall(taskScheduler)		
	) transform(TaskScheduler taskScheduler) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(taskScheduler))
		// write implementation file
		outputBuffer.appendTo("SRC", it.module, toCpp(taskScheduler))
	}

	private def String toH(TaskScheduler taskScheduler) '''
		«val scheduler = chooseSchedulerBasedOnSchedulerDefinition((taskScheduler.definition))»
		#include "SoftwareModel.h"
		#include "«scheduler.module»"
		
		//Task runnableA----
		std::shared_ptr<«scheduler.call»> «getCall(taskScheduler)»();
	'''

	private def String transformSchedulerAssociation(TaskScheduler taskScheduler) '''
		«IF taskScheduler.parentAssociation?.schedulingParameters !== null»
			TaskAllocation ta;
			«FOR paramEntry : taskScheduler.parentAssociation.schedulingParameters.entrySet»
				«schedulingParameterTransformer.createSchedParam("ta", "setSchedulingParameter", false, paramEntry.key, paramEntry.value)»
			«ENDFOR»
			«taskScheduler.name»->setTaskAllocation(ta);
		«ENDIF»
		«taskScheduler.parentScheduler.call»()->addTaskMapping(«taskScheduler.name»);
	'''

	private def String toCpp(TaskScheduler taskScheduler) '''	
		#include "Common.h"
		#include "«getModulePath(taskScheduler)».h"
		«val scheduler = chooseSchedulerBasedOnSchedulerDefinition((taskScheduler.definition))»
		
		std::shared_ptr<«scheduler.call»> «taskScheduler.name» = nullptr;
		
		std::shared_ptr<«scheduler.call»> «getCall(taskScheduler)»() {
			if («taskScheduler.name» == nullptr) {
				//initialize
				«taskScheduler.name» = std::make_shared<«scheduler.call»>("«taskScheduler.name»");
				«IF taskScheduler.parentAssociation !== null»
					«transformSchedulerAssociation(taskScheduler)»
				«ENDIF»
				«IF taskScheduler?.schedulingParameters !== null»
					«FOR paramEntry : taskScheduler.schedulingParameters.entrySet»
						«schedulingParameterTransformer.createSchedParam(taskScheduler.name, "setAlgorithmSchedulingParameter", true, paramEntry.key, paramEntry.value)»
					«ENDFOR»
				«ENDIF»
			}
			return «taskScheduler.name»;
		}
	'''
	
	private final static def TranslationUnit chooseSchedulerBasedOnSchedulerDefinition(
		SchedulerDefinition schedulerDefinition) {
		switch(schedulerDefinition.name){
			case Algorithm.FIXED_PRIORITY_PREEMPTIVE.algorithmName:
				return new TranslationUnit("PriorityScheduler.h", "PriorityScheduler")
			case Algorithm.PRIORITY_BASED_ROUND_ROBIN.algorithmName:
				return new TranslationUnit("PriorityRoundRobinScheduler.h", "PriorityRoundRobinScheduler")
			default:
				throw new IllegalArgumentException("Scheduler definition " + schedulerDefinition.name + " is not supported.")
		}
	}
	
	def getCache() { return this._createCache_transform }

}
