 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.HWModel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort
import org.eclipse.app4mc.transformation.util.OutputBuffer
import java.util.LinkedList
import org.eclipse.app4mc.amalthea.model.HwStructure

@Singleton
class HwModelTransformer extends BaseTransformer {

	protected static def String getModulePath() {
		return AmaltheaTransformer.getModulePath() + "/hwModel"
	}

	protected def getModuleName() {
		return getModulePath + "/hwModel"
	}

	private def getFunctionDef() {
		return "init_hwModel()"
	}

	@Inject OutputBuffer outputBuffer
	@Inject HwStructureTransformer hwStructureTransformer
	@Inject FrequencyDomainTransformer frequencyDomainTransformer
	@Inject ProcessingUnitDefinitionTransformer processingUnitDefinitionTransformer
	@Inject ProcessingUnitTransformer processingUnitTransformer
	@Inject MemoryTransformer memoryTransformer
	@Inject ConnectionHandlerTransformer connectionHandlerTransformer
	@Inject HwConnectionTransformer hwConnectionTransformer
	@Inject HwFeatureTransformer hwFeatureTransformer
	@Inject HwAccessElementTransformer hwAccessElementTransformer
	
	val topLevelStructures = new LinkedList<HwStructure>

	def create new TranslationUnit(
		getModuleName(),
		getFunctionDef()		
	) transform(HWModel[] hwModels) {
		
		//loop through structures an keep top level elements in a list
		//due to app4mc.sim's usage of create methods throughout the 
		//hardware model, it is essential to construct the
		//hierarchies strictly top->down
		hwModels.forEach[hwModel | {
			hwModel.structures.forEach[structure |{
				topLevelStructures.add(structure)
				hwStructureTransformer.transform(structure)
			}]
		}]
		// start transformation @ task level, 
		// Note: generate referenced object (e.g. runnables) when referenced (eg. in a runnable call within the call graph) 
		outputBuffer.appendTo("INC", it.module, toH())
		outputBuffer.appendTo("SRC", it.module, toCpp())
		val buf = getCMake(it.module);
		outputBuffer.appendTo("OTHER", getModulePath() + "/CMakeLists.txt", buf);
	}

	private def String toH() '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "HardwareModel.h"
		//include model elements
		«FOR structure :  topLevelStructures»
			#include "«hwStructureTransformer.transform(structure).module».h"
		«ENDFOR»
			
		void «getFunctionDef()»;
		
	'''

	private def String toCpp() '''
		#include "«getModuleName».h"	
		
		/* Hardware */
		void «getFunctionDef()»{
			//INITIALIZE ONLY TOP LEVEL STRUCTURES HERE! 
			//Substructures will be initialized automatically by their parent.
			//this is necessary to ensure that hierarchical hardware structures are built top->down
			//due to app4mc.sim's create methods, which are widely spread in the hardware model 
			«FOR structure :  topLevelStructures»				
				«hwStructureTransformer.transform(structure).call»();
			«ENDFOR»
		}
	'''

	def String getCMake(String thisModule) '''	
		# CMakeList.txt: CMake project for HwModel of "«getProperty(PropertyKeys.PROJECT_NAME)»".
				
		
		# Add sources of HwModel
		target_sources («getProperty(PropertyKeys.PROJECT_NAME)»  PRIVATE
		
		#features 
		«FOR obj : TuSort.byModule(hwFeatureTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		#definitions
		«FOR obj : TuSort.byModule(processingUnitDefinitionTransformer.cache.values)» 
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		#domains	
		«FOR obj : TuSort.byModule(frequencyDomainTransformer.cache.values)» 
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		#hw structure
		«FOR obj : TuSort.byModule(hwStructureTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		# modules
		«FOR obj : TuSort.byModule(memoryTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		«FOR obj : TuSort.byModule(processingUnitTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		«FOR obj : TuSort.byModule(connectionHandlerTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		#connections
		«FOR obj : TuSort.byModule(hwConnectionTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		#HW access elements
		«FOR obj : TuSort.byModule(hwAccessElementTransformer.cache.values)»
			"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
		«ENDFOR»
		
		"${PROJECT_SOURCE_DIR}/«thisModule».cpp")
		
	'''
	
	def getCache(){
		return this._createCache_transform
	}
}
