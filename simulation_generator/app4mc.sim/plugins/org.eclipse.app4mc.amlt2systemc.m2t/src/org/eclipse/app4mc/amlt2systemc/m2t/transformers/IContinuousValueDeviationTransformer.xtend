 /**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers

import org.eclipse.app4mc.amalthea.model.ContinuousValueConstant
import org.eclipse.app4mc.amalthea.model.ContinuousValueGaussDistribution
import org.eclipse.app4mc.amalthea.model.ContinuousValueWeibullEstimatorsDistribution
import org.eclipse.app4mc.amalthea.model.IContinuousValueDeviation
import org.eclipse.app4mc.amalthea.model.ContinuousValueUniformDistribution
import org.eclipse.app4mc.amalthea.model.ContinuousValueBetaDistribution
import org.eclipse.app4mc.amalthea.model.ContinuousValueBoundaries

class IContinuousValueDeviationTransformer {
//IContinuousValueDeviation
	static def dispatch String getDeviation(IContinuousValueDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''
	static def dispatch String getDeviationTemplate(IContinuousValueDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''
	static def dispatch String getDeviationValue(IContinuousValueDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''

//ContinuousValueBetaDistribution
static def dispatch String getDeviation(ContinuousValueBetaDistribution value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(ContinuousValueBetaDistribution value)'''
		ContinuousValueBetaDistribution'''

	static def dispatch String getDeviationValue(ContinuousValueBetaDistribution value)'''
		«value.alpha»,«value.beta»,«value.lowerBound»,«value.upperBound»'''

private static def String stype(ContinuousValueBoundaries value) {
	switch(value.samplingType){
		case BEST_CASE: {
			return "BoundariesSamplingType::BestCase";
		}
		case WORST_CASE: {
			return "BoundariesSamplingType::WorstCase";
		}
		case AVERAGE_CASE: {
			return "BoundariesSamplingType::AverageCase";
		}
		case CORNER_CASE: {
			return "BoundariesSamplingType::CornerCase";
		}
		case UNIFORM: {
			return "BoundariesSamplingType::Uniform";
		}
		//TODO: what is DEFAULT enum case?
		default: {
			return "BoundariesSamplingType::WorstCase";
		}
	}
		
}
		
//ContinuousValueBoundaries
static def dispatch String getDeviation(ContinuousValueBoundaries value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(ContinuousValueBoundaries value)'''
		ContinuousValueBoundaries'''

	static def dispatch String getDeviationValue(ContinuousValueBoundaries value)'''
		«stype(value)»,«value.lowerBound»,«value.upperBound»'''

//ContinuousValueConstant
	static def dispatch String getDeviation(ContinuousValueConstant value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''

	static def dispatch String getDeviationTemplate(ContinuousValueConstant value)'''
		ContinuousValueConstant'''

	static def dispatch String getDeviationValue(ContinuousValueConstant value)'''
		«value.value»'''

//ContinuousValueGaussDistribution
	static def dispatch String getDeviation(ContinuousValueGaussDistribution value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(ContinuousValueGaussDistribution value)'''
		ContinuousValueGaussDistribution'''

	static def dispatch String getDeviationValue(ContinuousValueGaussDistribution value)'''
		«value.mean»,«value.sd»,«value.lowerBound?:-Double.MAX_VALUE»,«value.upperBound?:Double.MAX_VALUE»'''

//TODO: ContinuousValueHistogram
//TODO: ContinuousValueStatistics

//ContinuousValueUniformDistribution
	static def dispatch String getDeviation(ContinuousValueUniformDistribution value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(ContinuousValueUniformDistribution value)'''
		ContinuousValueUniformDistribution'''
	
	static def dispatch String getDeviationValue(ContinuousValueUniformDistribution value)'''
		«value.lowerBound»,«value.upperBound»'''

//ContinuousValueWeibullEstimatorsDistribution
	static def dispatch String getDeviation(ContinuousValueWeibullEstimatorsDistribution value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''

	static def dispatch String getDeviationTemplate(ContinuousValueWeibullEstimatorsDistribution value)'''
		ContinuousValueWeibullEstimatorsDistribution'''

	static def dispatch String getDeviationValue(ContinuousValueWeibullEstimatorsDistribution value)'''
		«IContinuousValueDeviationTransformer.getDeviationTemplate(value)»::findParameter(«value.average»,«value.PRemainPromille»,«value.lowerBound»,«value.upperBound»),«value.lowerBound»,«value.upperBound»'''
}