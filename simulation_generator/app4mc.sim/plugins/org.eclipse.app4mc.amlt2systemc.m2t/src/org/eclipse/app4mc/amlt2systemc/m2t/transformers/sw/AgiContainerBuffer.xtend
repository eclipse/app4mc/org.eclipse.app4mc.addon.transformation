/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import java.util.LinkedHashSet
import java.util.LinkedList
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer

class AgiContainerBuffer {
	val LinkedHashSet<String> activityItemIncludes
	val LinkedList<String> activityItemCalls
	public val String module
	public val String instanceName
	public val boolean parentIsPointer

	new(String _name, String _module, boolean _parentIsPointer) {
		instanceName = _name
		module = _module
		parentIsPointer = _parentIsPointer
		activityItemIncludes = new LinkedHashSet<String>
		activityItemCalls = new LinkedList<String>()
	}

	new(AgiContainerBuffer other) {
		instanceName = other.instanceName
		module = other.module
		parentIsPointer = other.parentIsPointer
		activityItemIncludes = other.activityItemIncludes
		activityItemCalls = other.activityItemCalls
	}

	def addAll(AgiContainerBuffer other) {
		activityItemIncludes.addAll(other.activityItemIncludes)
		activityItemCalls.addAll(other.activityItemCalls)
	}

	def addAll(ConditionBuffer other) {
		if (other !== null) {
			activityItemIncludes.addAll(other.includes)
			activityItemCalls.addAll(0, other.conditions) // prepend conditions for readability
		}
	}

	def LinkedHashSet<String> getIncludes() {
		return activityItemIncludes
	}

	def LinkedList<String> getSource() {
		return activityItemCalls
	}

	def void addInclude(String includePath) {
		activityItemIncludes.add("#include \"" + includePath + ".h\"")
	}

	def void addAsActivityGraphItem(String itemConstructor) {
		if (parentIsPointer)
			activityItemCalls.add(instanceName + "->addActivityGraphItem" + itemConstructor + ";")
		else
			activityItemCalls.add(instanceName + ".addActivityGraphItem" + itemConstructor + ";")
	}

	def void addBody(String body) {
		activityItemCalls.add(body)
	}

}
