/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.Switch

class SwitchTransformer  {

	@Inject SwitchEntryTransformer switchEntryTransformer
	static int cnt =0;
	
	static def getName(){
		return "switch_" + cnt;
	}	
	
	def void transform(Switch _switch, AgiContainerBuffer parentContent) {
		val name = getName()
		cnt++;	
		parentContent.addBody("Switch " + name + ";")
		
		val switchContent = new AgiContainerBuffer(name, parentContent.module, false)
		
		//run through entries
		_switch.entries.forEach[entry |
			switchEntryTransformer.transform(entry, switchContent)
		]
		if (_switch.defaultEntry !== null){
			switchEntryTransformer.transform(_switch.defaultEntry, switchContent)
		}
		
		parentContent.getIncludes().addAll(switchContent.getIncludes());
		parentContent.getSource().addAll(switchContent.getSource());
		
		parentContent.addAsActivityGraphItem("<Switch>(" + name + ")")
		
	}
	
}


