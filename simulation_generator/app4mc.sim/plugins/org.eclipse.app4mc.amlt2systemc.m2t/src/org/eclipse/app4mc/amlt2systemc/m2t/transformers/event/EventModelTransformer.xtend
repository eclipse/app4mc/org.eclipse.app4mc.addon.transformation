/**
 * *******************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.event

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.ChannelEvent
import org.eclipse.app4mc.amalthea.model.EventModel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.AmaltheaTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TuSort
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys

@Singleton
class EventModelTransformer extends BaseTransformer {
	
	 //static final Logger LOG = LoggerFactory.getLogger(EventModelTransformer.getClass());

	@Inject OutputBuffer outputBuffer
	@Inject EventTransformer eventsDispatcher
	

	protected static def getModulePath() {
		return AmaltheaTransformer.getModulePath() + "/eventModel"
	}

	private def getModuleName() { 
		return getModulePath + "/eventModel"
	}

	private def getFunctionDef() {
		return "init_eventModel()"
	}

	def create new TranslationUnit(
		getModuleName(),
		getFunctionDef()		
	) transform(EventModel[] eventModels) {
		eventModels.forEach[eventModel |{
			// start transformation @ task level, 
			// Note: generate referenced object (e.g. runnables) when referenced (eg. in a runnable call within the call graph) 
			eventModel.events.forEach[eventsDispatcher.transform(it)]
		}]
		outputBuffer.appendTo("INC", it.module, toH())
		outputBuffer.appendTo("SRC", it.module, toCpp())
		outputBuffer.appendTo("OTHER", getModulePath() + "/CMakeLists.txt", getCMake(it.module));
	}

	private def String toH() '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "SoftwareModel.h"
		#include "Channel.h"
		#include "ChannelAccess.h"
		#include "PriorityScheduler.h"
		//include model elements»
		«FOR obj : TuSort.byModule(eventsDispatcher.getCache(typeof(ChannelEvent)).values)»
			#include "«(obj as TranslationUnit).module».h"
		«ENDFOR»
		void «getFunctionDef()»;
		
	'''

	private def String toCpp() '''
		#include "«getModuleName».h"	
		
		/* Software */
		void «getFunctionDef()»{		
			«FOR obj : TuSort.byCall(eventsDispatcher.getCache(typeof(ChannelEvent)).values)»
				«(obj as TranslationUnit).call»;
			«ENDFOR»
		}
	'''


	def String getCMake(String thisModule) '''	
		# CMakeList.txt: CMake project for EventModel of "«getProperty(PropertyKeys.PROJECT_NAME)»".
						
				
		# Add sources of EventModel
		target_sources («getProperty(PropertyKeys.PROJECT_NAME)»  PRIVATE
			«FOR obj : TuSort.byModule(eventsDispatcher.getCache(typeof(ChannelEvent)).values)»
				"${PROJECT_SOURCE_DIR}/«(obj as TranslationUnit).module».cpp" 
			«ENDFOR»
			
			"${PROJECT_SOURCE_DIR}/«thisModule».cpp")
	'''

	def getCache(){
		return this._createCache_transform
	}
}
