/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Semaphore
import org.eclipse.app4mc.amalthea.model.SemaphoreType
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class SemaphoreTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer

	def getName(Semaphore semaphore) {
		return semaphore.name
	}

	private def getModulePath(Semaphore semaphore) {
		return OsModelTransformer.getModulePath() + "/semaphores/" + getName(semaphore)
	}

	private def getCall(Semaphore semaphore) {
		return "get_" + getName(semaphore) + "()"
	}

	def create new TranslationUnit(
		getModulePath(semaphore),
		getCall(semaphore)		
	) transform(Semaphore semaphore) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(semaphore))
		// write implementation file
		outputBuffer.appendTo("SRC", it.module, toCpp(semaphore))
	}

	// TODO is "#include "Common.h" needed as well?
	private def String toH(Semaphore semaphore) '''
		#include "Semaphore.h"
		
		std::shared_ptr<Semaphore> «getCall(semaphore)»;
	'''


	private def String toCpp(Semaphore semaphore) '''	
		«val name = getName(semaphore)»
		#include "«getModulePath(semaphore)».h"
		
		std::shared_ptr<Semaphore> «name» = nullptr;
		
		std::shared_ptr<Semaphore> «getCall(semaphore)» {
			if («name» == nullptr){
				//instantiate
				«name» = Semaphore::createSemaphore("«name»", «semaphore.maxValue», «semaphore.initialValue», SemaphoreType::«getSemaphoreType(semaphore)»);
			}
			return «name»;
		}
	'''
	
	private def getSemaphoreType(Semaphore semaphore){
		switch semaphore.semaphoreType {
			case SemaphoreType.COUNTING_SEMAPHORE: "CountingSemaphore"
			case SemaphoreType.SPINLOCK: "Spinlock"
			default: throw new IllegalArgumentException("Unsupported semaphore type: " + semaphore.semaphoreType)
		}
	}

	def getCache() { return this._createCache_transform }

}
