/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Channel
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class ChannelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer

	private def String getChannelName(Channel channel) {
		var uniqueName = channel.uniqueName
		uniqueName = uniqueName.replaceAll("\\?", "_")
		uniqueName = uniqueName.replace("=", "_")
		uniqueName = uniqueName.replace("/", "_")
		return uniqueName
	}

	private def String getModuleName(Channel channel) {
		return SWModelTransformer.getModulePath() + "/channels/" + getChannelName(channel)
	}

	private def getFunctionDef(Channel _channel) {
		return "get_" + getChannelName(_channel) + "()"
	}

	def create new TranslationUnit(
		getModuleName(channel),
		getFunctionDef(channel)
	) transform(Channel channel) {
		outputBuffer.appendTo("INC", it.module, toH(channel))
		outputBuffer.appendTo("SRC", it.module, toCpp(channel))
	}

	private def String toH(Channel channel) '''
		#include "Common.h"
		#include "Channel.h"
		#include "HardwareModel.h"
		#include "MappingModel.h"
		
		std::shared_ptr<Channel> «getFunctionDef(channel)»;
	'''

	private def String toCpp(Channel channel) '''
		«val name = getChannelName(channel)»
		#include "«getModuleName(channel)».h"
		
		std::shared_ptr<Channel> «name» = nullptr;
		
		std::shared_ptr<Channel>  «getFunctionDef(channel)» {
			if («name» == nullptr){
				«name» = std::make_shared<Channel>("«name»", «getSizeInByte(channel)», «channel.defaultElements», «channel.maxElements»);
			}
			return «name»;
		}
		
	'''

	private def getSizeInByte(Channel channel) {
		val size = channel.size
		if (size === null)
			return 0
		else
			return size.numberBytes
	}

	def getCache() {
		this._createCache_transform
	}

}
