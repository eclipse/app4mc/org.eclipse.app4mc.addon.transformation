 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.Cache
import org.eclipse.app4mc.amalthea.model.ConnectionHandler
import org.eclipse.app4mc.amalthea.model.HwDestination
import org.eclipse.app4mc.amalthea.model.HwModule
import org.eclipse.app4mc.amalthea.model.Memory
import org.eclipse.app4mc.amalthea.model.PortType
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.emf.ecore.EObject

@Singleton
class HwModuleTransformer extends BaseTransformer {

	@Inject HwConnectionTransformer hwConnectionTransformer
	@Inject HwStructureTransformer hwStructureTransformer

	def static HwModule getParentHwModuleOrNull(EObject obj) {
		val ret = obj.eContainer
		if (ret instanceof HwModule) {
			return (ret as HwModule)
		} else {
			return null;
		}
	}

	def String getName(HwModule hwModule) {
		return hwModule.name // or uniqueName
	}

	protected def String getCall(HwModule hwModule) '''
	get_«getName(hwModule)»'''

	protected def String getModulePath(HwModule hwModule) {
		// hw modules are created within the files of their containing hw structure
		HwModelTransformer.getModulePath() + "/modules/" + getName(hwModule);
	}

	protected def getIncludesForConnections(HwModule hwModule) '''
		//Connections
		«FOR port : hwModule.ports»
			«FOR connection : port.connections»
				#include "«hwConnectionTransformer.transform(connection).module».h"			
			«ENDFOR»
		«ENDFOR»
	'''

	protected def setPortsAndConnections(HwModule hwModule) '''	
		«val name = getName(hwModule)»
		//HW Ports	
		«HwPortsTransformer.initialize(name, hwModule.ports)»
		//Connections
		«FOR port : hwModule.ports»
			«FOR connection : port.connections»
				«val tuConnection = hwConnectionTransformer.transform(connection)»
				«name»->bindConnection(«tuConnection.call»(), "«HwPortsTransformer.getName(port)»");
				«IF port.portType === PortType.INITIATOR»
					«tuConnection.call»()->setFreqDomain(«name»);
				«ENDIF»
			«ENDFOR»
		«ENDFOR»
	'''

	protected def getParentGetter(HwModule module) {
		val parentHwStructure = HwStructureTransformer.getParentHwStructureOrNull(module)
		if (parentHwStructure !== null) {
			val parentTu = hwStructureTransformer.transform(parentHwStructure)
			return parentTu.call;
		} else {
			throw new RuntimeException("Module " + module.qualifiedName + " is not embedded in a HwStructure.")
		}
	}

	protected def getParentInclude(HwModule module) {
		val parentHwStructure = HwStructureTransformer.getParentHwStructureOrNull(module)
		if (parentHwStructure !== null) {
			val parentTu = hwStructureTransformer.transform(parentHwStructure)
			return "#include \"" + parentTu.module + ".h\""
		} else {
			throw new RuntimeException("Module " + module.qualifiedName + " is not embedded in a HwStructure.")
		}
	}

//----------------------------------
// HW Destination
//----------------------------------
	def dispatch TranslationUnit transform(HwDestination hwDestination) {
		if (hwDestination instanceof ProcessingUnit) {
			return transform(hwDestination as ProcessingUnit)
		} else if (hwDestination instanceof Memory) {
			return transform(hwDestination as Memory)
		}
	}

//----------------------------------
// Memory
//----------------------------------
	@Inject MemoryTransformer memoryTransformer

	def dispatch TranslationUnit transform(Memory memory) {
		return memoryTransformer.transform(memory)
	}

//----------------------------------
// Cache
//----------------------------------	
	def dispatch TranslationUnit transform(Cache cache) {
		// check out back pointers to connections, these might help to solve "factories" problem
		// cache.ports.get(0).connections
		return new TranslationUnit("Transformation of Cache transformation not yet implemented",
			"Transformation of Cache transformation not yet implemented")
	}

//------------------------------
// ConnectionHandler
//------------------------------
	@Inject ConnectionHandlerTransformer connectionHandlerTransformer

	def dispatch TranslationUnit transform(ConnectionHandler connectionHandler) {
		return connectionHandlerTransformer.transform(connectionHandler)
	}

//------------------------------
// ProcessingUnit
//------------------------------
	@Inject ProcessingUnitTransformer processingUnitTransformer

	def dispatch TranslationUnit transform(ProcessingUnit processingUnit) {
		processingUnitTransformer.transform(processingUnit)
	}

}
