 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.ConnectionHandler
import org.eclipse.app4mc.amalthea.model.HwConnection
import org.eclipse.app4mc.amalthea.model.HwPathElement
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.ConnectionHandlerTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.ProcessingUnitTransformer

class HwPathElementTransformer extends BaseTransformer {

	// @Inject CacheTransformer connectionHandlerTransformer
	@Inject ConnectionHandlerTransformer connectionHandlerTransformer
	@Inject HwConnectionTransformer hwConnectionTransformer
	@Inject ProcessingUnitTransformer processingUnitTransformer

	// static def dispatch TranslationUnit transform(Cache elem){	
	// }
	def dispatch TranslationUnit transform(ConnectionHandler elem) {
		return connectionHandlerTransformer.transform(elem)
	}

	def dispatch TranslationUnit transform(HwConnection elem) {
		return hwConnectionTransformer.transform(elem)
	}

	def dispatch TranslationUnit transform(ProcessingUnit elem) {
		return processingUnitTransformer.transform(elem)
	}

	def dispatch TranslationUnit transform(HwPathElement elem) {
		return new TranslationUnit("/*HwPathElement not yet supported*/", "/*HwPathElement not yet supported*/")
	}

}
