/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.EnumMode
import org.eclipse.app4mc.amalthea.model.ModeLabel
import org.eclipse.app4mc.amalthea.model.NumericMode
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class ModeLabelTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject ModeTransformer modeTransformer

	private def String getModeLabelName(ModeLabel modeLabel) {
		return modeLabel.name
	}

	private def String getModuleName(ModeLabel modeLabel) {
		return SWModelTransformer.getModulePath() + "/modeLabels/" + getModeLabelName(modeLabel)
	}

	private def getFunctionDef(ModeLabel _modeLabel) {
		return "get_" + getModeLabelName(_modeLabel) + "()"
	}

	def create new TranslationUnit(
		getModuleName(modeLabel),
		getFunctionDef(modeLabel)
	) transform(ModeLabel modeLabel) {
		outputBuffer.appendTo("INC", it.module, toH(modeLabel))
		outputBuffer.appendTo("SRC", it.module, toCpp(modeLabel))
	}

	private def String toH(ModeLabel modeLabel) '''
		#include "Common.h"
		#include "ModeLabel.h"
		
		std::shared_ptr<ModeLabel> «getFunctionDef(modeLabel)»;
	'''

	private def String toCpp(ModeLabel modeLabel) '''
		«val name = getModeLabelName(modeLabel)»
		«val modeTu = modeTransformer.transform(modeLabel.mode)»
		#include "«getModuleName(modeLabel)».h"
		#include "«modeTu.module».h"
		
		std::shared_ptr<ModeLabel> «name» = nullptr;
		
		std::shared_ptr<ModeLabel>  «getFunctionDef(modeLabel)» {
			if («name» == nullptr){
				«IF modeLabel.mode instanceof EnumMode»
					«IF modeLabel.size !== null»
						«name» = std::make_shared<ModeLabel>("«name»", «modeTu.call», «modeTu.call»->getLiteral("«modeLabel.initialValue»"), «getSizeInByte(modeLabel)»);
					«ELSE»
						«name» = std::make_shared<ModeLabel>("«name»", «modeTu.call», «modeTu.call»->getLiteral("«modeLabel.initialValue»"));
					«ENDIF»
				«ELSEIF modeLabel.mode instanceof NumericMode»
					«IF modeLabel.size !== null»
						«name» = std::make_shared<ModeLabel>("«name»", «modeTu.call», «modeLabel.initialValue», «getSizeInByte(modeLabel)»);
					«ELSE»
						«name» = std::make_shared<ModeLabel>("«name»", «modeTu.call», «modeLabel.initialValue»);
					«ENDIF»
				«ENDIF»
			}
			return «name»;
		}
		
	'''
		
	def String getInitializer(EnumMode mode, String initialValue){
		return ModeTransformer.getFunctionDef(mode) + "->getLiteral(\"" + initialValue + "\")"	
	}
	
	def String getInitializer(NumericMode mode, String initialValue){
		return initialValue	
	}

	private def getSizeInByte(ModeLabel modeLabel) {
		val size = modeLabel.size
		if (size === null)
			return 0
		else
			return size.numberBytes
	}

	def getCache() {
		this._createCache_transform
	}

}
