/**
 ********************************************************************************
 * Copyright (c) 2021 Uni Rostock and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Uni Rostock - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.utils;

import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;

public class TimeUtil {

	private TimeUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static String transform(Time time) {

		TimeUnit unit = time.getUnit();

		switch (unit) {
		case PS:
			return time.getValue() + "_ps";
		case NS:
			return time.getValue() + "_ns";
		case US:
			return time.getValue() + "_us";
		case MS:
			return time.getValue() + "_ms";
		case S:
			return time.getValue() + "_s";
		default:
			break;
		}

		return "/*unknown TimeUnit*/";
	}

}
