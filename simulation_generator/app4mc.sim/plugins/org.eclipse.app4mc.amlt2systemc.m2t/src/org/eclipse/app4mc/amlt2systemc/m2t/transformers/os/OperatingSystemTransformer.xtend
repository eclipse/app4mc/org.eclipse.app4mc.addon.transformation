/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.os

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.OperatingSystem
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class OperatingSystemTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject TaskSchedulerTransformer taskSchedulerTransformer

	private static def String getModulePath(OperatingSystem os) {
		if (os !== null) {
			return OsModelTransformer.getModulePath() + "/operatingSystems/" + getName(os);
		} else {
			return OsModelTransformer.getModulePath();
		}
	}

	protected static def String getCall(OperatingSystem os) '''
	get_OperatingSystem_«getName(os)»'''

	private static def String getName(OperatingSystem os) {
		return os.name // or uniqueName
	}

	def create new TranslationUnit(getModulePath(os), getCall(os)) transform(OperatingSystem os) {
		outputBuffer.appendTo("INC", it.module, toH(os))
		outputBuffer.appendTo("SRC", it.module, toCpp(os))
	}

	private def String toH(OperatingSystem os) '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "OSModel.h"
		//include model elements
		void «getCall(os)»();
		
	'''

	private def String toCpp(OperatingSystem os) '''
			#include "«getModulePath(os)».h"
			«FOR ts : os.taskSchedulers»
			#include "«taskSchedulerTransformer.transform(ts).module».h"
			«ENDFOR»
		
			
			/* OS */
			void «getCall(os)»(){
				«FOR ts : os.taskSchedulers»
					«taskSchedulerTransformer.transform(ts).call»();
				«ENDFOR»
			}
	'''

	def getCache() { return this._createCache_transform }

}
