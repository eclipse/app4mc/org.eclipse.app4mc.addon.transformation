/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.module;

import java.util.Properties;

import com.google.inject.Inject;

public abstract class BaseTransformer {

	@Inject private Properties properties;

	protected String getProperty(final String propKey) {
		return this.getProperty(propKey, null);
	}

	protected String getProperty(final String propKey, final String defaultValue) {
		if ((this.properties == null)) {
			throw new NullPointerException(
					"Properties object not bound: Verify the custom Google Guice Module configuration ");
		}
		final Object value = this.properties.get(propKey);
		if (((value == null) && (defaultValue == null))) {
			throw new NullPointerException(
					(("Request input key : \"" + propKey) + "\" not supplied in the input properties file"));
		}

		return (value != null) ? value.toString() : defaultValue;
	}
}
