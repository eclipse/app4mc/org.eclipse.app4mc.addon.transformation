/**
 * *******************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import java.util.LinkedHashSet
import java.util.LinkedList
import org.eclipse.app4mc.amalthea.model.Task

class TaskGenerator {

	// Suppress default constructor
	private new () {
		throw new IllegalStateException("Utility class");
	}

	static def getName(Task task) {
		return task.name
	}

	static def getModulePath(Task task) {
		return SWModelTransformer.getModulePath() + "/tasks/" + getName(task)
	}

	static def getFunctionDef(Task task) {
		return "get_" + task.name + "()"
	}

	static def String toH(Task task) '''
		#include "APP4MCsim.h"
		
		//Task runnableA----
		std::shared_ptr<Task> «getFunctionDef(task)»;
	'''

	static def String toCpp(Task task,
			LinkedList<String> stimuliCalls,
			LinkedHashSet<String> activityItemIncludes,
			LinkedList<String> activityItemCalls) '''
		#include <systemc>
		#include "APP4MCsim.h"
		#include "«getModulePath(task)».h"
		
		«FOR include : activityItemIncludes»
			«include»
		«ENDFOR»
		
		std::shared_ptr<Task> «task.name» = nullptr;
		
		std::shared_ptr<Task>  «getFunctionDef(task)» {
		
			if («task.name» == nullptr) {
				//initialize
				«task.name» = Task::createTask("«task.name»");
				«IF (task.multipleTaskActivationLimit >= 1)»
					«task.name»->setActivationLimit(«task.multipleTaskActivationLimit»);
				«ENDIF»
				«FOR activityItemCall : activityItemCalls»
					«IF (activityItemCall.startsWith("//") || activityItemCall.startsWith("/*"))»
						//«activityItemCall»
					«ELSE»
						«activityItemCall»
					«ENDIF»
				«ENDFOR»
				
				«FOR stimulusCall : stimuliCalls»
					«task.name»->addStimulus(«stimulusCall»);
				«ENDFOR»
			}
			return «task.name»;
		}
		
	'''

}
