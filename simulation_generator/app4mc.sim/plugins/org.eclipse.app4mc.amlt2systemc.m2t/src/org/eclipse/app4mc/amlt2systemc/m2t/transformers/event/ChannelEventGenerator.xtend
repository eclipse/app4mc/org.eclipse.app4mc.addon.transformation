/**
 * *******************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.event

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.ChannelEvent
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ChannelTransformer
import org.eclipse.app4mc.amalthea.model.ChannelEventType
import com.google.inject.Singleton

@Singleton
class ChannelEventGenerator {
	
	@Inject ChannelTransformer channelTransformer

	static def getName(ChannelEvent channelEvent) {
		return channelEvent.name
	}

	static def getModulePath(ChannelEvent channelEvent) {
		return EventModelTransformer.getModulePath() + "/channelEvents/" + getName(channelEvent)
	}

	static def getFunctionDef(ChannelEvent channelEvent) {
		return "get_" + getName(channelEvent) + "()"
	}

	def String toH(ChannelEvent channelEvent) '''
		#include "Event.h"
		
		//ChannelEvent----
		std::shared_ptr<Event> «getFunctionDef(channelEvent)»;
	'''

	def String toCpp(ChannelEvent channelEvent) '''
		#include <systemc>
		#include "Common.h"
		#include "Channel.h"
		//#include "GenericQueueAccess.h"
		//#include "Deviation.h"
		#include "«getModulePath(channelEvent)».h"
		#include "«channelTransformer.transform(channelEvent.entity).module».h"
		
		
		std::shared_ptr<Event> «getName(channelEvent)» = nullptr;
		
		std::shared_ptr<Event>  «getFunctionDef(channelEvent)» {
			if («getName(channelEvent)» == nullptr) {
				«getName(channelEvent)» = «channelTransformer.transform(channelEvent.entity).call»->addEvent("«getName(channelEvent)»", EventType::«channelEvent.type», StateType::DONE);
			}
			return «getName(channelEvent)»;
		}
		
	'''
	
	static private def String getType(ChannelEvent channelEvent){
		switch channelEvent.eventType {
			case ChannelEventType.SEND : return "WRITE_EVENT"
			case ChannelEventType.RECEIVE : return "READ_EVENT"
			default : return "ACCESS_EVENT"
		}
	}

}
