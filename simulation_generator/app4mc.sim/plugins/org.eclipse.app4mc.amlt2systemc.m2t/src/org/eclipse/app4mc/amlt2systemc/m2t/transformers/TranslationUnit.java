/**
********************************************************************************
* Copyright (c) 2020 Robert Bosch GmbH.
* 
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
* 
* SPDX-License-Identifier: EPL-2.0
* 
* Contributors:
*     Robert Bosch GmbH - initial API and implementation
********************************************************************************
*/

package org.eclipse.app4mc.amlt2systemc.m2t.transformers;

public class TranslationUnit {
	protected boolean valid;
	private String module;
	protected String call;

	public TranslationUnit(final String _module, final String _call) {
		module = _module;
		call = _call;
		valid = true;
	}
	
	public TranslationUnit(final String errorMessage) {
		module = errorMessage;
		call = errorMessage;
		valid = false;
	}

	public String getModule() {
		return module;
	}

	public String getCall() {
		return call;
	}

	public boolean isValid() {
		return valid;
	}

}
