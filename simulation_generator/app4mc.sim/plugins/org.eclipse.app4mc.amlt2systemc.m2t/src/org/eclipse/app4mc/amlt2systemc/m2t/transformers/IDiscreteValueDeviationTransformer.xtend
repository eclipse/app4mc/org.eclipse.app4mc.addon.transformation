 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers

import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution
import org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation
import org.eclipse.app4mc.amalthea.model.DiscreteValueUniformDistribution
import org.eclipse.app4mc.amalthea.model.DiscreteValueBetaDistribution
import org.eclipse.app4mc.amalthea.model.DiscreteValueBoundaries

class IDiscreteValueDeviationTransformer {
//IDiscreteValueDeviation
	static def dispatch String getDeviation(IDiscreteValueDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''
	static def dispatch String getDeviationTemplate(IDiscreteValueDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''
	static def dispatch String getDeviationValue(IDiscreteValueDeviation value)'''
		WARNING:VALUE_FORMAT_NOT_SUPPORTED'''

//DiscreteValueBetaDistribution
static def dispatch String getDeviation(DiscreteValueBetaDistribution value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(DiscreteValueBetaDistribution value)'''
		DiscreteValueBetaDistribution'''

	static def dispatch String getDeviationValue(DiscreteValueBetaDistribution value)'''
		«value.alpha»,«value.beta»,«value.lowerBound»,«value.upperBound»'''

private static def String stype(DiscreteValueBoundaries value) {
	switch(value.samplingType){
		case BEST_CASE: {
			return "BoundariesSamplingType::BestCase";
		}
		case WORST_CASE: {
			return "BoundariesSamplingType::WorstCase";
		}
		case AVERAGE_CASE: {
			return "BoundariesSamplingType::AverageCase";
		}
		case CORNER_CASE: {
			return "BoundariesSamplingType::CornerCase";
		}
		case UNIFORM: {
			return "BoundariesSamplingType::Uniform";
		}
		//TODO: what is DEFAULT enum case?
		default: {
			return "BoundariesSamplingType::WorstCase";
		}
	}
		
}
		
//DiscreteValueBoundaries
static def dispatch String getDeviation(DiscreteValueBoundaries value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(DiscreteValueBoundaries value)'''
		DiscreteValueBoundaries'''

	static def dispatch String getDeviationValue(DiscreteValueBoundaries value)'''
		«stype(value)»,«value.lowerBound»,«value.upperBound»'''

//DiscreteValueConstant
	static def dispatch String getDeviation(DiscreteValueConstant value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''

	static def dispatch String getDeviationTemplate(DiscreteValueConstant value)'''
		DiscreteValueConstant'''

	static def dispatch String getDeviationValue(DiscreteValueConstant value)'''
		«value.value»'''

//DiscreteValueGaussDistribution
	static def dispatch String getDeviation(DiscreteValueGaussDistribution value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(DiscreteValueGaussDistribution value)'''
		DiscreteValueGaussDistribution'''

	static def dispatch String getDeviationValue(DiscreteValueGaussDistribution value)'''
		«value.mean»,«value.sd»,«value.lowerBound?:Long.MIN_VALUE»,«value.upperBound?:Long.MAX_VALUE»'''

//TODO: DiscreteValueHistogram
//TODO: DiscreteValueStatistics

//DiscreteValueUniformDistribution
	static def dispatch String getDeviation(DiscreteValueUniformDistribution value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''
	
	static def dispatch String getDeviationTemplate(DiscreteValueUniformDistribution value)'''
		DiscreteValueUniformDistribution'''
	
	static def dispatch String getDeviationValue(DiscreteValueUniformDistribution value)'''
		«value.lowerBound»,«value.upperBound»'''

//DiscreteValueWeibullEstimatorsDistribution
	static def dispatch String getDeviation(DiscreteValueWeibullEstimatorsDistribution value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»(«getDeviationValue(value)»)'''

	static def dispatch String getDeviationTemplate(DiscreteValueWeibullEstimatorsDistribution value)'''
		DiscreteValueWeibullEstimatorsDistribution'''

	static def dispatch String getDeviationValue(DiscreteValueWeibullEstimatorsDistribution value)'''
		«IDiscreteValueDeviationTransformer.getDeviationTemplate(value)»::findParameter(«value.average»,«value.PRemainPromille»,«value.lowerBound»,«value.upperBound»),«value.lowerBound»,«value.upperBound»'''
}