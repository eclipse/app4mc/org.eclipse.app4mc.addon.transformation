/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.EnumMode
import org.eclipse.app4mc.amalthea.model.Mode
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.app4mc.amalthea.model.NumericMode

@Singleton
class ModeTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer

	
	def create new TranslationUnit(
		getModuleName(mode),
		getFunctionDef(mode)) 
	transform(Mode mode) {
		outputBuffer.appendTo("INC", it.module, toH(mode))
		outputBuffer.appendTo("SRC", it.module, toCpp(mode))
	}
	
	static private def String getName(Mode mode) {
		return mode.name;
	}
	
	
	static def getFunctionDef(Mode mode) {
		return "get_" + getName(mode) + "()"
	}

	
	static def getModuleName(Mode mode) {
		SWModelTransformer.getModulePath() + "/modes/" + getName(mode)
	}
	
	private def dispatch String toH(EnumMode mode) '''
		#include "Mode.h"			
		std::shared_ptr<EnumMode> «getFunctionDef(mode)»;
	'''
	
	private def dispatch String toH(NumericMode mode) '''
		#include "Mode.h"			
		std::shared_ptr<NumericMode> «getFunctionDef(mode)»;
	'''

	private def dispatch String toCpp(EnumMode mode) '''
		#include "«getModuleName(mode)».h"
		
		std::shared_ptr<EnumMode> «getName(mode)» = nullptr;
		
		std::shared_ptr<EnumMode>  «getFunctionDef(mode)» {
			if («getName(mode)» == nullptr){
				«getName(mode)» = EnumMode::CreateMode("«getName(mode)»");
				«FOR literal : mode.literals»
						«getName(mode)»->addLiteral("«literal.name»");
				«ENDFOR»
			}
			return «getName(mode)»;
		}
	'''
	
	private def dispatch String toCpp(NumericMode mode) '''
		#include "«getModuleName(mode)».h"
		
		std::shared_ptr<NumericMode> «getName(mode)» = nullptr;
		
		std::shared_ptr<NumericMode>  «getFunctionDef(mode)» {
			if («getName(mode)» == nullptr){
				«getName(mode)» = NumericMode::CreateMode("«getName(mode)»");
			}
			return «getName(mode)»;
		}
	'''
	
	def getCache() {
		this._createCache_transform
	}
	
}
