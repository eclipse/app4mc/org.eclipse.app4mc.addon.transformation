/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.common

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.ChannelFillCondition
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.ChannelTransformer

class ChannelFillConditionTransformer extends BaseTransformer {

	@Inject ChannelTransformer channelTransformer

	def createCondition(String parent, ChannelFillCondition condition, ConditionBuffer content) {
		val tuChannel = channelTransformer.transform(condition.channel)
		val fillLevel = condition.fillLevel
		content.addInclude(tuChannel.module)	
		content.addCondition(tuChannel.call + ", " + fillLevel + ", " + "RelationalOperator::GREATER_THAN")
		content.addCondition(tuChannel.call + ", " + fillLevel + ", " + "RelationalOperator::EQUAL")
	}
}
