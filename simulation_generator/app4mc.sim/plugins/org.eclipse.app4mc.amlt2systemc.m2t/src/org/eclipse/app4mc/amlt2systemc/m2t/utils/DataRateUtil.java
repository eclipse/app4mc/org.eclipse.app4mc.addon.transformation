/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.utils;

import java.math.BigInteger;

import org.eclipse.app4mc.amalthea.model.DataRate;
import org.eclipse.app4mc.amalthea.model.DataRateUnit;

public class DataRateUtil {

	// Suppress default constructor
	private DataRateUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static String transform(DataRate dataRate) {
		final BigInteger factor = BigInteger.valueOf(8);	// byte to bit factor
		final BigInteger value = dataRate.getValue();
		final DataRateUnit unit = dataRate.getUnit();

		switch (unit) {
			case BIT_PER_SECOND:	return "{" + value + ", DataRateUnit::bitPerSecond}";
			case KBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::kbitPerSecond}";
			case MBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::MbitPerSecond}";
			case GBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::GbitPerSecond}";
			case TBIT_PER_SECOND: 	return "{" + value + ", DataRateUnit::TGbitPerSecond}";
			case KIBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::KibitPerSecond}";
			case MIBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::MibitPerSecond}";
			case GIBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::GibitPerSecond}";
			case TIBIT_PER_SECOND:	return "{" + value + ", DataRateUnit::TibitPerSecond}";
			case BPER_SECOND:		return "{" + value.multiply(factor) + ", DataRateUnit::bitPerSecond}";
			case KB_PER_SECOND:		return "{" + value.multiply(factor) + ", DataRateUnit::kbitPerSecond}";
			case MB_PER_SECOND:		return "{" + value.multiply(factor) + ", DataRateUnit::MbitPerSecond}";
			case GB_PER_SECOND:		return "{" + value.multiply(factor) + ", DataRateUnit::GbitPerSecond}";
			case TB_PER_SECOND:		return "{" + value.multiply(factor) + ", DataRateUnit::TGbitPerSecond}";
			case KI_BPER_SECOND:	return "{" + value.multiply(factor) + ", DataRateUnit::KibitPerSecond}";
			case MI_BPER_SECOND:	return "{" + value.multiply(factor) + ", DataRateUnit::MibitPerSecond}";
			case GI_BPER_SECOND:	return "{" + value.multiply(factor) + ", DataRateUnit::GibitPerSecond}";
			case TI_BPER_SECOND:	return "{" + value.multiply(factor) + ", DataRateUnit::TibitPerSecond}";
		 	default: return "/*unknown DataRateUnit*/";
		}
	}

}
