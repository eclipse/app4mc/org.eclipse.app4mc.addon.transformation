/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.TaskAllocation
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.SchedulingParameterTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.TaskSchedulerTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.TaskGenerator
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.TaskTransformer
import org.eclipse.app4mc.transformation.util.OutputBuffer

class TaskAllocationTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject TaskTransformer taskTransformer
	@Inject TaskSchedulerTransformer taskSchedulerTransformer
	@Inject SchedulingParameterTransformer schedulingParameterTransformer

	private def String getModulePath() {
		return MappingModelTransformer.getModulePath() + "/taskAllocation"
	}

	private def String getModuleName(TaskAllocation taskAllocation) '''
	«getModulePath()»/taskAllocation_«taskAllocation.scheduler.name»_«taskAllocation.task.name»'''

	private def String getCall(TaskAllocation taskAllocation) '''
	init_TaskAllocation_«taskSchedulerTransformer.getName(taskAllocation.scheduler)»_«TaskGenerator.getName(taskAllocation.task)»'''

	def create new TranslationUnit(getModuleName(taskAllocation),getCall(taskAllocation))	
		transform(TaskAllocation taskAllocation) {
		outputBuffer.appendTo("INC", it.module, toH(it))
		outputBuffer.appendTo("SRC", it.module, toCpp(taskAllocation))
	}

	private def String toH(TranslationUnit tu) '''
		void «tu.call»();		
	'''

	private def String toCpp(TaskAllocation taskAllocation) '''
		«««TranslationUnit tu,TaskScheduler taskScheduler, Task task)'''
		«val tuScheduler = taskSchedulerTransformer.transform(taskAllocation.scheduler)»
		«val tuTask = taskTransformer.transform(taskAllocation.task)»
		#include "«tuScheduler.module».h"
		#include "«tuTask.module».h"
		
		void «getCall(taskAllocation)»(){
			«IF taskAllocation?.schedulingParameters !== null»
				TaskAllocation ta;
				«FOR paramEntry : taskAllocation.schedulingParameters.entrySet»
					«schedulingParameterTransformer.createSchedParam("ta", "setSchedulingParameter", false, paramEntry.key, paramEntry.value)»
				«ENDFOR»
				«tuTask.call»->setTaskAllocation(ta);
			«ENDIF»
			«tuScheduler.call»()->addTaskMapping(«tuTask.call»);
		}
	'''

	def getCache() { return this._createCache_transform }

}
