 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class ProcessingUnitDefinitionTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject HwFeatureTransformer hwFeatureTransformer

	static def String getModulePath(ProcessingUnitDefinition processingUnitDefinition) {
		return HwModelTransformer.getModulePath() + "/definitions/" + getName(processingUnitDefinition);
	}

	static def String getCall(ProcessingUnitDefinition processingUnitDefinition) '''
	get_ProcessingUnitDefinition_«getName(processingUnitDefinition)»()'''

	static def String getName(ProcessingUnitDefinition processingUnitDefinition) {
		return processingUnitDefinition.name // or uniqueName
	}

	def create new TranslationUnit(
		getModulePath(processingUnitDefinition),
		getCall(processingUnitDefinition)		
	) transform(ProcessingUnitDefinition processingUnitDefinition) {
		outputBuffer.appendTo("INC", it.module, toH(processingUnitDefinition))
		outputBuffer.appendTo("SRC", it.module, toCpp(processingUnitDefinition))
	}

	def String toH(ProcessingUnitDefinition processingUnitDefinition) '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "HardwareModel.h"
		
		ProcessingUnitDefinition* «getCall(processingUnitDefinition)»;
	'''

	def String toCpp(ProcessingUnitDefinition processingUnitDefinition) '''
		#include "«getModulePath(processingUnitDefinition)».h"	
		«FOR feature : processingUnitDefinition.features»
			#include "«hwFeatureTransformer.transform(feature).module».h"
		«ENDFOR»
		
		«val name = getName(processingUnitDefinition)»
		ProcessingUnitDefinition* «name» = NULL;
		ProcessingUnitDefinition* «getCall(processingUnitDefinition)»{
			if («name»==NULL){
				«name» = new ProcessingUnitDefinition("«name»");
				«FOR feature : processingUnitDefinition.features»
					«name»->addFeature(*«hwFeatureTransformer.transform(feature).call»);
				«ENDFOR»
			}
			return «name»;
		}
	'''

	def getCache() { return this._createCache_transform }
}
