/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.stimuli

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.SingleStimulus
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer.ConditionBuffer
import org.eclipse.app4mc.amlt2systemc.m2t.utils.TimeUtil
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class SingleStimulusTransformer extends StimulusBaseTransformer {
	@Inject OutputBuffer outputBuffer
	@Inject ConditionTransformer conditionTransformer;

	
	def create new TranslationUnit(
		getModulePath(singleStimulus),
		getFunctionDef(singleStimulus)		
	) transform(SingleStimulus singleStimulus) {
		// write header file
		outputBuffer.appendTo("INC", it.module, toH(singleStimulus))
		// write implementation file
		outputBuffer.appendTo("SRC", it.module, toCpp(singleStimulus))
	}

	private def String toH(SingleStimulus singleStimulus) '''
		// This code was generated for simulation with app4mc.sim
			
		#pragma once
		#include "Common.h"
		#include "Stimuli/Stimuli.h"
		
		//SingleStimulus runnableA----
		std::shared_ptr<SingleStimulus> «getFunctionDef(singleStimulus)»;
	'''

	private def String toCpp(SingleStimulus singleStimulus) '''
		// This code was generated for simulation with app4mc.sim
			
		#include "../../«getModulePath(singleStimulus)».h"
		«val conditionContent = conditionTransformer.transformCondition(
			singleStimulus.executionCondition, getName(singleStimulus), true)»
		«ConditionBuffer.getConditionIncludes(conditionContent)»
		
		std::shared_ptr<SingleStimulus> «singleStimulus.name» = nullptr;
		
		std::shared_ptr<SingleStimulus>  «getFunctionDef(singleStimulus)» {
		
			if («singleStimulus.name» == nullptr) {
				//initialize
				«singleStimulus.name» = std::make_shared<SingleStimulus>("«singleStimulus.name»",
																			 «TimeUtil.transform(singleStimulus.occurrence)»);
			«ConditionBuffer.getConditionSource(conditionContent)»
			}
			return «singleStimulus.name»;
		}
		
	'''

	def getCache() { return this._createCache_transform }


}
