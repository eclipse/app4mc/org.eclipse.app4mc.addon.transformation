/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.ChannelAccess
import org.eclipse.app4mc.amalthea.model.ChannelReceive
import org.eclipse.app4mc.amalthea.model.ChannelSend
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer

class ChannelAccessTransformer extends BaseTransformer {

	@Inject ChannelTransformer channelTransformer

	def void transform(ChannelAccess _access, AgiContainerBuffer content) {
		val tu = channelTransformer.transform(_access.data);
		content.addInclude(tu.module)
		if (_access instanceof ChannelSend)
			content.addAsActivityGraphItem(transformSend(tu.call, _access.elements))
		else if (_access instanceof ChannelReceive)
			content.addAsActivityGraphItem(transformReceive(tu.call, _access.elements))
	}

	private def String transformReceive(String call, int elements) '''
	<ChannelRead>({«call», «elements»})'''

	private def String transformSend(String call, int elements) '''
	<ChannelWrite>({«call», «elements»})'''

}
