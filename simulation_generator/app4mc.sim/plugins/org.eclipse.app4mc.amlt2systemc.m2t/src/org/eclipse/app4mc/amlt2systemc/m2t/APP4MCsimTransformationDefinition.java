/**
 ********************************************************************************
 * Copyright (c) 2020-2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys;
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys.TraceType;
import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.osgi.service.component.annotations.Component;

@Component
public class APP4MCsimTransformationDefinition implements TransformationDefinition {

	@Override
	public String getName() {
		return "Amalthea to APP4MC.sim project transformation";
	}

	@Override
	public String getDescription() {
		return "Transformation definition for converting an Amalthea model to an APP4MC.sim project";
	}

	@Override
	public String getM2MKey() {
		return null;
	}

	@Override
	public String getM2TKey() {
		return "APP4MCSIM";
	}

	@Override
	public List<TransformationParameter> getTransformationParameter() {

		List<TransformationParameter> transformationParameters = new ArrayList<>();

		transformationParameters.addAll(List.of(
				new TransformationParameter(PropertyKeys.TRANSFORMATION_PARAMETERGROUP_NAME_APP4MCSIM_SETTINGS,
						PropertyKeys.APP4MCSIM_FOLDER, "APP4MC.sim folder", File.class),
				new TransformationParameter(PropertyKeys.TRANSFORMATION_PARAMETERGROUP_NAME_APP4MCSIM_SETTINGS,
						PropertyKeys.SIM_DURATION_IN_MS, "Simulation duration [ms]", String.class),
				new TransformationParameter(PropertyKeys.TRANSFORMATION_PARAMETERGROUP_NAME_APP4MCSIM_SETTINGS,
						PropertyKeys.TRACE_FOLDER, "Trace folder", File.class)));
		
		// adding a check box for each trace type
		transformationParameters.addAll(Arrays.stream(TraceType.values())
				.map(traceType -> new TransformationParameter(
						PropertyKeys.TRANSFORMATION_PARAMETERGROUP_NAME_APP4MCSIM_SETTINGS, traceType.toString(),
						traceType.getName(), Boolean.class))
				.collect(Collectors.toList()));

		return transformationParameters;
	}

}
