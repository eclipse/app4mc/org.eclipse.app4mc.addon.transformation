/**
 ********************************************************************************
 * Copyright (c) 2021-2023 Uni Rostock and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Uni Rostock - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit;

public class TuSort {

	// Suppress default constructor
	private TuSort() {
		throw new IllegalStateException("Utility class");
	}

	public static List<TranslationUnit> byModule(Collection<TranslationUnit> values) {
		List<TranslationUnit> list = new ArrayList<>(values);
		Comparator<TranslationUnit> comp = (tu1, tu2) -> tu1.getModule().compareTo(tu2.getModule());
		Collections.sort(list, comp);
		return list;
	}

	public static List<TranslationUnit> byCall(Collection<TranslationUnit> values) {
		List<TranslationUnit> list = new ArrayList<>(values);
		Comparator<TranslationUnit> comp = (tu1, tu2) -> tu1.getCall().compareTo(tu2.getCall());
		Collections.sort(list, comp);
		return list;
	}

}
