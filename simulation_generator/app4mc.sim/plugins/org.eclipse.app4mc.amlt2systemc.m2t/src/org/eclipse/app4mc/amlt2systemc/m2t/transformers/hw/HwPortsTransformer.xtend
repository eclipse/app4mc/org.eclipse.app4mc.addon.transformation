 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import java.util.Collection
import org.eclipse.app4mc.amalthea.model.HwPort
import org.eclipse.app4mc.amalthea.model.PortType

class HwPortsTransformer {
	
	static def getName (HwPort port){
		return port.qualifiedName.replace(".", "__");
	}

	static def initialize(String parentName, Collection<HwPort> ports) '''
		«FOR port : ports»
			«val fullname = getName(port)»
			«IF port.portType === PortType.INITIATOR »
				«parentName»->addInitPort("«fullname»");
			«ELSEIF port.portType === PortType.RESPONDER»
				«parentName»->addTargetPort("«fullname»");
			«ELSE»
				//port type not set for port «port.qualifiedName» on module «parentName»
			«ENDIF»
		«ENDFOR»
	'''

}