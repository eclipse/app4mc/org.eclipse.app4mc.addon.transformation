/**
 * *******************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer
import org.eclipse.app4mc.amalthea.model.SwitchDefault
import org.eclipse.app4mc.amalthea.model.SwitchEntry
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.common.ConditionTransformer

/** Target code example
 * 
	SwitchEntry caseA("case A");
	caseA.addCondition(modelabel1, lita);
	caseA.addActivityGraphItem<ModeLabelAccess>({modelabel1, litb});
	caseA.addActivityGraphItem<RunnableCall>({ra});
	* 
	ConditionConjunction conj_caseDandD;// = caseDandD.createConjunction();
	conj_caseDandD.addCondition(modelabel1, litd);
	conj_caseDandD.addCondition(modelabel2, litd)
	* 
	usage in Switch:
	switch.addEntry(caseA); OR switch.setDefaultEntry(caseA);
	
 */

class SwitchEntryTransformer extends IActivityGraphItemContainerTransformer {
	
	@Inject ConditionTransformer conditionTransformer
	
	static var defaultCnt = 0;
	
	private static def getName(SwitchEntry switchEntry){
		return switchEntry.name;
	}
	
	private static def getName(SwitchDefault switchDefault){
		return "switchDefault" + defaultCnt
	}

	private def void transformContainer(SwitchEntry switchEntry, AgiContainerBuffer content) {
		val name =  getName(switchEntry)
		content.addBody("SwitchEntry " + name + "(\"" + name + "\");\n")
		val conditioContents = conditionTransformer.transformCondition(switchEntry.condition, name, false);
		val activityGraphContents = transformItems(switchEntry, name, content.module, content.parentIsPointer)
		activityGraphContents.addAll(conditioContents) //merge condition and activity graph content before adding
		content.getIncludes().addAll(activityGraphContents.getIncludes())
		content.getSource().addAll(activityGraphContents.getSource())
		content.addBody(content.instanceName + ".addEntry(" + name  + ");")
	}
	
	private def void transformContainer(SwitchDefault switchDefault, AgiContainerBuffer content) {
		val name =  getName(switchDefault)
		content.addBody("SwitchEntry " + name + "(\"" + name + "\");\n")
		val activityGraphContents = transformItems(switchDefault, name, content.module, content.parentIsPointer)
		content.getIncludes().addAll(activityGraphContents.getIncludes())
		content.getSource().addAll(activityGraphContents.getSource())
		content.addBody(content.instanceName + ".setDefaultEntry(" + name  + ");")
		org.eclipse.app4mc.amlt2systemc.m2t.transformers.sw.SwitchEntryTransformer.defaultCnt++;
	}
	
	
	override protected transformContainer(IActivityGraphItemContainer container, AgiContainerBuffer parentContent) {
		if(container instanceof SwitchEntry){
			transformContainer(container as SwitchEntry, parentContent)
		} else if(container instanceof SwitchDefault){
			transformContainer(container as SwitchDefault, parentContent)
		} else {
			throw new UnsupportedOperationException("Container type not supported")	
		}
	}
	
	override protected getContainerDescription(IActivityGraphItemContainer container) {
		if(container instanceof SwitchEntry){
			"SwitchEntry: " + getName(container as SwitchEntry)
		} else if(container instanceof SwitchDefault){
			"SwitchDefault: " + getName(container as SwitchDefault)
		} else {
			throw new UnsupportedOperationException("Container type not supported")	
		}
	}
	
}