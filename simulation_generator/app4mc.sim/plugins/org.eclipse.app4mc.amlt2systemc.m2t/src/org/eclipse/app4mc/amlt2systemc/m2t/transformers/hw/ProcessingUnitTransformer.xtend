 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class ProcessingUnitTransformer extends HwModuleTransformer {

	@Inject OutputBuffer outputBuffer;
	@Inject FrequencyDomainTransformer frequencyDomainTransformer
	@Inject ProcessingUnitDefinitionTransformer processingUnitDefinitionTransformer
	@Inject HwAccessElementTransformer hwAccessElementTransformer

	def create tu : new TranslationUnit(getModulePath(pu), getCall(pu)) transform(ProcessingUnit pu) {
		//sanity check
		if (pu.definition === null)
			throw new RuntimeException("Processing unit " + pu.qualifiedName + " is not associated with a processing unit definition.")
		if (pu.frequencyDomain === null)
			throw new RuntimeException("Processing unit " + pu.qualifiedName + " is not associated with a frequency domain.")
		
		// tu.includeFile = getIncludeFile(pu)
		outputBuffer.appendTo("INC", tu.module, toH(pu))
		outputBuffer.appendTo("SRC", tu.module, toCpp(pu))
	}

	private def String toH(ProcessingUnit pu) '''
		//framework
		#include "HardwareModel.h"
		std::shared_ptr<ProcessingUnit> «getCall(pu)»();
	'''

	private def String toCpp(ProcessingUnit pu) '''	
		#include "«getModulePath(pu)».h"
		«getParentInclude(pu)»
		
		«IF (pu.frequencyDomain !== null)»
			#include "«frequencyDomainTransformer.transform(pu.frequencyDomain).module».h"
		«ENDIF»
		«IF (pu.definition !== null)»
			#include "«processingUnitDefinitionTransformer.transform(pu.definition).module».h"
		«ENDIF»
		«FOR accessElement : pu.accessElements»
			#include "«hwAccessElementTransformer.transform(accessElement).module».h"
		«ENDFOR»
		«getIncludesForConnections(pu)»
		
		«val name = getName(pu)»
		std::shared_ptr<ProcessingUnit> «getName(pu)» = nullptr;
		//for usage in structures
		std::shared_ptr<ProcessingUnit> «getCall(pu)»(){
			auto parent = «getParentGetter(pu)»();
			if («getName(pu)»==NULL){
				«getName(pu)» = parent->createModule<ProcessingUnit>("«name»", *«processingUnitDefinitionTransformer.transform(pu.definition).call»);
				«getName(pu)»->clock_period= «frequencyDomainTransformer.transform(pu.frequencyDomain).call»;
				//ports & connections
				«setPortsAndConnections(pu)»
				//access elements
				«FOR accessElement : pu.accessElements»
					«getName(pu)»->addHWAccessElement(«hwAccessElementTransformer.transform(accessElement).call»());
				«ENDFOR»
			}
			return «name»;
		}
		
	'''

	def getCache() { return this._createCache_transform }

}
