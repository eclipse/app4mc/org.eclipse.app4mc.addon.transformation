/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;

public class HwUtil {

	// Suppress default constructor
	private HwUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static List<HwStructure> getAllHwStructures(HWModel hwModel) {

		List<HwStructure> results = new ArrayList<>();
		for (HwStructure hwStruct : hwModel.getStructures()) {
			getContainedStructures(hwStruct, results);
			results.add(hwStruct);
		}
		return results;
	}

	private static void getContainedStructures(HwStructure structure, List<HwStructure> results) {

		for (HwStructure hwStruct : structure.getStructures()) {
			results.add(hwStruct);
			getContainedStructures(hwStruct, results);
		}
	}

}
