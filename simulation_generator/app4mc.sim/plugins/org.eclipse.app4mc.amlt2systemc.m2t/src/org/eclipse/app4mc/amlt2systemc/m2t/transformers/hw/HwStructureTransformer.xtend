 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amalthea.model.HwStructure
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.transformation.util.OutputBuffer
import org.eclipse.emf.ecore.EObject

@Singleton
class HwStructureTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject HwModuleTransformer hwModuleTransformer
	@Inject HwConnectionTransformer hwConnectionTransformer

	def static HwStructure getParentHwStructureOrNull(EObject obj) {
		val ret = obj.eContainer
		if (ret instanceof HwStructure) {
			return (ret as HwStructure)
		} else {
			return null;
		}
	}

	private def String getModulePath(HwStructure hwStructure) {
		if (hwStructure !== null) {
			return HwModelTransformer.getModulePath() + "/structures/" + getName(hwStructure);
		} else {
			return HwModelTransformer.getModulePath();
		}
	}

	protected static def String getCall(HwStructure hwStructure) '''
	get_HwStructure_«getName(hwStructure)»'''

	private static def String getName(HwStructure hwStructure) {
		return hwStructure.name // or uniqueName
	}

	def create 	tu: new TranslationUnit(getModulePath(hwStructure), getCall(hwStructure)) transform(
		HwStructure hwStructure) {
		outputBuffer.appendTo("INC", tu.module, toH(hwStructure, tu))
		val containingHwStructure = HwStructureTransformer.getParentHwStructureOrNull(hwStructure)
		if (containingHwStructure !== null) {
			outputBuffer.appendTo("SRC", tu.module, toCpp(hwStructure, tu, this.transform(containingHwStructure)))
		} else {
			outputBuffer.appendTo("SRC", tu.module, toCpp(hwStructure, tu))
		}
	}

	def String toH(HwStructure hwStructure, TranslationUnit tu) '''
		#include <systemc>
		#include <memory>
		#include "Common.h"
		#include "HardwareModel.h"
			
		
		
		std::shared_ptr<HwStructure> «tu.call»();
		
	'''

	// Note: Caches contained in PU's are currently not supported (hwStrucutre.modules does not provide modules contained by other modules)
	def String toCpp(HwStructure hwStructure, TranslationUnit tu) '''
		#include "«tu.module».h"
		«FOR structure : hwStructure.structures»
			#include "«this.transform(structure).module».h"
		«ENDFOR»
		«FOR module : hwStructure.modules»
			#include "«hwModuleTransformer.transform(module).module».h"
		«ENDFOR»		
		«FOR connection : hwStructure.connections»
			#include "«hwConnectionTransformer.transform(connection).module».h"
		«ENDFOR»
		
		
		«val name = getName(hwStructure)»
		std::shared_ptr<HwStructure> «name» = nullptr;
		
		
		//for usage on hw model top level (eg. in flat hierarchies)
		std::shared_ptr<HwStructure> «tu.call»() {
			if («name» == nullptr) {
				//initialize
				«name» = std::make_shared<HwStructure>("«name»");
				«setParams(hwStructure)»
			}
			return «name»;
		}
	'''

	def String toCpp(HwStructure hwStructure, TranslationUnit tu, TranslationUnit parentTu) '''
		#include "«tu.module».h"
		#include "«parentTu.module».h"
		«FOR structure : hwStructure.structures»
			#include "«this.transform(structure).module».h"
		«ENDFOR»
		«FOR module : hwStructure.modules»
			#include "«hwModuleTransformer.transform(module).module».h"
		«ENDFOR»		
		«FOR connection : hwStructure.connections»
			#include "«hwConnectionTransformer.transform(connection).module».h"
		«ENDFOR»
		
		
		«val name = getName(hwStructure)»
		std::shared_ptr<HwStructure> «name» = nullptr;
		//for usage in structures
		std::shared_ptr<HwStructure> «tu.call»() {
			std::shared_ptr<HwStructure> parent = «parentTu.call»();
			if («name» == nullptr) {
				//initialize
				«name» = parent->createSubStructure("«name»");
				«setParams(hwStructure)»
			}
			return «name»;
		}
	'''

	private def setParams(HwStructure hwStructure) '''	
	«««		«val name = getName(hwStructure)»
		//Sub-Structures
		«FOR structure : hwStructure.structures»
		«this.transform(structure).call»();
	«ENDFOR»
	//Sub-Modules
	«FOR module : hwStructure.modules»
		«hwModuleTransformer.transform(module).call»();
	«ENDFOR»
	//HW Ports	
	«HwPortsTransformer.initialize(getCall(hwStructure) + "()", hwStructure.ports)»
	//Connections (Deferred Bindings of delegate ports)
	«««		«FOR connection : hwStructure.connections»
«««			«val port = hwStructure.ports.contains(connection.port1)?connection.port1:connection.port2»
«««			«IF port !== null»
«««				«name»->bindConnectionDelegatedPort(«hwConnectionTransformer.transform(connection).call»(), "«port.qualifiedName»");
«««			«ENDIF»
«««		«ENDFOR»
	'''

	def getCache() { return this._createCache_transform }

}
