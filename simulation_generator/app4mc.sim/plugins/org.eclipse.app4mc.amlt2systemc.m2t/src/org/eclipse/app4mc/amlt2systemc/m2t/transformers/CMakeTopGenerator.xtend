 /**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers

import com.google.inject.Inject
import com.google.inject.Singleton
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.module.PropertyKeys
import org.eclipse.app4mc.transformation.util.OutputBuffer

@Singleton
class CMakeTopGenerator extends BaseTransformer {

	@Inject
	OutputBuffer outputBuffer

	def create boolean transform() {
		outputBuffer.appendTo("OTHER", "CMakeLists.txt", getContent())
	}

	private def String getContent() '''	
		# CMakeList.txt: Top level CMake project file.
		# Perform a global configuration and include subprojects.
		#
		cmake_minimum_required (VERSION 3.20)
		
		project («getProperty(PropertyKeys.PROJECT_NAME)»)
		
		# leave redundant settings below in place (visual studio seems to default to c++ 11 if statements below are missing)
		set (CMAKE_CXX_STANDARD 17 CACHE STRING
		     "C++ standard to build all targets.")
		set (CMAKE_CXX_STANDARD_REQUIRED ON CACHE BOOL
		     "The with CMAKE_CXX_STANDARD selected C++ standard is a requirement.")
		mark_as_advanced (CMAKE_CXX_STANDARD_REQUIRED)

		«val app4mc_loc= getProperty(PropertyKeys.APP4MCSIM_FOLDER, "$ENV{APP4MCSIM_LOC}").replace("\\", "//")»
		set(APP4MCSIM_LOC «app4mc_loc» CACHE PATH "Path for App4mc.sim library")
		set(MODEL_WORKING_DIR "." CACHE PATH "Working directory for build system based execution")
		
		if(NOT EXISTS ${APP4MCSIM_LOC})
			message(FATAL_ERROR "APP4MCSIM_LOC needs to be configured (either as CMakeParameter or environment variable) with app4mc.sim source location")
		endif()
		
		# include subprojects
		add_subdirectory (${APP4MCSIM_LOC} "./app4mc.sim" EXCLUDE_FROM_ALL)
		add_subdirectory(./«AmaltheaTransformer.getModulePath()»)
	'''

}
