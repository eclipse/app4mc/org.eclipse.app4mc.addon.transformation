/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.transformers.mapping

import com.google.inject.Inject
import org.eclipse.app4mc.amalthea.model.HwModule
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amlt2systemc.m2t.module.BaseTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.TranslationUnit
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.hw.ProcessingUnitTransformer
import org.eclipse.app4mc.amlt2systemc.m2t.transformers.os.TaskSchedulerTransformer
import org.eclipse.app4mc.transformation.util.OutputBuffer

class SchedulerAllocationTransformer extends BaseTransformer {

	@Inject OutputBuffer outputBuffer
	@Inject ProcessingUnitTransformer processingUnitTransformer
	@Inject TaskSchedulerTransformer taskSchedulerTransformer

	private def String getModulePath() {
		return MappingModelTransformer.getModulePath() + "/schedulerAllocation"
	}

	private def String getModuleName(SchedulerAllocation schedulerAllocation) '''
		«IF schedulerAllocation.executingPU === null»
			«getModulePath()»/schedulerAllocation_«schedulerAllocation.scheduler.name»_«schedulerAllocation.responsibility.get(0).name
		»«ELSE»
			«getModulePath()»/schedulerAllocation_«schedulerAllocation.scheduler.name»_«schedulerAllocation.executingPU.name
		»«ENDIF»'''

	private def String getCall(SchedulerAllocation schedulerAllocation) '''
		«IF schedulerAllocation.scheduler instanceof TaskScheduler»
			init_SchedulerAllocation_«taskSchedulerTransformer.getName(schedulerAllocation.scheduler as TaskScheduler)»_«processingUnitTransformer.getName(schedulerAllocation.executingPU as HwModule)
			»«ELSE»
			/*Scheduler allocation for Interrupt Controller not yet implemented*/
		«ENDIF»
	'''

	def create new TranslationUnit(getModuleName(schedulerAllocation),getCall(schedulerAllocation))	
		transform(SchedulerAllocation schedulerAllocation) {
		outputBuffer.appendTo("INC", it.module, toH(it))
		outputBuffer.appendTo("SRC", it.module, toCpp(schedulerAllocation))
	}

	private def String toH(TranslationUnit tu) '''
		void «tu.call»();		
	'''

	private def String toCpp(SchedulerAllocation schedulerAllocation) '''
		«IF schedulerAllocation.scheduler instanceof TaskScheduler»
			«val tuScheduler = taskSchedulerTransformer.transform(schedulerAllocation.scheduler as TaskScheduler)»
			«val tuPu = processingUnitTransformer.transform(schedulerAllocation.executingPU as ProcessingUnit)»
			#include "«tuScheduler.module».h"
			#include "«tuPu.module».h"
			«FOR responsiblePU : schedulerAllocation.responsibility»
				«val tuResponsiblePU = processingUnitTransformer.transform(responsiblePU as ProcessingUnit)»
				#include "«tuResponsiblePU.module».h"
			«ENDFOR»
			
			void «getCall(schedulerAllocation)»(){
				«tuScheduler.call»()->setExecutionCore(«tuPu.call»());
				«FOR responsiblePU : schedulerAllocation.responsibility»
					«val tuResponsiblePU = processingUnitTransformer.transform(responsiblePU as ProcessingUnit)»
					«tuScheduler.call»()->addResponsibleCore(«tuResponsiblePU.call»());
				«ENDFOR»
			}
		«ELSE»
			/*Scheduler allocation for Interrupt Controller not yet implemented*/
		«ENDIF»
	'''

	def getCache() { return this._createCache_transform }

}
