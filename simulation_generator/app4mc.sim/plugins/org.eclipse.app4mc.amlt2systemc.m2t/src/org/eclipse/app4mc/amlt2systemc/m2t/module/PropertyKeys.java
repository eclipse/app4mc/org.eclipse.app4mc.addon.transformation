/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amlt2systemc.m2t.module;

public class PropertyKeys {

	// Suppress default constructor
	private PropertyKeys() {
		throw new IllegalStateException("Utility class");
	}

	public static final String TRANSFORMATION_PARAMETERGROUP_NAME_APP4MCSIM_SETTINGS = "APP4MC.sim Settings";

	public static final String APP4MCSIM_FOLDER = "app4mcsim_folder";
	public static final String PROJECT_NAME = "project_name";
	public static final String SIM_DURATION_IN_MS = "sim_duration_in_ms";
	public static final String TRACE_FOLDER = "trace_folder";
	public static final String TRACE_TYPES = "trace_types";

	public static enum TraceType {
		BTF("BtfTracer", "1"),
		BTF_AGI("BtfTracerWithActivityGraphItemTracing", "2"),
		BTF_AGI_BRANCH("BtfTracerWithActivityGraphItemTracingHierarchy", "3"),
		BTF_SIGNAL("BtfTracerWithSignalTracing", "4");

		private final String name;
		private final String id;

		TraceType(String name, String id) {
			this.name = name;
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

	}

}
