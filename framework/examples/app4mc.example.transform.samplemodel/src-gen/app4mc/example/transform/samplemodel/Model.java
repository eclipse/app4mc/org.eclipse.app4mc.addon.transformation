/**
 * *******************************************************************************
 *  Copyright (c) 2018-2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package app4mc.example.transform.samplemodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link app4mc.example.transform.samplemodel.Model#getRunnables <em>Runnables</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.Model#getLabels <em>Labels</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.Model#getMemories <em>Memories</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.Model#getSchedulers <em>Schedulers</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.Model#getCaches <em>Caches</em>}</li>
 * </ul>
 *
 * @see app4mc.example.transform.samplemodel.SampleModelPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject {
	/**
	 * Returns the value of the '<em><b>Runnables</b></em>' containment reference list.
	 * The list contents are of type {@link app4mc.example.transform.samplemodel.Runnable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runnables</em>' containment reference list.
	 * @see app4mc.example.transform.samplemodel.SampleModelPackage#getModel_Runnables()
	 * @model containment="true"
	 * @generated
	 */
	EList<app4mc.example.transform.samplemodel.Runnable> getRunnables();

	/**
	 * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
	 * The list contents are of type {@link app4mc.example.transform.samplemodel.Label}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Labels</em>' containment reference list.
	 * @see app4mc.example.transform.samplemodel.SampleModelPackage#getModel_Labels()
	 * @model containment="true"
	 * @generated
	 */
	EList<Label> getLabels();

	/**
	 * Returns the value of the '<em><b>Memories</b></em>' containment reference list.
	 * The list contents are of type {@link app4mc.example.transform.samplemodel.Memory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memories</em>' containment reference list.
	 * @see app4mc.example.transform.samplemodel.SampleModelPackage#getModel_Memories()
	 * @model containment="true"
	 * @generated
	 */
	EList<Memory> getMemories();

	/**
	 * Returns the value of the '<em><b>Schedulers</b></em>' containment reference list.
	 * The list contents are of type {@link app4mc.example.transform.samplemodel.Scheduler}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedulers</em>' containment reference list.
	 * @see app4mc.example.transform.samplemodel.SampleModelPackage#getModel_Schedulers()
	 * @model containment="true"
	 * @generated
	 */
	EList<Scheduler> getSchedulers();

	/**
	 * Returns the value of the '<em><b>Caches</b></em>' containment reference list.
	 * The list contents are of type {@link app4mc.example.transform.samplemodel.Cache}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caches</em>' containment reference list.
	 * @see app4mc.example.transform.samplemodel.SampleModelPackage#getModel_Caches()
	 * @model containment="true"
	 * @generated
	 */
	EList<Cache> getCaches();

} // Model
