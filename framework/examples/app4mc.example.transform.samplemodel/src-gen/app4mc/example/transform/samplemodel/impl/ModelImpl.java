/**
 * *******************************************************************************
 *  Copyright (c) 2018-2020 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package app4mc.example.transform.samplemodel.impl;

import app4mc.example.transform.samplemodel.Cache;
import app4mc.example.transform.samplemodel.Label;
import app4mc.example.transform.samplemodel.Memory;
import app4mc.example.transform.samplemodel.Model;
import app4mc.example.transform.samplemodel.SampleModelPackage;
import app4mc.example.transform.samplemodel.Scheduler;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link app4mc.example.transform.samplemodel.impl.ModelImpl#getRunnables <em>Runnables</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.impl.ModelImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.impl.ModelImpl#getMemories <em>Memories</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.impl.ModelImpl#getSchedulers <em>Schedulers</em>}</li>
 *   <li>{@link app4mc.example.transform.samplemodel.impl.ModelImpl#getCaches <em>Caches</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model {
	/**
	 * The cached value of the '{@link #getRunnables() <em>Runnables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunnables()
	 * @generated
	 * @ordered
	 */
	protected EList<app4mc.example.transform.samplemodel.Runnable> runnables;

	/**
	 * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabels()
	 * @generated
	 * @ordered
	 */
	protected EList<Label> labels;

	/**
	 * The cached value of the '{@link #getMemories() <em>Memories</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemories()
	 * @generated
	 * @ordered
	 */
	protected EList<Memory> memories;

	/**
	 * The cached value of the '{@link #getSchedulers() <em>Schedulers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulers()
	 * @generated
	 * @ordered
	 */
	protected EList<Scheduler> schedulers;

	/**
	 * The cached value of the '{@link #getCaches() <em>Caches</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaches()
	 * @generated
	 * @ordered
	 */
	protected EList<Cache> caches;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SampleModelPackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<app4mc.example.transform.samplemodel.Runnable> getRunnables() {
		if (runnables == null) {
			runnables = new EObjectContainmentEList<app4mc.example.transform.samplemodel.Runnable>(app4mc.example.transform.samplemodel.Runnable.class, this, SampleModelPackage.MODEL__RUNNABLES);
		}
		return runnables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Label> getLabels() {
		if (labels == null) {
			labels = new EObjectContainmentEList<Label>(Label.class, this, SampleModelPackage.MODEL__LABELS);
		}
		return labels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Memory> getMemories() {
		if (memories == null) {
			memories = new EObjectContainmentEList<Memory>(Memory.class, this, SampleModelPackage.MODEL__MEMORIES);
		}
		return memories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Scheduler> getSchedulers() {
		if (schedulers == null) {
			schedulers = new EObjectContainmentEList<Scheduler>(Scheduler.class, this, SampleModelPackage.MODEL__SCHEDULERS);
		}
		return schedulers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Cache> getCaches() {
		if (caches == null) {
			caches = new EObjectContainmentEList<Cache>(Cache.class, this, SampleModelPackage.MODEL__CACHES);
		}
		return caches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SampleModelPackage.MODEL__RUNNABLES:
				return ((InternalEList<?>)getRunnables()).basicRemove(otherEnd, msgs);
			case SampleModelPackage.MODEL__LABELS:
				return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
			case SampleModelPackage.MODEL__MEMORIES:
				return ((InternalEList<?>)getMemories()).basicRemove(otherEnd, msgs);
			case SampleModelPackage.MODEL__SCHEDULERS:
				return ((InternalEList<?>)getSchedulers()).basicRemove(otherEnd, msgs);
			case SampleModelPackage.MODEL__CACHES:
				return ((InternalEList<?>)getCaches()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SampleModelPackage.MODEL__RUNNABLES:
				return getRunnables();
			case SampleModelPackage.MODEL__LABELS:
				return getLabels();
			case SampleModelPackage.MODEL__MEMORIES:
				return getMemories();
			case SampleModelPackage.MODEL__SCHEDULERS:
				return getSchedulers();
			case SampleModelPackage.MODEL__CACHES:
				return getCaches();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SampleModelPackage.MODEL__RUNNABLES:
				getRunnables().clear();
				getRunnables().addAll((Collection<? extends app4mc.example.transform.samplemodel.Runnable>)newValue);
				return;
			case SampleModelPackage.MODEL__LABELS:
				getLabels().clear();
				getLabels().addAll((Collection<? extends Label>)newValue);
				return;
			case SampleModelPackage.MODEL__MEMORIES:
				getMemories().clear();
				getMemories().addAll((Collection<? extends Memory>)newValue);
				return;
			case SampleModelPackage.MODEL__SCHEDULERS:
				getSchedulers().clear();
				getSchedulers().addAll((Collection<? extends Scheduler>)newValue);
				return;
			case SampleModelPackage.MODEL__CACHES:
				getCaches().clear();
				getCaches().addAll((Collection<? extends Cache>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SampleModelPackage.MODEL__RUNNABLES:
				getRunnables().clear();
				return;
			case SampleModelPackage.MODEL__LABELS:
				getLabels().clear();
				return;
			case SampleModelPackage.MODEL__MEMORIES:
				getMemories().clear();
				return;
			case SampleModelPackage.MODEL__SCHEDULERS:
				getSchedulers().clear();
				return;
			case SampleModelPackage.MODEL__CACHES:
				getCaches().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SampleModelPackage.MODEL__RUNNABLES:
				return runnables != null && !runnables.isEmpty();
			case SampleModelPackage.MODEL__LABELS:
				return labels != null && !labels.isEmpty();
			case SampleModelPackage.MODEL__MEMORIES:
				return memories != null && !memories.isEmpty();
			case SampleModelPackage.MODEL__SCHEDULERS:
				return schedulers != null && !schedulers.isEmpty();
			case SampleModelPackage.MODEL__CACHES:
				return caches != null && !caches.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ModelImpl
