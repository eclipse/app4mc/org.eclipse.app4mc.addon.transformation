/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2t.cust.module;

import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.osgi.service.component.annotations.Component;

import app4mc.example.transform.m2t.transformers.ExampleGuiceModuleFactory;

@Component(
	property = { "service.ranking:Integer=10" }, 
	service= ExampleGuiceModuleFactory.class
)
public class CustExampleGuiceModuleFactory extends ExampleGuiceModuleFactory {
	
	@Override
	public CustExampleGuiceModule getModule(SessionLogger logger) {
		return new CustExampleGuiceModule(logger);
	}

}
