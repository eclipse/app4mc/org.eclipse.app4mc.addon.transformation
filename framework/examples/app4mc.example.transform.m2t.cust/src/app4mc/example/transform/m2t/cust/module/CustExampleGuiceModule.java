/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2t.cust.module;

import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import app4mc.example.transform.m2t.cust.transformers.CustTransformer;
import app4mc.example.transform.m2t.transformers.ExampleGuiceModule;
import app4mc.example.transform.m2t.transformers.ExampleTransformer;

public class CustExampleGuiceModule extends ExampleGuiceModule {

	public CustExampleGuiceModule(SessionLogger logger) {
		super(logger);
	}

	@Override
	protected void configure() {
		super.configure();
		bind(ExampleTransformer.class).to(CustTransformer.class);
	}

}
