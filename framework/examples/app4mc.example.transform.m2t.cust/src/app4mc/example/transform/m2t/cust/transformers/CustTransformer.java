/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2t.cust.transformers;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.transformation.util.OutputBuffer;

import com.google.inject.Singleton;

import app4mc.example.transform.m2t.transformers.ExampleTransformer;

@Singleton
public class CustTransformer extends ExampleTransformer {
	
	@Override
	public void generateOutput1(final Amalthea amalthea, final OutputBuffer outputBuffer) {
		logger.info(" - executing CustTransformer.generateOutput1");
		outputBuffer.appendTo("TEXT", "1", "Customer template\n");

		super.generateOutput1(amalthea, outputBuffer);
	}

	@Override
	public void generateOutput2(final Amalthea amalthea, final OutputBuffer outputBuffer) {
		logger.info(" - executing CustTransformer.generateOutput2");
		outputBuffer.appendTo("TEXT", "2", "Customer template\n");

		super.generateOutput2(amalthea, outputBuffer);
	}

}
