/**
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package app4mc.example.transform.m2t.generators;

import java.util.List;
import java.util.function.Function;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ExampleGenerator {
  private ExampleGenerator() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Creates output with a "template only" style (like Xpand/Xtend).
   */
  public static String generateOutput1(final Amalthea amalthea) {
    StringConcatenation _builder = new StringConcatenation();
    SWModel swModel = amalthea.getSwModel();
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("// generating info about tasks and runnables (in execution order)");
    _builder.newLine();
    _builder.newLine();
    {
      EList<Task> _tasks = swModel.getTasks();
      for(final Task task : _tasks) {
        _builder.append("-----------------------------------------------");
        _builder.newLine();
        _builder.append("Task: ");
        String _name = task.getName();
        _builder.append(_name);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("\t");
        EList<ActivityGraphItem> graphEntries = task.getActivityGraph().getItems();
        _builder.newLineIfNotEmpty();
        {
          for(final ActivityGraphItem graphEntry : graphEntries) {
            {
              if ((graphEntry instanceof RunnableCall)) {
                _builder.append("\t");
                _builder.append("Associated Runnable: ");
                String _name_1 = ((RunnableCall) graphEntry).getRunnable().getName();
                _builder.append(_name_1, "\t");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
      }
    }
    _builder.append("-----------------------------------------------");
    _builder.newLine();
    return _builder.toString();
  }

  /**
   * Creates output with a combination of template and functions.
   * This allows a more flexible use of utility functions and lambdas.
   */
  public static String generateOutput2(final Amalthea amalthea) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("// generating info about tasks and runnables (in alphabetic order)");
    _builder.newLine();
    _builder.newLine();
    {
      EList<Task> _tasks = ModelUtil.getOrCreateSwModel(amalthea).getTasks();
      for(final Task task : _tasks) {
        _builder.append("-----------------------------------------------");
        _builder.newLine();
        _builder.append("Task: ");
        String _name = task.getName();
        _builder.append(_name);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        {
          List<String> _runnableNamesOf = ExampleGenerator.runnableNamesOf(task);
          for(final String name : _runnableNamesOf) {
            _builder.append("\t");
            _builder.append("Associated Runnable: ");
            _builder.append(name, "\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("-----------------------------------------------");
    _builder.newLine();
    return _builder.toString();
  }

  public static List<String> runnableNamesOf(final Task task) {
    final Function<ActivityGraphItem, Boolean> _function = (ActivityGraphItem e) -> {
      return Boolean.valueOf((e instanceof RunnableCall));
    };
    final Function1<ActivityGraphItem, String> _function_1 = (ActivityGraphItem e) -> {
      return ((RunnableCall) e).getRunnable().getName();
    };
    return IterableExtensions.<String>sort(ListExtensions.<ActivityGraphItem, String>map(SoftwareUtil.collectActivityGraphItems(task.getActivityGraph(), null, _function), _function_1));
  }
}
