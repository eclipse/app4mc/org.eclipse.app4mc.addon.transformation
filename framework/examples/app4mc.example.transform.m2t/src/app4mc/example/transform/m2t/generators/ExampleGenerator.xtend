/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package app4mc.example.transform.m2t.generators

import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.RunnableCall
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.util.ModelUtil
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil

class ExampleGenerator {

	// Suppress default constructor
	private new() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Creates output with a "template only" style (like Xpand/Xtend).
	 */
	static def String generateOutput1(Amalthea amalthea) '''
		«var swModel=amalthea.swModel»
		
		// generating info about tasks and runnables (in execution order)
		
		«FOR task : swModel.tasks»
			-----------------------------------------------
			Task: «task.name»
			
				«var graphEntries=task.activityGraph.items»
				«FOR graphEntry: graphEntries»
					«IF graphEntry instanceof RunnableCall»
						Associated Runnable: «(graphEntry as  RunnableCall).runnable.name»
					«ENDIF»
				«ENDFOR»
		«ENDFOR»
		-----------------------------------------------
	'''

	/**
	 * Creates output with a combination of template and functions.
	 * This allows a more flexible use of utility functions and lambdas.
	 */
	static def String generateOutput2(Amalthea amalthea) '''
		
		// generating info about tasks and runnables (in alphabetic order)
		
		«FOR task : ModelUtil.getOrCreateSwModel(amalthea).tasks»
			-----------------------------------------------
			Task: «task.name»
			
				«FOR name: runnableNamesOf(task)»
					Associated Runnable: «name»
				«ENDFOR»
		«ENDFOR»
		-----------------------------------------------
	'''

	static def runnableNamesOf(Task task) {
		SoftwareUtil.collectActivityGraphItems(task.activityGraph, null, [e|e instanceof RunnableCall]).map [ e |
			(e as RunnableCall).runnable.name
		].sort
	}

}
