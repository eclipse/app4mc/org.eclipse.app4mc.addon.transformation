/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package app4mc.example.transform.m2t.transformers;

import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.osgi.service.component.annotations.Component;

@Component(service = ExampleGuiceModuleFactory.class)
public class ExampleGuiceModuleFactory {
	
	public ExampleGuiceModule getModule(SessionLogger logger) {
		return new ExampleGuiceModule(logger);
	}

}
