/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2t.transformers;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import app4mc.example.transform.m2t.generators.ExampleGenerator;

@Singleton
public class ExampleTransformer {

	@Inject
	protected SessionLogger logger;
	
	public void generate(Amalthea model, OutputBuffer outputBuffer) {

		generateOutput1(model, outputBuffer);
		generateOutput2(model, outputBuffer);

		logger.info("Script file generated at : {0}", outputBuffer.getOutputFolder());
	}

	protected void generateOutput1(Amalthea model, OutputBuffer outputBuffer) {
		logger.info(" - executing ExampleTransformer.generateOutput1");
		outputBuffer.appendTo("TEXT", "1", ExampleGenerator.generateOutput1(model));
	}

	protected void generateOutput2(Amalthea model, OutputBuffer outputBuffer) {
		logger.info(" - executing ExampleTransformer.generateOutput2");
		outputBuffer.appendTo("TEXT", "2", ExampleGenerator.generateOutput2(model));
	}

}
