/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2t;

import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.transformers.Model2TextRootTransformer;
import org.eclipse.app4mc.transformation.util.OutputBuffer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.google.inject.Guice;
import com.google.inject.Injector;

import app4mc.example.transform.m2t.transformers.ExampleGuiceModuleFactory;
import app4mc.example.transform.m2t.transformers.ExampleTransformer;

@Component(
	configurationPid = ServiceConstants.SESSION_CONFIGURATION_PID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	property = { ServiceConstants.TRANSFORMATION_PROPERTY + "=Amalthea2Text" },
	service = Model2TextRootTransformer.class
)
public class ExampleModel2TextTransformer extends Model2TextRootTransformer {

	@Reference
	ExampleGuiceModuleFactory factory;

	@Activate
	@Override
	protected void activate(Map<String, ?> properties) {
		super.activate(properties);
	}

	@Override
	public void m2tTransformation() {
		Injector injector = Guice.createInjector(factory.getModule(logger));
		
		ExampleTransformer exampleTransformer = injector.getInstance(ExampleTransformer.class);
		
		OutputBuffer outputBuffer = injector.getInstance(OutputBuffer.class);
		
		outputBuffer.initialize(getOutputFolder());
		outputBuffer.configureFiletype("TEXT", ".txt", "**************** BEGIN ***************\n\n", "***************** END ****************");
		
		for (final Resource resource : getInputResourceSet(getInputFolder(), logger).getResources()) {
			for (final EObject model : resource.getContents()) {
				logger.info("Processing file : {0}", resource.getURI());

				if (model instanceof Amalthea) {
					exampleTransformer.generate((Amalthea) model, outputBuffer);
				}
			}
		}

		outputBuffer.finish(false, false);
	}

}
