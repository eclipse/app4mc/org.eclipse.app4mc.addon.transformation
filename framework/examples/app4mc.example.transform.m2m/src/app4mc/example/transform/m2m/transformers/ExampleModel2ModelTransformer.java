/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2m.transformers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.transformers.Model2ModelRootTransformer;
import org.eclipse.app4mc.transformation.util.TransformationGuiceModule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

import com.google.inject.Guice;
import com.google.inject.Injector;

import app4mc.example.transform.m2m.transformers.hw.ExampleHwTransformer;
import app4mc.example.transform.samplemodel.Model;
import app4mc.example.transform.samplemodel.SampleModelFactory;
import app4mc.example.transform.samplemodel.SampleModelPackage;

@Component(
	configurationPid = ServiceConstants.SESSION_CONFIGURATION_PID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	property = { ServiceConstants.TRANSFORMATION_PROPERTY + "=Amalthea2SampleModel" },
	service = Model2ModelRootTransformer.class
)
public class ExampleModel2ModelTransformer extends Model2ModelRootTransformer {

	/**
	 * - Factory initialization
	 */
	private static final SampleModelFactory outputModelFactory = SampleModelFactory.eINSTANCE;
	
	private final HashMap<ArrayList<?>, Model> transformCache = CollectionLiterals.newHashMap();
	
	@Activate
	@Override
	protected void activate(final Map<String, ?> properties) {
		super.activate(properties);
	}

	@Override
	public ResourceSet getOutputResourceSet() {

		ResourceSet outputResourceSet = new ResourceSetImpl();

		outputResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

		outputResourceSet.getPackageRegistry().put(SampleModelPackage.eNS_URI, SampleModelPackage.eINSTANCE);

		return outputResourceSet;
	}

	@Override
	public void m2mTransformation() {
		try {
			Injector injector = Guice.createInjector(new TransformationGuiceModule(logger));

			ExampleHwTransformer hardwareTransformer = injector.getInstance(ExampleHwTransformer.class);
			
			int fileIndex = 1;
			for (final Resource inputResource : getInputResourceSet(getInputFolder(), logger).getResources()) {
				for (final EObject content : inputResource.getContents()) {

					logger.info("Processing file : {0}", inputResource.getURI());

					final Model simulationModelRoot = transform((Amalthea) content, hardwareTransformer);
					URI uri = URI.createFileURI(getOutputFolder() + File.separator + Integer.valueOf(fileIndex++) + ".root");
					final Resource outputResource = getOutputResourceSet().createResource(uri);
					outputResource.getContents().add(simulationModelRoot);
					outputResource.save(null);

					logger.info("Transformed model file generated at : {0}", uri);
				}
			}
		} catch (IOException e) {
			throw Exceptions.sneakyThrow(e);
		}
	}

	public Model transform(Amalthea amalthea, ExampleHwTransformer hardwareTransformer) {
		final ArrayList<?> transformKey = CollectionLiterals.newArrayList(amalthea);
		final Model sampleModel;
		synchronized (transformCache) {
			if (transformCache.containsKey(transformKey)) {
				return transformCache.get(transformKey);
			}
			sampleModel = outputModelFactory.createModel();
			transformCache.put(transformKey, sampleModel);
		}
		doTransform(sampleModel, amalthea, hardwareTransformer);
		return sampleModel;
	}

	private void doTransform(Model sampleModel, Amalthea amalthea, ExampleHwTransformer hardwareTransformer) {
		HWModel hwModel = amalthea.getHwModel();
		if (hwModel == null) return;

		hardwareTransformer.transform(hwModel, sampleModel);
	}

}
