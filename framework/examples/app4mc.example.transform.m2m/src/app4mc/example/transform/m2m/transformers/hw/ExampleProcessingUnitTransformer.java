/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2m.transformers.hw;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.Cache;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import app4mc.example.transform.samplemodel.Model;

@Singleton
public class ExampleProcessingUnitTransformer {
	
	@Inject
	private SessionLogger logger;

	@Inject
	private ExampleCacheTransformer cacheTransformer;

	public void transfrom(final ProcessingUnit pu, final Model outputModel) {

		for (Cache puSpecificCache : pu.getCaches()) {
			
			final app4mc.example.transform.samplemodel.Cache outputCache = cacheTransformer.transfrom(puSpecificCache);
			outputModel.getCaches().add(outputCache);
		}

		for (HwAccessElement hwAccessElement : pu.getAccessElements()) {

			if (hwAccessElement != null && hwAccessElement.getAccessPath() != null) {
				List<Cache> referredCaches = hwAccessElement.getAccessPath().getPathElements()
						.stream()
						.filter(it -> it instanceof Cache)
						.map(it -> (Cache) it)
						.collect(Collectors.toList());

				for (Cache referredCache : referredCaches) {

						final app4mc.example.transform.samplemodel.Cache outputCache = cacheTransformer.transfrom(referredCache);
						logger.info("Cache referred in path elements is : {0}", outputCache);
				}
			}
		}
	}

}
