/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2m.transformers.hw;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.xtext.xbase.lib.CollectionLiterals;

import com.google.inject.Singleton;

import app4mc.example.transform.samplemodel.Memory;
import app4mc.example.transform.samplemodel.SampleModelFactory;

@Singleton
public class ExampleMemoryTransformer {

	private final HashMap<ArrayList<?>, Memory> transformCache = CollectionLiterals.newHashMap();

	public Memory transfrom(final org.eclipse.app4mc.amalthea.model.Memory amltMemory) {
		final ArrayList<?> transformKey = CollectionLiterals.newArrayList(amltMemory);
		final Memory sampleMemory;
		synchronized (transformCache) {
			if (transformCache.containsKey(transformKey)) {
				return transformCache.get(transformKey);
			}
			sampleMemory = SampleModelFactory.eINSTANCE.createMemory();
			transformCache.put(transformKey, sampleMemory);
		}
		doTransform(sampleMemory, amltMemory);
		return sampleMemory;
	}

	private void doTransform(final Memory sampleMemory, final org.eclipse.app4mc.amalthea.model.Memory amltMemory) {
		sampleMemory.setName(amltMemory.getName());
	}

}
