/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2m.configuration;

import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.osgi.service.component.annotations.Component;

@Component
public class AmaltheaModel2ModelTransformationDefinition implements TransformationDefinition {

	@Override
	public String getName() {
		return "Amalthea to Sample model transformation";
	}

	@Override
	public String getDescription() {
		return "Example that shows the implementation of an Amalthea model to Sample model transformation";
	}

	@Override
	public String getM2MKey() {
		return "Amalthea2SampleModel";
	}

	@Override
	public String getM2TKey() {
		return null;
	}

}
