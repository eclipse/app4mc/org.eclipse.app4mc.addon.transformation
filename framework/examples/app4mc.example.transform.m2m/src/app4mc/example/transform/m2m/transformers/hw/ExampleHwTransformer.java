/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2m.transformers.hw;

import org.eclipse.app4mc.amalthea.model.Cache;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.StructureType;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import app4mc.example.transform.samplemodel.Model;

@Singleton
public class ExampleHwTransformer {
	
	@Inject
	private SessionLogger logger;

	@Inject
	private ExampleMemoryTransformer memoryTransformer;

	@Inject
	private ExampleCacheTransformer cacheTransformer;

	@Inject
	private ExampleProcessingUnitTransformer processingUnitTransformer;

	public void transform(final HWModel hwModel, final Model outputModel) {

		for (final HwStructure structSystem : hwModel.getStructures()) {
			if (structSystem.getStructureType() == StructureType.SYSTEM) {

				for (final HwStructure structECU : structSystem.getStructures()) {
					if (structECU.getStructureType() == StructureType.ECU) {

						for (final HwStructure structMC : structECU.getStructures()) {
							if (structMC.getStructureType() == StructureType.MICROCONTROLLER) {

								for (HwModule hwModule : structMC.getModules()) {
									transformModule(outputModel, hwModule);
								}
							}
						}
					}
				}
			}
		}
	}

	private void transformModule(final Model outputModel, HwModule hwModule) {
		if (hwModule instanceof Cache) {
			Cache amCache = (Cache) hwModule;

			final app4mc.example.transform.samplemodel.Cache sampleCache = cacheTransformer.transfrom(amCache);
			outputModel.getCaches().add(sampleCache);

		} else if (hwModule instanceof Memory) {
			Memory amMemory = (Memory) hwModule;

			final app4mc.example.transform.samplemodel.Memory sampleMemory = memoryTransformer.transfrom(amMemory);
			outputModel.getMemories().add(sampleMemory);

		} else if (hwModule instanceof ProcessingUnit) {
			ProcessingUnit amPU = (ProcessingUnit) hwModule;

			logger.info("Transforming PU : {0}", amPU);
			processingUnitTransformer.transfrom(amPU, outputModel);
		}
	}

}
