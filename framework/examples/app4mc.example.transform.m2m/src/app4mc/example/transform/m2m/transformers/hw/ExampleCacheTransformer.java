/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.transform.m2m.transformers.hw;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.xtext.xbase.lib.CollectionLiterals;

import com.google.inject.Singleton;

import app4mc.example.transform.samplemodel.Cache;
import app4mc.example.transform.samplemodel.SampleModelFactory;

@Singleton
public class ExampleCacheTransformer {
	
	private final HashMap<ArrayList<?>, Cache> transformCache = CollectionLiterals.newHashMap();

	public Cache transfrom(final org.eclipse.app4mc.amalthea.model.Cache amltCache) {
		final ArrayList<?> transformKey = CollectionLiterals.newArrayList(amltCache);
		final Cache sampleCache;
		synchronized (transformCache) {
			if (transformCache.containsKey(transformKey)) {
				return transformCache.get(transformKey);
			}
			sampleCache = SampleModelFactory.eINSTANCE.createCache();
			transformCache.put(transformKey, sampleCache);
		}
		doTransform(sampleCache, amltCache);
		return sampleCache;
	}

	private void doTransform(final Cache sampleCache, final org.eclipse.app4mc.amalthea.model.Cache amltCache) {
		sampleCache.setName(amltCache.getName());
	}

}
