/*******************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.transformation;

import java.util.Arrays;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator to ensure that the Equinox ConfigAdmin bundle is started. Without
 * the ConfigAdmin the TransformationProcessor will not resolve. Needed because
 * Equinox by default does not activate all bundles.
 */
public class TransformationActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		Bundle cmBundle = Arrays.stream(context.getBundles())
				.filter(bundle -> "org.eclipse.equinox.cm".equals(bundle.getSymbolicName()))
				.findFirst()
				.orElse(null);

		if (cmBundle != null && (cmBundle.getState() != Bundle.ACTIVE && cmBundle.getState() != Bundle.STARTING)) {
			cmBundle.start(Bundle.START_ACTIVATION_POLICY);
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// nothing to do
	}

}
