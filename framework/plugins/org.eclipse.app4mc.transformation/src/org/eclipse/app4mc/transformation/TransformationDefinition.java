/*******************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.transformation;

import java.util.Collections;
import java.util.List;

/**
 * A TransformationDefinition is used to promote information on the provided
 * transformation implementation to the runtime.
 */
public interface TransformationDefinition {

	/**
	 * 
	 * @return The user friendly name of the transformation to be shown in a UI.
	 */
	String getName();

	/**
	 * 
	 * @return The description of the transformation to be shown in a UI.
	 */
	String getDescription();

	/**
	 * 
	 * @return The key of the provided m2m transformation under which the root
	 *         transformer and the configuration are registered.
	 */
	String getM2MKey();

	/**
	 * 
	 * @return The key of the provided m2t transformation under which the root
	 *         transformer and the configuration are registered.
	 */
	String getM2TKey();

	
	default List<TransformationParameter> getTransformationParameter() {
		return Collections.emptyList();
	}
	

	public static class TransformationParameter {

		public final String groupName;
		public final String parameterKey;
		public final String parameterName;
		public final Class<?> parameterType;
		public final String parameterDefault;

		public TransformationParameter(String groupName, String parameterKey, String parameterName, Class<?> parameterType) {
			this(groupName, parameterKey, parameterName, parameterType, null);
		}

		public TransformationParameter(String groupName, String parameterKey, String parameterName, Class<?> parameterType, String parameterDefault) {
			this.groupName = groupName;
			this.parameterKey = parameterKey;
			this.parameterName = parameterName;
			this.parameterType = parameterType;
			this.parameterDefault = parameterDefault;
		}
	}

}
