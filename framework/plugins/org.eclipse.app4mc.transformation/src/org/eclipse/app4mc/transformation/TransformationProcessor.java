/*******************************************************************************
 *
 * Copyright (c) 2018, 2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.transformation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;

import org.eclipse.app4mc.transformation.registry.TransformerRegistry;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.ComponentFactory;
import org.osgi.service.component.ComponentInstance;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;
import org.osgi.util.tracker.ServiceTracker;

/**
 * OSGi service component to start a transformation in an OSGi context. Uses the
 * {@link ConfigurationAdmin} to manage transformation sessions.
 */
@Component(service = TransformationProcessor.class)
public class TransformationProcessor {

	LoggerFactory factory;
	Logger logger;
	
	@Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC)
	void setLogger(LoggerFactory factory) {
		this.factory = factory;
		this.logger = factory.getLogger(getClass());
	}
	
	void unsetLogger(LoggerFactory loggerFactory) {
		if (this.factory == loggerFactory) {
			this.factory = null;
			this.logger = null;
		}
	}

	@Reference
	ConfigurationAdmin configAdmin;

	@Reference(target = "(component.factory=" + "org.eclipse.app4mc.amalthea.sessionlog.factory" + ")")
	ComponentFactory<SessionLogger> loggerFactory;

	ServiceRegistration<?> eventHandlerRegistration;
	
	HashMap<String, ComponentInstance<SessionLogger>> sessionLoggerMap = new HashMap<>();
	
	BundleContext context;
	
	@Activate
	void activate(BundleContext context) {
		this.context = context;
		
		Hashtable<String, Object> properties = new Hashtable<>();
	    properties.put(EventConstants.EVENT_TOPIC, TransformationConstants.TRANSFORMATION_DONE_TOPIC);
	    // register the EventHandler service
	    eventHandlerRegistration = context.registerService(
	        EventHandler.class.getName(),
	        (EventHandler) event -> {
				String sessionName = event.containsProperty(ServiceConstants.SESSION_ID) ? event.getProperty(ServiceConstants.SESSION_ID).toString() : null;
				cleanup(sessionName);
			},
	        properties);
	}
	
	@Deactivate
	void deactivate() {
		if (eventHandlerRegistration != null) {
			eventHandlerRegistration.unregister();
		}
	}
	
	/**
	 * Start the transformation with the configuration provided by the given
	 * {@link File}.
	 * 
	 * @param propertiesFile The properties file that contains the necessary
	 *                       transformation configuration.
	 */
	public void startTransformation(File propertiesFile) {
		startTransformation(propertiesFile, null);
	}

	/**
	 * Start the transformation with the configuration provided by the given
	 * {@link File}.
	 * 
	 * @param propertiesFile   The properties file that contains the necessary
	 *                         transformation configuration.
	 * @param workingDirectory The working directory to resolve relative paths for
	 *                         values in the properties file.
	 */
	public void startTransformation(File propertiesFile, String workingDirectory) {
		try {
			Properties inputParameters = getInputParameter(propertiesFile, workingDirectory);

			startTransformation(inputParameters, false);
		} catch (IOException e) {
			logger.error("Error in extracting properties from provided input file", e);
		}
	}

	/**
	 * Start the transformation with the provided configuration properties. The
	 * following parameters should be specified:
	 * <ul>
	 * <li>input_models_folder - the folder with the model files that should be
	 * transformed</li>
	 * <li>m2m_output_folder - the folder where the m2m transformation results
	 * should be generated</li>
	 * <li>m2t_output_folder - the folder where the m2t transformation results
	 * should be generated</li>
	 * <li>log_file - the name of the session log file</li>
	 * <li>m2mTransformers - comma separated key list of m2m transformations that
	 * should be executed</li>
	 * <li>m2tTransformers - comma separated key list of m2t transformations that
	 * should be executed</li>
	 * </ul>
	 * 
	 * @param inputParameters The properties with the necessary configuration
	 *                        parameter.
	 */
	public void startTransformation(Properties inputParameters) {
		startTransformation(inputParameters, true);
	}

	/**
	 * Start the transformation with the provided configuration properties. The
	 * following parameters should be specified:
	 * <ul>
	 * <li>input_models_folder - the folder with the model files that should be
	 * transformed</li>
	 * <li>m2m_output_folder - the folder where the m2m transformation results
	 * should be generated</li>
	 * <li>m2t_output_folder - the folder where the m2t transformation results
	 * should be generated</li>
	 * <li>log_file - the name of the session log file</li>
	 * <li>m2mTransformers - comma separated key list of m2m transformations that
	 * should be executed</li>
	 * <li>m2tTransformers - comma separated key list of m2t transformations that
	 * should be executed</li>
	 * </ul>
	 * 
	 * @param inputParameters The properties with the necessary configuration
	 *                        parameter.
	 */
	private void startTransformation(Properties inputParameters, boolean harmonize) {
		if (inputParameters != null) {
			try {
				Object workingDirectoryPath = inputParameters.get(TransformationConstants.WORKING_DIRECTORY);

				File rootDir = null;
				if (workingDirectoryPath != null && !workingDirectoryPath.toString().trim().equals("")) {
					rootDir = new File(workingDirectoryPath.toString());
				}
				Properties params = harmonize ? harmonizeInputParameter(inputParameters, rootDir) : inputParameters;
				Hashtable<String, Object> dictionaryForConfiguration = new Hashtable<>();

				for (String name : params.stringPropertyNames()) {
					String value = params.getProperty(name);
					dictionaryForConfiguration.put(name, value);
				}

				String name = params.getProperty(ServiceConstants.SESSION_ID);
				if (name == null) {
					name = "single";
					dictionaryForConfiguration.put(ServiceConstants.SESSION_ID, name);
				}

				// ensure that a session is not triggered twice
				String servicePid = ServiceConstants.SESSION_CONFIGURATION_PID + "~" + name;
				String pidFilter = "(service.pid=" + servicePid + ")";
				String factoryPidFilter = "(service.factoryPid=" + ServiceConstants.SESSION_CONFIGURATION_PID + ")";
				String filter = "(&" + pidFilter + factoryPidFilter + ")";
				Configuration[] existing = this.configAdmin.listConfigurations(filter);
				if (existing == null || existing.length == 0) {
					
					// first we need to create a SessionLogger so all referring services can be satisfied
					Hashtable<String, Object> sessionLoggerDictionary = new Hashtable<>();
					sessionLoggerDictionary.put(ServiceConstants.SESSION_ID, name);
					ComponentInstance<SessionLogger> sessionLoggerInstance = this.loggerFactory.newInstance(sessionLoggerDictionary);
					this.sessionLoggerMap.put(name, sessionLoggerInstance);
					SessionLogger sessionLogger = sessionLoggerInstance.getInstance();
					
					// after the transformers are created via session configuration we create the TransformerRegistry to start the transformation session
					// specify target properties to consume only the created instances for the created session
					String m2mTransformersKey = (String) params.get(TransformationConstants.M2M_TRANSFORMERS);
					String[] m2mKeys = m2mTransformersKey != null ? m2mTransformersKey.split(",") : new String[0];
					if (m2mKeys.length > 0) {
						String m2mFilter = "";
						for (String key : m2mKeys) {
							m2mFilter += ("(" + TransformationConstants.M2M_TRANSFORMERS + "=" + key + ")");
						}
						
						if (m2mKeys.length > 1) {
							m2mFilter = "(|" + m2mFilter + ")";
						}
						
						m2mFilter = "(&" + pidFilter + m2mFilter + ")";
						dictionaryForConfiguration.put("m2mTransformers.target", m2mFilter);
						dictionaryForConfiguration.put("m2mTransformers.cardinality.minimum", m2mKeys.length);
					}
					
					String m2tTransformersKey = (String) params.get(TransformationConstants.M2T_TRANSFORMERS);
					String[] m2tKeys = m2tTransformersKey != null ? m2tTransformersKey.split(",") : new String[0];
					if (m2tKeys.length > 0) {
						String m2tFilter = "";
						for (String key : m2tKeys) {
							m2tFilter += ("(" + ServiceConstants.TRANSFORMATION_PROPERTY + "=" + key.trim() + ")");
						}
						
						if (m2tKeys.length > 1) {
							m2tFilter = "(|" + m2tFilter + ")";
						}
						
						m2tFilter = "(&" + pidFilter + m2tFilter + ")";
						dictionaryForConfiguration.put("m2tTransformers.target", m2tFilter);
						dictionaryForConfiguration.put("m2tTransformers.cardinality.minimum", m2tKeys.length);
					}
					
					dictionaryForConfiguration.put("sessionLogger.target", "(" + ServiceConstants.SESSION_ID + "=" + name + ")");
					// create the configuration to trigger the activation of transformation components and the transformation
					// this way we start a new session to get new transformer instances
					Configuration configuration = this.configAdmin
							.getFactoryConfiguration(ServiceConstants.SESSION_CONFIGURATION_PID, name, "?");
					configuration.update(dictionaryForConfiguration);
					
					TransformerRegistry transformerRegistry = getTransformerRegistry(pidFilter);
					if (transformerRegistry != null) {
						transformerRegistry.startTransformation();
					} else {
						StringBuilder errorBuilder = new StringBuilder("Failed to create TransformerRegistry. Double check the transformer configuration.")
								.append(System.lineSeparator())
								.append(System.lineSeparator());
						dictionaryForConfiguration.entrySet().stream()
							.filter(entry -> TransformationConstants.M2M_TRANSFORMERS.equals(entry.getKey()) || TransformationConstants.M2T_TRANSFORMERS.equals(entry.getKey()))
							.forEach(entry ->  errorBuilder.append(entry.getKey()).append(" : ").append(entry.getValue()).append(System.lineSeparator()));
						errorBuilder.append(System.lineSeparator());
						sessionLogger.error(errorBuilder.toString());
						sessionLogger.flush(new File((String) params.get(TransformationConstants.LOG_FILE)));
						
						cleanup(name);
					}
				} else {
					logger.error("Transformation configuration for session id {} is already available.", servicePid);
				}
					
			} catch (InvalidSyntaxException | IOException e) {
				logger.error("Error in retrieving existing session configurations", e);
			}
		} else {
			logger.error("ERROR !! Unable to start transformation as required parameters are not set in input properties file");
		}
	}
	
	/**
	 * Method to get the {@link TransformerRegistry} from the OSGi service context.
	 * Uses a {@link ServiceTracker} with a filter on the given session id. The
	 * tracker is used because the session instance of the TransfomerRegistry and
	 * the referenced root transformer are created when the configuration is created
	 * via ConfigAdmin. We therefore need to wait until the references can be
	 * satisfied and the instance is created.
	 * 
	 * @param pidFilter The filter for the service.pid of the current session.
	 * @return The {@link TransformerRegistry} for the given filter.
	 */
	private TransformerRegistry getTransformerRegistry(String pidFilter) {
		try {
			String filterString = "(&(" + Constants.OBJECTCLASS + 
	                "=" + TransformerRegistry.class.getName() + ")" + pidFilter + ")";
			Filter filter = context.createFilter(filterString);
			
			ServiceTracker<TransformerRegistry, TransformerRegistry> st = 
					new ServiceTracker<>(context, filter, null);
			st.open();
			return st.waitForService(1000);
		} catch (InterruptedException | InvalidSyntaxException e) {
			logger.error("Failed to get TransformerRegistry", e);
			Thread.currentThread().interrupt();
		}
		return null;
	}

	private void cleanup(String sessionName) {
		if (sessionLoggerMap.containsKey(sessionName)) {
			// dispose the SessionLogger stored for the session id
			sessionLoggerMap.get(sessionName).dispose();
		}
		
		// once we are done, we delete the configuration to clean up
		try {
			Configuration configuration = configAdmin.getFactoryConfiguration(ServiceConstants.SESSION_CONFIGURATION_PID, sessionName, "?");
			configuration.delete();
		} catch (IOException e) {
			if (logger != null) {
				logger.error("Failed to delete the session configuration", e);
			}
		}	
	}
	
	private Properties getInputParameter(File propertiesFile, String workingDirectory) throws IOException {

		Properties properties = new Properties();
		try (FileInputStream fis = new FileInputStream(propertiesFile)) {
			properties.load(fis);
		}

		File workDir = null;

		// if a working directory is provided as argument, it should override a possible value in the properties file
		if (workingDirectory != null && !workingDirectory.isEmpty()) {
			properties.put(TransformationConstants.WORKING_DIRECTORY, workingDirectory);
			workDir = new File(workingDirectory);
		} else {
			// if no working directory value was provided as argument, check if there is a value in the properties
			String dir = properties.getProperty(TransformationConstants.WORKING_DIRECTORY);
			if (dir != null && !dir.isEmpty()) {
				workDir = new File(dir);
			}
		}

		if (workDir == null) {
			// if no working directory is provided as argument and not in the properties, use the parent of the properties file
			workDir = propertiesFile.getAbsoluteFile().getParentFile();
			// if properties doesn't contain 'workingDirectory' and command line parameter is also not specified, 
			// then add parent of properties file -> in properties as value for 'workingDirectory'
			if (!properties.contains(TransformationConstants.WORKING_DIRECTORY)) {
				properties.put(TransformationConstants.WORKING_DIRECTORY, workDir.getPath());
			}
		}

		return harmonizeInputParameter(properties, workDir);
	}

	private Properties harmonizeInputParameter(Properties properties, File rootFolder) {

		// Now checking if the user has specified absolute paths in the properties file

		Object inputModelsFolder = properties.get(TransformationConstants.INPUT_MODELS_FOLDER);
		Object outputFolder = properties.get(TransformationConstants.OUTPUT_FOLDER);

		File inputFile = null;
		if (inputModelsFolder != null) {
			String path = inputModelsFolder.toString();
			inputFile = new File(path);
			if (inputFile.exists()) {
				// the file exists so we can simply use the specified path
				properties.put(TransformationConstants.INPUT_MODELS_FOLDER, path);
			} else if (rootFolder != null) {
				// the file seems to not exist, try to evaluate with the provided parent
				inputFile = new File(rootFolder, path);
				if (inputFile.exists()) {
					properties.put(TransformationConstants.INPUT_MODELS_FOLDER, inputFile.getAbsolutePath());
				}
			}
		} else {
			throw new IllegalArgumentException("'input_models_folder' parameter needs to be set");
		}

		if (!inputFile.exists()) {
			throw new IllegalArgumentException(
					"'input_models_folder' doesn't seem to exist. Please check configuration of 'input_models_folder' and optional 'workingDirectory'.");
		}

		if (rootFolder == null) {
			rootFolder = inputFile.getParentFile();
		}

		if (outputFolder != null) {
			// check for generic output folder setting and derive needed values
			Path outputFolderPath = Paths.get(outputFolder.toString());

			properties.put(
					TransformationConstants.M2M_OUTPUT_FOLDER, 
					Paths.get(outputFolderPath.toString(), "m2m_output_models").toString());
			properties.put(
					TransformationConstants.M2T_OUTPUT_FOLDER,
					Paths.get(outputFolderPath.toString(), "m2t_output_text_files").toString());
			properties.put(
					TransformationConstants.LOG_FILE, 
					Paths.get(outputFolderPath.toString(), "transformation.log").toString());

		}

		Object m2mOutputFolder = properties.get(TransformationConstants.M2M_OUTPUT_FOLDER);
		Object m2tOutputFolder = properties.get(TransformationConstants.M2T_OUTPUT_FOLDER);
		Object logFile = properties.get(TransformationConstants.LOG_FILE);

		// check for separate values
		if (m2mOutputFolder != null) {
			String path = m2mOutputFolder.toString();

			String newPath = new File(path).isAbsolute() ? path : new File(rootFolder, path).getAbsolutePath();

			properties.put(TransformationConstants.M2M_OUTPUT_FOLDER, newPath);
		} else {
			throw new IllegalArgumentException("'m2m_output_folder' parameter needs to be set");
		}

		if (m2tOutputFolder != null) {
			String path = m2tOutputFolder.toString();

			String newPath = new File(path).isAbsolute() ? path : new File(rootFolder, path).getAbsolutePath();

			properties.put(TransformationConstants.M2T_OUTPUT_FOLDER, newPath);
		} else {
			throw new IllegalArgumentException("'m2t_output_folder' parameter needs to be set");
		}

		if (logFile != null) {
			String path = logFile.toString();

			String newPath = new File(path).isAbsolute() ? path : new File(rootFolder, path).getAbsolutePath();

			properties.put(TransformationConstants.LOG_FILE, newPath);
		}

		return properties;
	}

}
