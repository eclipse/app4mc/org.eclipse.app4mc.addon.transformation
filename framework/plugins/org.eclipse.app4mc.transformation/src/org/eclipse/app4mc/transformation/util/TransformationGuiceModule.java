/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.transformation.util;

import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.AbstractModule;

/**
 * Default module that should be added to the created injector by creating a
 * child-injector. Needed to be able to get the SessionLogger instance injected
 * via Google Guice injection.
 */
public class TransformationGuiceModule extends AbstractModule {

	private SessionLogger logger;
	
	public TransformationGuiceModule(SessionLogger logger) {
		this.logger = logger;
	}

	@Override
	protected void configure() {
		bind(SessionLogger.class).toInstance(logger);
	}

}
