/*******************************************************************************
 *
 * Copyright (c) 2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.transformation;

/**
 * Constants used for APP4MC transformation.
 */
public final class TransformationConstants {

	private TransformationConstants() {
		// private empty constructor for helper class
	}

	// Property constants
	/**
	 * Property key to specify the input folder that contains the model files to process.
	 */
	public static final String INPUT_MODELS_FOLDER = "input_models_folder";
	
	/**
	 * Property key to specify the root output folder.
	 * Using this key for configuration, the transformation results will be placed in sub-folders of the root output folder:
	 * <ul>
	 * <li><i>m2m_output_folder</i> - sub-folder that contains the m2m transformation results</li>
	 * <li><i>m2t_output_folder</i> - sub-folder that contains the m2t transformation results</li>
	 * <li><i>log_file</i> - session log file</li>
	 * </ul>
	 */
	public static final String OUTPUT_FOLDER = "output_folder";
	
	/**
	 * Property key to specify the working directory to resolve relative paths for values in the properties file.
	 */
	public static final String WORKING_DIRECTORY = "workingDirectory";
	
	/**
	 * Property key to specify the m2m transformation result location.
	 */
	public static final String M2M_OUTPUT_FOLDER = "m2m_output_folder";
	
	/**
	 * Property key to specify the m2t transformation result location.
	 */
	public static final String M2T_OUTPUT_FOLDER = "m2t_output_folder";
	
	/**
	 * Property key to specify the transformation result location. Derived from the
	 * m2m/m2t output folder and the transformation key.
	 */
	public static final String TRANSFORMATION_OUTPUT_FOLDER = "transformation_output_folder";
	
	/**
	 * Property key to specify the session log file location.
	 */
	public static final String LOG_FILE = "log_file";

	/**
	 * Property key to specify the m2m transformers that should be executed.
	 */
	public static final String M2M_TRANSFORMERS = "m2mTransformers";
	
	/**
	 * Property key to specify the m2t transformers that should be executed.
	 */
	public static final String M2T_TRANSFORMERS = "m2tTransformers";
	
	/**
	 * The event topic for the event that is sent when a transformation is finished.
	 */
	public static final String TRANSFORMATION_DONE_TOPIC = "org/eclipse/app4mc/transformation/DONE";
}
