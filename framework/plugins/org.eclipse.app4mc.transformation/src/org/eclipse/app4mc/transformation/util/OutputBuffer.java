/**
* *******************************************************************************
* Copyright (c) 2019-2023 Robert Bosch GmbH.
* 
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
* 
* SPDX-License-Identifier: EPL-2.0
* 
* Contributors:
*     Robert Bosch GmbH - initial API and implementation
* *******************************************************************************
*/

package org.eclipse.app4mc.transformation.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.eclipse.app4mc.util.sessionlog.SessionLogger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class OutputBuffer {

	SessionLogger logger;

	private final HashMap<String, TypedBuffer> filetypeToBufferMap = new HashMap<>();

	private String outputFolder;

	private List<Path> excludePathsOfResult = Collections.emptyList();

	@Inject
	public void setSessionLogger(SessionLogger logger) {
		this.logger = logger;
	}

	public void initialize(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public void configureFiletype(String type, String fileExtension, String header, String footer) {
		if (type == null || type.isEmpty())
			throw new IllegalArgumentException("Illegal type string");

		if (filetypeToBufferMap.containsKey(type))
			throw new IllegalArgumentException("Type is already configured");

		filetypeToBufferMap.put(type, new TypedBuffer(fileExtension, header, footer));
	}

	/**
	 * @return: true if new file handle has been created
	 */
	public boolean appendTo(String type, String targetFile, String content) {
		if (! filetypeToBufferMap.containsKey(type))
			throw new IllegalArgumentException("Unknown type");

		if (targetFile == null || targetFile.isEmpty())
			throw new IllegalArgumentException("Undefined filename");

		return filetypeToBufferMap.get(type).appendTo(targetFile, content);
	}

	public void finish() {
		finish(true, true, false);
	}

	public void finish(boolean createZip, boolean writeChangedFilesOnly) {
		finish(createZip, true, writeChangedFilesOnly);
	}

	public void finish(boolean createZip, boolean createContentTree, boolean writeChangedFilesOnly) {

		// Write files to disk
		writeFiles(writeChangedFilesOnly);

		if (createZip) {
			// Create Zip archive
			try {
				FileHelper.zipResult(outputFolder, excludePathsOfResult, "result.zip", logger);
			} catch (Exception e) {
				logger.error("Failed to produce result zip archive: ", e);
			}
		}

		if (createContentTree) {
			try {
				Path path = Paths.get(outputFolder, "result-tree.txt");
				Files.writeString(path, getOutputTree(), StandardCharsets.UTF_8, StandardOpenOption.CREATE);
			} catch (IOException ex) {
				logger.error("Failed to write result file: result-tree.txt");
			}
		}
	}

	private void writeFiles(boolean writeChangedFilesOnly) {
		for (TypedBuffer typedBuffer : filetypeToBufferMap.values()) {

			for (Entry<String, StringBuilder> entry : typedBuffer.filenameToBufferMap.entrySet()) {
				String targetFile = entry.getKey();
				StringBuilder buffer = entry.getValue();

				StringBuilder builder = new StringBuilder(
						typedBuffer.header.length() + buffer.length() + typedBuffer.footer.length());
				builder.append(typedBuffer.header);
				builder.append(buffer);
				builder.append(typedBuffer.footer);
				String newContent = builder.toString();

				File file = getFile(outputFolder, targetFile + typedBuffer.fileExtension);

				if (writeChangedFilesOnly && contentEquals(file, newContent))
					continue;

				try (FileWriter fw = new FileWriter(file); BufferedWriter writer = new BufferedWriter(fw);) {
					writer.append(newContent);
					writer.flush();
				} catch (IOException e) {
					logger.error("Failed to write result file: {0}", targetFile);
				}
			}
		}
	}

	private boolean contentEquals(File file, String newContent) {
		if (!file.exists() || !file.canRead() || file.length() != newContent.getBytes().length)
			return false;

		try (FileReader fr = new FileReader(file); BufferedReader reader = new BufferedReader(fr);) {
			// read the complete file
			char[] fileContent = new char[newContent.length()];
			int fileLength = reader.read(fileContent);

			if (fileLength != newContent.length())
				return false;

			return newContent.equals(String.valueOf(fileContent));

		} catch (IOException e1) {
			logger.error("Failed to read File {0}, which should be present", file.getPath(), logger);
		}
		return false;
	}

	/**
	 * This method will get the File object by creating all the parent directories
	 * of the file
	 */
	private File getFile(final String outputFolder, final String targetFile) {
		final File file = new File(outputFolder, targetFile);
		final File parentFile = file.getParentFile();

		if (! parentFile.exists())
			parentFile.mkdirs();

		return file;
	}

	public String getOutputFolder() {
		return outputFolder;
	}

	public String getFileExtension(String type) {
		TypedBuffer typedBuffer = filetypeToBufferMap.get(type);
		if (typedBuffer == null)
			return null;

		return typedBuffer.getFileExtension();
	}

	/**
	 * @return: true if buffer has already been created
	 */
	public boolean bufferExists(String type, String targetFile) {
		TypedBuffer typedBuffer = filetypeToBufferMap.get(type);

		return typedBuffer != null && typedBuffer.filenameToBufferMap.containsKey(targetFile);
	}

	public void setExcludePathsOfResult(List<Path> excludePathsOfResult) {
		this.excludePathsOfResult = excludePathsOfResult;
	}

	public List<String> getOutputFiles() {
		List<String> files = new ArrayList<>();
		for (TypedBuffer buffer : filetypeToBufferMap.values()) {
			for (String filename : buffer.filenameToBufferMap.keySet()) {
				files.add(filename + buffer.fileExtension);
			}
		}
		return files;
	}

	public CharSequence getOutputTree() {
		FileTree tree = new FileTree();

		/*
		 * ===== Create a file tree (nested TreeMap) =====
		 * 
		 * Prefix ("D", "F") is used to ensure sort order (directories before files)
		 */
		for (String path : getOutputFiles()) {
			// start at top level
			FileTree node = tree;

			String[] segments = path.split("/");
			// add directories
			for (int i = 0; i < segments.length - 1; i++) {
				node = node.computeIfAbsent("D" + segments[i], s -> new FileTree());
			}
			// add file (last segment -> map value is null)
			node.put("F" + segments[segments.length - 1], null);
		}

		/*
		 * ===== Create string representation of file tree =====
		 */
		StringBuilder builder = new StringBuilder();
		Counters counters = new Counters();
		appendSubtree(tree, "", counters, builder);
		builder.append("\n" + counters.directories + " directories, " + counters.files + " files\n");

		return builder;
	}

	private void appendSubtree(FileTree tree, String prefix, Counters counters, StringBuilder builder) {
		tree.forEach((key, value) -> {
			boolean isLastEntry = tree.lastKey().equals(key);
			builder.append(prefix + (isLastEntry ? "└── " : "├── ") + key.substring(1) + "\n");
			if (value == null) {
				counters.files++;
			} else {
				counters.directories++;
				appendSubtree(value, prefix + (isLastEntry ? "    " : "│   "), counters, builder);
			}
		});
	}

	@SuppressWarnings("serial")
	private class FileTree extends TreeMap<String, FileTree> {
	}

	private class Counters {
		int directories = 0;
		int files = 0;
	}

	private class TypedBuffer {
		private final HashMap<String, StringBuilder> filenameToBufferMap = new HashMap<>();

		private String fileExtension = "";
		private String header = "";
		private String footer = "";

		public TypedBuffer(String fileExtension, String header, String footer) {
			super();
			if (fileExtension != null) {
				this.fileExtension = fileExtension;
			}
			if (header != null) {
				this.header = header;
			}
			if (footer != null) {
				this.footer = footer;
			}
		}

		/**
		 * @return: true if new file buffer has been created
		 */
		public boolean appendTo(String targetFile, String content) {
			boolean isNewBuffer = !filenameToBufferMap.containsKey(targetFile);

			filenameToBufferMap.computeIfAbsent(targetFile, k -> new StringBuilder()).append(content);

			return isNewBuffer;
		}

		public String getFileExtension() {
			return fileExtension;
		}
	}

}
