/**
 * Copyright (c) 2018-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.transformation.transformers;

import java.nio.file.Paths;
import java.util.Map;

import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.util.PropertyUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;

public abstract class Model2TextRootTransformer implements IRootTransformer {

	protected SessionLogger logger;
	
	private String inputFolder;
	private String transformationKey;
	private String outputFolder;

	protected void activate(Map<String, ?> properties) {
		this.inputFolder = PropertyUtil.getProperty(TransformationConstants.INPUT_MODELS_FOLDER, properties);
		this.transformationKey = PropertyUtil.getProperty(ServiceConstants.TRANSFORMATION_PROPERTY, properties);

		String m2tOutputFolder = PropertyUtil.getProperty(TransformationConstants.M2T_OUTPUT_FOLDER, properties);
		this.outputFolder = Paths.get(m2tOutputFolder, transformationKey).toString();
	}
	
	public String getInputFolder() {
		return inputFolder;
	}
	
	public String getOutputFolder() {
		return outputFolder;
	}
	
	public String getTransformationKey() {
		return transformationKey;
	}
	
	public void setSessionLogger(SessionLogger logger) {
		this.logger = logger;
	}
	
	public abstract void m2tTransformation();

}
