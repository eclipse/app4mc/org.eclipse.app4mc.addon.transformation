/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.transformation.util;

import java.util.Map;

public class PropertyUtil {

	// Suppress default constructor
	private PropertyUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static String getProperty(final String propKey, final Map<String, ?> properties) {
		final Object value = properties.get(propKey);
		if ((value == null)) {
			throw new NullPointerException(
					(("Request input key : \"" + propKey) + "\" not supplied in the input properties file"));
		}
		return value.toString();
	}
}
