/*******************************************************************************
 *
 * Copyright (c) 2018, 2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.transformation;

/**
 * Constants used for APP4MC transformation Declarative Services.
 */
public final class ServiceConstants {

	private ServiceConstants() {
		// private empty constructor for helper class
	}

	/**
	 * Service property key to define the properties.
	 */
	public static final String PROPERTIES_PROPERTY = "properties";

	/**
	 * Service property key to define the logger.
	 */
	public static final String LOGGER_PROPERTY = "logger";

	/**
	 * Factory identifier of the component factory for TransformerRegistry components.
	 */
	public static final String TRANSFORMER_REGISTRY_FACTORY = "org.eclipse.app4mc.transformation.registry.factory";

	/**
	 * The configuration pid that should be used to activate/deactivate a component per session.
	 */
	public static final String SESSION_CONFIGURATION_PID = "TRANSFORMATION_SESSION_CONFIG";

	/**
	 * The key of an optional session id in the properties for creating a new transformation session.
	 */
	public static final String SESSION_ID = "SESSION_ID";

	/**
	 * Service property key to specify the unique key of the root transformer and the corresponding configuration.
	 */
	public static final String TRANSFORMATION_PROPERTY = "transformation";
}
