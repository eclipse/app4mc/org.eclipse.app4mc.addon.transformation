/*******************************************************************************
 *
 * Copyright (c) 2018, 2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.transformation.transformers;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.eclipse.emf.ecore.resource.ResourceSet;

public interface IRootTransformer {

	default ResourceSet getInputResourceSet(String folderPath, SessionLogger logger) {

		if (folderPath != null) {

			ResourceSet resourceSet = AmaltheaLoader.loadFromDirectoryNamed(folderPath);

			if (resourceSet.getResources().stream().noneMatch(r -> !r.getContents().isEmpty())) {
				logger.error(
						"No Amalthea model files are loaded. Verify if the model version is : {0}",
						AmaltheaFactory.eINSTANCE.createAmalthea().getVersion());
			}

			return resourceSet;
		} else {
			logger.error("Input_models_folder parameter not set",
					new NullPointerException("input_models_folder property not set"));
		}

		return null;
	}

}
