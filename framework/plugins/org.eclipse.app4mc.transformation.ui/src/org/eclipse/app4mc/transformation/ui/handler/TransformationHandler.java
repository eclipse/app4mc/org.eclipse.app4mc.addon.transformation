/*******************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.transformation.ui.handler;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.io.AmaltheaFileHelper;
import org.eclipse.app4mc.transformation.ui.dialog.TransformationDialog;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;

public class TransformationHandler {
	
	@Execute
	public void execute(
			Shell shell,
			@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
			IEclipseContext context) {

		Path inputPath = null;
		IProject project = null;
		if (selection != null && !selection.isEmpty()
				&& selection.size() == 1
				&& selection.getFirstElement() instanceof IResource) {
			
			project = getProjectFromSelection((TreeSelection) selection, shell);

			IResource resource = (IResource) selection.getFirstElement();
			if (resource instanceof IFile && AmaltheaFileHelper.isModelFileExtension(resource.getFileExtension())) {
				inputPath = Paths.get(resource.getLocationURI()).getParent();
			}
			
			if (resource instanceof IContainer) {
				IContainer folder = (IContainer) resource;
				inputPath = Paths.get(folder.getLocationURI());
			}
		}

		IEclipseContext childContext = context.createChild("TransformationDialogContext");
		if (inputPath != null) {
			childContext.set(Path.class, inputPath);
		}
		if (project != null) {
			childContext.set(IProject.class, project);
		}
		
		TransformationDialog dialog = ContextInjectionFactory.make(TransformationDialog.class, childContext);
		dialog.open();
	}
	
	private IProject getProjectFromSelection(TreeSelection selection, Shell shell) {
		TreePath[] paths = selection.getPaths();

		String projectSegment = null;
		IProject iProject = null;
		
		for (TreePath treePath : paths) {
			Object firstSegment = treePath.getFirstSegment();

			if (projectSegment == null) {
				
				projectSegment = firstSegment.toString();

				iProject = ((IAdaptable) firstSegment).getAdapter(IProject.class);

				if (iProject == null) {
					// if the first segment could not be transformed to an IProject, try the second segment
					// could happen in case of enabled WorkingSets
					firstSegment = treePath.getSegment(1);
					projectSegment = firstSegment.toString();
					iProject = ((IAdaptable) firstSegment).getAdapter(IProject.class);
				}
			}
			else if (!projectSegment.equals(firstSegment.toString())) {
				MessageDialog.openError(shell, "AMALTHEA Model Transformation",
						"Transformation across multiple projects is not supported.");
			}
		}
		
		return iProject;
	}

}