/*******************************************************************************
 * Copyright (c) 20212023 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *******************************************************************************/ 

package org.eclipse.app4mc.transformation.ui.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.io.AmaltheaFileHelper;
import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;

public class TransformationAvailableExpression {
	
	@Evaluate
	public boolean evaluate(
			@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection, 
			@Service List<TransformationDefinition> definitions) {
		
		if (selection != null && !selection.isEmpty()
				&& selection.size() == 1
				&& selection.getFirstElement() instanceof IResource) {
			
			IResource resource = (IResource) selection.getFirstElement();
			if (resource instanceof IFile 
					&& AmaltheaFileHelper.isModelFileExtension(resource.getFileExtension())
					&& AmaltheaFileHelper.isModelVersionValid(Paths.get(((IFile)resource).getLocationURI()).toFile())) {
				return !definitions.isEmpty();
			}
			
			if (resource instanceof IContainer) {
				// check if there are amxmi files in the folder or project
				IContainer folder = (IContainer) resource;
				Path modelFilePath = Paths.get(folder.getLocationURI());
				
				try (Stream<Path> directoryStream = Files.walk(modelFilePath, Integer.MAX_VALUE)) {
					return directoryStream
							.filter(Files::isRegularFile)
							.anyMatch(file -> AmaltheaFileHelper.isModelFile(file.toString())
									&& !definitions.isEmpty());
					
				} catch (IOException e) {
					Platform.getLog(getClass()).error("Failed to collect model files", e);
				}	

			}
		}
		return false;
	}
}