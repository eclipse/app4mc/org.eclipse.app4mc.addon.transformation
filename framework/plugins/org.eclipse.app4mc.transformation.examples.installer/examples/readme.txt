Note: 
- During workspace build:  plugins from "framework/examples" folder are copied into this folder by the "examples builder"
- During Maven build:  plugins from "framework/examples" folder are copied into this folder by the ant task specified in the pom.xml of the corresponding example plugins.