/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.transformation.starter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.eclipse.app4mc.transformation.TransformationProcessor;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

/**
 * Immediate component that gets activated once the launcher arguments provided
 * by the bnd launcher are available. Triggers the transformation process based on
 * the command line arguments. Via -console parameter it is also possible to
 * simply start the application in an interactive mode by keeping the OSGi
 * console open. Otherwise the application will be immediately closed after
 * execution.
 */
@Component(immediate = true,
	property = EventConstants.EVENT_TOPIC + "=" + "org/osgi/service/cm/ConfigurationEvent/CM_DELETED",
	service = EventHandler.class
)
public class BndTransformationStarter extends AbstractTransformationStarter {

	/**
	 * Launcher arguments provided by the bnd launcher.
	 */
	String[] launcherArgs;

	@Reference(target = "(launcher.arguments=*)")
	void setLauncherArguments(Object object, Map<String, Object> map) {
		this.launcherArgs = (String[]) map.get("launcher.arguments");
	}

	@Reference
	List<TransformationDefinition> definitions;

	@Reference
	void setTransformationProcessor(TransformationProcessor transformationProcessor) {
		this.transformationProcessor = transformationProcessor;
	}

	@Activate
	void activate() {
		String console = System.getProperty("osgi.console");
		this.isInteractive =  console != null && console.length() == 0;

		// clear launcher arguments from possible framework parameter
		String[] args = Arrays.stream(launcherArgs)
				.filter(arg -> !"-console".equals(arg) && !"-consoleLog".equals(arg))
				.toArray(String[]::new);

		if (this.started.compareAndSet(false, true)) {
			startTransformation(args, definitions);
		}
	}

}
