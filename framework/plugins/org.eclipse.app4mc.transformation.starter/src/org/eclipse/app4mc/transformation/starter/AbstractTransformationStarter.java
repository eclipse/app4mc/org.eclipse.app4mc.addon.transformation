/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.transformation.starter;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.eclipse.app4mc.transformation.ServiceConstants;
import org.eclipse.app4mc.transformation.TransformationConstants;
import org.eclipse.app4mc.transformation.TransformationDefinition;
import org.eclipse.app4mc.transformation.TransformationProcessor;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

/**
 * Abstract implementation for transformation starter components. Contains the logic
 * for inspecting the command line arguments and starts the transformation.
 */
public class AbstractTransformationStarter implements EventHandler {

	protected AtomicBoolean started = new AtomicBoolean(false);

	/**
	 * Flag to configure if the OSGi console should be opened and stay open.
	 * <code>false</code> if the application should automatically shutdown once the
	 * process is finished.
	 */
	protected boolean isInteractive = false;
	
	/**
	 * The TransformationProcessor that does the actual transformation task.
	 */
	protected TransformationProcessor transformationProcessor;
	
	/**
	 * Start the transformation process based on the given parameter.
	 *
	 * @param args           The non-framework command line arguments.
	 * @param definitions    The transformations available in the runtime.
	 */
	// suppress the sonar finding for throwing the return value of BufferedReader#readline() away and reading from standard input
	// the return value is actually not needed in this case as we only block the process until any user input and the input is not evaluated
	@SuppressWarnings( {"squid:S2677" , "squid:S4829"})
	protected void startTransformation(String[] args, List<TransformationDefinition> definitions) {
		if (args != null && args.length > 0) {

			// arguments for properties file approach
			String propertiesFilePath = null;
			String workingDirectory = null;
			
			// arguments for parameter approach
			String input = null;
			String output = null;
			String m2m = null;
			String m2t = null;
			
			// arguments for additional transformation specific parameters
			HashMap<String, String> additional = new HashMap<>();

			// check if a version parameter was used and collect the filenames to convert
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];

				if (arg.startsWith("-p") || arg.startsWith("--properties")) {
					propertiesFilePath = args[++i];
				} else if (arg.startsWith("-w") || arg.startsWith("--workingDirectory")) {
					workingDirectory = args[++i];
				} else if (arg.startsWith("-o") || arg.startsWith("--output")) {
					output = args[++i];
				} else if (arg.startsWith("-m") || arg.startsWith("--m2m")) {
					m2m = args[++i];
				} else if (arg.startsWith("-t") || arg.startsWith("--m2t")) {
					m2t = args[++i];
				} else if (arg.startsWith("-A")) {
					String argument = arg.substring(2);
					String[] argArray = argument.split("=");
					additional.put(argArray[0], argArray[1]);
				} else if (arg.startsWith("/?") || arg.startsWith("-h") || arg.startsWith("--help")) {
					// only print the help and exit
					System.out.println();
					System.out.println("Transformation configured via Properties file:");
					System.out.println("[-p, --properties <file>] [-w, --workingDirectory <dir>]");
					System.out.println();
					System.out.println("Transformation configured via parameter:");
					System.out.println("[-o, --output <dir>] [-m, --m2m <keys>] [-t, --m2t <keys>] [-A<key-value>] <directory>");
					System.out.println();
					System.out.println("Options:");
					System.out.println("\t-p, --properties  \tThe Properties file that should be used to configure the transformation.");
					System.out.println("\t\t\tIf the properties option is not set, the input needs to be provided as parameter.");
					System.out.println();
					System.out.println("\t-w, --workingDirectory\tThe working directory. Used to resolve file parameters in the Properties file in a relative manner. [optional].");
					System.out.println();
					System.out.println("\t-o, --output\tThe directory in which the output should be generated. Only interpreted if --properties is not set. [optional].");
					System.out.println("\t\t\tIf not provided the output directory defaults to <input_directory>/result.");
					System.out.println();
					System.out.println("\t-m, --m2m\tComma separated list of m2m transformation keys [optional].");
					System.out.println("\t\t\tIf not provided and no m2t transformation keys are set, all available m2m transformations will be executed.");
					System.out.println();
					System.out.println("\t-t, --m2t\tComma separated list of m2t transformation keys [optional].");
					System.out.println("\t\t\tIf not provided and no m2m transformation keys are set, all available m2t transformations will be executed.");
					System.out.println();
					System.out.println("\t-A\t\tOption to provide additional transformation specific properties. [optional].");
					System.out.println("\t\t\tNeed to be provided in the key=value format.");
					System.out.println();
					System.out.println("\t-h, --help, /?\tShow this help.");
					System.out.println();
					System.out.println("Parameter:");
					System.out.println("\tdirectory\tThe directory that contains model files to transform");
					System.out.println();
					System.out.println("Available M2M transformations:");
					System.out.println(getAvailableM2MKeys(definitions));
					System.out.println("Available M2T transformations:");
					System.out.println(getAvailableM2TKeys(definitions));
					System.exit(0);
				} else if (!arg.startsWith("-") && arg.trim().length() > 0) {
					input = arg;
				}
			}

			if (propertiesFilePath != null && !propertiesFilePath.isEmpty()) {
				// Properties file approach
				File propertiesFile = new File(propertiesFilePath);
				if (!propertiesFile.exists()) {
					if (workingDirectory != null) {
						propertiesFile = new File(workingDirectory, propertiesFilePath);
						if (!propertiesFile.exists()) {
							// the file seems to not exist
							System.err.println("input properties file doesn't seem to exist. Check the value of command line parameter '--properties'");
							System.exit(0);
						}
					} else {
						System.err.println("input properties file doesn't seem to exist, also '--workingDirectory' commandline parameter is not supplied");
						System.exit(0);
					}
				} else {
					// start the transformation configured via Properties file
					try {
						this.transformationProcessor.startTransformation(propertiesFile, workingDirectory);
					} catch (Exception e) {
						// if an exception occurs we kill the application
						System.err.println("Error in transformation setup: " + e.getMessage());
						e.printStackTrace();
						System.exit(0);
					}
				}
			} else if (input != null && !input.isEmpty()) {
				// single parameter approach
				Properties properties = new Properties();
				properties.put(TransformationConstants.INPUT_MODELS_FOLDER, input);
				
				if (output == null || output.isEmpty()) {
					Path inputPath = Paths.get(input);
					output = inputPath.resolve("result").toString(); 
				}
				properties.put(TransformationConstants.OUTPUT_FOLDER, output);

				// collect all available transformation keys if none was specified as option
				if ((m2m == null || m2m.isEmpty())
						&& (m2t == null || m2t.isEmpty())) {
					m2m = getAvailableM2MKeys(definitions);
					m2t = getAvailableM2TKeys(definitions);
				}

				if (m2m != null && !m2m.isEmpty()) {
					properties.put(TransformationConstants.M2M_TRANSFORMERS, m2m);
				}
				
				if (m2t != null && !m2t.isEmpty()) {
					properties.put(TransformationConstants.M2T_TRANSFORMERS, m2t);
				}

				if (!additional.isEmpty()) {
					properties.putAll(additional);
				}
				
				this.transformationProcessor.startTransformation(properties);
				
			} else {
				System.err.println("Model transformation can not be performed as neither Properties file nor input path is supplied");
				System.exit(0);
			}
		}
	}

	private String getAvailableM2MKeys(List<TransformationDefinition> definitions) {
		List<String> keys = definitions.stream()
			.filter(def -> def.getM2MKey() != null)
			.map(TransformationDefinition::getM2MKey)
			.collect(Collectors.toList());
		return String.join(",", keys);
	}
	
	private String getAvailableM2TKeys(List<TransformationDefinition> definitions) {
		List<String> keys = definitions.stream()
				.filter(def -> def.getM2TKey() != null)
				.map(TransformationDefinition::getM2TKey)
				.collect(Collectors.toList());
		return String.join(",", keys);
	}
	
	@Override
	public void handleEvent(Event event) {
		if (this.started.get()
				&& !this.isInteractive
				&& event.getProperty("cm.factoryPid").equals(ServiceConstants.SESSION_CONFIGURATION_PID)) {
			// shut down the application if this immediate component started the
			// transformation and the session configuration was deleted
			// and if no console was opened and this component triggered the transformation
			System.exit(0);
		}
	}

}
