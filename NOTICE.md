# Notices for Eclipse APP4MC

This content is produced and maintained by the Eclipse APP4MC project.

* Project home: https://projects.eclipse.org/projects/automotive.app4mc

## Trademarks

Eclipse APP4MC, and APP4MC are trademarks of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.addon.migration
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.addon.transformation
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.cloud
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.tools
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.examples

## Third-party Content

This project leverages the following third party content.

Apache Commons Lang (2.6)

* License: Apache License, 2.0

Apache Commons Lang (3.1.0)

* License: Apache License, 2.0

Apache Commons Math (2.1.0)

* License: Apache License, 2.0, New BSD license 

Apache Commons Math (3.5.0)

* License: Apache License, 2.0, New BSD License

Apache Jena-ARQ (2.10.0)

* License: Apache-2.0 

Apache Jena-Core (2.7.1)

* License: Apache License 2.0, New BSD license

Apache jena-iri (0.9.2)

* License: Apache License, 2.0

Apache Jena-TDB (0.10.0)

* License: Apache-2.0 

ArduiPi Adafruit SSD1306 OLED Driver SHA Commit
37956b16aa8514dde5b6ecba04a20cc0babe39f1 (n/a)

* License: BSD-2-Clause

BCEL.Jar (5.2)

* License: Apache License, 2.0

choco (4.0.6)

* License: BSD-4-Clause
* Project: http://www.choco-solver.org
* Source: https://github.com/chocoteam/choco-solver

choco-cutoffseq (1.0.4)

* License: BSD-4-Clause
* Project: https://github.com/chocoteam/cutoffseq
* Source: https://github.com/chocoteam/cutoffseq/releases/tag/pf4cs-1.0.4

choco-graph (4.2.0)

* License: BSD-4-Clause AND BSD-2-Clause AND BSD-3-Clause
* Project: https://github.com/chocoteam/choco-graph
* Source: https://github.com/chocoteam/choco-graph

choco-sat (1.0.2)

* License: BSD-3-Clause
* Project: https://github.com/chocoteam/choco-sat
* Source: https://github.com/chocoteam/choco-sat

choco-solver (4.0.9)

* License: BSD-4-Clause
* Project: http://www.choco-solver.org
* Source: https://github.com/chocoteam/choco-solver/releases/tag/4.0.9

cpprof-java (1.3.0)

* License: MIT
* Project: https://github.com/cp-profiler/java-integration
* Source: https://github.com/cp-profiler/java-integration

dk.brics.automaton (18.01.2018)

* License: BSD 3-Clause
* Project: http://www.brics.dk/automaton/
* Source: https://github.com/cs-au-dk/dk.brics.automaton

easymock (3.3.1)

* License: Apache License, 2.0
* Project: http://easymock.org/
* Source: https://github.com/easymock/easymock/archive/easymock-3.3.1.zip

Google Guava (15.0.0)

* License: Apache License, 2.0

Google Guava (21.0)

* License: Apache License, 2.0

INCHRON Realtime System Model (2.98.2)

* License: EPL-2.0
* Project: http://www.inchron.com
* Source: http://eclipse.inchron.com/realtime/updatesites/release/2.98.2/

Inchron Realtime System model (2.98.5)

* License: EPL-2.0
* Project: http://www.inchron.com
* Source: http://eclipse.inchron.com/realtime/updatesites/release/2.98.5/

java_cup runtime Version: 10k (n/a)

* License: Java Cup License (MIT Style)

javassist (3.19.0)

* License: Mozilla Public License 1.1 (MPL), Apache 2.0 
* Project: http://jboss-javassist.github.io/javassist/
* Source:
   https://github.com/jboss-javassist/javassist/archive/rel_3_19_0_ga.zip

jaxen (1.1.6)

* License: BSD License, W3C (One File)
* Project: https://github.com/codehaus/jaxen
* Source: https://github.com/codehaus/jaxen/tree/V_1_1_6_Final/jaxen

jcl-over-slf4j-1.7.10.jar (1.7.10)

* License: Apache-2.0

jdom (2.0.6)

* License: JDom License (based on Apache 1.1 Style License)
* Project: http://www.jdom.org
* Source: http://www.jdom.org/dist/binary/jdom-2.0.6.zip

jenetics (3.0.1)

* License: Apache License, 2.0
* Project: http://jenetics.io/
* Source: https://github.com/jenetics/jenetics/releases/tag/v3.0.1

Jenetics (3.8.0)

* License: Apache-2.0
* Project: http://jenetics.io/
* Source: https://github.com/jenetics/jenetics/tree/v3.8.0

jgrapht (0.9.0)

* License: Eclipse Public License 1.0
* Project: http://jgrapht.org/

jgrapht-core (1.1.0)

* License: EPL-1.0 OR LGPL-2.1-or-later 	

JHHC-SR04 (n/a)

* License: Pending

JHPWMDriver (n/a)

* License: MIT

JUnit (4.12)

* License: Eclipse Public License

log4j (1.2.15)

* License: Apache License, 2.0

log4j over slf4j 1.7.10.jar (1.7.10)

* License: Apache-2.0

mxGraph (3.7.0.0)

* License: Apache-2.0 AND BSD-3-Clause
* Project: https://jgraph.github.io/mxgraph/
* Source: https://github.com/jgraph/mxgraph

net.sf.trove4j:trove4j (3.0.3)

* License: GNU Library or 'Lesser' General Public License (LGPL) 2.1, MIT Style
   (2 Files)

ojalgo (39.0)

* License: MIT License, BSD, Public Domain
* Project: http://ojalgo.org/
* Source: https://github.com/optimatika/ojAlgo/tree/v39

openjfx (n/a)

* License: GPL-2.0 WITH Classpath-exception-2.0

org.chocosolver.cutoffseq:cutoffseq:1.0.5 (1.0.5)

* License: BSD-3-Clause

org.jgrapht:jgrapht-core:1.4.0 (1.4.0)

* License: EPL-2.0 OR LGPL-2.1-or-later

org.jheaps:jheaps:0.14 (0.14)

* License: Pending

org.xerial.sqlite (3.7.2)

* License: Apache License, 2.0, ISC License

pf4cs (1.0.5)

* License: BSD-4-Clause
* Project: https://github.com/chocoteam/pf4cs
* Source: https://github.com/chocoteam/pf4cs

plantUML (2018.8)

* License: (EPL-1.0 OR BSD-3-CLAUSE OR MIT OR Apache-2.0 OR GPL-2.0-or-later OR
   LGPL-3.0-or-later) AND (EPL-1.0 OR BSD-3-CLAUSE OR MIT OR Apache-2.0 OR
   GPL-2.0-or-later OR LGPL-3.0-or-later OR AGPL-3.0-or-later) AND BSD-3-Clause
   AND MIT AND Apache-2.0  

PlantUML Eclipse Plugin (1.1.23)

* License: EPL-1.0 AND Apache-2.0

PlantUML Eclipse Plugin (1.1.24)

* License: EPL-1.0

PlantUML EPL Version: 8000 (n/a)

* License: Eclipse Public License + Public Domain (One File)

protobuf-java (3.2.0)

* License: New BSD license

resolver.jar (1.2)

* License: Apache License, 2.0

RTC Toolbox (1.2)

* License: BSD-3-Clause

serializer.jar (2.7.1)

* License: Apache License, 2.0

SLF4J API Module (slf4j-api-1.7.10.jar) (1.7.10)

* License: MIT

xalan 2.7.1 top level jar (2.7.1)

* License: Apache License, 2.0

Xerces (2.9.0)

* License: Apache License, 2.0

xml-apis.jar (1.3.04)

* License: Apache License, 2.0, Public Domain, W3C

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.